#!/bin/bash
# This script generates the file "get_ordered_results.commands"
# This file generates a file called ordered_results.names which in turn it is used by make_vina_rec_rank_3.py on the analysis folder.
# The input of the present script is a list the names of the receptor to treat as first input and the list of ligand names as second. 
recep=$1
liga=$2
#where=`uname`
#if [ "$where"=="Linux" ]
#then
#	number_cores=`awk '/^processor/ { N++} END { print N }' /proc/cpuinfo`
#else
#	number_cores=`/usr/sbin/system_profiler -detailLevel full SPHardwareDataType | awk '/Total Number of Cores/ {print $5}'`
#fi

for j in `cat "$recep"`
  do
    if [ -d "$j" ] 
      then
        for i in `cat ../../../common_values/$liga`
	do
        	echo "cd $j ; single_pdbqt.sh $i ; cd ../ " >> get_converted_results.commands
	done
      else
         echo "There is no data for the receptor $j"
    fi
  done
# keep ordered_results.name file without repeticions
awk '!($0 in a){a[$0]; print}' get_converted_results.commands > temp
mv temp get_converted_results.commands
