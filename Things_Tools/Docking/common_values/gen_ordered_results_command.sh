#!/bin/bash
recep=$1
liga=$2
if [ "$recep" == "-h" ] || [ "$recep" == "h" ] || [ "$recep" == "help" ] || [ "$recep" == "-help" ] ; then
   echo " This script generates the file \"get_ordered_results.commands\""
   echo " Before run it, the script single_pdbqt.sh must be run it first. This script is within each receptor folder (where the pdbqt are)."
   echo " This file generates a file called ordered_results.names which in turn it is used by make_vina_rec_rank_6.py on the analysis folder"
   echo " Its input is a list the names of the receptor to treat as first input and the list of ligand names as second." 
   echo " Example: "
   echo " ./gen_ordered_results_command.sh list_of_receptors list_1"
   exit
fi

for j in `cat "$recep"`
  do
    if [ -d "$j" ] 
      then
        echo "cd $j ; for i in \`cat ../../../../common_values/$liga\`; do awk -v name=\"\$i\"_$j 'BEGIN { n=0 }(\$3==\"RESULT:\"){n++;print name\" \"n\" \"\$4\" \"\$5\" \"\$6}' \"\$i\"_$j.pdbqt ; done > ordered_results.names ; cd ../ " >> get_ordered_results.commands
      else
         echo "There is no data for the receptor $j"
    fi
  done
# keep ordered_results.name file without repeticions
awk '!($0 in a){a[$0]; print}' get_ordered_results.commands > temp
mv temp get_ordered_results.commands
