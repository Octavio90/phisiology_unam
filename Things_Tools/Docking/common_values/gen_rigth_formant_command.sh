#!/bin/bash
recep=$1
liga=$2

if [ "$recep" == "-h" ] || [ "$recep" == "h" ] || [ "$recep" == "help" ] || [ "$recep" == "-help" ] ; then
   echo " This script generates the file \"get_right_format.commands\""
   echo " Before run it, the script single_pdbqt.sh must be within each receptor folder (where the pdbqt are)."
   echo " This file generates a file called gen_right_format.commands which in turn it is used modify the pdbqt files to use in the analysis."
   echo " Its input is a list the names of the receptor to treat as first input and the list of ligand names as second." 
   echo " Example: "
   echo " ./gen_right_format_command.sh list_of_receptors list_1"
   exit
fi

for j in `cat "$recep"`
  do
    if [ -d "$j" ] 
      then
        for i in `cat ../../../common_values/$liga`
	do
        	echo "cd $j ; single_pdbqt.sh $i ; cd ../ " >> get_rigth_format.commands
	done
      else
         echo "There is no data for the receptor $j"
    fi
  done
# keep ordered_results.name file without repeticions
awk '!($0 in a){a[$0]; print}' get_rigth_format.commands > temp
mv temp get_rigth_format.commands
