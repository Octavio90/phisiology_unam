#!/bin/bash
project=$1
lig_list=$2

echo $project
if [ "$1" == "-h" ] || [ "$project" == "-help" ] || [ "$project" == "h" ] || [ "$project" == "help" ] 
  then
    echo "This script takes 2 inputs"
    echo "    1) The project name "
    echo "    2) The list of ligands to read. This list should exist in the \"common_values\" folder. "
    echo "This file reads the fingerprint vectors files in the \"vectors\" folder "
    echo "and creates a matrix file with all the information. Then it runs the gen_5nn_tanimoto.py script"
    echo "to genenerate a resume file with the nearest neighborgs of ligand (in the tanimoto metrics ) and its fingerprint "
    exit
 else
   if [ ! -f matrix_"$project".tanimotos ]
      then 
         for i in `cat ../../common_values/$lig_list`; do cat vectors/"$i".vector ;  done >> matrix_"$project".tanimotos 
         awk '!($0 in a){a[$0]; print}' matrix_"$project".tanimotos > temp 
         mv temp matrix_"$project".tanimotos
      else
         echo "There was a matrix_"$project"_tanimotos"
         echo "The matrix_"$project"_tanimotos was not recomputed"
    fi

    gen_5nn_tanimoto.py "$project"
fi


