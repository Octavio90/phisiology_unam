#!/usr/bin/env python2.7
import sys

file2read = sys.argv[1]
try:
   tanimoto_matrix = open('matrix_%s.tanimotos'%file2read,'r').readlines()
except IOError:
   print 'Theres is tanimoto matrix to read ( matrix_%s.tanimotos ) '%file2read
   print 'run the script fptV2Matrix.sh first '
   sys.exit()

list_of_lig = tanimoto_matrix.pop(0) 
list_of_lig = list_of_lig.split('\n')[0]
list_of_lig = list_of_lig.split()
list_of_lig.pop(0)                                        # Removing first and last columns,
list_of_lig.pop()                                         # since there is info in those columms the first line. 
dict = {} 				                  # Dictionary translating columns into lig_names
for i in range(len(list_of_lig)):
    ligname = list_of_lig[i]
    if not ligname in dict.keys():
        dict['%s'%i] = ligname
    else:
        print "Somethin is wrong"
        print "This ligand %s appeared for a second time in the column list"
        print "That should be possible."
        print "Look errors in: "
        print "   1) The corresponing list of ligands in the \"common_values\" folder"
        print "   2) sim_lig_vector.py"
        print "   3) fptV2Matrix.sh"
        sys.exit()

outfile = open('ligand_%s.resume'%file2read,'w')
line_out = '# lig_name ; five nearest neighborns ; finger print '
outfile.write('%s\n'%line_out)
for line in tanimoto_matrix:                                # Getting the 5 nearest neighbors for each ligand 
    line = line.split('\n')[0]                              # and print them together with the fpt of the ligand
    line = line.split()                                     # This file will be read it for the make_vina_rank script
    lig = line.pop(0)                                       # To make the final conversion of the sdf files
    fpt = line.pop()
    pairs = [ (float(line[col]),int(col) ) for col in range(len(line)) ]
    pairs = sorted(pairs, key=lambda pair: pair[0],reverse=True)
    nearest_neigh = [ '%s'%dict['%s'%pair[1]]+':'+'%s'%str('%.2f'%pair[0]) for pair in pairs[1:6] ]
    line_out = '%s$$   '%lig
    for neigh in nearest_neigh:
        line_out += '%s ;'%neigh
    line_out += '$$ %s'%fpt       
    outfile.write('%s\n'%line_out)
outfile.close()
