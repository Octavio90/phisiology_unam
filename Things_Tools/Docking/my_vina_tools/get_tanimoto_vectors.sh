#!/bin/bash
# This script generates the file "get_ordered_results.commands"
# This file generates a file called ordered_results.names which in turn it is used by make_vina_rec_rank_3.py on the analysis folder.
# The input of the present script is a list the names of the receptor to treat as first input and the list of ligand names as second. 
liga=$1
#where=`uname`
#if [ "$where"=="Linux" ]
#then
#       number_cores=`awk '/^processor/ { N++} END { print N }' /proc/cpuinfo`
#else
#       number_cores=`/usr/sbin/system_profiler -detailLevel full SPHardwareDataType | awk '/Total Number of Cores/ {print $5}'`
#fi
for j in `cat ../../common_values/$liga`
	do
		if [ ! -f tanimoto_matrix/"$j".vector ]
		then
			echo "sim_lig_vector.py $j.pdbqt $liga > vectors/"$j".vector  " >> gen_tanimoto_vectors.commands
		else	
			echo "There is already a tanimoto file for $j "
		fi
	done
# keep ordered_results.name file without repeticions
awk '!($0 in a){a[$0]; print}' gen_tanimoto_vectors.commands > temp
mv temp gen_tanimoto_vectors.commands
