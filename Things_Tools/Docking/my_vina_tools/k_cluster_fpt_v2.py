#!/usr/bin/env python2.6
from openbabel import *
from pybel import *
import numpy as np
import random
import matplotlib.pyplot as plt
import sys
# From an initial sdf file get the fragments and generate the fingerprint
# for each fragment. With an average distance divide in different groups.

#######################
###### Defenitions ####
#######################
def centroid_indentification(storage,k): # class objects, number of groups
    centroid_list = []
    for gr in k.keys():
       max_value = 0                                                          # generating the group list
       group = [ entry for entry in storage if entry.flag == gr ]             # condition for checking the group           
       if len(group) > 1:
           for each in group:                                                 # Loops for getting all the tanimoto
               promedio = []                                                  # values of molecule with all the other
               each_data = each.molecule.data                                 # molecules in the group. The average of
               for other in group:                                            # average of this values is stored for each
                   other_data = other.molecule.data                           # molecule. The molecule with the highest average value
                   if not other_data['lig_name'] == each_data['lig_name']:    # is the choosen as the centroid
                      tanimoto = each.fpt | other.fpt                         # of the group.
                      if not tanimoto == 1:                                       
                         promedio.append(tanimoto)                                 
               if np.average(promedio) > max_value:                               
                  max_value = np.average(promedio)
                  each.average = max_value                                 
                  centroid_candidate = each
       if len(group) == 1:
          print 'here'
          group[0].average = 1.0                                
          centroid_candidate = group[0]
#       print gr, centroid_candidate.average, centroid_candidate.molecule.data['lig_name'], len(group)
       centroid_list.append(centroid_candidate)
    return centroid_list

def update(storage,centroid_list,k):               # Class objects, centroids, number of groups
    group_flag_updated=[]                          # This loops just read a list of molecular names
    vec_cor = range(len(k.keys()))
    for each in storage:                           # and assign each molecule in this list to the group
       for number in vec_cor:                      # in which it has the highest tanimoto coefficient.
           centroid = centroid_list[number]
           tanimoto = each.fpt | centroid.fpt
           if tanimoto > each.cluster:
              each.cluster = tanimoto
              each.tani_gru = tanimoto
              each.flag = k[number]   
       each.cluster = 0
       group_flag_updated.append(each)
    return group_flag_updated

def evaluation(storage,k):
    dist = 0.0
    for number in k.keys():
       value = 0.0
       count = 0.0
       pop_count = 0
       for entry in storage:
           pop_count += 1
           if entry.flag == number:
              value += entry.tani_gru
              count += 1
       print number,count
#       if count == 0:
#          count = -1.0 
       value = value*(1.0/count)
       dist += value
    return dist
 
def evaluation2(storage,k):
    dist = 0.0 
    for entry in storage:
        dist += entry.average   
    return dist

class Get_flags():
      def __init__(self,molecule,flag,cluster,fpt,average='',tani_gru=''):
          self.molecule = molecule         # FLAGS VECTOR GENERATION.
          self.flag = flag                 # POS 0 = FRAGMENT'S NAME,                                                
          self.cluster = cluster           # POS 1 = GROUP AT WHICH THE FRAGMENT IS ASSIGNED.                       
          self.fpt = fpt                   # fingerprint  
          self.average = average           # POS 2 = FLAG THAT INDICATES THAT THE FRAGMENT WAS ALREADY COMPARED.    
          self.tani_gru = tani_gru
                                          
###################################################
############### Main programm section #############
###################################################
input_file = sys.argv[1]                                      # Path to sdf file
thres = sys.argv[2]                                           # For an easy output naming.
sim_thres = float('0.'+sys.argv[2])                           # The similarity threshold. THIS SHOULD BE IN PARSE
storage_mol = []                  
mol_name = {}                                                 # For storing the molecule name
           
for molecule in readfile("sdf", input_file):
     data = molecule.data
     if not data['lig_name'] in mol_name.keys():
        fpt = molecule.calcfp()
        entry = Get_flags(molecule,-1,-1,fpt,0)
        storage_mol.append(entry)                          # Storing the fragment's name
        mol_name[data['lig_name']] = -1                    # Storing name in dic
gru = 1                                                    # For indicate the group
print len(mol_name.keys()), len(storage_mol)
seq = range(len(mol_name.keys()))
random.shuffle(seq)

dos = 0
for position in seq:
    lig = mol_name.keys()[position]
    for i in storage_mol:
       if i.molecule.data['lig_name'] == lig:
          guy = i
    ref_fpt= guy.fpt                                                               # reference 
    counter_count = len(mol_name.keys())
    count = 0
    for molecule in storage_mol:
        runing_name = molecule.molecule.data['lig_name']
        if molecule.flag == -1 and not lig == runing_name: # If molecule is unassigned 
           tanimoto = ref_fpt | molecule.fpt 
           if tanimoto >= sim_thres and not tanimoto == 1:                        # similarity threshold
              guy.flag = gru
              molecule.flag = gru
              if count == 0:
                 dos += 1 
                 mol_name[ lig ] = gru
                 mol_name[ runing_name ] = gru
                 count += 1   
              else:
                 mol_name[ runing_name ] = gru
    if count > 0:
       gru += 1                                               # increase the group assignment.
k = {}
cccc = 0

for molecule in storage_mol:
    runing_name = molecule.molecule.data['lig_name']
    if molecule.flag == -1 and mol_name[runing_name]== -1 :
       cccc += 1
k[0] = cccc
sumados = 0
for m in range(dos):
  m = m+1
  huevos = 0
  for molecule in storage_mol:
       runing_name = molecule.molecule.data['lig_name']
       if molecule.flag == m and mol_name[runing_name]== m :
           huevos += 1
  k[m] = huevos
  sumados += huevos
sum = 0
for cur in k.keys():
    sum += k[cur]
    print cur ,k[cur]
print len(k.keys()) , 'pos 1', sum , sumados+cccc
#sys.exit()
##### Renaming the "unknowns" group and getting the total number of groups
k = {}
for molecule in storage_mol:
    if molecule.flag == -1:
       molecule.flag = 0
    if not molecule.flag in k.keys():
       k[molecule.flag] = 0

for cur in k.keys():
    for molecule in storage_mol:
        if molecule.flag == cur:
            k[molecule.flag] += 1
sum = 0
for cur in k.keys():
    sum += k[cur]
    print cur , k[cur]
print len(k.keys()) , 'pos 1', sum
#sys.exit()
#### Identification of centroid
centroid_list = centroid_indentification(storage_mol,k)
#for i in centroid_list:
#   print i.molecule.data['lig_name'] , i.molecule.title
### Using the singletones from the last step, reassign the original list
new_distribution = update(storage_mol,centroid_list,k)
#kaka = {}
#for i in new_distribution:
#   if not i.flag in kaka.keys():
#      print i.flag
#      kaka[i.flag] = i.flag
#print len(kaka.keys()),'pos  2'
#sys.exit()
# Loop to search for a high-distance clustering
# the "cluster distance" is intented to measure the similarity within a given cluster
# by adding the averaged tanimoto values of all the elements in the cluster. Each molecule has
# has an averaged tanimoto value, this value is obtained by averaging all the tanimoto coefficients
# of a given molecule with all the molecules in the cluster. So the higher the better similarity.
cluster_distance = 0.0
times = 0
new_centro = centroid_list
for times in range(10):
  new_cluster_distance = evaluation(new_distribution,k) 
  print new_cluster_distance
  if  new_cluster_distance > cluster_distance:
      cluster_distance = new_cluster_distance
      new_centro = centroid_indentification(new_distribution,k)
      new_distribution = update(new_distribution,new_centro,k)
  else:
	  break
####################################################
############## Final output file and plots #########
####################################################
# Generating the histogram
his_data = []
his_data2 = []
for number in k:
    count = 0
    count2 = 0
#    print number
    for entry in new_distribution:
        if entry.flag == number:
           count += 1
           count2 += entry.tani_gru 
#           print entry.molecule.data['lig_name'] 
    his_data.append(count)
    his_data2.append(count2)
fd=open("singletones_%s.txt"%thres,'w')
fd.write("##### This singletones comes from file in : %s\n"%input_file)
fd.write("Group \t Name \t tanimoto group  #elements\n")
c = 0
for x in new_centro:
      data = x.molecule.data
      fd.write('%s  :   %s\t %.3f\t %s\n'%(c,data['lig_name'],x.average,his_data[c]))
      c = c+1
fd.close()
