#!/usr/bin/env python2.6
from openbabel import *
from pybel import *
import numpy as np
import random
import matplotlib.pyplot as plt
import sys
# From an initial sdf file get the fragments and generate the fingerprint
# for each fragment. With an average distance divide in different groups.

#######################
###### Defenitions ####
#######################
def centroid_indentification(storage,k): # class objects, number of groups
    centroid_list = []
    index_centroid = 0
    for gr in k.keys():
       max_value = 0                                                          # generating the group list
       group = [ entry for entry in storage if entry.flag == gr ]             # condition for checking the group           
       if len(group) > 1:
           for each in group:                                                 # Loops for getting all the tanimoto
               promedio = []                                                  # values of molecule with all the other
               each_data = each.molecule.data                                 # molecules in the group. The average of
               for other in group:                                            # average of this values is stored for each
                   other_data = other.molecule.data                           # molecule. The molecule with the highest average value
                   if not other_data['lig_name'] == each_data['lig_name']:    # is the choosen as the centroid
                      tanimoto = each.fpt | other.fpt                         # of the group.
                      if not tanimoto == 1:                                       
                         promedio.append(tanimoto)                                 
               if np.average(promedio) > max_value:                               
                  max_value = np.average(promedio)
                  each.average = max_value                                 
                  centroid_candidate = each
       if len(group) == 1:
          group[0].average = 1.0                                
          centroid_candidate = group[0]
#       print gr, centroid_candidate.average, centroid_candidate.molecule.data['lig_name'], len(group)
       centroid_list.append(centroid_candidate)
       k[gr] = index_centroid
       index_centroid += 1
    return centroid_list

def update(storage,centroid_list,k):               # Class objects, centroids, number of groups
    group_flag_updated=[]                          # This loops just read a list of molecular names
    for each in storage:                           # and assign each molecule in this list to the group
       for number in k.keys():                     # in which it has the highest tanimoto coefficient.
           centroid = centroid_list[k[number]]
           tanimoto = each.fpt | centroid.fpt
           if tanimoto > each.cluster:
              each.cluster = tanimoto
              each.tani_gru = tanimoto
              each.flag = number    
       each.cluster = 0
       group_flag_updated.append(each)
    return group_flag_updated

def evaluation(storage,k):
    dist = 0.0
    for number in k.keys():
       value = 0.0
       count = 0.0
       for entry in storage:
           if entry.flag == number:
              value += entry.tani_gru
              count += 1
       if count == 0:
          value = 0
       else:
         value = value*(1.0/count)
       dist += value
    return dist
 
def evaluation2(storage,k):
    dist = 0.0 
    for entry in storage:
        dist += entry.average   
    return dist

def clean_dic(storage,k):
    redo = []
    for j in k.keys():
       sum = 0  
       for i in storage:
          if i.flag == j:
             sum += 1
       k[j] = sum
       if sum <= 1:
          redo.append(j) 
    total = 0
    for j in k.keys():
        total += k[j]
    
    if len(redo)>0:
       cuenta = 0
       for i in redo:
          pop_it = 0
          for j in k.keys():
              if j == i:
                 keep_to_pop = pop_it  
                 for m in storage:
                     if m.flag == j:
                        cuenta += 1
                        m.flag = 0
              pop_it += 1
          del(k[k.keys()[keep_to_pop]])
          k[0] = cuenta
    total = 0
    print 'group element'    
    for j in k.keys():
        print j, k[j]      
        total += k[j]
    return storage, k



class Get_flags():
      def __init__(self,molecule,flag,cluster,fpt,average='',tani_gru=''):
          self.molecule = molecule         # FLAGS VECTOR GENERATION.
          self.flag = flag                 # POS 0 = FRAGMENT'S NAME,                                                
          self.cluster = cluster           # POS 1 = GROUP AT WHICH THE FRAGMENT IS ASSIGNED.                       
          self.fpt = fpt                   # fingerprint  
          self.average = average           # POS 2 = FLAG THAT INDICATES THAT THE FRAGMENT WAS ALREADY COMPARED.    
          self.tani_gru = tani_gru
 

                                         
###################################################
############### Main programm section #############
###################################################
input_file = sys.argv[1]                                      # Path to sdf file
thres = sys.argv[2]                                           # For an easy output naming.
sim_thres = float('0.'+sys.argv[2])                           # The similarity threshold. THIS SHOULD BE IN PARSE
storage_mol = []                  
mol_name = {}                                                 # For storing the molecule name
           
for molecule in readfile("sdf", input_file):
     data = molecule.data
     if not data['lig_name'] in mol_name.keys():
        fpt = molecule.calcfp()
        entry = Get_flags(molecule,-1,-1,fpt,0)
        storage_mol.append(entry)                          # Storing the fragment's name
        mol_name[data['lig_name']] = -1                    # Storing name in dic
gru = 1                                                    # For indicate the group
seq = range(len(mol_name.keys()))
random.shuffle(seq)
print len(mol_name.keys())
for position in seq:
    lig = mol_name.keys()[position]
    count = 0
    ref_fpt= [ i.fpt for i in storage_mol if i.molecule.data['lig_name'] == lig ]   # Reference fpt
    for i in storage_mol:
        check_name = i.molecule.data['lig_name']
        if check_name == lig:
           ref_fpt=  i.fpt                          # Reference fpt
           mol_name[lig]= gru                       # Reference fpt
    for molecule in storage_mol:
        name_running = molecule.molecule.data['lig_name']
        if molecule.flag == -1 and mol_name[name_running] == -1 and not lig == name_running: # If molecule is unassigned 
           tanimoto = ref_fpt | molecule.fpt 
           if tanimoto >= sim_thres and not tanimoto == 1:    # similarity threshold
              molecule.flag = gru
              mol_name[name_running] = gru
              count += 1                              
    if count > 0:
       gru += 1                                               # increase the group assignment.

k = {}
for i in storage_mol:
    name_running = i.molecule.data['lig_name']
    for j in mol_name.keys():
        if j == name_running:
           i.flag = mol_name[j]
        if not mol_name[j] in k.keys():
            k[mol_name[j]] = 0
##### Renaming the "unknowns" group and getting the total number of groups
storage_mol,k = clean_dic(storage_mol,k)
#### Identification of centroid
centroid_list = centroid_indentification(storage_mol,k)
### Using the singletones from the last step, reassign the original list
new_distribution = update(storage_mol,centroid_list,k)
new_distribution, k = clean_dic(new_distribution,k)
# Loop to search for a high-distance clustering
# the "cluster distance" is intented to measure the similarity within a given cluster
# by adding the averaged tanimoto values of all the elements in the cluster. Each molecule has
# has an averaged tanimoto value, this value is obtained by averaging all the tanimoto coefficients
# of a given molecule with all the molecules in the cluster. So the higher the better similarity.
cluster_distance = 0.0
times = 0
new_centro = centroid_list
for times in range(10):
  new_cluster_distance = evaluation(new_distribution,k) 
  print new_cluster_distance
  if  new_cluster_distance > cluster_distance:
      cluster_distance = new_cluster_distance
      new_centro = centroid_indentification(new_distribution,k)
      new_distribution = update(new_distribution,new_centro,k)
      new_distribution, k = clean_dic(new_distribution,k)
  else:
	  break
####################################################
############## Final output file and plots #########
####################################################
# Generating the histogram
his_data = []
his_data2 = []
for number in k.keys():
    count = 0
    count2 = 0
#    print number
    for entry in new_distribution:
        if entry.flag == number:
           count += 1
           count2 += entry.tani_gru 
#           print entry.molecule.data['lig_name'] 
    his_data.append(count)
    his_data2.append(count2)
fd=open("singletones_%s.txt"%thres,'w')
fd.write("##### This singletones comes from file in : %s\n"%input_file)
fd.write("Group \t Name \t tanimoto group  #elements\n")
c = 0
for x in new_centro:
      data = x.molecule.data
      fd.write('%s  :   %s\t %.3f\t %s\n'%(c,data['lig_name'],x.average,his_data[c]))
      c = c+1
fd.close()
