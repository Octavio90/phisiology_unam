#!/usr/bin/env python2.7
import sys
#for i in `cat ../results/rec_dones | awk '{print $1}'`; do cd receptor_"$i" ; echo "[('receptor_"$i"', 0)]" > MODELS/results_10_00_11111.txt ; awk -F'$' '{print $1","$5}' CANDIDATES/candidates_receptor_"$i".resultsv3 | awk -F',' '{print $1,$2}' >> MODELS/results_10_00_10000.txt ; cd .. ;done

def read_file(name):
    infile = open('%s'%name,'r').readlines()
    head = (infile.pop()).split('\n')[0]
    len_data = len(infile) 
    array = range(len_data)
    count = 0
    for line in infile:
        line = line.split('\n')[0]
        lig_name = line.split()[0]
        score = (line.split('$$')[2]).split(',')[0]
        array[count] = (lig_name,score)
        count += 1
    return array,len_data

rec_number = sys.argv[1]
cutoff = [ '1.0', '0.2', '0.1', '0.05' ]

data,num_data = read_file('CANDIDATES/candidates_receptor_%s.resultsv3'%rec_number)

for frac in cutoff:
    frac_out = ''.join(frac.split('.'))
    lines_in = int(float(frac)*num_data)
    outfile = open('MODELS/results_%s_00_10000.txt'%frac_out,'w')
    line = "[('receptor_%s', 0)]"%rec_number
    outfile.write('%s\n'%line)
    for entry in range(lines_in):
        values = data[entry]
        line = '%s %s'%(values[0],values[1])
        outfile.write('%s\n'%line)
    outfile.close() 
