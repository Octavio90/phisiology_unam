#!/usr/bin/env python2.6
import sys
import os
import subprocess
import numpy as np
import openbabel as opb
import pybel as pyb
from module_fix_pdb_from_pdbqt import pdbqt2pdb
from read_moles_files import cal_rmsd
import rescoring as resco

class Mol_Info():
   def __init__(self,name,intrank,genrank,dock,rmsd='',freq='',simil_dock='',molecule='',ligefi=''):
       self.molecule = molecule
       self.name = name 
       self.intrank = int(intrank) 
       self.genrank = genrank 
       self.dock = float(dock) 
       self.rmsd = int(rmsd) 
       self.freq = freq
       self.ligefi= ligefi
       self.simil_dock= simil_dock

def print_mol_info(mol):
    attrs = vars(mol)
    print ', '.join("%s: %s" % item for item in attrs.items())

def look_mol_in_database(molname,data):
    for lig in data:
        if molname == lig.name: 
           print_mol_info(lig)

#####################################
### Main body of the script  #########
def main(args):
    try:
        assert(len(args)>0 and len(args)<5)
        listofrec = args[0]
        if listofrec == '-h' or listofrec == '-help':
           print " This file creates a rank list of the ligands stored in the CANDIDADES folder as sdfs"
           print " The list in the CANDIDADES folder and the poses in CANDIDADES_sdfs come from "
           print " make_vina_rec_rank_3.py "
           print " This file takes as input the list of receptor candidates to read (the sdf files). "
           print " These lists are then sorted by score and filtered "
           print " It output a file containing a list of ligand information "
           print " Example: ./make_unique_list.py listof.receptors"
           sys.exit() 
        else:
           # Reading the input 
           list = open('%s'%listofrec,'r').readlines()
           number_of_receptors = len(list)                                     # To generate the columns of matrix
           known_receptors = {}                                                # To keep track of the receptors
           total_store = []                                                    # Storing all the ligands from all the sdf files
#           total_store_max_values = []                                        # Storing max values for later normalization
           treshold = float(args[1])
           constants = [ float(i) for i in args[2].split(',') ]              
           project = args[3]
           if not len(constants) == 10:
              print "The third argument should should be have 5 element separted by commas"
              print "example: 1,0,1,0.5,0,1,0,1,0.5,0"
              print "The first 5 to compute autodockvina, the second 5 to compute autodock4 "
              sys.exit()
           method_constat = {}
           method_constat['autodockvina'] = constants[:5]
           method_constat['autodock4'] = constants[5:]
           if not len(method_constat['autodock4']) == len(method_constat['autodockvina']):
              print "The constants for the methods are not the same length "
              print "autodockvina contants: %s , %s "% (len(method_constat['autodockvina']),method_constat['autodockvina'])
              print "autodock4    contants: %s , %s "% (len(method_constat['autodock4']), method_constat['autodock4'])
              print "correction on the third argument is needed."
              print "The first 5 constants  compute autodockvina, the second 5 to compute autodock4 "
              sys.exit()
           pos = 0
           for rec in list:
               rec = rec.split('\n')[0]                                        
               name_rec = rec.split('.')[0].split('_')                         # Getting rid of extra information
               name_rec = name_rec[2]+'_'+name_rec[3]                          # and keeping the recpector name
               known_receptors[name_rec] = pos                                 # Creating the entry in the dictionary
               pos += 1
               for i in method_constat.keys():
                  store = []                                                          # Storing all the ligands from all the sdf files
                  store_max_values = []                                               # Storing max values for later normalization
                  path_to_data = '%s/analysis/%s'%(i,project)
#                  number_of_ligands = subprocess.check_output(["grep", "-c", "$$$$", "%s/CANDIDATES/%s"%(path_to_data,rec)])
#                  number_of_ligands = int(number_of_ligands.split('\n')[0])
                  number_of_ligands = subprocess.Popen(["grep", "-c", "$$$$", "%s/CANDIDATES/%s"%(path_to_data,rec)],stdout=subprocess.PIPE)
                  numberofligands = number_of_ligands.communicate()[0]
                  numberofligands = numberofligands.split('\n')[0]
                  number_of_ligands = int(numberofligands.split('\n')[0])
                  print number_of_ligands, path_to_data
                  local_store = range(number_of_ligands)                          # Storing all ligands from a single sdf file
                  local_store_info = range(number_of_ligands)                     # Storing all ligands info from a single sdf file
                  count = 0
                  for candidate in pyb.readfile('sdf','%s/CANDIDATES/%s'%(path_to_data,rec)):
                      candidate_info = resco.info_to_vec(candidate.data)
                      local_store_info[count] = candidate_info
                      local_store[count] = candidate
                      count += 1
                  store_max_values = store_max_values + local_store_info
                  store = store + local_store                                      # Merging all the sdf files
          ###########################
          # Place to set the filter # 
          ########################### 
                  max_values = resco.get_max_values(store_max_values)
                  store = resco.filter(store,max_values,treshold,method_constat[i]) 
#           transformed = resco.do_pca(store,max_values)
#           for i in transformed:
#               print i 
           # Identifying uniques ligands  
                  total_store = total_store + store                             # Merging all the sdf files
           uniques = {}                                                         # To store the last list 
           for lig in total_store:  
               data = lig.data
               if data['lig_name'] in uniques.keys():                           
                  if data['rec_name'] in known_receptors.keys(): 
                     receptor = data['rec_name']
                     position = known_receptors[receptor]
                     uniques[data['lig_name']][position] += 1  
                  else:
                     print "The rec_name in the sdf file is not within the list of known_receptors"
                     print "This means that the in the sdf file one of the ligands have a wrong name "
                     print "or files names are mixed. The known_receptors are taken from the input list. "
                     print "The molecule.title and rec_name are writen from make_vina_rank_3.py"
                     print "In principle should be easy to track back ligand name and receptor"
                     sys.exit()
               else:
                  uniques[data['lig_name']] = np.array(range(number_of_receptors))*0.0
                  if data['rec_name'] in known_receptors.keys():
                     receptor = data['rec_name']
                     position = known_receptors[receptor]
                     uniques[data['lig_name']][position] += 1
#           print len(uniques.keys())

           # Writing output file of information
           outfile = open('results_%s.txt'%treshold,'w')
           line = [ (i,known_receptors[i]) for i in known_receptors.keys() ]
           line = '%s'%line
           outfile.write('%s\n'%line)              
           for lig in uniques.keys():
               line = '%s %s'%(lig, uniques[lig])
               outfile.write('%s\n'%line)              

    except AssertionError:
           print " This file creates a rank list of the ligands in the input file ordered_results.names"
           print " This comes from the output generated by extract_ranks.commands "
           print " It output two files: \".unique\" and \".bypose\"  "
           print " The file \"bypose\" contains the top 5\% all the poses of the ligands in LIGANDS_LIST"
           print " The file \"unique\" contains the top 5\% all ligands in LIGANDS_LIST (ligand,average score among poses, rmds among poses)"
           print "   The properties assigned to each ligand in this list is computed using all its poses, althougt to get in just the top "
           print "   score is considering."
           print " Example: ./make_vina_rec_rank.py RECEPTOR "
           return 1  # exit on error
    else:
           return 0  # exit errorlessly

if __name__ == '__main__':
    sys.argv.pop(0)
    sys.exit(main(sys.argv))
