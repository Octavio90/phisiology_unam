#!/usr/bin/env python
import sys

def pdbqt2pdb(args):  
    infile = open('%s'%args[0],'r').readlines()
    outfile = open('%s'%args[1],'w')
    especials = {'l':'Cl','r':'Br'} # Special cases of two letter elements
    for line in infile:
       if 'HETATM' in line or 'ATOM' in line:
          element = line[13]
          atomtype= line[77]
          if not element == atomtype:
             if not element in especials.keys(): 
#                print line
                line = line[:77]+element+line[78:]
#                print line
             else: 
#                print line
                line = line[:76]+especials[element]+"\n"
#                print line
       outfile.write('%s'%line)     
    outfile.close()

if __name__ == '__main__':
    sys.argv.pop(0)  
    sys.exit(main(sys.argv))
