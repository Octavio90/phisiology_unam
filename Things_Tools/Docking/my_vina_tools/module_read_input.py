#!/usr/bin/env python2.7
import sys
import os
import subprocess
import numpy as np
import openbabel as opb
import pybel as pyb
from read_moles_files import Mol_Info_General
import module_treat_docking_files as tdf
import module_write_output as wo
def read_input(rec,path_to_results,machine):
    try:   
           project = path_to_results.split('/')[-1]
           infile=open('%s/%s/ordered_results.names'%(path_to_results,rec),'r').readlines() # read input file 
           number_of_poses = len(infile)                                                    # Count the number of poses
           out_positions_poses = int(number_of_poses)                                       # Final number list of poses
           bypos = range(out_positions_poses)                                               # Final list of poses 
           # Store the ligands information and ordered by docking value
	   # Mol_Info( name_of_the_file , interal_rank , dock_score)
#           general_rank = [ Mol_Info_pose(line.split()[0], line.split()[1], line.split()[2]) for line in infile ] 
           general_rank = []
           for line in infile:
                dock_file_name = line.split()[0]
                lig_name = 'lig_%s'%dock_file_name.split('_')[1]
                rec_name = 'receptor_%s'%dock_file_name.split('_')[3]
                dock_score_vec = line.split()[2]
                rmsd_pose_vec = line.split()[3]
                lig_efi = line.split()[4]
                entry = Mol_Info_General(dock_file_name,machine,rec_name,lig_name,project)
                (entry.dock_vec).append(dock_score_vec)
                (entry.rmsd_vec).append(rmsd_pose_vec)
                entry.ligefi = float(entry.dock_vec[0])/float(lig_efi)
                entry.status = 'dismissed'
                if int(lig_name.split('_')[1]) < 3962:
                   entry.nature = 'active'
                else:
                   entry.nature = 'decoy'
                general_rank.append(entry)
#           print ((general_rank[0]).dock_vec)[0]
           general_rank = sorted(general_rank, key=lambda ligand: float((ligand.dock_vec)[0]))
           return general_rank

    except AssertionError:
           print " This function exists just within \"make_vina_rank_v3.py\""
           print " This function takes two arguments"
           print "     1) rec = This is the main input of make_vina_rank_v3.py. An represents the receptor's results are being processed."
           print "     2) cutoff = This value defines how many poses and ligands are going to be use for the analysis."
           print " This function return three objects: "
           print "     1) An array for storing the best top poses (controled by the cutoff). The objects contained will be from the Class Mol_info."
           print "     1) An array for storing the best top ligand (controled by the cutoff). The objects contained will be from the Class Mol_info."
           print "     3) general_rank = This is a python array. All the poses that are associaded with the given receptor are considered in this list."
           print "                       They are ordered by docking score. The objects contained are from the Class Mol_info."
           print " The output is referred as bypos,bylig,general_rank array "
           print " THIS FUNCTION MAKES THE ASSUMTION THAN EACH LIGAND CONTRIBUTS WITH 10 POSES "
           return 1  # exit on error
    else:
           return 0  # exit errorlessly

def read_input_unique_list(listofrec,path_to_data,cutoff):
    try:   
           list = open('%s'%listofrec,'r').readlines()
           known_receptors = {}                                                # To keep track of the receptors
           store = []                                                          # Storing all the ligands from all the sdf files
           store_max_values = []                                               # Storing max values for later normalization
           pos = 0
           for rec in list:
               rec = rec.split('\n')[0]                                        
               name_rec = rec.split('.')[0].split('_')                         # Getting rid of extra information
               name_rec = name_rec[1]+'_'+name_rec[2]                          # keeping the receptor name
               known_receptors[name_rec] = pos                                 # Creating the entry in the dictionary
               pos += 1
	       local_store,max_pose_value = tdf.tread_molecules_from_dock_2(rec,path_to_data,cutoff)
               semifinal_list= tdf.trans_to_vector(local_store,max_pose_value)
	       store = store+semifinal_list
           return store, known_receptors 
    except AssertionError:
           print " This function exists just within \"make_unique_list_V2.py\""
           print " This function takes one arguments"
           print "     1) rec = The list of files to read. The files should be in the CANDIDADTES folder."
           print " This function returns 3 objects: "
           print "     1) An array for storing the best top poses (controled by the cutoff). The objects contained will be from the Class Mol_info."
           print "     2) An array for storing the best top ligand (controled by the cutoff). The objects contained will be from the Class Mol_info."
           print "     3) An dictionary making a relation between the receptor's name and its column position in the reading file."
           return 1  # exit on error
    else:
           return 0  # exit errorlessly

def read_constants(constants_arg):
    try:   
           constants = [ float(i) for i in constants_arg.split(',') ]              
           if not len(constants) == 5:
              print "The third argument should should be have 5 element separted by commas"
              print "example: 1,0,1,0.5,0"
              sys.exit()
           return constants
    except AssertionError:
           print " This function exists just within \"make_unique_list_V2.py\""
           print " This function takes one arguments"
           print "     1) constants  = This is an string given in the command line. It should be 5 valules separated by commas"
           print "                     The values should be between 0 and 1."
           print " This function output 1 object.  "
           print "     1) a python list of the given constants "
           print " The output is writen as constants and is use in the filtering part "
           return 1  # exit on error
    else:
           return 0  # exit errorlessly

def read_constants_consensus(constants_arg):
    try:   
           constants = [ float(i) for i in constants_arg.split(',') ]              
           if not len(constants) == 10:
              print "The third argument should should be have 5 element separted by commas"
              print "example: 1,0,1,0.5,0,1,0,1,0.5,0"
              print "The first 5 to compute autodockvina, the second 5 to compute autodock4 "
              sys.exit()
           method_constat = {}
           method_constat['autodockvina'] = constants[:5]
           method_constat['autodock4'] = constants[5:]
           if not len(method_constat['autodock4']) == len(method_constat['autodockvina']):
              print "The constants for the methods are not the same length "
              print "autodockvina contants: %s , %s "% (len(method_constat['autodockvina']),method_constat['autodockvina'])
              print "autodock4    contants: %s , %s "% (len(method_constat['autodock4']), method_constat['autodock4'])
              print "correction on the third argument is needed."
              print "The first 5 constants  compute autodockvina, the second 5 to compute autodock4 "
              sys.exit()
           return method_constat
    except AssertionError:
           print " This function exists just within \"make_unique_consensus_selected.py\""
           print " This function takes one arguments"
           print "     1) constants  = This is an string given in the command line. It should be 10 valules separated by commas"
           print "                     The values should be between 0 and 1. The first 5 are used in autodockvina, the second 5 in autodock4"
           print " This function output 1 object.  "
           print "     1) a python dictionary given constants for each docking machine "
           print " The output is writen as method_constat and is use in the filtering part "
           return 1  # exit on error
    else:
           return 0  # exit errorlessly

def read_input_all(listofrec,cutoff,path_to_write,path_to_results,path_to_fpt,project,machine):
    try:   
           list = open('%s'%listofrec,'r').readlines()
           known_receptors = {}                                                  # To keep track of the receptors
           store = []                                                            # Storing all the ligands from all the sdf files
           store_max_values = []                                                 # Storing max values for later normalization
           pos = 0
           for rec in list:
               rec = rec.split('\n')[0] 
               known_receptors[rec] = pos                                        # Creating the entry in the dictionary
               pos += 1
               # Read the file 'ordered_results.names' from the receptor folder                                    
               general_rank = read_input(rec,path_to_results,machine)            # paths to find the files are defined internaly in "read_input" 
               bylig = tdf.fill_lig_array_2(general_rank)                        # Filling the array of poses to output
               bylig = tdf.manage_sim_lig_2(bylig,path_to_fpt)                   # Looking for similar molecules in the final list
               wo.write_output_bylig_2(rec,bylig,path_to_write)                  # First writing the ligand list
               # Setting the scoring vector
               max_pose_value = int(len(general_rank)*cutoff)
               semifinal_list= tdf.trans_to_vector(bylig,max_pose_value)
	       store = store+semifinal_list
           return store, known_receptors 
    except AssertionError:
           print " This function exists just within \"make_unique_list_V4.py\""
           return 1  # exit on error
    else:
           return 0  # exit errorlessly
