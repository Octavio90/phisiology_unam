#!/usr/bin/env python2.7
import sys
import os
import numpy as np
import openbabel as opb
import pybel as pyb
from module_fix_pdb_from_pdbqt import pdbqt2pdb
from read_moles_files import Mol_Info_General
from read_moles_files import cal_rmsd
import subprocess
import rescoring_2 as resco

def pdb2sdf(toresults,rec,ligname,toanalysis):
                  os.chdir( '%s/%s'% (toresults,rec) )
		  output_lig_sdf = pyb.Outputfile( 'sdf', '%s.sdf'%(ligname), overwrite=True )
                  for candidate in pyb.readfile('pdb','temp%s.pdb'%(ligname)):
			  candidate_fpt = candidate.calcfp().bits
			  stringbit = ','.join([ str(i) for i in candidate_fpt ])
			  data = candidate.data
			  dock_value = data['REMARK']
			  dock_value = dock_value.split('\n')[0]
			  pose_value = data['MODEL']
			  data.clear()
			  data['dock_original'] = dock_value
			  data['fp_index'] = stringbit
			  data['dock'] = dock_value.split()[2]
			  data['rank_int'] = pose_value
	                  data['lig_name'] = 'lig_%s'%ligname.split('_')[1]
	                  data['rec_name'] = 'receptor_%s'%ligname.split('_')[3]
                          output_lig_sdf.write(candidate)
                  os.system('rm %s.pdb temp%s.pdb'%(ligname,ligname))
                  os.chdir( '%s/'%(toanalysis) )

def fill_lig_array_2(general_rank):
    try:   
	   bylig = [] 
	   uniques = {}						             # Storing the uniques files and its frequency
	   count_lig = 0                                                     # Counting unique ligands
           for count_pos in range(len(general_rank)):  
              lig = general_rank[count_pos]                                  # Getting ligand
	      if not lig.lig_name in uniques.keys():                         # Storing the ligand if it appears for first time
		      lig.genrank_lig = count_lig+1
		      (lig.genrank_pos).append(count_pos+1)
		      bylig.append(lig)
		      uniques[lig.lig_name] = count_lig                      # Storing the name and its position in a dictionary
		      count_lig += 1
	      else:
		      postion_in_bylig = uniques['%s'%lig.lig_name]          # Getting the position of lig.name in bylig array
		      update_lig = bylig[postion_in_bylig]                   # Getting lig.name molecule as "update_lig"
                      (update_lig.genrank_pos).append(count_pos+1)           # Updating the vector of general rank
                      (update_lig.dock_vec).append((lig.dock_vec)[0])        # Updating the vector of general rank 
                      (update_lig.rmsd_vec).append((lig.rmsd_vec)[0])        # Updating the vector of general rank 
           return bylig

    except AssertionError:
           print " This function exists just within \"make_vina_rank_v4.py\""
           print " This function takes threes arguments"
           print "     1) rec = This is the main input of make_vina_rank_v4.py. An represents the receptor's results are being processed."
           print "     2) bypos = This is a python array. At this poin the array is empty (i.e is just a place holder). There are as many spaces  "
           print "                as the cutoff permits. Just top scored poses have a place to further analysis."
           print "     3) general_rank = This is a python array. All the poses that are associaded with the given receptor are considered in this list."
           print "                       They are ordered by docking score. The objects contained are from the Class Mol_info."
           print " This function return one object: "
           print "     1) An array of the best top poses (controled by the cutoff). The objects contained are from the Class Mol_info."
           print " The output is referred as bypos array "
           return 1  # exit on error
    else:
           return 0  # exit errorlessly

def manage_sim_lig_2(bylig,path_to_fpt):
    try:   
	fptfile = open('%s'%(path_to_fpt),'r').readlines()
	fptfile.pop(0)                                    # Getting rid of file header
	for lig in bylig:    
	    if len(lig.dock_vec) == len(lig.genrank_pos):
		    for line in fptfile:
			    line = line.split('\n')[0]
			    line = line.split('$$')
			    if line[0] == lig.lig_name:
                                    input = line[1].split(';')
				    lig.neighborns = [ i.replace(" ","") for i in input ] 
				    lig.fpt = line[2]
                                    break
	    else:
		 print "something is terrible wrong"
	         print "The file %s seem to have %s molecules but just %s positions in the general rank were found"%(lig.file_name,len(lig.dock_vec),len(lig.genrank_pos))
	         sys.exit()
	           
        return bylig
    except AssertionError:
           print " This function exists just within \"make_vina_rank_v4.py\""
           print " This function takes 1 arguments"
           print "     1) bylig = This is a python array. The objects contained are from the Class Mol_info. At this poin the array contains just "
           print "                one pose per molecule (the best scored pose)"
           print " This function return 1 object: "
           print "     1) An array of the best top molecules (controled by the cutoff). The objects contained are from the Class Mol_info."
           print " The output is referred as bylig array the molecular finger print has been added"
           return 1  # exit on error
    else:
           return 0  # exit errorlessly

def trans_to_vector(local_store,max_pose_value):
               semifinal_list = []
               for mol_info in local_store:
		        vector_info = resco.info_to_vec(mol_info,local_store,max_pose_value) # Computes a docking rescoring vector
                        if vector_info[1] > 0:                                               # (dock,freq,rmsd,simil_dock,ligand_efi)  
                            mol_info.score_vec = vector_info                                 # If freq > 0. store the info. 
                            semifinal_list.append(mol_info)
               return semifinal_list

def indentify_uniques_3(store,known_receptors):
    try:   
           number_of_receptors = len(known_receptors.keys())                                 # To generate the columns of matrix
           uniques = {}                                                                      # To store the last list 
	   uniques_index = {}
           for pos_in_store in range(len(store)):
	       lig = store[pos_in_store]	   
               if lig.lig_name in uniques.keys():                           
                  if lig.rec_name in known_receptors.keys(): 
                     receptor = lig.rec_name
                     receptor_position = known_receptors[receptor]
                     current = uniques[lig.lig_name][receptor_position]            # New list update
                     if lig.gen_score > current:                                   # It the molecule was already in the list
                        uniques_index[lig.lig_name] = pos_in_store                 # choose the one with higher score.
                        uniques[lig.lig_name][receptor_position] = lig.gen_score   # Another idea may be to choose the higher gen rank within their own original ranking list 
                  else:
                     print "The rec_name in the sdf file is not within the list of known_receptors"
                     print "This means that the in the sdf file one of the ligands have a wrong name "
                     print "or files names are mixed. The known_receptors are taken from the input list. "
                     print "The molecule.title and rec_name are writen from make_vina_rank_3.py"
                     print "In principle should be easy to track back ligand name and receptor"
                     sys.exit()
               else:
                  uniques[lig.lig_name] = np.arange(number_of_receptors)*0.0 
		  uniques_index[lig.lig_name] = pos_in_store
                  if lig.rec_name in known_receptors.keys():
                     receptor = lig.rec_name
                     receptor_position = known_receptors[receptor]
                     uniques[lig.lig_name][receptor_position] = lig.gen_score
	       lig.activity_vec= '%.3f'%uniques[lig.lig_name][receptor_position] 	

           final_list = range(len(uniques_index.values()))
           count = 0
	   for value in uniques_index.values():                                     # Getting the final list from the index dictionary
		final_list[count]=store[value]
                count += 1                              
           final_list = sorted(final_list, key=lambda ligand: float(ligand.gen_score),reverse=True)
           return store,final_list
#           return uniques,known_receptors    
#           print len(uniques.keys())
    except AssertionError:
           print " This function exists within \"make_unique_list_V2.py\" and \"make_unique_consensus_selected_V2.py\""
           print " This function takes 3 arguments"
           print "     1) store = This is a python array. The objects contained are from the Class Mol_info. At this poin the array contains "
           print "                all the filtered ligands found from all the receptors( same protein different pdb)"
           print "     2) known_receptors = This is a dictionary with all the receptors (same protein different  pdb) that are being treated. " 
           print " This function return 2 object: "
           print "     1) An array of the best top unique molecules (controled by the cutoff). The objects contained are from the Class Mol_info."
           print "     2) The dictonary of knonw recetors"
           print " The output is referred as uniques,known_receptors"
           return 1  # exit on error
    else:
           return 0  # exit errorlessly

def tread_molecules_from_dock_2(rec,path_to_data,cutoff):
               rec_name = (rec.split('.')[0]).split('_')
               rec_name = '%s'%rec_name[1]+'_'+'%s'%rec_name[2]
               machine = ''
               project = path_to_results.split('/')[-1]
               resume_file = open('%s/CANDIDATES/%s'%(path_to_data,rec),'r').readlines()
               number_of_ligands = len(resume_file)
               local_store = range(number_of_ligands)                                    # Storing all ligands from a single sdf file
               max_pose = 0
               count = 0
               for candidate in resume_file:      
			c = candidate.split('$$')
                        lig_name = c[0].replace(' ','')
			get_candidate = Mol_Info_General('%s_%s'%(lig_name,rec_name),machine,rec_name,lig_name,project)
                        get_candidate.genrank_lig = c[1].split(',')[0]                   # Best position in rank
                        get_candidate.genrank_pos = c[1].split(',')                      # All postions
                        get_candidate.dock_vec = c[2].split(',')                         # Dock score vector
                        get_candidate.rmsd_vec = c[3].split(',')                         # RMSD vector
                        get_candidate.neighborns = c[4].split(';')                       # Getting the possible similars
                        get_candidate.ligefi = c[5]                                      # Getting Ligand Efficiency
                        if int((get_candidate.genrank_pos)[-1]) > max_pose:
                           max_pose = int((get_candidate.genrank_pos)[-1])
                        get_candidate.status = 'dismissed'
                        if int((get_candidate.lig_name).split('_')[1]) < 3962:
                           get_candidate.nature = 'active'
                        else:
                           get_candidate.nature = 'decoy'
			local_store[count] = get_candidate                                  
			count += 1
               max_pose_value = int(max_pose*cutoff)
	       return local_store,max_pose_value
