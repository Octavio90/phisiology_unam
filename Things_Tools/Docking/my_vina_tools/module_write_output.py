#!/usr/bin/env python2.7
import sys
import os
import numpy as np
import openbabel as opb
import pybel as pyb
from module_fix_pdb_from_pdbqt import pdbqt2pdb
from read_moles_files import Mol_Info_General
from read_moles_files import cal_rmsd

def write_output_general_list(general_list):
     dones = {}	
     outfile = open('general_list.txt','w')
     line = '# name   rank '
     outfile.write('%s\n'%line)
     rank = 1
     for lig in general_list:
	     ligand = lig.name.split('_')
	     data = str(ligand[0])+'_'+str(ligand[1])
	     name = data
	     if not name in dones.keys():
		dones[name] = rank
		line = '%s     %s'%(name,rank)
		outfile.write('%s\n'%line)
		rank += 1 
     outfile.close()

def write_output_bylig_2(rec,bylig,path_to_data):
    try:   
           output = open('%s/CANDIDATES/candidates_%s.resultsv3'%(path_to_data,rec),'w')
#           output_lig_sdf = pyb.Outputfile( "sdf", "CANDIDATES_sdfs/candidates_lig_%s_%s.sdf"%(rec,cutoff), overwrite=True )          
           for lig in bylig:
                  line_out = '%-14s'%lig.lig_name
                  line_out += '$$'
                  line_out += '%s'% ','.join([str(i) for i in lig.genrank_pos])
                  line_out += '$$'
                  line_out += '%s'% ','.join([str(i) for i in lig.dock_vec])
                  line_out += '$$'
                  line_out += '%s'% ','.join([str(i) for i in lig.rmsd_vec])
                  line_out += '$$'
                  line_out += '%s'% ';'.join([str(i) for i in lig.neighborns])
                  line_out += '$$'
                  line_out += '%s'%lig.ligefi
                  line_out += '$$'
                  line_out += '%s,'%lig.fpt
                  output.write('%s\n'%line_out)
#                  lig.molecule.data.clear()
#                  lig.molecule.data['dock'] = lig.dock
#                  lig.molecule.data['freq'] = lig.freq
#                  lig.molecule.data['rmsd'] = lig.rmsd
#                  lig.molecule.data['simil_dock'] = lig.simil_dock
#                  lig.molecule.data['ligefi'] = lig.ligefi
#                  lig.molecule.data['lig_name'] = 'lig_%s'%lig.name.split('_')[1]
#                  lig.molecule.data['rec_name'] = 'receptor_%s'%lig.name.split('_')[3]
#                  output_lig_sdf.write(lig.molecule)
           output.close()
#           output_lig_sdf.close()

    except AssertionError:
           print " This function exists just within \"make_vina_rank_v3.py\""
           print " This function takes threes arguments"
           print "     1) rec = This is the main input of make_vina_rank_v3.py. An represents the receptor's results are being processed."
           print "     2) bylig = This is a python array. The objects contained are from the Class Mol_info. At this poin the array contains just "
           print "                one pose per molecule (the best scored pose)"
           print "     3) min_lig = Is the minimun scored registed "
           print " This function writes 2 files.  "
           print "     1) Table of the best results found for the given receptor. "
           print "     2) An sdf file with the best pose of the selected molecules (the best scored pose)"
           print " The output is writen in the CANDIDATES folder "
           return 1  # exit on error
    else:
           return 0  # exit errorlessly

def write_output_bypos_method(cutoff,treshold,final_list,known_receptors,constants,path_to_data):
           cutoff = str(cutoff)
           cutoff = str(cutoff.split('.')[0])+str(cutoff.split('.')[1])
           const = [ str(i) for i in constants ]
           const=''.join(const)
           treshold = ''.join((str(treshold)).split('.'))
           outfile = open('%s/MODELS/results_%s_%s_%s.txt'%(path_to_data,cutoff,treshold,const),'w')
           outfile_2 = open('%s/CANDIDATES_sdfs/to_sdfs_%s_%s_%s.txt'%(path_to_data,cutoff,treshold,const),'w')
           line = [ (i,known_receptors[i]) for i in known_receptors.keys() ]
           line = '%s'%line
           outfile.write('%s\n'%line)              
           for lig in final_list:
               line = '%s %s'%(lig.lig_name, lig.activity_vec)
               outfile.write('%s\n'%line)               
               attrs = vars(lig)
               to_sdf = []
               for item in attrs.items():
                   if not item[0] == 'fpt':
                      to_sdf.append('%s: %s'%item)
               line_2 = '; '.join(to_sdf)
               outfile_2.write('%s\n'%line_2)    
           outfile.close()
