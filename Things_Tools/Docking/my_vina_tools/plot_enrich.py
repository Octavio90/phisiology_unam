#!/usr/bin/env python2.7
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sys import argv
import subprocess
import glob

class roc_data():
   def __init__(self,method,name,se,sp,hits,total,cutoff):
       self.method = method
       self.name = name
       self.se = se
       self.sp = sp
       self.hits = hits
       self.total = total
       self.cutoff = cutoff

assert( len(argv)>1)
dir = argv[1]
cond = argv[2]
roc_values = []
infile=open('%s'%(dir),'r').readlines()
total = len(infile)
count = 0
active = 0
percent = []
ref = np.array(range(101))/100.0
for i in infile:
  i = i.split('\n')[0]	
  if not '@' in i and not '#' in i:
     lig= float((i.split()[0]).split('_')[1])
     pos = float(i.split()[1])
     if lig < 3962:
	     active += 1 
     count += 1	     
     acti_per = active
     gen_per = count/float(total)
     data = [gen_per,acti_per]
     percent.append(data)

fig = plt.figure(figsize=(8,8))
ax = fig.add_subplot(111)
ax.plot( [ i[0] for i in percent ] , [ i[1]/float(active) for i in percent ] , lw=3, label=cond)
ax.plot(ref, ref,c='k', ls='--', lw = 2)
ax.set_xlim(0,1.0)
ax.set_ylim(0,1.0)
#ax.set_xlabel('1-Sp')
#ax.set_ylabel('Se')
#ax.set_title('ROC plot %s'% synonim[0].method )
ax.grid()
ax.legend(loc='lower right')
ax.grid()
plt.show()
