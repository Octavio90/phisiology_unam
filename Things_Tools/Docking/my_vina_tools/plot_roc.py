!/usr/bin/env python2.7
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sys import argv
import subprocess
import glob

class roc_data():
   def __init__(self,method,name,se,sp,hits,total,cutoff):
       self.method = method
       self.name = name
       self.se = se
       self.sp = sp
       self.hits = hits
       self.total = total
       self.cutoff = cutoff

assert( len(argv)>1)
dir = argv[1]
#listoffiles = subprocess.check_output(["ls","%s/"%(dir)])
#listoffiles = subprocess.Popen(['ls', '%s/'%dir], stdout=subprocess.PIPE).communicate()[0]
#listoffiles = listoffiles.split('\n')
#listoffiles.pop()
listoffiles = glob.glob("%s/*.txt"%(dir))
ref = np.array(range(101))/100.0
roc_values = []
for file in listoffiles:
    print '%s'%file
    file = file.split('/')[-1]
    infile=open('%s/%s'%(dir,file),'r').readlines()
    for i in infile:
      if not '@' in i and not '#' in i:
         method_name = file.split('_')[0]
         constants_used = file.split('_')[2].split('.txt')[0]
         sen = float(i.split()[2])
         spe = float(i.split()[3])
         num_hits = float(i.split()[4])
         num_total = float(i.split()[5])
         cutoff_used = i.split()[0].split('_')[-1].split('.txt')[0] 
         values = roc_data(method_name,constants_used,sen,spe,num_hits,num_total,cutoff_used )
         roc_values.append(values) 


#roc_values.sort(key=lambda x: x.cutoff, reverse=True)
roc_values.sort(key=lambda x: x.name, reverse=True)


plot_dic = {}
count = 0
for entry in roc_values:
    if not entry.method in plot_dic.keys():
       plot_dic[entry.method] = count+1
       count += 1
       exec "group_%s= []" % plot_dic[entry.method]
    else:
       locals()["group_%s"%plot_dic[entry.method]].append(entry)

fig = plt.figure(figsize=(8,8))
for gr in plot_dic.values():
    synonim = locals()["group_%s"%gr]
    index = '22'+str(gr)
    ax = fig.add_subplot('%s'%index)
    condition = []
    for i in synonim:
      if not i.name in condition:
        condition.append(i.name)
    for cond in condition:
	plot_data = [ (entry.sp,entry.se) for entry in synonim if entry.name == cond ]
	plot_data.sort(key=lambda x: x[0],reverse=False)
	ax.plot( [ i[0] for i in plot_data ] , [ i[1] for i in plot_data ] , lw=3, label=cond)
    ax.plot(ref, ref,c='k', ls='--', lw = 2)
    ax.set_xlim(0,1.0)
    ax.set_ylim(0,1.0)
    ax.set_xlabel('1-Sp')
    ax.set_ylabel('Se')
    ax.set_title('ROC plot %s'% synonim[0].method )
    ax.grid()
    ax.legend(loc='lower right')

ax = fig.add_subplot(224)
color = ['r','b','g','y','k','c']
marker = ['s','v','^','o','>','<','D' ]
for gr in plot_dic.values():
    synonim = locals()["group_%s"%gr]
    cond = ['11111','1111111111']
#    ax.scatter([ entry.total for entry in synonim if not entry.name in cond ] , [ entry.hits  for entry in synonim if not entry.name in cond  ], c='%s'%color[gr],marker='%s'%marker[gr],s=85 ,label=synonim[0].method  )
    ax.scatter([ entry.total for entry in synonim ] , [ entry.hits  for entry in synonim ], c='%s'%color[gr],marker='%s'%marker[gr],s=85 ,label=synonim[0].method  )
#ax.set_xlim(0,155)
#ax.set_ylim(0,20)
ax.set_xlabel('Total in list')
ax.set_ylabel('# of Hits')
ax.set_title('Hard numbers' )
ax.grid()
ax.legend(loc='upper left',scatterpoints=1)
plt.show()
