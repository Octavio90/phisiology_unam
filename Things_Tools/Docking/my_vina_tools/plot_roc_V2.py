#!/usr/bin/env python2.6
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sys import argv
import subprocess
import glob

class roc_data():
   def __init__(self,method,name,se,sp,hits,total,cutoff,total_ef,recover):
       self.method = method
       self.name = name
       self.se = se
       self.sp = sp
       self.hits = hits
       self.total = total
       self.cutoff = cutoff
       self.total_ef = total_ef
       self.recover = recover

assert( len(argv)>1)
dir = argv[1]
listoffiles = glob.glob("%s/*.txt"%(dir))
ref = np.array(range(101))/100.0
roc_values = []
for file in listoffiles:
    print '%s'%file
    file = file.split('/')[-1]
    infile=open('%s/%s'%(dir,file),'r').readlines()
    for i in infile:
      if not '@' in i and not '#' in i:
         method_name = file.split('_')[0]
         constants_used = file.split('_')[2].split('.txt')[0]
         sen = float(i.split()[2])
         spe = float(i.split()[3])
         num_hits = float(i.split()[4])
         num_total = float(i.split()[5])
	 total_ef  = float(i.split()[7])
         cutoff_used = i.split()[0]
         recover = float(i.split()[4])
         values = roc_data(method_name,constants_used,sen,spe,num_hits,num_total,cutoff_used,total_ef,recover )
         roc_values.append(values) 

#roc_values.sort(key=lambda x: x.cutoff, reverse=True)
roc_values.sort(key=lambda x: x.name, reverse=True)

plot_dic = {}
count = 0
for entry in roc_values:
    if not entry.method in plot_dic.keys():
       plot_dic[entry.method] = count+1
       count += 1
       exec "group_%s= []" % plot_dic[entry.method]
    else:
       locals()["group_%s"%plot_dic[entry.method]].append(entry)

color = ['r','b','g','y','k','c']
marker = ['o-','s-','v-','>-','<-','D-' ]
fig = plt.figure(figsize=(8,8))
for gr in plot_dic.values():
    synonim = locals()["group_%s"%gr]
    index = '22'+str(gr)
    ax = fig.add_subplot('%s'%index)
    condition = []
    for i in synonim:
      if not i.name in condition:
        condition.append(i.name)
    count = 0
    for cond in condition:
	plot_data = [ (entry.sp,entry.se) for entry in synonim if entry.name == cond ]
	plot_data.sort(key=lambda x: x[0],reverse=False)
        if cond == '10000' or cond =='1000010000':
 	    ax.plot( [ i[0] for i in plot_data ] , [ i[1] for i in plot_data ] ,'^-', markerfacecolor='black', markersize=12,lw=3, label=cond)
        else:
 	    ax.plot( [ i[0] for i in plot_data ] , [ i[1] for i in plot_data ] ,marker[count], lw=3, label=cond)
        if count == len(marker)-1:
           count = 0
        else:
           count += 1  
    ax.plot(ref, ref,c='k', ls='--', lw = 2)
    ax.set_xlim(0,1.0)
    ax.set_ylim(0,1.0)
    ax.set_xlabel('1-Sp')
    ax.set_ylabel('Se')
    ax.set_title('ROC plot %s'% synonim[0].method )
    ax.grid()
    ax.legend(loc='lower right')

ax = fig.add_subplot(224)
for gr in plot_dic.values():
    synonim = locals()["group_%s"%gr]
    condition = []
    final = []
    for i in synonim:
      if not i.name in condition:
        condition.append(i.name)
    for cond in condition:
	plot_data2 = [ (entry.cutoff,entry.hits) for entry in synonim if entry.name == cond ]
	plot_data2.sort(key=lambda x: x[0],reverse=True)
	final=final+plot_data2
    xval=[ float(final[i][0]) for i in range(len(final)-1) ]
    yval=[ float(final[i][1]) for i in range(len(final)-1) ]
#    ax.scatter( xval,yval, c='%s'%color[gr-1],marker='%s'%marker[gr-1],s=85 ,label=synonim[0].method  )
    print xval
    print yval
    ax.semilogx( xval,yval,lw=3,label=synonim[0].method  )
ax.semilogx(ref, ref,c='k', ls='--', lw = 2)
ax.set_xlim(0,)
ax.set_ylim(0,)
ax.grid()
ax.legend(loc='lower right',scatterpoints=1)
plt.show()
