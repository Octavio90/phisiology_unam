#!/usr/bin/env python2.6
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sys
import subprocess
import glob
import os

class roc_data(object):
   def __init__(self,method,name,se,sp,hits,total,cutoff,total_ef,recover,auc):
       self.method = method
       self.name = name
       self.se = se
       self.sp = sp
       self.hits = hits
       self.total = total
       self.cutoff = cutoff
       self.total_ef = total_ef
       self.recover = recover
       self.auc = float(auc)

   def getROC(self):
       pair = (self.sp,self.se)
       return pair

class PlotData(object):
      def __init__(self,name,condition=None,stored=None):
          self.name = name
          if condition is None:
             self.condition = []
          if stored is None:
             self.stored = []

      def sortbyCond(self,used_cond):
          store = []
          for entry in self.stored:
              if entry.name == used_cond:
                 store.append(entry.getROC())
          store = np.array(store)
          return store     

      def getAUC(self):
          store = []
          dones = []
          for entry in self.stored:
                 con = entry.name 
                 if not con in dones:
                    store.append((entry.auc,con))
                    dones.append(con)
          return store

assert( len(sys.argv)>1)
dir = sys.argv[1]
project = os.getcwd().split('/')[-1]
listoffiles = glob.glob("%s/*.txt"%(dir))
ref = np.array(range(101))/100.0
roc_values = []
roc_values.append(PlotData('autodock4'))
roc_values.append(PlotData('autodockvina'))
roc_values.append(PlotData('consensus'))
for case in roc_values:
    for file in listoffiles:                                                                                              
#        print '%s'%file
        file = file.split('/')[-1]
        infile=open('%s/%s'%(dir,file),'r').readlines()
        auc = infile.pop(0)
        auc = float(auc.split()[-1])
        for i in infile:
             method_name = file.split('_')[0]
             if method_name == case.name:
                constants_used = file.split('_')[2].split('.txt')[0]
                sen = float(i.split()[2])
                spe = float(i.split()[3])
                num_hits = float(i.split()[4])
                num_total = float(i.split()[5])
    	        total_ef  = float(i.split()[7])
                cutoff_used = i.split()[0]
                recover = float(i.split()[4])
                values = roc_data(method_name,constants_used,sen,spe,num_hits,num_total,cutoff_used,total_ef,recover,auc)
                (case.stored).append(values) 
                if not constants_used in case.condition:
                       (case.condition).append(constants_used)

color = ['r','g','b','y','k','c']
marker = ['o-','s-','v-','>-','<-','D-' ]
fig = plt.figure(figsize=(8,8))
gr = 0
count = 0
for case in roc_values:
    index = '22'+str(gr+1)
    ax = fig.add_subplot('%s'%index)
    for cond in case.condition:
	plot_data = case.sortbyCond(cond)
        if cond == '10000' or cond =='1000010000':
 	    ax.plot( [ i[0] for i in plot_data ] , [ i[1] for i in plot_data ] ,'^-', markerfacecolor='black', markersize=12,lw=3 )
        else:
 	    ax.plot( [ i[0] for i in plot_data ] , [ i[1] for i in plot_data ] ,marker[count], lw=3,label=cond)
        if count == len(marker)-1:
           count = 0
        else:
           count += 1  
    ax.plot(ref, ref,c='k', ls='--', lw = 2)
    ax.set_xlim(0,1.0)
    ax.set_ylim(0,1.0)
    ax.set_xlabel('1-Sp')
    ax.set_ylabel('Se')
    ax.set_title('ROC plot %s'% case.name )
    ax.grid()
    ax.legend(loc='lower right')
    gr += 1

ax = fig.add_subplot(224)
gr = 0
marker = ['o','s','v','>','<' ]
for case in roc_values:
    plot_data = case.getAUC()
    yval = [ i[0] for i in plot_data ]
    names = [ i[1] for i in plot_data ]
    for point in range(len(names)):
       if names[point] == '10000' or names[point] == '1000010000':
          ax.scatter(point,yval[point], c='k',marker='^',s=85,label=case.name )
       else:
          ax.scatter(point,yval[point], c='%s'%color[gr],s=85,label=case.name )
    gr += 1
ax.grid()
ax.set_ylim(0,1.0)
ax.set_xlabel('MODEL')
ax.set_ylabel('AUC')
ax.set_title('AUC plot %s'% project )
#ax.legend(loc='lower right',scatterpoints=1)
#plt.savefig('%s'%sys.argv[1],dpi=300,bbox_inches='tight')
plt.show()
