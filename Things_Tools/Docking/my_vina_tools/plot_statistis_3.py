#!/usr/bin/env python2.7
from __future__ import division
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import sys
from scipy.ndimage.filters import gaussian_filter

class ListData(object):
      def __init__(self,line):
          self.line = line

      def split_input_line(self):
          line = self.line
          line = line.split()
          self.rank = int(line[0])
          self.genscore = float(line[1])
          self.dock = float(line[2])
          self.rmsd = float(line[3])
          self.freq = float(line[4])
          self.simi = float(line[5])
          self.ligefi = float(line[6])
    
class MolesData(object):
      def __init__(self,nature,data=None):
          self.nature = nature
          if data is None:
             self.data = []

      def getColumn(self,attri):
          values = [ getattr(entry,attri) for entry in self.data ]
          return np.array(values)

      def gen_histo(self,attri,number_bins):
 
          def set_bins(data,number):                                        
              max_val = 1.0
              min_val = 0.0
              step = (max_val-min_val)/float(number)
              width = (max_val-min_val)/float(number)                
              bins = []
              for val in range(number):
                  add = val*step
                  bins.append(min_val+add)
              bins.append(max_val+step)
              return np.array(bins), width
                                                                              
          def put_in_bins(data,bins,step):                                   
              dic = {}
              for pos in range(len(bins)-1):
                  bin_range = [bins[pos],bins[pos+1]]
                  count = 0
                  for value in data:
                      if value >= bin_range[0] and value <= bin_range[1]:
                         count += 1
                  dic[pos] = count
              max_value = np.sum(dic.values())
              for val in dic.keys():
                  dic[val] = dic[val]/(max_value*step)
              return dic      

          data = [ getattr(entry,attri) for entry in self.data ]
          data = np.array(data)
          bins,width = set_bins(data,number_bins)          
          n = put_in_bins(data,bins,width) 
          bincenters = 0.5*(bins[1:]+bins[:-1])
          gaussian = gaussian_filter(n.values(),1) 
          dic_out = {}
          for i in range(len(bincenters)):
              dic_out[i]=gaussian[i]

          return dic_out

      def gauss_smooth(data):
          smooth = []
          const = 1.0/(np.sqrt(2*2.14159))
          for val in range(3,len(data[:-3])):
              win = data[val-2:val+2]
              win = np.array(win)
              mean = np.mean(win)
              std = np.std(win)
              coef = (const/std)
              arg = (data[val]-mean)*(data[val]-mean)/(std*std)
              gauss = coef*np.exp(-0.5*arg)
              smooth.append(gauss)
          return smooth

def plot_2_hist(decoys,actives,base_atri,atributes,no_bins,where):
          def fill_matrix(case,hist_1,atri,size,no_bins):
              Z = np.arange(size*size).reshape(size,size)
              hist_2 = case.gen_histo(atri,no_bins)
              for i in range(size):
                  for j in range(size):
                      Z[i][j] = hist_1[i]*hist_2[j]
              return Z

          hist_d1 = decoys.gen_histo(base_atri,no_bins)
          hist_a1 = actives.gen_histo(base_atri,no_bins)
          size = len(hist_d1.keys())
          count = 2
          for atri in atributes:
              fig = plt.figure(count)                                                         

              ax = fig.add_subplot(121)
              Z = fill_matrix(actives,hist_a1,atri,size,no_bins)
              ax.set_xlabel('%s'%base_atri)
              ax.set_ylabel('%s'%atri)
              ax.set_title('Actives')
              im = imshow(Z, cmap=cm.RdBu)
              im.set_interpolation('bilinear')

              ax = fig.add_subplot(122)
              Z = fill_matrix(decoys,hist_d1,atri,size,no_bins)
              #im = imshow(Z, cmap=cm.RdBu, vmax=abs(Z).max(), vmin=-abs(Z).max())
              ax.set_xlabel('%s'%base_atri)
              ax.set_ylabel('%s'%atri)
              ax.set_title('Decoys')
              im = imshow(Z, cmap=cm.RdBu)
              im.set_interpolation('bilinear')
              if not save == 'no':
                 plt.savefig('stats_pictures/histos_2D_%s_%s_%s'%(where,base_atri,count),dpi=300,bbox_inches='tight')
              count += 1
          return count 

def plot_scatters(case,count):
    fig = plt.figure(count)
    ax = fig.add_subplot(221)
    ax.scatter(case.getColumn('dock'),case.getColumn('freq'),c='k')
    ax.set_xlabel('dock')
    ax.set_ylabel('freq')
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.grid()
    ax.set_title('%s'%case.nature)
    ax = fig.add_subplot(222)
    ax.scatter(case.getColumn('freq'),case.getColumn('simi'),c='y')
    ax.set_xlabel('freq')
    ax.set_ylabel('simi')
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.grid()
    ax.set_title('%s'%case.nature)
    ax = fig.add_subplot(223)
    ax.scatter(case.getColumn('simi'),case.getColumn('ligefi'),c='r')
    ax.set_xlabel('simi')
    ax.set_ylabel('ligefi')
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.grid()
    ax.set_title('%s'%case.nature)
    ax = fig.add_subplot(224)
    ax.scatter(case.getColumn('ligefi'),case.getColumn('dock'),c='b')
    ax.set_xlabel('ligefi')
    ax.set_ylabel('dock')
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.grid()
    ax.set_title('%s'%case.nature)

def main(args):
    atributes = ['genscore','dock','freq','rmsd','simi','ligefi']                                                  
    where = args[0] 
    try: 
       save = args[1]                                                          # name to store
       if not save in atributes:
          print "The second argument should be one of these:"
          print "%s"%atributes
          sys.exit()
    except IndexError: 
       save = 'no'
    activesfile = open('stats_files/actives_%s.vectors'%where,'r').readlines()  # list of actives
    decoysfile = open('stats_files/decoys_%s.vectors'%where,'r').readlines()    # List of decoys

    # Deleting the  header and storing the labels
    decoysfile.pop(0)  
    labels = activesfile.pop(0)
    labels = labels.split('\n')[0]
    labels = labels.split()

    # Storing the data   
    actives = MolesData('actives')
    for line in activesfile:
        line = line.split('\n')[0]
        data = ListData(line)
        data.split_input_line()
        (actives.data).append(data)
    
    decoys = MolesData('decoys')
    for line in decoysfile:
        line = line.split('\n')[0]
        data = ListData(line)
        data.split_input_line()
        (decoys.data).append(data)
    ############################
    ###### Ploting section######
    
    # Distribution by rescoring vector
    fig = plt.figure(1)
    count = 0
    div = 100
    for atri in atributes:
        index = count + 1
        ax = fig.add_subplot('61%s'%index)
        hist_decoys = decoys.gen_histo(atri,div)
        hist_actives = actives.gen_histo(atri,div)
        ax.plot(hist_decoys.keys(),hist_decoys.values(), 'r-', linewidth=3)
        ax.plot(hist_actives.keys(),hist_actives.values(), 'b-', linewidth=3)
        if index == 1:
           ax.set_title('Actives (Blue)  Decoys (Red)')
        ax.set_ylabel('%s'%atri)
        ax.grid()
        count += 1
        ax.set_xlabel('Score')
    if not save == 'no':
        plt.savefig('stats_pictures/histos_%s'%where,dpi=300,bbox_inches='tight')

    # 2-D Histograms
    if not save == 'no':
       plot_agains = save
    else:
       plot_agains = 'dock'
    count = plot_2_hist(decoys,actives,plot_agains,atributes,div,where)

    # Scatter plots 
    plot_scatters(actives,count)
    count += 1 
    plot_scatters(decoys,count)
    if not save == 'no':
       plt.savefig('stats_pictures/scatter_%s'%where,dpi=300,bbox_inches='tight')
    plt.show()

if __name__ == '__main__':
    sys.argv.pop(0)
    sys.exit(main(sys.argv))
