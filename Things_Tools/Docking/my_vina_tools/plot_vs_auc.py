#!/usr/bin/env python2.6
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sys
import subprocess
import glob

class Auc_data(object):
   def __init__(self,method,rec_name,auc,cutoff,condition):
       self.method = method
       self.rec_name = int(rec_name)
       self.auc = float(auc)
       self.condition = condition
       self.cutoff = cutoff

class Machine(object):
      def __init__(self,name,aucdata=None):
          self.name = name
          if aucdata is None:
             self.aucdata = []

      def getbyValueCC(self,used_cond,used_cut):    
          """Finds value with the specified cutoff and condition"""
          store = []
          for entry in self.aucdata:
              if entry.condition == used_cond and entry.cutoff == used_cut:
                 store.append(entry.auc)
          store = np.array(store)
          return store

      def statbyCond(self,used_cond,used_cut):
          """Return the AUC average over receptors at constant cutoff
          and condition"""
          store = self.getbyValueCC(used_cond,used_cut)
          return np.mean(store),np.std(store)

      def betterThan(self,used_cond,used_cut):
          rec_list = []
          for entry in self.aucdata:
              if not entry.rec_name in rec_list:
                 rec_list.append(entry.rec_name)
          total = 0
          for rec in rec_list:
             for entry in self.aucdata:
                 if entry.condition == '10000' and entry.cutoff == used_cut and entry.rec_name == rec:
                    tobeat = entry.auc
             count = 0
             countg = 0
             for entry in self.aucdata:
                 if entry.auc > tobeat and entry.cutoff == used_cut and entry.condition == used_cond and entry.rec_name == rec: 
                    count += 1
                 countg += 1
             total += count
          return total

      def genDict(self):
           cut_dic = []
           rec_dic = []
           cond_dic = []
           for entry in self.aucdata:
             if not entry.cutoff in cut_dic:
                   cut_dic.append(entry.cutoff) 
             if not entry.rec_name in rec_dic:
                   rec_dic.append(entry.rec_name) 
             if not entry.condition in cond_dic:
                   cond_dic.append(entry.condition)
           self.cuts = cut_dic
           self.recs = rec_dic
           self.cons = cond_dic

def read_input(listoffiles,name):
    store=Machine(name)
    for file in listoffiles:
        file = file.split('\n')[0]
        infile=open('../%s/AUC_%s.resume'%(file,file),'r').readlines()
        for line in infile:
          if not '@' in line and not '#' in line:
             method = line.split()[3]
             rec_name = file.split('_')[1]
             cutoff = line.split()[2]
             auc = line.split()[0]
             if method == 'consensus' and name == 'consensus':
                condition = ''.join(list(line.split()[4])[:5])
             else:
                condition = line.split()[4]
             if method == name:
                values = Auc_data(method,rec_name,auc,cutoff,condition)
                (store.aucdata).append(values) 
    store.genDict()
    return store

def main(args):
    assert(len(args)>0)
    listofrec = args[0]
    listoffiles = open('%s'%listofrec,'r').readlines() 
    auc_dock4 = read_input(listoffiles,'autodock4')
    auc_dockv = read_input(listoffiles,'autodockvina')
    auc_conse = read_input(listoffiles,'consensus')
    gen = []
    gen.append(auc_dockv)
    gen.append(auc_conse)
    gen.append(auc_dock4)
    
    condensed = []
    for method in gen:
       auc_value_plot = []
       for vec in method.cons:
           vectors = []
           for cut in method.cuts:
               vectors.append((cut,method.getbyValueCC(vec,cut))) 
           auc_value_plot.append((vec,vectors))
       condensed.append((method.name,auc_value_plot))
    
    ref = np.array(range(101))/100.0
    color = ['r','b','g','y','c']
    marker = ['8','s','v','>','<' ]
    
    gr = 0
    width = 0
    monitor = 0
    for pair in [[1,2],[1,0],[0,2]] :
        datax = condensed[pair[0]][1]
        datay = condensed[pair[1]][1] 
        index = '22'+str(gr+1)
        count = 0
        for vec in range(len(datax)):
            count += 1
            xcon = datax[vec][1]
            ycon = datay[vec][1]
            xval = [ entry[1] for entry in xcon ]
            yval = [ entry[1] for entry in ycon ]
            fig = plt.figure(count,figsize=(8,8))
            ax = fig.add_subplot('%s'%index)
            ax.set_title('%s'%datax[vec][0] )
            ax.grid()
            ax.set_ylabel('%s'%condensed[pair[1]][0]) # X-changed of labels for  
            ax.set_xlabel('%s'%condensed[pair[0]][0]) # an easy vizualization
            for entry in range(len(xval)):
                ax.scatter(xval[entry],yval[entry],c='%s'%color[entry],marker='o',s=40,label='%s'%xcon[entry][0])
#                for number in range(len(xval[entry])):
#                  t = ax.text(xval[entry][number],yval[entry][number], str(number+1),size='x-large')
#            ax.legend(loc='lower right',scatterpoints=1)
            ax.legend(loc='best',scatterpoints=1)
            ax.scatter(ref,ref,c='k',s=10)
            ax.set_ylim(0,1.0)
            ax.set_xlim(0,1.0)
        gr += 1
    plt.show()

if __name__ == '__main__':
    sys.argv.pop(0)
    sys.exit(main(sys.argv))
