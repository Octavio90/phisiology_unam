#!/usr/bin/env python2.6
import sys
import numpy as np
import openbabel as opb
import pybel as pyb

#def cal_rmsd(posref,posalter): 
#   refcoor = [ atom.coords for atom in posref if not atom.atomicnum == 1 ]
#   altercoor = [ atom.coords for atom in posalter if not atom.atomicnum == 1 ]
#   refcoor= np.array(refcoor)
#   altercoor= np.array(altercoor)
#   rmsd = []
#   print len(altercoor)
#   for i in range(len(altercoor)):
#       val = (altercoor[i]-refcoor[i])*(altercoor[i]-refcoor[i])
#       rmsd.append(val)
#   print len(val)
#   rmsd = np.sum(rmsd,axis=0)
#   rmsd = np.sqrt(np.sum(rmsd)/float(len(altercoor)))
#   print rmsd

def cal_rmsd(mol1,mol2):
    coord1 = [ atom.coords for atom in mol1 if not atom.atomicnum == 1 ]
    coord2 = [ atom.coords for atom in mol2 if not atom.atomicnum == 1 ]
    numatoms = len(coord1)
    if numatoms == 0:
       print mol1.title, mol2.title
       sys.exit()
    sq = 0
    if len(coord1) == len(coord2):
       for i in range(numatoms):
           a = [ coord1[i][j]-coord2[i][j] for j in range(len(coord1[i])) ]
           a = np.array(a)
           sq += np.sum(a*a)
       sq = np.sqrt(sq/float(numatoms))
    else :
       print "ERROR the molecules have differente number of atoms "
       print len(coord1), len(coord2)
    return sq 


class Mol_Info_pose():
   def __init__(self,name,rec_name,lig_name,intrank,dock):
       self.name = name
       self.intrank = int(intrank)
       self.dock = float(dock)

class Mol_Info_General():
   def __init__(self,file_name,machine,rec_name,lig_name,project,ligefi='',dock_vec=None,rmsd_vec=None,genrank_pos=None,score_vec=None,activity_vec=None,neighborns='',fpt='',genrank_lig='',gen_score='',res_score_vec=None,nature='',status=''):
       self.file_name = file_name
       self.machine = machine
       self.rec_name = rec_name
       self.lig_name = lig_name
       self.ligefi = ligefi
       self.project = project
       self.neighborns = neighborns
       self.fpt = fpt
       self.genrank_lig = genrank_lig
       self.status = status
       self.nature = nature
       if dock_vec is None:
          self.dock_vec = []
       if rmsd_vec is None:
          self.rmsd_vec = []
       if genrank_pos is None:
          self.genrank_pos = []
       if score_vec is None:
          self.score_vec = []
       if res_score_vec is None:
          self.res_score_vec = []
       if activity_vec is None:
          self.activity_vec = []
       self.gen_score = []
       
   def write_res_vector(self,case):
       line = ''
       if case == 'rescoring':
          for pos in self.res_score_vec:
              line += ' %-4.7s '%pos
       if case == 'raw':
          for pos in self.score_vec:
              line += ' %-4.7s '%pos
       return line 

   def print_mol_info(self):
       attrs = vars(self)
       print ', '.join("%s: %s" % item for item in attrs.items() if not item[0] == 'fpt')
