#!/usr/bin/env python
import sys
import numpy as np
import openbabel as opb
import pybel as pyb

def scoring_function(lig,maxvalues,constants):
    constants = np.array(constants)
    values = [float(lig.dock) ,float(lig.freq),float(lig.rmsd), float(lig.simil_dock), float(lig.ligefi)]
    if values[2] < 1.0:
	    values[2] = 1.0
    values[2] = 1.0/float(values[2])
    maxvalues = np.array(maxvalues)
    norm_values = values/maxvalues
#    print  values[2],maxvalues[2],norm_values[2] 
    score = np.sum(constants*norm_values)/np.sum(constants)
#    print score, constants
    return score

def info_to_vec(mol_data):
    vector = (float(mol_data['dock']) ,float(mol_data['freq']),float(mol_data['rmsd']), float(mol_data['simil_dock']), float(mol_data['ligefi']))
    return vector

def pca(array):
    array = np.array(array)
    array = np.transpose(array)
    mean = np.average(array,axis=1)
    mean = np.transpose(np.matrix(mean))
    array_center = array-mean
    array_center = np.array(array_center)
    cova = np.dot(array_center,array_center.transpose())
    print cova
    covas = np.corrcoef(array_center)
    print covas
    w,v = np.linalg.eig(cova)                                                    # Eigenvalues, Eigenvectors 
    return  w,v 

def pca_eig(orig_data):
    data = np.array(orig_data)
    C = np.corrcoef(data, rowvar=0)
    w,v = np.linalg.eig(C)                                                    # Eigenvalues, Eigenvectors 
    return  w,v 

def cal_mag(vect):
    vect = np.array(vect)
    mag = np.sqrt(np.sum(vect*vect))
    return mag

def cal_rmsd(mol1,mol2):
    coord1 = [ atom.coords for atom in mol1 if not atom.atomicnum == 1 ]
    coord2 = [ atom.coords for atom in mol2 if not atom.atomicnum == 1 ]
    numatoms = len(coord1)
    sq = 0
    if len(coord1) == len(coord2):
       for i in range(numatoms):
           a = [ coord1[i][j]-coord2[i][j] for j in range(len(coord1[i])) ]
           a = np.array(a)
           sq += np.sum(a*a)
       sq = np.sqrt(sq/float(numatoms))
    else :
       print "ERROR the molecules have differente number of atoms "
       print len(coord1), len(coord2)
    return sq

def get_max_values(current,values):
#    current = [ 999999 for i in arrayofvectors[0] ]
#    current[1] = 1
#    for values in arrayofvectors:
       if values[0] < current[0]:  # docking score
          current[0] = values[0]
       if values[1] > current[1]:  # frequency
          current[1] = values[1]
       if values[2] < current[2] and  values[2] > 1.0:  # rmsd
          current[2] = values[2]
       if values[2] < current[2] and  values[2] <= 1.0:  # rmsd
          current[2] = 1.0
       if values[3] < current[3]:  # docking score simil
          current[3] = values[3]
       if values[4] < current[4]:  # ligand eficiency
          current[4] = values[4]                
#    if current[2] < 1.0:
#      current[2] = 1.0
#    else:
#      current[2] = 1/current
#    print "The max values %s"%current
       return current                              
                                
def find_max_values(store_local_values):
    current = np.array(range(5))*99999.9
    current[1] = 1
    current[2] = 0.0
    for values in store_local_values:
       if values[0] < current[0]:  # docking score
          current[0] = values[0]
       if values[1] > current[1]:  # frequency
          current[1] = values[1]
       if values[2] > current[2]:  # rmsd
          current[2] = values[2]
       if values[3] < current[3]:  # docking score simil
          current[3] = values[3]
       if values[4] < current[4]:  # ligand eficiency
          current[4] = values[4]                
    return current                              

def filter(list,max_values,threshold,constants):         # Input is a list of instances and max values
    new_list = range(len(list))                          # To store the succesfull ligands
    count = 0                                            
    for ligand in list:                                  
#        data = ligand.data                               
#        vector_info = info_to_vec(data)                   
        ligand.gen_score = scoring_function(ligand,max_values,constants)      
        if ligand.gen_score >= threshold:                      # Filter condition   
           new_list[count] = ligand               
           count += 1                           
	   print ligand.gen_score,ligand.dock ,ligand.name
        else:            
           new_list.pop()
    return new_list

def do_pca(list,maxvalues):
    new_list = range(len(list))                   # To store the succesfull ligands
    count = 0                                   
    for ligand in list:                         
        data = ligand.data                      
        vector_info = info_to_vec(data)  
        norm_values = vector_info/np.array(maxvalues)
        new_list[count] = norm_values
        count += 1                           
#    einval, einvec = pca(new_list)
    val_o, vec_o = pca_eig(new_list)
    eingen_pairs = [(val_o[i],vec_o[i]) for i in range(len(val_o)) ] # Get the list of eigenvalues and eigenvectors
    vector_ordered = sorted(eingen_pairs, key=lambda eigen: eigen[0], reverse= True)     # and ordened in descendent order respect the eigenvalue
    vectors = [ i[1] for i in vector_ordered ]   # Get the eigenvectors together and
    while len(vectors) > 3:
          vectors.pop()
    points_pca = []
    for point in new_list:                          # Makes the coodinates transformation.
        point = np.dot(vectors,np.transpose(point)) # It works. without transpose
        points_pca.append(point)
    return points_pca
