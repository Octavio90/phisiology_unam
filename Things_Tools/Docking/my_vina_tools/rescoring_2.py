#!/usr/bin/env python
import sys
import numpy as np
import openbabel as opb
import pybel as pyb

def scoring_Z(lig,ave,std,max_values):
#    values = [float(lig.dock) ,float(lig.freq),float(lig.rmsd), float(lig.simil_dock), float(lig.ligefi)]
    values = lig.score_vec
    z_score = [(-1.0)*(values[0]-ave[0])/std[0],(values[1]-ave[1])/std[1],(-1.0)*(values[2]-ave[2])/std[2],(-1.0)*(values[3]-ave[3])/std[3],(-1.0)*(values[4]-ave[4])/std[4]]
#    z_score = (values-ave)/std       # The max_values contains the appropiate sing to get the right extrem of the data
#    z_score = z_score/max_values   
    return z_score           

def info_to_vec(mol_info,total_lig,max_pose_value):
    dock = float((mol_info.dock_vec)[0])
    freq,rmsd,sim_dock = apply_cutoff(mol_info,total_lig,max_pose_value)
    lig_efficiency = float(mol_info.ligefi)
    vector = [ dock, freq, rmsd, sim_dock, lig_efficiency ]
    return vector
   
def get_max_values(data):
    """ Get the maximun value at each position. for dock scores get the minimun """
    max = range(len(data[0]))
    for i in range(len(max)):
        all = [ val[i] for val in data ]
        val = 0
        if i == 2 or i == 1:
           for j in all:
               if j > val:
                  val = j
        else:
           for j in all:
               if j < val:
                  val = j
        max[i] = val
    return max        

def remove_zero(data):
    risk_pos = np.where(np.array(data) == 0.0 )
    if len(risk_pos) > 0:
        for i in risk_pos:
            data[i] = 1.0
    return data

def find_max_values_3(store):
    """ This actually gets the Average and Standard Deviation """
#                     Dock        Freq        Rmsd       Simil      LigEfi
    data = range(len(store))
    count = 0
    for lig in store:
       data[count] = lig.score_vec
       count += 1
    data = np.array(data)
    max = get_max_values(data)
    data_ave = np.average(data,axis=0)
    data_std = np.std(data,axis=0)
    data_std = remove_zero(data_std)
    max_values = (max-data_ave)/data_std
    max_values = remove_zero(max_values)
    print max_values
    return zip(data_ave,data_std), max_values

def NEW_new_filter(list_of_lig,norm,max_values,threshold,constants):     # Input is a list of instances and max values
    constants = np.array(constants)
#    wg_constants = constants*(1.0/np.sum(constants))
    if np.sum(constants) == 3:
       wg_constants = [0.2,0.0,0.0,0.4,0.4]
    if np.sum(constants) == 4:
       wg_constants = [0.15,0.0,0.35,0.25,0.25]
    ave,std = zip(*norm)
    ave = np.array(list(ave))
    std = np.array(list(std))
    new_list = range(len(list_of_lig))                          # To store the succesfull ligands
    count = 0 
    max = 0
    for ligand in list_of_lig:                                  
        ligand.res_score_vec = scoring_Z(ligand,ave,std,max_values)           
        ligand.gen_score = np.sum(ligand.res_score_vec*wg_constants)
        new_list[count] = ligand               
        if max < ligand.gen_score:
           max = ligand.gen_score
        count += 1                           
    for ligand in new_list:
        ligand.gen_score = ligand.gen_score/(float(max))
    return new_list

def NEW_apply_cutoff(mol_info,total_lig,max_pose_value):
    new_pos = [ entry for entry in mol_info.genrank_pos if int(entry) <= max_pose_value ]         # Checking how many poses are below teh cutoff
    if len(new_pos) > 0 :                                                                         # If any. Then get its rmsd to the best pose
       new_rmsd = [ float((mol_info.rmsd_vec)[entry]) for entry in range(len(new_pos)) ]          # And get its average. 
       new_rmsd = np.array(new_rmsd)
       new_rmsd = np.mean(new_rmsd)
       argument = (new_rmsd-5.0)*(1.0)                                                            # sigmoid. m = 5.0 and tau = 1
       new_rmsd = 1.0/(1+np.exp(argument))
       similar = [ entry for entry in mol_info.neighborns if float(entry.split(':')[1]) > 0.70 ]  # Looking If the molecule has similar molecules above 
       if len(similar) > 0 :                                                                      # 0.7 tanimoto. If any, get its best dock_score and 
           new_similar = []                                                                       # Multiplied by the its tanimoto. If more than one molecule
           for sim in similar:                                                                    # is found, get the average. 
               name = sim.split(':')[0]
               name = name.replace(' ','')
               tani = float(sim.split(':')[1])
               for lig in total_lig:
                  if lig.lig_name == name:
#                     new_similar.append((lig.dock_vec)[0])
                     new_similar.append(float((lig.dock_vec)[0])*tani)
                     break
           if len(new_similar) > 0:
               new_similar = np.array(new_similar)
               new_similar = np.average(new_similar)
           else:
               print "PROBLEM WITH LIGAND %s at the score_similar position"%lig.lig_name
               new_similar = -0.5 
       else:                                                                                      # If no simlar molecule were found give a
            new_similar = -0.5                                                                    # high penalization number 
       initial = int(new_pos[0])
       arg_pos = [ (int(pos)-initial-1000)*0.005 for pos in new_pos ]                             # sigmoid. m = 25 and tau = 0.2 
       pos_dist_weigth = 1.0/(1+np.exp(arg_pos))
       pos_dist_weigth = np.sum(pos_dist_weigth)
    else:
          new_rmsd = 0.0                                                                          # If no molecule were found within the cutoff
          new_similar = -0.5                                                                      # give simbolic values. This lig is not going to be used. 
          pos_dist_weigth = 0.0
    return pos_dist_weigth,new_rmsd,new_similar

def gen_lig_stats(ligands,case,name,path_to_data):
    """Ligands array, raw or rescoring, outputname """
    data_actives = open('%s/stats_files/actives_%s.vectors'%(path_to_data,name),'w')
    data_decoys = open('%s/stats_files/decoys_%s.vectors'%(path_to_data,name),'w')
    data_decoys.write('# Rank     Score  ScoreVec: Dock     Freq   RMSD    SimilDock   LigEfi \n')
    data_actives.write('# Rank     Score  ScoreVec: Dock     Freq   RMSD    SimilDock   LigEfi \n')
    if case == 'rescoring':
       for lig in ligands:                      
          if lig.nature == 'active':
               line = '%-7s '% lig.genrank_lig
               line += '%-2.3f '% lig.gen_score
               line += lig.write_res_vector(case)
               data_actives.write('%s\n'%line)
          if lig.nature == 'decoy':
               line = '%-7s '% lig.genrank_lig
               line += '%-2.3f '% lig.gen_score
               line += lig.write_res_vector(case)
               data_decoys.write('%s\n'%line)
    if case == 'raw':
       for lig in ligands:                      
          if lig.nature == 'active':
               line = '%-7s '% lig.genrank_lig
               line += '%-4.7s '% lig.dock_vec[0]
               line += lig.write_res_vector(case)
               data_actives.write('%s\n'%line)
          if lig.nature == 'decoy':
               line = '%-7s '% lig.genrank_lig
               line += '%-4.7s '% lig.dock_vec[0]
               line += lig.write_res_vector(case)
               data_decoys.write('%s\n'%line)
    data_actives.close()
    data_decoys.close()

def apply_cutoff(mol_info,total_lig,max_pose_value):
    new_pos = [ entry for entry in mol_info.genrank_pos if int(entry) <= max_pose_value ]         # Checking how many poses are below teh cutoff
    if len(new_pos) > 0 :                                                                         # If any. Then get its rmsd to the best pose
       new_rmsd = [ float((mol_info.rmsd_vec)[entry]) for entry in range(len(new_pos)) ]          # And get its average. 
       new_rmsd = np.array(new_rmsd)
       new_rmsd = np.mean(new_rmsd)
       #argument = (new_rmsd-5.0)*(1.0)                                                            # sigmoid. m = 5.0 and tau = 1
       #new_rmsd = 1.0/(1+np.exp(argument))
       similar = [ entry for entry in mol_info.neighborns if float(entry.split(':')[1]) > 0.70 ]  # Looking If the molecule has similar molecules above 
       if len(similar) > 0 :                                                                      # 0.7 tanimoto. If any, get its best dock_score and 
           new_similar = []                                                                       # Multiplied by the its tanimoto. If more than one molecule
           for sim in similar:                                                                    # is found, get the average. 
               name = sim.split(':')[0]
               name = name.replace(' ','')
               tani = float(sim.split(':')[1])
               for lig in total_lig:
                  if lig.lig_name == name:
#                     new_similar.append((lig.dock_vec)[0])
                     new_similar.append(float((lig.dock_vec)[0])*tani)
                     break
           if len(new_similar) > 0:
               new_similar = np.array(new_similar)
               new_similar = np.average(new_similar)
           else:
               print "PROBLEM WITH LIGAND %s at the score_similar position"%lig.lig_name
               new_similar = -0.5 
       else:                                                                                      # If no simlar molecule were found give a
            new_similar = -0.5                                                                    # high penalization number 
       #ainitial = int(new_pos[0])
       #arg_pos = [ (int(pos)-initial-1000)*0.005 for pos in new_pos ]                             # sigmoid. m = 25 and tau = 0.2 
       #pos_dist_weigth = 1.0/(1+np.exp(arg_pos))
       #pos_dist_weigth = np.sum(pos_dist_weigth)
    else:
          new_rmsd = 9999                                                                          # If no molecule were found within the cutoff
          new_similar = -0.5                                                                      # give simbolic values. This lig is not going to be used. 
         # pos_dist_weigth = 0.0
    return len(new_pos),new_rmsd,new_similar


def new_filter(list_of_lig,norm,max_values,threshold,constants):     # Input is a list of instances and max values
    constants = np.array(constants)
    wg_constants = constants*(1.0/np.sum(constants))
#    if np.sum(constants) == 3:
#       wg_constants = [0.2,0.0,0.0,0.4,0.4]
#    if np.sum(constants) == 4:
#       wg_constants = [0.15,0.0,0.35,0.25,0.25]
    ave,std = zip(*norm)
    ave = np.array(list(ave))
    std = np.array(list(std))
    new_list = range(len(list_of_lig))                          # To store the succesfull ligands
    count = 0 
    max = 0
    for ligand in list_of_lig:                                  
        ligand.res_score_vec = scoring_Z(ligand,ave,std,max_values)           
        ligand.gen_score = np.sum(ligand.res_score_vec*wg_constants)
        new_list[count] = ligand               
        if max < ligand.gen_score:
           max = ligand.gen_score
        count += 1                           
    for ligand in new_list:
        ligand.gen_score = ligand.gen_score/(float(max))
    return new_list
