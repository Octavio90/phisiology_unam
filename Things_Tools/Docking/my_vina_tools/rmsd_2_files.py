#!/usr/bin/env python2.7
import sys
import numpy as np
import openbabel as opb
import pybel as pyb
from read_moles_files import cal_rmsd 

refname = sys.argv[1]
refformat = refname.split('.')[1]
filename = sys.argv[2]
fileformat = filename.split('.')[1]

ref = pyb.readfile('%s'%refformat,'%s'%(refname)).next()
mol = pyb.readfile('%s'%fileformat,'%s'%(filename)).next()
print '%4.2f'%cal_rmsd(ref,mol)

#ligands = list(pyb.readfile('%s'%fileformat,'%s'%(filename)))
#ref = ligands[0]
#for i in ligands:
#    rmsd = cal_rmsd(ref,i)
#    print rmsd
