#!/bin/bash
set -e
usage()
{
cat << EOF
usage: $0 options

This script runs Rosetta Ligand using poses taken from Autodock4 and Autodockvina results.

OPTIONS:
   -h      Show this message.
   -l      Number of the ligand. See the ligand numbers associated with this receptor.
   -o      Number of output structures. nstruct option in Rosetta.      
   -p      Number of processors to do the task. Should be equal or minor the number of structures.
   -r      Repeat the ligand docking.
EOF
}

repeat=
while getopts “hl:o:p:r” OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         l)
             lig=$OPTARG
             ;;
         o)
             nstruct=$OPTARG
             ;;
         p)
             processors=$OPTARG
             ;;
         r)
             repeat=1
             ;;
         ?)
             usage
             exit
             ;;
     esac
done
if [ -z "$lig" ] || [ -z "$nstruct" ] || [ -z "$processors" ]
then
     usage
     exit 1
fi

receptor=`pwd | awk -F'/' '{print $NF}'`
project=`pwd | awk -F'/' '{print $NF}'`

##########################
###### Set up Step ####### 
##########################
if [ ! -z "$repeat" ] && [ -f single_pose/lig_"$lig".params ] ; then 
   rst_lig_setup.sh "$lig" "$receptor" "$project" ;
elif [ -z "$repeat" ] && [ -f single_pose/lig_"$lig".params ] ; then 
   echo "There are parameters for "$lig"" ;
   echo "The ligand setup step was skipped. Use flag -r" 
else
   rst_lig_setup.sh "$lig" "$receptor" "$project" ;
fi  

##########################
#### Docking protocol ####
##########################
if [ ! -f docked/docked_lig_"$lig".pdb ] ; then
      echo ""$receptor".pdb single_pose/lig_"$lig".pdb" > lig_"$lig".pair  
      rosetta_scripts.linuxgccrelease -parser:protocol dock_scripts/RL_dock_lig_"$lig".xml -in:file:list lig_"$lig".pair -in:file:extra_res_fa single_pose/lig_"$lig".params -mute protocols.moves.RigidBodyMover protocols.geometry.RB_geometry -out:nstruct "$nstruct" -out:file:atom_tree_diff docked_lig_"$lig".pdb -in:file:extra_res_path ../../cofactors &>lig_"$lig".err
      mv docked_lig_"$lig".pdb docked/docked_lig_"$lig".pdb 
      rm lig_"$lig".pair
      rm lig_"$lig".err
   else
      echo " There is already a results file. Delete this file \"docked/docked_lig_"$lig".pdb\""
      echo " if you want to rerun the docking on this ligand."
      echo " The ligand docking was skipped."
fi
###############################
#### Generating the output ####
###############################
if [ ! -f results_by_lig/resume_lig_"$lig".pdb ]; then
     rosetta_scripts.linuxgccrelease -parser:protocol RL_get_poses.xml -in:file:atom_tree_diff docked/docked_lig_"$lig".pdb -in:file:extra_res_fa single_pose/lig_"$lig".params -out:file:scorefile score_lig_"$lig".sc -in:file:extra_res_path ../../cofactors &>lig_"$lig"_trans.err
     rm lig_"$lig"_trans.err
     #########################################
     #### Generating results for analysis ####
     #########################################
     # Selecting the top 10
     selected=`awk '/^total_score / {print "REMARK   " FILENAME, $NF}' "$receptor"_lig_"$lig"_00*.pdb | sort -nk2 | head | awk '{print $1","$2","$3}'`
     # HERE THE RMSD CALCULATOR SHOUL BE
     for item in ${selected[*]}
         do
            echo $item
            file_sele=`echo "$item" | awk -F',' '{print $2}'`
            awk '/^TER/{exit}1' "$file_sele"
            echo "TER"
            echo "ENDMDL" 
         done >> results_by_lig/resume_lig_"$lig".pdb
     echo END >> results_by_lig/resume_lig_"$lig".pdb
     mv score_lig_"$lig".sc results_by_lig 
     rm "$receptor"_lig_"$lig"_00*_0001.pdb 
   else
      echo " There is already a resume results file. Delete this file \"results_by_lig/resume_lig_"$lig".pdb\""
      echo " If you want to reselect the top poses. That just makes sense if you reran the docking."
      echo " The top 10 poses selection process was skipped."
fi
exit 
