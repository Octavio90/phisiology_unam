#!/bin/bash
okcheck()
{
   if [ -f $1 ]; then
         echo 'ok'
      else 
         echo 'Nofile'
   fi
}

# Read the input conditions
#machine=autodockvina
#project=`pwd | awk -F'/' '{print $NF}'`
#receptor=`pwd | awk -F'/' '{print $NF}'`
ligand=lig_$1
receptor=$2
project=$3
file1="../../../autodockvina/results/"$project"/"$receptor"/"$ligand"_"$receptor".pdbqt"
file2="../../../autodock4/results/"$project"/"$receptor"/"$ligand"_"$receptor".pdbqt"

# Getting the poses from autodock4 or autodockvina and storing
if [ `okcheck $file1` == "ok" ]; then 
      echo "Getting autodockvina poses"
      obabel -ipdbqt "$file1" -omol2 -O"$ligand"_"$receptor".mol2 -h 
fi
if [ `okcheck $file2` == "ok" ]; then 
      echo "Getting autodock4 poses"
      obabel -ipdbqt "$file2" -omol2 -h >> "$ligand"_"$receptor".mol2
fi
if  [ `okcheck $file1` == "Nofile" ] &&  [ `okcheck $file2` == "Nofile" ]; then
    echo " This ligand "$ligand" does not have useful poses. "
    echo " Job killed."
    exit -1
fi
# Rosetta Transformation
molfile_to_params.py "$ligand"_"$receptor".mol2  -p "$ligand"

# Getting the COM of the poses to include them as possible starting points for rosetta
list_of_poses=`ls "$ligand"_00*.pdb`
possible_starts=`get_COM.py "$ligand"_"$receptor".mol2 | awk '{print $1","$2","$3}'`

# Deleting the previous files
if [ -f single_pose/all_"$ligand".pdb ]; then rm single_pose/all_"$ligand".pdb ; fi
if [ -f single_pose/"$ligand".pdb ]; then rm single_pose/"$ligand".pdb ; fi
if [ -f dock_scripts/RL_dock_"$ligand".xml ]; then rm dock_scripts/RL_dock_"$ligand".xml ; fi

#Storing the Conformations in a single file
for item in ${list_of_poses[*]}
    do
       cat $item >>  single_pose/all_"$ligand".pdb
    done
echo END >>  single_pose/all_"$ligand".pdb

# Storing in folder and cleaning up
mv "$ligand"_0001.pdb single_pose/"$ligand".pdb
mv "$ligand".params single_pose/
echo "PDB_ROTAMERS all_"$ligand".pdb" >> single_pose/"$ligand".params
rm "$ligand"_00*.pdb
rm "$ligand"_"$receptor".mol2 

# Modifying the xml protocol
cp ../../protocols/RL_dock_VHTS.xml dock_scripts/RL_dock_"$ligand".xml
for pos in ${possible_starts[*]}
     do 
        new_line=`echo $pos | awk -F',' '{print "                   <Coordinates x="$1" y="$2" z="$3"/>"}'`
        awk -v add="$new_line" '/\/StartFrom/{print add}1' dock_scripts/RL_dock_"$ligand".xml > RL_dock_"$ligand".xml
        mv RL_dock_"$ligand".xml dock_scripts/
     done
