#!/usr/bin/env python2.7
import sys
import numpy as np
import openbabel as opb
import pybel as pyb

# Read input 
refname = sys.argv[1]                   # Get the simimilarity vector for a specific ligand "reference"
refformat = refname.split('.')[1]       # Get the format of the file.
filename = sys.argv[2]                  # Read list of ligands to compared against 

# Getting the molecules ready 
path_to_list = '../../common_values'
path_to_files = '../../ligands'
ref = pyb.readfile('%s'%refformat,'%s/%s'%(path_to_files,refname)).next()
list_of_files = open('%s/%s'%(path_to_list,filename),'r').readlines()
tanimoto_vector = {}

# Calculating the reference fingerprint
ref_fpt = ref.calcfp()
ref_fpt_bits = ref_fpt.bits
ref_stringbit = ','.join([ str(i) for i in ref_fpt_bits ])
ref_real_bits = np.zeros(1024,dtype=np.int)
for pos in ref_fpt_bits:
	    ref_real_bits[int(pos)-1]='1'
ref_real_stringbit = ','.join([ str(i) for i in ref_real_bits ])
#print refname.split('.')[0] , ref_stringbit
#print refname.split('.')[0] , ref_real_stringbit

# Calculating the fingerprint of the files in list a getting the tanimoto
line_to_write_title = ''
line_to_tanimoto = ''
for ligfile in list_of_files:
	ligfile = ligfile.split('\n')[0]
	line_to_write_title += '  '+'%10s'%ligfile
	lig = pyb.readfile('%s'%refformat,'%s/%s.%s'%(path_to_files,ligfile,refformat)).next()
        lig_fpt = lig.calcfp()
	lig_fpt_bits = lig_fpt.bits
	tanimoto = ref_fpt | lig_fpt
	tanimoto_vector[ligfile] = '%.3f'%tanimoto
        line_to_tanimoto += '  '+'%5s'%str(tanimoto_vector[ligfile])

#for ligfile in list_of_files:

#        line_to_tanimoto += line_to_tanimoto+'  '+'%5s'%str(tanimoto_vector[ligfile])


print 'reference     '+line_to_write_title+'        fpt_indexs'    
print refname.split('.')[0]+'     '+line_to_tanimoto+'        '+ref_real_stringbit

