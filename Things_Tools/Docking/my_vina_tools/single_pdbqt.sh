#!/bin/bash

lig_name=$1                                            # List of ligands to convert.
recep=`basename $PWD`                                  # Current directory. That is the receptor to treat.
file_name="$lig_name"_"$recep"                         # Base name of the file.
machine=`echo $PWD | awk -F'/' '{(a=NF-3) ;print $a}'` # Get which docking score 

if [ $machine == "autodock4" ]; then
    if [ ! -f "$file_name".pdbqt ]                     # Just do it if NOT already converted 
    then
    
       if [ ! -L write_conformations_from_dlg.py ];	then
       	ln -s ~/bin/Utilities24/write_conformations_from_dlg.py
       fi		
       if [ ! -L rmsd_2_files.py ]; then
       	ln -s  ~/Things_Tools/Docking/my_vina_tools/rmsd_2_files.py
       fi
    
       ./write_conformations_from_dlg.py -d "$file_name".dlg -o "$file_name"                # Generating the conformations.
    
       scores=(`awk '{ if ($NF=="RANKING" ) print $4 }' "$file_name".dlg`)                  # Getting the Scores. 
       heavys=`awk '/ATOM/ {print $3}' "$file_name"_1.pdbqt | awk '!($0 in a){a[$0];print substr($0,1,1) }' | awk '!($1=="H"){n++}; END {print n+0}'` # Getting the number of heavy atoms. 
    
       for index in ${!scores[*]};
          do
            ((out_struct=index+1))                                                          # Index of the structure to read.
    	    dock=${scores[$index]}	                                                    # Getting its Score.
            rmsd=`rmsd_2_files.py  "$file_name"_1.pdbqt  "$file_name"_"$out_struct".pdbqt`  # Getting the rmsd to the best scored pose. 
            echo "MODEL $out_struct" >> "$file_name".pdbqt                                  # Writing the output file 
            echo "REMARK AD4 RESULT:      $dock   $rmsd   $heavys" >> "$file_name".pdbqt
            cat  "$file_name"_"$out_struct".pdbqt >> "$file_name".pdbqt
            echo "ENDMDL" >> "$file_name".pdbqt
          done
       echo "END"  >> "$file_name".pdbqt
       rm  "$file_name"_*.pdbqt
    
    else
       echo " already done: "$file_name".pdbqt "   
    fi
fi

if [ $machine == "autodockvina" ]; then
    if [ -f "$file_name".pdbqt ] ; then                    # Just do it if file exist
       heavys=`awk '/MODEL 1/ {if (NR==1) flag=1;next} /ENDMDL/{flag=0} flag {if ($1=="ATOM") print $3}' "$file_name".pdbqt | awk '{print substr($0,1,1) }'  | awk '!($1=="H"){n++}; END {print n+0}'`
       awk -v count="$heavys" '{if ($2=="VINA") ($6=count) ; print $0}' "$file_name".pdbqt > temp_"$lig_name" ; mv temp_$lig_name "$file_name".pdbqt
    fi
fi

if [ $machine == "rstlig" ]; then
   if [ ! -f "$file_name".pdbqt ]; then 
      file_to_read=../results_by_lig/resume_"$lig_name".pdb
      if [ -f "$file_to_read" ] ; then
          awk -F',' '/REMARK/{print $1"  "$2" RESULT:   "$3};/LG1/;/TER/;/END/' "$file_to_read" | awk '/REMARK/{cnt++;print "MODEL "cnt}1' > temp_"$lig_name".pdb
          obabel -ipdb temp_"$lig_name".pdb -opdbqt -Otemp_"$lig_name".pdbqt
          rmsd=(`rmsd_1_files.py temp_"$lig_name".pdb`)
          heavys=`awk '/HETATM/ {print $3}' temp_"$lig_name".pdb | awk '!($0 in a){a[$0];print substr($0,1,1) }' | awk '!($1=="H"){n++}; END {print n+0}'` # Getting the number of heavy atoms. 
          for index in ${!rmsd[*]};
              do
                ((out_struct=index+1))
                insert="${rmsd[$index]}  $heavys"
                change=`awk -v a=$out_struct '/REMARK/{cnt++; if (cnt==a) print $0}' temp_"$lig_name".pdb`
                new="$change  $insert"
                awk -v a=$out_struct '/Name/{cnt++; if (cnt==a) print "HERE"}1' temp_"$lig_name".pdbqt | sed "s/HERE/$new/g" > temp_"$lig_name".temp
                mv temp_"$lig_name".temp temp_"$lig_name".pdbqt
              done
          mv temp_"$lig_name".pdbqt "$file_name".pdbqt
          rm temp_"$lig_name".pdb  
      else
         echo "There is no file results_by_lig/resume_"$lig_name".pdb"
         echo "$lig_name" >> todo
         echo "Nothing Done"
         exit 1           
      fi
   else
       echo " already done: "$file_name".pdbqt "   
   fi
fi
