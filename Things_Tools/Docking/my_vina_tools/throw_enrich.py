#!/usr/bin/env python2.6
import sys

rec=sys.argv[1]
listoffiles = open('results_list.name','r').readlines()
listofactives = open('real_actives.list','r').readlines()
ractives = [ int(line.split('\n')[0].split('_')[1]) for line in listofactives ]
number = []

for file in listoffiles:
   infile = open('%s/%s'%(rec,file.split('\n')[0]),'r').readlines()
   espenumber = [ line.split('_')[1]  for line in infile]
   print file.split('\n')[0], ':  %s'%(len(espenumber)*'#')
   for num in espenumber:
       if not num in number:
          number.append(num)
count = 0
for i in number:
   for real in ractives:
      if int(i) == real:
         print i
         count += 1

total_in_list = len(number)
EF1 = float(count)/float(total_in_list)
EF2 = float(38)/float(771)
se = EF1
sp = float(700-(len(number)-count))/float(700+(len(ractives)-count))
print '%.1f'%(EF1/EF2) ,  '%.2f'%se , '%.2f'%sp
