#!/usr/bin/env python2.7
import sys
import os
import glob

class roc_data():
   def __init__(self,name,selected,tp,fp='',tn='',fn='',real_decoys='',real_actives=''):
       self.name = name
       self.selected = float(selected)
       self.tp = float(tp)
       self.fp = fp
       self.fn = fn                              # negative to later add the number of maximun number of true positives in the condition 
       self.tn = tn
       self.real_decoys = real_decoys
       self.real_actives = real_actives

def compute_stads(roc,max_count,max_count_tot):
	for value in roc:
            value.real_actives = max_count
            value.real_decoys = max_count_tot - max_count
            value.fp = value.selected - value.tp
            value.fn = value.real_actives - value.tp
            value.tn = value.real_decoys - value.fp
            if value.selected > 0:
               percent = value.tp/value.selected
            else:
               percent = 0
            if value.real_actives == 0:
                se = 0
                sp = 0
            else:
                se = value.tp/(value.tp+value.fn)
                sp = value.tn/(value.tn+value.fp)
        return roc

def sort_dock_data(path_to_files,cutoff,condition):
	listoffiles = glob.glob("%s/results_%s_*_%s.txt"%(path_to_files,cutoff,condition))
	roc = []
	max_count = 0
	max_count_tot = 0
	for file in listoffiles:
		file_to_read = file.split('/')[-1]
		infile = open('%s/%s'%(path_to_files,file_to_read),'r').readlines()
		ligs_in_file = [ line.split()[0].split('_')[1] for line in infile if not "receptor" in line.split()[0] ] # get the ligand number from the list
		count = 0
		if not len(ligs_in_file) == 0:                                 # ligands are found, then count how many are actives
		    for i in ligs_in_file:
		        for real in reactives:
		            if int(i) == real:
				count += 1
				data = roc_data(file,len(ligs_in_file),count)  # Generate the roc data
		 		roc.append(data)
				if count > max_count:                          # max_count represents the maximun number of actives ligands found in the list
				   max_count = count
				if len(ligs_in_file) > max_count_tot:          # max_count_tot represents the maximun number of ligands found in the list 
				   max_count_tot = len(ligs_in_file)          
	return roc,max_count,max_count_tot                                     # max_count_tot and max_count helps to contabilize in hard numbers how wrong it the prediction can get  

def write_stads(roc,cutoff):
	if not os.path.exists('resume_results_%s'%cutoff):                                   	
	    os.makedirs('resume_results_%s'%cutoff)
	outfile = open('resume_results_%s/%s_%s_%s.txt'%(cutoff,method,cutoff,condition),'w')
	outfile.write('#cutoff      percentace    se    1-sp    hits     selected \n')
	print '#pose_cut score_cut  \%    se    1-sp    hits     selected '
	for value in roc:
		line = '%s    %.2f  %.2f  %.2f  %4s  %4s'%(value.name, percent , se , 1-sp, value.tp, value.selected)
		line_friendly = '%s   %4s  %.2f  %.2f  %.2f  %4s  %4s'%(cutoff, ((value.name).split('/')[-1]).split('_')[2], percent , se , 1-sp, value.tp, value.selected)
		outfile.write('%s\n'%line)
		print line_friendly
	outfile.close()
def read_input_roc(method,cutoff,condition,project):
        possible_methods=['autodockvina','autodock4','consensus']
        if not method in possible_methods:
           print "first argument should be one of these:"
           print "    autodockvina autodock4 consensus"
           sys.exit()
        
        if method == 'consensus' :
           if not len(condition) == 10:
                print "second argument for consensus should have 10 digits"
                sys.exit()
           else:
                path_to_files = '../../%s/%s/MODELS'%(method,project)
        
        if method == 'autodockvina' or method == 'autodock4':
           if not len(condition) == 5:
                print "second argument for autodockvina autodock4 should have 5 digits"
                sys.exit()
           else:
                path_to_files = '../../%s/analysis/%s/MODELS'%(method,project)
	return path_to_files
# Reading the input files

method = sys.argv[1]    # choose: autovina autodock consensus
cutoff = sys.argv[2]
condition = sys.argv[3] # constants used : 11111 11110 111111111

possible_methods=['autodockvina','autodock4','consensus']
project = os.getcwd().split('/')[-1]
listofactives = open('real_actives.list','r').readlines()
reactives = [ int(line.split('\n')[0].split('_')[1]) for line in listofactives ]


if not method in possible_methods:
   print "first argument should be one of these:"
   print "    autodockvina autodock4 consensus"
   sys.exit()

if method == 'consensus' :
   if not len(condition) == 10:
        print "second argument for consensus should have 10 digits"
        sys.exit()
   else:
        path_to_files = '../../%s/%s/MODELS'%(method,project)

if method == 'autodockvina' or method == 'autodock4':
   if not len(condition) == 5:
        print "second argument for autodockvina autodock4 should have 5 digits"
        sys.exit()
   else:
        path_to_files = '../../%s/analysis/%s/MODELS'%(method,project)


# Storing the data
listoffiles = glob.glob("%s/results_%s_*_%s.txt"%(path_to_files,cutoff,condition))
roc = []
max_count = 0
max_count_tot = 0
for file in listoffiles:
   infile = open('%s/%s'%(path_to_files,file.split('/')[-1]),'r').readlines()
   ligs_in_file = [ line.split()[0].split('_')[1] for line in infile if not "receptor" in line.split()[0] ] # get the ligand number from the list
   count = 0
   if not len(ligs_in_file) == 0:  # ligands are found, then count how many are actives
     for i in ligs_in_file:
        for real in reactives:
            if int(i) == real:
               count += 1
   data = roc_data(file,len(ligs_in_file),count)  # Generate the roc data
   roc.append(data)
   if count > max_count:                          # max_count represents the maximun number of actives ligands found in the list
       max_count = count
   if len(ligs_in_file) > max_count_tot:          # max_count_tot represents the maximun number of ligands found in the list 
       max_count_tot = len(ligs_in_file)          
                                                  # max_count_tot and max_count helps to contabilize in hard numbers how wrong it the prediction can get   
# writing the output
if not os.path.exists('resume_results_%s'%cutoff):
	    os.makedirs('resume_results_%s'%cutoff)
outfile = open('resume_results_%s/%s_%s_%s.txt'%(cutoff,method,cutoff,condition),'w')
outfile.write('#cutoff      percentace    se    1-sp    hits     selected \n')
print '#pose_cut score_cut  \%    se    1-sp    hits     selected '
for value in roc:
    value.real_actives = max_count
    value.real_decoys = max_count_tot - max_count
    value.fp = value.selected - value.tp
    value.fn = value.real_actives - value.tp
    value.tn = value.real_decoys - value.fp
    if value.selected > 0:
       percent = value.tp/value.selected
    else:
       percent = 0
    if value.real_actives == 0:
        se = 0
        sp = 0
    else:
        se = value.tp/(value.tp+value.fn)
        sp = value.tn/(value.tn+value.fp)
    line = '%s    %.2f  %.2f  %.2f  %4s  %4s'%(value.name, percent , se , 1-sp, value.tp, value.selected)
    line_friendly = '%s   %4s  %.2f  %.2f  %.2f  %4s  %4s'%(cutoff, ((value.name).split('/')[-1]).split('_')[2], percent , se , 1-sp, value.tp, value.selected)
    outfile.write('%s\n'%line)
    print line_friendly
