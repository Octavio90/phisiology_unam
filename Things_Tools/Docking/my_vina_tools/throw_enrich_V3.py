#!/usr/bin/env python2.7
import sys
import os
import glob

class roc_data():
   def __init__(self,name,selected,tp,fp='',tn='',fn='',real_decoys='',real_actives='',percent='',se='',sp='',ef_local='',ef_total=''):
       self.name = name
       self.selected = float(selected)
       self.tp = float(tp)
       self.fp = fp
       self.fn = fn                              # negative to later add the number of maximun number of true positives in the condition 
       self.tn = tn
       self.real_decoys = real_decoys
       self.real_actives = real_actives
       self.percent = percent
       self.se = se
       self.sp = sp
       self.ef_local = ef_local
       self.ef_total = ef_total

def compute_stads(roc,max_count,max_count_tot,ef_ratio):
	for value in roc:
            value.real_actives = float(max_count)
            value.real_decoys = float(max_count_tot - max_count)
            value.fp = float(value.selected - value.tp)
            value.fn = float(value.real_actives - value.tp)
            value.tn = float(value.real_decoys - value.fp)
            if value.selected > 0:
               value.percent = float(value.tp/value.selected)
            else:
               value.percent = 0
            if value.real_actives == 0:
                value.se = 0
                value.sp = 0
            else:
                value.se = value.tp/(value.tp+value.fn)
                value.sp = value.tn/(value.tn+value.fp)
	    value.ef_local = float((value.tp/max_count_tot)/(value.real_actives/value.real_decoys))
	    value.ef_total = float((value.tp/max_count_tot)/(ef_ratio))
        return roc

def check_if_fine(listoffiles):
	try:
	   file = listoffiles[0]	
           file_to_read = file.split('/')[-1]
           infile = open('%s/%s'%(path_to_files,file_to_read),'r').readlines()
	   if len(infile) < 2:
		print 'This file is empty %s/%s'%(path_to_files,file_to_read)
		print 'Better to check what happened'
		sys.exit()
	except IOError:
		print 'This file is empty %s/%s'%(path_to_files,file_to_read)
		sys.exit()
	except IndexError:
		print 'There is no files with cutoff %s and condition %s  in this folder %s'%(path_to_files,cutoff,condition)
		sys.exit()

def sort_dock_data(path_to_files,cutoff,condition,reactives):
	listoffiles = glob.glob("%s/results_%s_*_%s.txt"%(path_to_files,cutoff,condition))
	check_if_fine(listoffiles)
	roc = []
	max_count = 0
	max_count_tot = 0
	for file in listoffiles:
		file_to_read = file.split('/')[-1]
		infile = open('%s/%s'%(path_to_files,file_to_read),'r').readlines()
		ligs_in_file = [ line.split()[0].split('_')[1] for line in infile if not "receptor" in line.split()[0] ] # get the ligand number from the list
		count = 0
		if len(ligs_in_file) == 0:
			print 'This file %s contained no data. It was deleted'%file
			os.remove(file)
			continue
		for i in ligs_in_file:
		    for real in reactives:
		        if int(i) == real:
				count += 1
		data = roc_data(file,len(ligs_in_file),count)  # Generate the roc data
		roc.append(data)
		if count > max_count:                          # max_count represents the maximun number of actives ligands found in the list
		   max_count = count
		if len(ligs_in_file) > max_count_tot:          # max_count_tot represents the maximun number of ligands found in the list 
		   max_count_tot = len(ligs_in_file)          
	return roc,max_count,max_count_tot                     # max_count_tot and max_count helps to contabilize in hard numbers how wrong it the prediction can get  

def write_stats(roc,cutoff):
	if not os.path.exists('resume_results_%s'%cutoff):                                   	
	    os.makedirs('resume_results_%s'%cutoff)
	outfile = open('resume_results_%s/%s_%s_%s.txt'%(cutoff,method,cutoff,condition),'w')
	outfile.write('#cutoff      percentace    se    1-sp    hits     selected \n')
	print '#pose_cut score_cut  \%    se    1-sp    hits     selected ef_local ef_total'
	for value in roc:
		line = '%s    %.2f  %.2f  %.2f  %4s  %4s  %.2f  %.2f'%(value.name, float(value.percent) , float(value.se) , 1-value.sp, str(value.tp), str(value.selected) ,value.ef_local , value.ef_total)
		line_friendly = '%s   %4s  %.2f  %.2f  %.2f  %4s  %4s  %.2f  %.2f'%(cutoff, ((value.name).split('/')[-1]).split('_')[2], value.percent, value.se, 1-value.sp, str(value.tp), str(value.selected) ,value.ef_local , value.ef_total)
		outfile.write('%s\n'%line)
		print line_friendly
	outfile.close()

def read_input_roc(method,cutoff,condition,project):
        possible_methods=['autodockvina','autodock4','consensus']
        if not method in possible_methods:
           print "First argument should be one of these:"
           print "    autodockvina autodock4 consensus"
           sys.exit()
        
        if method == 'consensus' :
           if not len(condition) == 10:
                print "Third argument for consensus should have 10 digits"
                sys.exit()
           else:
                path_to_files = '../../%s/%s/MODELS'%(method,project)
        
        if method == 'autodockvina' or method == 'autodock4':
           if not len(condition) == 5:
                print "Third argument for autodockvina autodock4 should have 5 digits"
                sys.exit()
           else:
                path_to_files = '../../%s/analysis/%s/MODELS'%(method,project)
	return path_to_files

def get_ef_ratio():
	listofactives = open('real_actives.list','r').readlines()
        reactives = [ int(line.split('\n')[0].split('_')[1]) for line in listofactives ]
        listofdecoys = open('real_decoys.list','r').readlines()
        redecoys = [ int(line.split('\n')[0].split('_')[1]) for line in listofdecoys ]
        ef_ratio = float(len(reactives))/float(len(redecoys))
	return ef_ratio,reactives

# Reading the input
method = sys.argv[1]                  # choose: autovina autodock consensus
cutoff = sys.argv[2]                  #
condition = sys.argv[3]               # constants used : 11111 11110 111111111
project = os.getcwd().split('/')[-1]

# Find which files to read

path_to_files = read_input_roc(method,cutoff,condition,project)

# Reading additional data

ef_ratio,reactives = get_ef_ratio()

# Storing the data

roc,max_count,max_count_tot = sort_dock_data(path_to_files,cutoff,condition,reactives)

# Sorting the data

roc = compute_stads(roc,max_count,max_count_tot,ef_ratio)

# Writing the output

write_stats(roc,cutoff)
