#!/usr/bin/env python2.7
import sys
import os
import glob
import numpy as np

class roc_data():
   def __init__(self,name,selected,tp,cutoff,fp='',tn='',fn='',real_decoys='',real_actives='',percent='',se='',sp='',ef_local='',ef_total=''):
       self.name = name
       self.selected = float(selected)
       self.tp = float(tp)
       self.cutoff = float(cutoff)
       self.fp = fp
       self.fn = fn                              # negative to later add the number of maximun number of true positives in the condition 
       self.tn = tn
       self.real_decoys = real_decoys
       self.real_actives = real_actives
       self.percent = percent
       self.se = se
       self.sp = sp
       self.ef_local = ef_local
       self.ef_total = ef_total

def compute_stads(roc,max_count,max_count_tot,ef_ratio):
	for value in roc:
            value.real_actives = float(max_count)

            value.real_decoys = float(max_count_tot - max_count)

		#false positive = predicted by not true positive: check
            value.fp = float(value.selected - value.tp)
	
		#TP+FN = active: check
            value.fn = float(value.real_actives - value.tp)

		#TN+FP = inactive: check
            value.tn = float(value.real_decoys - value.fp)

            value.percent = divide(value.tp,value.selected)
	    value.ef_total = divide(value.percent,ef_ratio)
            value.se = divide(value.tp,(value.tp+value.fn))
            value.sp = divide(value.tn,(value.tn+value.fp))
	    value.ef_local = divide(value.percent,divide(value.real_actives,float(max_count_tot)))
        recovered = [ divide(value.tp,max_count) for value in roc ]
        cutoff = [ value.cutoff for value in roc ]
        sps = [ 1-value.sp for value in roc ]
        ses = [ value.se for value in roc ]
        auc_recover = get_auc(cutoff,recovered)
        auc_roc = get_auc(sps,ses)
        print auc_recover,auc_roc
        return roc,auc_roc,auc_recover

def divide(x, y):
     try:
        result = float(x)/float(y)
     except ZeroDivisionError:
        result = 0
     return result
def check_if_fine(listoffiles):
	try:
	   file = listoffiles[0]	
           file_to_read = file.split('/')[-1]
           infile = open('%s/%s'%(path_to_files,file_to_read),'r').readlines()
	   if len(infile) < 2:
		print 'This file is empty %s/%s'%(path_to_files,file_to_read)
		print 'Better to check what happened'
		sys.exit()
	except IOError:
		print 'This file is empty %s/%s'%(path_to_files,file_to_read)
		sys.exit()
	except IndexError:
		print 'There is no files with cutoff %s and condition %s  in this folder %s'%(path_to_files,cutoff,condition)
		sys.exit()

def sort_dock_data(infile):
#        cutoff_list = ['0.0', '0.01', '0.02', '0.05', '0.1','0.15','0.2','0.15' '0.3', '0.5', '0.6', '0.7', '0.8', '0.9', '1.0']
        cutoff_list = np.array(range(101))*0.01
	roc = []
	max_count = 0
	max_count_tot = 0
	ligs_in_file = [ line.split()[0].split('_')[1] for line in infile if not "receptor" in line.split()[0] ] # get the ligand number from the list
	if len(ligs_in_file) == 0:
		print 'This file %s contained no data. It was deleted'%file
		os.remove(file)
		sys.exit()
        for cutoff in cutoff_list:
            cutoff_pos = int(float(cutoff)*len(ligs_in_file))
            count = 0
	    for i in ligs_in_file[:cutoff_pos]:
	        if int(i) < 3962:
			count += 1
	    data = roc_data(file,cutoff_pos,count,cutoff)  # Generate the roc data
	    roc.append(data)
	    if count > max_count:                          # max_count represents the maximun number of actives ligands found in the list
		   max_count = count
	    if len(ligs_in_file) > max_count_tot:          # max_count_tot represents the maximun number of ligands found in the list 
		   max_count_tot = len(ligs_in_file)          
	return roc,max_count,max_count_tot                     # max_count_tot and max_count helps to contabilize in hard numbers how wrong it the prediction can get  

def write_stats(roc,cutoff,max_count,auc_roc,auc_recover):
	if not os.path.exists('new_resume_results_%s'%cutoff):                                   	
	    os.makedirs('new_resume_results_%s'%cutoff)
	outfile = open('new_resume_results_%s/%s_%s_%s.txt'%(cutoff,method,cutoff,condition),'w')
	outfile.write('#cutoff      percentace    se    1-sp    hits     selected  ef_local ef_total AUC_roc %.2f AUC_recovered %.2f\n'%(auc_roc,auc_recover))
	print '#pose_cut score_cut  percent    1-sp    hits     selected ef_local ef_total AUC_roc %.2f AUC_recovered %.2f'%(auc_roc,auc_recover)
	for value in roc:
		line = '%s    %.2f  %.2f  %.2f  %4s  %4s  %.2f  %.2f'%(value.cutoff, float(value.percent) , float(value.se) , 1-value.sp, str(divide(value.tp,max_count)), str(value.selected) ,value.ef_local , value.ef_total)
		line_friendly = '%s   %4s  %.2f  %.2f  %.2f  %4s  %4s  %.2f  %.2f'%(cutoff, value.cutoff, value.percent, value.se, 1-value.sp, str(divide(value.tp,max_count)), str(value.selected) ,value.ef_local , value.ef_total)
		outfile.write('%s\n'%line)
		print line_friendly
	outfile.close()

def read_input_roc(method,condition,project):
        possible_methods=['autodockvina','autodock4','consensus']
        if not method in possible_methods:
           print "First argument should be one of these:"
           print "    autodockvina autodock4 consensus"
           sys.exit()
        
        if method == 'consensus' :
           if not len(condition) == 10:
                print "Third argument for consensus should have 10 digits"
                sys.exit()
           else:
                path_to_files = '../../%s/%s/MODELS'%(method,project)
        
        if method == 'autodockvina' or method == 'autodock4':
           if not len(condition) == 5:
                print "Third argument for autodockvina autodock4 should have 5 digits"
                sys.exit()
           else:
                path_to_files = '../../%s/analysis/%s/MODELS'%(method,project)
	return path_to_files

def get_ef_ratio():
	listofactives = open('real_actives.list','r').readlines()
        reactives = [ int(line.split('\n')[0].split('_')[1]) for line in listofactives ]
        listofdecoys = open('real_decoys.list','r').readlines()
        redecoys = [ int(line.split('\n')[0].split('_')[1]) for line in listofdecoys ]
        ef_ratio = float(len(reactives))/(float(len(redecoys))+ float(len(reactives)))
	return ef_ratio,reactives

def get_auc(cutoff,recovered):
    auc = 0
    for i in range(1,len(cutoff)):
        window = cutoff[i]-cutoff[i-1]
        value = recovered[i]+recovered[i-1]
        auc += window*value 
    auc = auc*0.5
    return auc


# Reading the input
method = sys.argv[1]                  # choose: autovina autodock consensus
cutoff = sys.argv[2]                          #
condition = sys.argv[3]               # constants used : 11111 11110 111111111
project = os.getcwd().split('/')[-1]

# Find which files to read

path_to_files = read_input_roc(method,condition,project)
infile = open('%s/results_%s_00_%s.txt'%(path_to_files,cutoff,condition),'r').readlines()
# Reading additional data

ef_ratio,reactives = get_ef_ratio()

# Storing the data

roc,max_count,max_count_tot = sort_dock_data(infile)

# Sorting the data

roc,auc_roc,auc_recover = compute_stads(roc,max_count,max_count_tot,ef_ratio)

# Writing the output

write_stats(roc,cutoff,max_count,auc_roc,auc_recover)
