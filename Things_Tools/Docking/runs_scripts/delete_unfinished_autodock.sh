#!/bin/bash
# Get the files in the folder.
#files=`find . -name "*".dlg | awk -F'/' '{print $2}'`
# give the folder where the dlg files are located 
folder=$1  
files=`find "$folder" -name "*".dlg | awk -F'/' '{print $2}'`
possitives=0
negatives=0
# Check if the variable is not empty
if [ ! -z "$files" ]
 then
   for i in `echo $files`;
      do 
        finish_ok=`grep "autodock4: Successful Completion" "$folder"/$i`
        finish_ok2=`grep "distinct conformational clusters" "$folder"/$i`
# Check if the variable finish_ok is not empty
        if [ ! -z "$finish_ok" ] && [ ! -z "$finish_ok2" ]
           then
            (( possitives= possitives+1 ))
           else
            (( negatives= negatives+1 )) # if empty; store in file to delte
           echo "$i" >> temp             # the dlg file later. 
        fi
      done
 else
   echo "There is no \".dlg\" files in this folder"
fi
# Deleting the files that did not finished correctly
if [ -f temp ]
  then
    for i in `cat temp`
      do 
        rm "$folder"/"$i"
      done   
    rm temp
fi
# Making status report file
total=$(( $negatives + $possitives ))
expected=`awk 'END{print NR}' command.list`
difference=$(( $expected-$possitives ))
date >> status_report.file 
echo "$negatives  : Negatives" >> status_report.file
echo "$possitives : Possitives" >> status_report.file
echo "$total  : Total" >> status_report.file
echo "$difference : Difference" >> status_report.file
echo "In the folder $folder remain $difference files to do."
