#!/bin/bash
# Get the files in the folder.
#files=`find . -name "*".pdbqt | awk -F'/' '{print $2}'`
# give the folder where the pdbqt files are located 
folder=receptor_$1
ligand_list=list_$1
files=`find "$folder" -name "*".pdbqt | awk -F'/' '{print $2}'`
possitives=0
negatives=0
# Check if the variable is not empty
if [ ! -z "$files" ]
 then
   for i in `echo $files`;
      do 
        finish_ok=`tail -1 "$folder"/$i`
#        echo $finish_ok
# Check if the variable finish_ok is not empty
        if [ "$finish_ok" == "ENDMDL" ]
           then
            (( possitives= possitives+1 ))
           else
            (( negatives= negatives+1 )) # if empty; store in file to delte
           echo "$i" >> temp             # the pdbqt file later. 
        fi
      done
 else
   echo "There is no \".pdbqt\" files in this folder"
fi
# Deleting the files that did not finished correctly
if [ -f temp ]
 then 
 for i in `cat temp`
   do 
     rm "$folder"/"$i"
   done   
 rm temp
fi
# Making status report file
total=$(( $negatives + $possitives ))
expected=`awk 'END{print NR}' ../../../common_values/$ligand_list`
difference=$(( $expected-$possitives ))
date >> status_report.file 
echo "$negatives  : Negatives" >> status_report.file
echo "$possitives : Possitives" >> status_report.file
echo "$total  : Total" >> status_report.file
echo "$difference : Difference" >> status_report.file
echo "In the folder $folder remain $difference files to do."
