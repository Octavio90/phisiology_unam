#!/bin/bash
# This script generates the file the "command_*.list" file used for the job_submition.autodock script
# The input of the present script is a list the names of the receptor to treat as first input,
# The list of ligand names as second.
# The condition (folder) where to store the file.
rece=$1
liga=$2
condi=$3
nstruct=$4
# Check number of structures to produce
if [ -z "$nstruct" ]
  then
     $nstruct=10
  else
    if ! [[ "$nstruct" =~ ^[0-9]+$ ]]
      then
        exec >&2
        echo "error: The fourth argument is not a number. This is the nstruct argument. "
        exit 1
    fi
fi

for j in `cat ../common_values/"$rece"`;
  do 
    if [ -d results/"$condi"/"$j" ]
      then 
        for i in `cat ../common_values/"$liga"`; 
           do
               name="$i"_"$j"
               echo "cd ../../../ ; ./set_up_autodock4V4.sh $i $j $condi $nstruct ; cd results/$condi/$j/ ; autodock4 -p ../../../dpf/$condi/$name.dpf -l $name.dlg ; rm ../../../dpf/$condi/$name.dpf" ; done > results/"$condi"/"$j"/command_"$condi".list 
    fi
  done
