#!/bin/bash 
recname=$1
condition=$2
updategrid=$3

if [ $recname == "help" ] ||  [ $recname == "h" ] ||  [ $recname == "-help" ] ||  [ $recname == "-h" ]  
   then
      echo "This script generates the receptor grids."
      echo "In order to run:" 
      echo "     1) a receptor pdbqt file should exist in the receptor's folder "
      echo "     2) a grid.size file should be the common_files folder "
      echo "        example of the file:   72 72 72   "
      echo "     3) a grid.center file should be the common_files folder "
      echo "        example of the file:   receptor_1   41.036 34.303 46.337  "
      echo "                               receptor_2    2.741 64.265 68.060  "
      echo "Your options are: " 
      echo "First: Receptor name "
      echo "Second: Docking condition (basically name of the folder. If really a change is needed modify the docking configuraion files, i.e. dpf or gpf) "  
      echo "Third: (OPTIONAL) used when recaulculate the grids are needed. If a new ligand is added to the ligand pdbqt folder updating the grid may be necessary" 
      echo "        To enter this argument you should give it as: update "
      echo "Example: set_up_autodock4_step_1.sh RECname CONDITION update "
      exit
fi

if [ -z $updategrid ] 
   then 
      updategrid=no
fi

if [ -f ../receptors/"$recname".pdbqt ]
  then
      if [ -f ../common_values/grid.center ] && [ -f ../common_values/grid.size ]
       then
          grid_size=`cat ../common_values/grid.size`
          grid_center=`awk -v ctr="$recname"  '{if ($1==ctr) print $2" "$3" "$4 }' ../common_values/grid.center`
       else
          echo "Theres no grid.center file or grid.size"
          exit
      fi
      if [ ! -f gpf/"$recname".gpf ] || [ $updategrid == "update" ]
         then
            cd gpf
            pythonsh ~/bin/Utilities24/prepare_gpf4.py -d ../../ligands -r ../../receptors/"$recname".pdbqt -p npts=10000001 
            sed "s/auto/$grid_center/1" "$recname".gpf > temp
            sed "s/10000001/$grid_size/1"  temp > temp2
            sed "s/receptor $recname.pdbqt/receptor ..\/..\/..\/receptors\/$recname.pdbqt/1" temp2 > "$recname".gpf
            rm temp*
            echo "grid center and grid size changed for $recname " 
            grep gridcenter "$recname".gpf
            grep npts "$recname".gpf
            cd ..
            if [ ! -d grids/"$recname" ]
               then
                 mkdir grids/"$recname"
               else
                  echo "################### "
                  echo "There is already folder containing grids for receptor $recname "
                  echo "################### "
            fi
            up=`date`
            echo "$up : cd $recname ; autogrid4 -p ../../gpf/$recname.gpf ; cd .." >> grids/command.list 
            cd grids/"$recname"
            autogrid4 -p ../../gpf/$recname.gpf 
            cd ../../ 
            echo "The grid for $recname was updated ussing the commands in grid/command.list"
            echo "Try this commmand to see it : awk -F\":\" '{print \$1}' grids/command.list"
         else
            echo "################### "
            echo "There is already grid files for receptor $recname "
            echo "Check folder grid/$recname and/or gpf/$recname.gpf"
            echo "For rewrite the grid files use \"update\" as third argument"
            echo "################### "
      fi  
      if [ ! -d results/"$condition"/"$recname" ]
         then     
            mkdir -p results/"$condition"/"$recname"
            mkdir -p analysis/"$condition"/CANDIDATES
            mkdir -p analysis/"$condition"/MODELS
            mkdir -p analysis/"$condition"/stats_files
            mkdir -p analysis/"$condition"/stats_pictures
            ln -s ../../../common_values/gen_rigth_formant_command.sh results/"$condition"/gen_rigth_formant_command.sh
            ln -s ../../../common_values/gen_ordered_results_command.sh results/"$condition"/gen_ordered_results_command.sh
            ln -s ~/Things_Tools/Docking/My_vina_tools/single_pdbqt.py results/"$condition"/"$recname"/single_pdbqt.py
            ln -s ~/Things_Tools/Docking/My_vina_tools/make_vina_rec_rank_6.py analysis/"$condition"/make_vina_rec_rank_6.py
            ln -s ~/Things_Tools/Docking/My_vina_tools/make_unique_list_V3.py analysis/"$condition"/make_unique_list_V3.py
            rece=`echo "$recname" | awk -F'_' '{print $2}'`
            sed "s/RECE/$rece/g" ../common_values/job_sumit.autodock > results/"$condition"/"$recname"/job_sumit.autodock
      fi
  else
      echo "################### "
      echo " One of these do not exist" 
      echo "../receptors/"$recname".pdbqt"
      echo "################### "
fi
