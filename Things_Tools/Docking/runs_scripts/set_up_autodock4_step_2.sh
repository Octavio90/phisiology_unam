#!/bin/bash
# feed script with a sequence of numbers, representing the list of ligands to read.
# This list should be found in the common_values folder
# They should look like common_values/list_NUMBER 
# To run the script do:
# set_up_autodock4_step_2.sh CONDITION RECEPTOR NUMBER,NUMBER,NUMBER

condition=$1
receptor=$2
list_of_list_of_ligands=$3

for lig_list in `echo "$list_of_list_of_ligands" | awk -F',' '{print}'`;
    do 
      for lig in `cat ../common_values/list_"$lig_list"`;
          do
            echo "cd ../../../ ; ./set_up_autodock4_step_3.sh "$lig" $receptor $condition ; cd results/"$condition"/"$receptor"/; autodock4 -p ../../../dpf/"$receptor"/"$lig"_"$receptor".dpf -l "$lig"_"$receptor".dlg ; rm ../../../dpf/"$receptor"/"$lig"_"$receptor".dpf" 
          done
    done > results/"$condition"/"$receptor"/command.list 
         
