#!/bin/bash 
ligname=$1
recname=$2
condition=$3

if [ $ligname == "help" ] ||  [ $ligname == "h" ] ||  [ $ligname == "-help" ] ||  [ $ligname == "-h" ]  
   then
      echo "Your options are: " 
      echo "First: ligand name "
      echo "Second: Receptor name "
      echo "Third: Docking condition (basically name of the folder. If really a change is needed modify the docking configuraion files, i.e. dpf or gpf) "  
      echo "Example: set_up_autodock4_step_3.sh LIGname RECname CONDITION"
      exit
fi

if [ -f ../ligands/"$ligname".pdbqt ] && [ -f ../receptors/"$recname".pdbqt ]
  then
      if [ ! -d dpf/$condition ]
       then
          mkdir dpf/$condition
          echo "Docking New docking condtion generated"
          echo "Please specify the the changes in the README file"
       else
          echo "Docking condition already existed "
          echo "Checking if there are new receptors or ligands to run"
      fi
      
      if [ ! -f dpf/"$condition"/"$ligname"_"$recname".dpf ] 
         then
           cd dpf/"$condition"
           if [ ! -f prepare_dpf42.py ] ; then ln -s ~/bin/Utilities24/prepare_dpf42.py . ; fi 
           ./prepare_dpf42.py -l ../../../ligands/"$ligname".pdbqt -r ../../../receptors/"$recname".pdbqt -o "$ligname"_"$recname".dpf
           sed "s/fld $recname/fld ..\/..\/..\/grids\/$recname\/$recname/g" "$ligname"_"$recname".dpf > temp
           sed "s/map $recname/map ..\/..\/..\/grids\/$recname\/$recname/g" temp > temp1
           sed "s/move $ligname/move ..\/..\/..\/..\/ligands\/$ligname/g" temp1 > "$ligname"_"$recname".dpf
           rm temp*
           echo "dpf was modified "
           grep fld "$ligname"_"$recname".dpf
           cd ../../
         else
           echo "################### "
           echo "There is already file dpf file using ligand $ligname, receptor $recname and condition $condition"
           echo "################### "
          exit
      fi
  else
      echo "################### "
      echo " One of these do not exist" 
      echo "../ligands/$ligname.pdbqt "
      echo "../receptors/"$recname".pdbqt"
      echo "################### "
fi
