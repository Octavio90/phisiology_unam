#!/bin/bash 
lignames=$1
recname=$2
condition=$3

if [ $lignames == "help" ] || [ $lignames == "h" ] || [ $lignames == "-help" ] || [ $lignames == "-h" ]  
   then
      echo "Your options are: " 
      echo "First: File containing the names of the ligand to dock unsing VINA it should be in common_values, since the same  "
      echo "       file is used by the docking using autodock4"
      echo "Second: Receptor name "
      echo "Third: Docking condition (basically name of the folder. If really a change is needed, then modify the docking configuraion file)"  
      echo "Example: set_up_autodockvina.sh LIGname RECname CONDITION "
      exit
fi

if [ -f ../common_values/"$lignames" ] && [ -f ../receptors/"$recname".pdbqt ] && [ -f ../common_values/config_vina.file ]
  then
     for i in `cat ../common_values/"$lignames"`
        do 
           if [ ! -f ../ligands/"$i".pdbqt ]
             then
                echo "ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  "
                echo "ATENTION: File $i does not exist in ../ligands/"
                echo "Delete it or created that pdbqt file"
                echo "ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  ATENTION  "
                exit
           fi
        done 
     if [ ! -d results/"$condition" ]
         then
            mkdir results/"$condition"
            cd results/"$condition"
            ln -s /home/mcastro/bin/run_parallel.py .
            ln -s ../../../common_values/config_vina.file .
            ln -s ../../../common_values/condence_results_vina.py 
            ln -s ../../../common_values/fix_pdb_from_pdbqt.py 
            ln -s ../../../common_values/resume_vina_results.sh
#          ln -s ../../../common_values/check_success.py .
            cd ../../
      fi
      if [ ! -d analysis/"$condition" ]
          then
             mkdir analysis/"$condition"
      fi
      if [ ! -d results/"$condition"/"$recname" ]
         then
            mkdir results/"$condition"/"$recname"
            cd results/"$condition"/"$recname"
            sed "s/RECEPTOR/$recname/g" ../../../../common_values/job_sumit.autovina > job_sumit.autovina
            sed "s/LIGANDFILE/$lignames/g" job_sumit.autovina > temp
            mv temp job_sumit.autovina
            ln -s /home/mcastro/bin/run_parallel.py .
#            ln -s ../check_success.py .
            cd ../../../
            echo "##############################"
            echo " The folder for the receptor $recname was created"
            echo " Everything should be fine"
            echo "##############################"
         else
            echo " The folder for the receptor $recname already exist"
            echo " Checking if the are any ligand file ..."
            cd results/"$condition"/"$recname"
            check=0
            for i in `cat ../../../../common_values/"$lignames"`
               do 
                 tempname="$i"_"$recname".pdbqt
                  if [ -f "$tempname" ]                  
                     then
                       echo "Results for ligand $tempname already exists"
                       echo "Delete it if need. This will no be docked again if you dont do so."
                       (( check=check+1 ))
                  fi
               done      
            if [ $check -gt 0 ]
              then
                  echo "There are $check ligands with results already"
                  echo "Delete them if need. They will no be docked again if you dont do so."
              else
                  echo "No previous result were found"
                  echo "Everything should be fine"
            fi
      fi
  else
      echo "################### "
      echo " One of these do not exist" 
      echo "../common_values/$lignames "
      echo "../receptors/"$recname".pdbqt"
      echo "../common_values/config_vina.file"
      echo "################### "
fi
