#!/bin/bash

# This creates just the folder tree  

name=$1
machine=$2

if [ $name == "help" ] ||  [ $name == "h" ] || [ $name == "-h" ] || [ $name == "-help" ]
   then
     echo " This script generates the folder tree for docking simulation"
     echo " This script takes 2 arguments"
     echo " First:  name of the project " 
     echo " Second: Machine to use. That is: autodockvina or autodock4 " 
     echo " Example: set_up_docking_folder.sh bench autodockvina"
     exit
fi

if [ "$machine" == 'autodock4' ] || [ "$machine" == 'autodockvina' ]
 then
    if [ ! -d project_"$name" ]
     then
       mkdir project_"$name"
       mkdir project_"$name"/ligands
       mkdir project_"$name"/receptors
       mkdir project_"$name"/common_values
     else
       echo "There is already a folder project_$name "
    fi
    if [ "$machine" == 'autodock4' ] && [ ! -d  project_"$name"/"$machine" ]
     then 
       mkdir project_"$name"/"$machine"
       mkdir project_"$name"/"$machine"/grids
       mkdir project_"$name"/"$machine"/dpf
       mkdir project_"$name"/"$machine"/gpf
       mkdir project_"$name"/"$machine"/results
       mkdir project_"$name"/"$machine"/analysis
       echo "Machine autodock4 folder structure was created"
       exit
    fi
    if  [ "$machine" == 'autodockvina' ] && [ ! -d  project_"$name"/"$machine" ]
     then
       mkdir project_"$name"/"$machine"
       mkdir project_"$name"/"$machine"/results
       mkdir project_"$name"/"$machine"/analysis
       echo "Machine autodockvina folder structure was created"
       exit
    fi
    if  [ -d  project_"$name"/"$machine" ]
     then
       echo "Machine found. use autodockvina or autodock4"
       exit
    fi
    echo "You reached the end of the script. That means that"
    echo "no machine was found. Use \"autodockvina\" or \"autodock4\""
    echo "as parameter" 
 else
    echo "no machine was found. Use \"autodockvina\" or \"autodock4\""
fi
