;
; STANDARD MD INPUT OPTIONS FOR MARTINI 2.P (polarizable water model)
;
; for use with GROMACS 4.x
;

title                    = Martini
cpp                      = /usr/bin/cpp

; TIMESTEP IN MARTINI 
; Most simulations are numerically stable 
; with dt=40 fs, some (especially rings) require 20-30 fs.
; Note that time steps of 40 fs and larger may create local heating or 
; cooling in your system. Although the use of a heat bath will globally 
; remove this effect, it is advised to check consistency of 
; your results for somewhat smaller time steps in the range 20-30 fs.
; Time steps exceeding 40 fs should not be used; time steps smaller
; than 20 fs are also not required.


integrator               = md
tinit                    = 0.0
dt                       = 0.02
nsteps                   = 50000
nstcomm                  = 1
comm-grps		 = 

nstxout                  = 5000
nstvout                  = 5000
nstfout                  = 0
nstlog                   = 1000
nstenergy                = 100
nstxtcout                = 1000
xtc_precision            = 100
xtc-grps                 = 
energygrps               = DPPC PW 

; NEIGHBOURLIST and MARTINI 
; Due to the use of shifted potentials, the noise generated 
; from particles leaving/entering the neighbour list is not so large, 
; even when large time steps are being used. In practice, once every 
; ten steps works fine with a neighborlist cutoff that is equal to the 
; non-bonded cutoff (1.2 nm). However, to improve energy conservation 
; or to avoid local heating/cooling, you may increase the update frequency (e.g. nstlist = 5) 
; and/or enlarge the neighbourlist cut-off (rlist = 1.4 or 1.5 nm). The latter option 
; is computationally less expensive and leads to improved energy conservation

nstlist                  = 10
ns_type                  = grid
pbc                      = xyz
rlist                    = 1.2

; MARTINI and NONBONDED 
; Standard cut-off schemes are used for the non-bonded interactions 
; in the Martini model: LJ interactions are shifted to zero in the 
; range 0.9-1.2 nm, and electrostatic interactions in the range 0.0-1.2 nm. 
; The treatment of the non-bonded cut-offs is considered to be part of 
; the force field parameterization, so we recommend not to touch these 
; values as they will alter the overall balance of the force field.
; In principle you can include long range electrostatics through the use
; of PME, which could be more realistic in certain applications 
;
; With the polarizable water model, the relative electrostatic screening 
; (epsilon_r) should have a value of 2.5, representative of a low-dielectric
; apolar solvent. The polarizable water itself will perform the explicit screening
; in aqueous environment.

coulombtype              = Shift ; PME can also be used with the polariable model
rcoulomb_switch          = 0.0
rcoulomb                 = 1.2
epsilon_r                = 2.5
vdw_type                 = Shift 
rvdw_switch              = 0.9
rvdw                     = 1.2
DispCorr                 = No

; MARTINI and TEMPRATURE/PRESSURE
; normal temperature and pressure coupling schemes can be used. 
; It is recommended to couple individual groups in your system separately.
; Good temperature control can be achieved with the Berendsen thermostat, 
; using a coupling constant of the order of Ï„ = 1 ps. Even better 
; temperature control can be achieved by reducing the temperature coupling 
; constant to 0.1 ps, although with such tight coupling (Ï„ approaching 
; the time step) one can no longer speak of a weak-coupling scheme.
; We therefore recommend a coupling time constant of at least 0.5 ps.
;
; Similarly, pressure can be controlled with the Berendsen barostat, 
; with a coupling constant in the range 1-5 ps and typical compressibility 
; in the order of 10-4 - 10-5 bar-1. Note that, in order to estimate 
; compressibilities from CG simulations, you should use Parrinello-Rahman 
; type coupling.

tcoupl                   = Berendsen
tc-grps                  = DPPC PW
tau_t                    = 1.0  1.0 
ref_t                    = 320 320 
Pcoupl                   = parrinello-rahman
Pcoupltype               = isotropic
tau_p                    = 1.0 1.0 
compressibility          = 3e-4  3e-4
ref_p                    = 1.0  1.0

gen_vel                  = no
gen_temp                 = 320
gen_seed                 = 473529

; MARTINI and CONSTRAINTS 
; for ring systems constraints are defined
; which are best handled using Lincs. 
; Note, during energy minimization the constrainst should be
; replaced by stiff bonds.

constraints              = none 
constraint_algorithm     = Lincs
unconstrained_start      = no
lincs_order              = 4
lincs_warnangle          = 30

; Relative tolerance of shake = 
;shake_tol                = 0.0001
;lincs_warnangle          = 90
; for polarizable water sometimes Lincs warnings appear for no apparent reason
; with a warnangle of 90 this is largely avoided
