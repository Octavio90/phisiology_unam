#!/bin/bash
if [ -f $1 ]
 then
   pro="$1"
   forcefield=FORCEFIELD
   ./martinize.py -f "$pro" -o topol.top -x gro.pdb -dssp ../process/dssp.exe -p backbone -n cg_protein.index -ff "$forcefield"
   ###### fixing the topology pathway #####
   ver=${forcefield:(-1)} # indentifying if the version is polar
   if [[ "$ver" == p ]]
      then
         echo "Polar version is going to be used "
         ver=P
   fi 
   sed '0,/martini/s//\.\.\/process\/martini/' topol.top > temp ; mv temp topol.top
   sed "0,/martini/s//martini\_v2\.$ver/" topol.top > temp ; mv temp topol.top
   echo "" >> topol.top
   ########################################
   if [ -f gro.pdb ]
     then
        editconf -f gro.pdb -o box.gro -d 1.4 -bt cubic
        old=`awk 'END{print NR}' topol.top`
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "This command did not work"
        echo "./martinize.py -f "$pro" -o topol.top -x gro.pdb -dssp ../process/ -p backbone -n cg_protein.index"
        exit
     fi
   if [ -f box.gro ]
     then
       if [[ "$ver" == P ]]
          then
             genbox -cp box.gro -o sol.gro -p topol.top -cs ../process/polarize-water.gro -vdwd 0.21
             num_water=`awk '{if ($2=="W") i++} END{print i}' sol.gro`
             echo "PW      $num_water" >> topol.top
          else
             genbox -cp box.gro -o sol.gro -p topol.top -cs ../process/water -vdwd 0.21
             num_water=`awk '{if (substr($2,1,1)=="W") i++} END{print i}' sol.gro`
             echo "W      $num_water" >> topol.top
       fi
       
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "This command did not work"
        echo "editconf -f gro_cg.gro -o box.gro -d 1.4 -bt cubic"
        exit
     fi
   if [ -f sol.gro ]
     then
        grompp -f 2_Min_steep.mdp -c sol.gro -p topol.top 
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "This command did not work"
        echo "genbox -cp box.gro -o sol.gro -p topol.top -cs spc216.gro"
        exit
     fi
   if [ -f topol.tpr ]
    then
      genion -s topol.tpr -o conf.gro -p topol.top -pname NA -nname CL -conc 0.15 -neutral <<EOF
13
EOF
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "This command did not work"
        echo "grompp -f 2_Min_steep.mdp -c sol.gro -p topol.top"
        exit
     fi
   if [ ! -d itps ] ;
      then
         mkdir itps 
         sed 's/#include \"Protein/#include \"itps\/Protein/g' topol.top > temp ; mv temp topol.top 
         awk '/\[ system \]/{print "#include \"martini_v2.0_ions.itp\""}1' topol.top > temp ; mv temp topol.top
         cp Protein_*.itp itps/
      else
         echo "There was a itps folder"
         echo "Check there to see whats wrong"
         exit
   fi
   check=`awk 'END{print NR}' topol.top `
   (( expected=$check-$old ))
   if [ 4 == $expected ]
     then
        echo " Everything seems fine"
        echo " In resume"
        ./make_setup_resume.sh
        cat setup.resume
        echo " "
        echo " "
        echo " Ready for Minimization"
        echo " grompp -f 2_Min_steep.mdp -c conf.gro -p topol.top "
        echo "  "
        echo "  "
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "genion -s topol.tpr -o conf.gro -p topol.top -pname NA -nname CL -conc 0.15 -neutral"
        echo "type \"15\" when ask "
        echo "Also check topol.top file"
     fi
 else
   echo "Protein file no found $1 "
   echo "This scprits setup the system to perform a simulation in gromacs." 
   echo "This scprits runs internaly the correct_topologies.sh, a script " 
   echo "designed to make the requiered changes in the topologies to deal with " 
   echo "the ligand and proteasome chains fragments restrictions. "  
   echo "It also runs a checking script: ./make_setup_resume.sh"
   echo "It takes two arguments the protein.pdb and the ligand name."  
   echo "Example:"
   echo "./1_Setup.sh PROTEIN.pdb LIG_NAME"
  exit
fi
