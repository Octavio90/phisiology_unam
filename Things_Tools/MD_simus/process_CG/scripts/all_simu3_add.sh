#!/bin/bash

cd 7_Equilibration
./pull_gen_distances_spec.py dist.xvg listof.add >> listof.searchCONF

awk '!($1 in a){a[$1] ; print $0 }' listof.searchCONF > temp
mv temp listof.searchCONF

for i in `awk '{print $1}' listof.searchCONF`
      do
        if [ ! -d conf"$i" ]
         then
           mkdir conf$i 
           b=`awk -v a="$i" '{if($1==a) print $4}' listof.searchCONF`
           cp 7_Equilibration.mdp conf"$i"/7_Equilibration.mdp 
           cd conf$i 
           sed "s/NUMBER/$i/g" ../job_sumit_MD_7_Equilibration.gromacs > job_sumit_MD_7_Equilibration.gromacs
           ln -s ../../conf_pulling_files/conf$i.gro 
           ln -s ../../6_Pulling/itps
           ln -s ../../6_Pulling/index.ndx 
           ln -s ../../6_Pulling/topol.top 
           ln -s ../../6_Pulling/ligand.itp
           sbatch job_sumit_MD_7_Equilibration.gromacs
           cd .. 
        fi
      done
cd ..
cp 7_Equilibration/listof.searchCONF 8_Analysis/
cd 8_Analysis
j=0;
for i in `awk '{print $1}' listof.searchCONF `
    do
      if [ -f topol"$j".tpr ] && [ -f pullx"$j".xvg ] 
        then 
           j=$(( j + 1 ))
        else    
           ln -s ../7_Equilibration/conf"$i"/topol.tpr topol"$j".tpr ; echo topol"$j".tpr >> tpr.dat
           ln -s ../7_Equilibration/conf"$i"/pullx.xvg pullx"$j".xvg ; echo pullx"$j".xvg >> pullx.dat
           j=$(( j + 1 ))
      fi
    done
awk '!($0 in a){a[$0]; print }' tpr.dat > tmp ; mv tmp tpr.dat
awk '!($0 in a){a[$0]; print }' pullx.dat > tmp ; mv tmp pullx.dat
