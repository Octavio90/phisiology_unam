#!/usr/bin/env python2.6
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sys

def main(args):
    try:
        assert(len(sys.argv)>1 and len(sys.argv)<4)
        in1=sys.argv[1]
        if in1 == '-h' or in1 == '-help':
           print " This file plots gromacs output files for analysis "
           print " They usually have the extension .xvg "
           print " This file will plot the first to columns "
           print " The fist one will be on the x-axis "
           print " It also will plot and histogram of the observable in the y-axis"
           print " Example: ./gromacs_plot.py dist.xvg distance.png "
           sys.exit() 
        if len(sys.argv)==3:
           pic_name = sys.argv[2]
        else:
           pic_name = None
        infile=open('%s'%in1,'r').readlines()
        data0 = [ float(line.split()[0]) for line in infile if not '@' in line and not '#' in line ]
        data1 = [ float(line.split()[1]) for line in infile if not '@' in line and not '#' in line ]
        labels = [ line.split('\"')[1] for line in infile if 'label' in line ]
        if not len(labels) == 2 :
           labels = []
           labels.append('x-value')    
           labels.append('y-value')    
        fig = plt.figure()
        ax = fig.add_subplot(111)
        #ax.set_title('Distance between peptide ends')
        ax.plot(data0, data1)
        ax.set_xlabel('%s'%labels[0])
        ax.set_ylabel('%s'%labels[1])
        #the histogram of the data
        #ax = fig.add_subplot(212)
        #n, bins, patches = ax.hist(data1, 100, normed=1, facecolor='green', alpha=0.75)
        #ax.set_ylabel('Frequency')
        #ax.set_xlabel('%s'%labels[1])
        if not pic_name is None:
           plt.savefig('%s'%pic_name,dpi=300,bbox_inches='tight')
           plt.show()
        else:
           plt.show()
    except AssertionError:
        print "This script accepts max 2 arguments "
        print "1: Gromacs \".xvg\" file to plot "
        print "2: Name of the png picture (optional) "
        print "Example: ./gromacs_plot.py dist.xvg distance.png"
        print "For a bit of more description type "
        print " ./gromacs_plot.py -h or -help "
        return 1  # exit on error
    else:
        return 0  # exit errorlessly
if __name__ == '__main__':
    sys.exit(main(sys.argv))
