#!/bin/bash
# set_up_CG.sh pdb/pdbfile forcefield temperature simulation-time integrador
pdbfile=$1 
ff=$2              # Just Coarse Grain Martini is possible
t=$3
simutime=$4

if [ "$pdbfile" == 'h' ] || [ "$pdbfile" == '-h' ]
   then 
     echo "This scripts setup a Coarse Grain simulation "
     echo "It takes 4 arguments"
     echo " 1.- receptor (This file should be properly aligned and stored in pdbs/rec/)"
     echo " 2.- forcefield to use: martini21 ,martini21p ,martini22"
     echo " 3.- temperature 300 or 310 are the usual choice "
     echo " 4.- time in femtoseconds:  Use 500000 (1 ns) "
     echo " Example: "
     echo -e " ./set_up_CG.sh RECEPTOR.pdb FORCEFIELD TEMPERATURE SIMULTIME\n"
     exit
fi

variable="$pdbfile"_"$ff"_"$t"
ver=${ff:(-1)} # indentifying if the version is polar
if [ ! -d "$variable" ]
   then 
       mkdir "$variable"
       echo "The folder $variable was generated"
   else
     echo "There is already a folder with this simulation parameters:"
     echo "receptor        : $pdbfile"
     echo "forcefild       : $ff"
     echo "temperature     : $t"
     echo "simulation time : $simutime"
     exit
fi
cd "$variable"

if [ -d ../process_CG ]
  then
    mkdir process
    sed "s/FORCEFIELD/$ff/g" ../process_CG/scripts/1_Setup.sh > process/1_Setup.sh ;
    chmod +x process/1_Setup.sh ; 
    cp ../process_CG/scripts/make_setup_resume.sh process/
    for i in 2_Min_steep 3_Min_cg 4_NTV 5_NTP 6_Production
       do
         sed "s/TEMPERATURE/$t/g" ../process_CG/mdps/"$i".mdp > process/"$i".mdp
         if  [[ "$ver" == p ]]
            then
               awk '/POLAR/{($3=$NF); print $0" POLAR USED";next}{print $0}' process/"$i".mdp > process/temp ; mv process/temp process/"$i".mdp  
         fi
       done
    sed "s/TIME/$simutime/g" process/6_Production.mdp > process/temp ; mv process/temp process/6_Production.mdp  
    for i in `cat ../process_CG/list_of_files/common_files.list`
       do
         cp ../process_CG/"$i" process/
       done 
  else
    echo "Theres no folder \"process\"    Be careful " >> my.log
    cd ..
    exit
fi

if [ -f ../pdbs/rec/"$pdbfile" ]
  then
    echo -n "DATE   " >> my.log
    date >> my.log
    old=`awk 'END{print NR}' my.log`
    if [ ! -d 1_Setup ]
       then
          mkdir 1_Setup
          cd 1_Setup
          ln -s ../../pdbs/rec/$pdbfile protein.pdb 
          ln -s ../process/1_Setup.sh .
          ln -s ../process/2_Min_steep.mdp .
          ln -s ../process/make_setup_resume.sh .
          ln -s ../process/martinize.py
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ..
       else 
           if [ -f 1_Stepup/conf.gro ]
              then
                  echo "There was already a folder \"1_Setup\" containg a conf.gro file      Be careful " >> my.log
           fi       
    fi
    if [ ! -d 2_Min_steep ] ;  
       then 
          mkdir 2_Min_steep ;
          cd 2_Min_steep
          ln -s ../process/2_Min_steep.mdp
          ln -s ../process/martini_v2.0_ions.itp
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ..
       else 
           if [ -f 2_Min_steep/topol.tpr ]
              then
                  echo "There was already a folder \"2_Min_steep\" containg a topol.tpr file      Be careful " >> my.log
           fi       
    fi
    if [ ! -d 3_Min_cg ] ;
       then
          mkdir 3_Min_cg ; 
          cd 3_Min_cg
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/itps 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../2_Min_steep/confout.gro conf.gro 
          ln -s ../process/3_Min_cg.mdp
          ln -s ../process/martini_v2.0_ions.itp
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 3_Min_cg/topol.tpr ]
              then
                  echo "There was already a folder \"3_Min_cg\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    if [ ! -d 4_NTV ] ;
       then
          mkdir 4_NTV ; 
          cd 4_NTV
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/itps 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/4_NTV.mdp
          ln -s ../3_Min_cg/confout.gro conf.gro 
          ln -s ../process/job_sumit_MD_4_NTV.gromacs
          ln -s ../process/martini_v2.0_ions.itp
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 4_NTV/topol.tpr ]
              then
                  echo "There was already a folder \"4_NTV\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    if [ ! -d 5_NTP ] ;
       then
          mkdir 5_NTP ; 
          cd 5_NTP
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/itps 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/5_NTP.mdp
          ln -s ../4_NTV/confout.gro conf.gro 
          ln -s ../4_NTV/state.cpt state_4.cpt
          ln -s ../process/job_sumit_MD_5_NTP.gromacs
          ln -s ../process/martini_v2.0_ions.itp
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 5_NTP/topol.tpr ]
              then
                  echo "There was already a folder \"5_NTP\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    if [ ! -d 6_Production ] ;
       then
          mkdir 6_Production ; 
          cd 6_Production
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/itps 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/6_Production.mdp
          ln -s ../5_NTP/confout.gro conf.gro 
          ln -s ../5_NTP/state.cpt state_5.cpt
          ln -s ../process/job_sumit_MD_6_Production.gromacs
          ln -s ../process/martini_v2.0_ions.itp
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 6_Production/topol.tpr ]
              then
                  echo "There was already a folder \"6_Production\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    ln -s ../process_CG/scripts/all_simu1.sh .
    ln -s ../process_CG/scripts/all_simu2.sh .
    check=`awk 'END{print NR}' my.log `
    (( expected=$check-$old ))
    if [ 37 != $expected ]
      then
        echo -e " Something seems strange. Check my.log file to find it out\n"
        echo " Something seems strange. Check my.log file to find it out" >> my.log
      else
        echo -e "Everything seems fine here\n"
        echo "Everything seems fine here" >> my.log
    fi
  else
     echo "No file $pdbfile found"
     echo "Give a PDB file as argument"
     echo "Example: "
     echo -e "./set_up_MD.sh myprotein.pdb\n"
     exit
fi

