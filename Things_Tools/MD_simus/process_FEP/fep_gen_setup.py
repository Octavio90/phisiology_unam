#!/usr/bin/env python2.7
from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists
from os import chdir
from os import mkdir
from os import symlink
from os import system
from fep_lib import *

parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="setup bunch of lambda-step directories",
                                 add_help=True)

parser.add_argument("-mdp", help="the folders name with the mdp-files with GROUP_XXX tags for replacement", required=True )
parser.add_argument("-top", help="the topology file for the system", required=True )
parser.add_argument("-gro", help="the gro file with the system coordinates", required=True )
parser.add_argument("-groups", help="File with the names of the groups to monitor", default=False)
parser.add_argument("-itp", help="list of itp files topology files", default=False)
parser.add_argument("-type_lig", help="immuno_lig,immuno_nolig,yeast_lig,yeast_nolig,consti_lig, etc ...", default=False)
args = parser.parse_args()

type=args.type_lig.split('_')[0]
lig=args.type_lig.split('_')[1]


for i in ('0_Setup','1_Charge','2_Atom','shared_files'):
        mkdir( i )

chdir('0_Setup')


folder_name=['2_Min_steep','3_Min_cg','4_NTV','5_NTP','6_Production']

for process in folder_name:
        dir="%s"%(process)
        print "generate folder for step %s..."%process
        mkdir( dir )
        if  dir == "2_Min_steep":
             shutil.copy( "../"+args.gro, dir )
             shutil.copy( "../"+args.top, dir )
             shutil.copy( "../"+args.mdp+"/"+dir+".mdp", dir)
             if args.itp:
                chdir(dir)
                for i in open('../../%s'%args.itp,'r').readlines():
                    system("ln -s ~/fep_toolkit/res_mut/%s/%s/%s %s"%(type,lig,i.split('\n')[0],i.split('\n')[0]))
                chdir("..")

        else:
             system("ln -s ~/fep_toolkit/job_sumit.job %s/job_sumit.job"%dir )
             symlink( "../2_Min_steep/index.ndx", dir+"/index.ndx" )
             symlink( "../2_Min_steep/"+args.top, dir+"/"+args.top )
             if args.itp:
                chdir(dir)
                for i in open('../../%s'%args.itp,'r').readlines():
                    system("ln -s ../2_Min_steep/%s"%(i.split('\n')[0]))
                chdir("..")
            #fix the mdp-file for the specific lambda
             if args.groups: 
                gro=open( "../%s"%args.groups, "r" ).readlines()
                groups="\n".join(gro)
                in_file=open("../"+args.mdp+"/"+dir+".mdp",'r').read().replace("GROUPS_TO_MONITOR",groups)
                fd=open(dir+"/"+dir+".mdp",'w')
                fd.write(in_file)
             if not args.groups:
                in_file=open(args.mdp+"/"+dir+".mdp",'r').read().replace("GROUPS_TO_MONITOR"," ")
                fd=open(dir+"/"+dir+".mdp",'w')
                fd.write(in_file)

symlink( "../2_Min_steep/confout.gro", "3_Min_cg/conf.gro" )
symlink( "../3_Min_cg/confout.gro", "4_NTV/conf.gro" )
symlink( "../4_NTV/confout.gro", "5_NTP/conf.gro" )
symlink( "../4_NTV/state.cpt", "5_NTP/state_4.cpt" )
symlink( "../5_NTP/confout.gro", "6_Production/conf.gro" )
symlink( "../5_NTP/state.cpt", "6_Production/state_5.cpt" )

chdir('../shared_files')
shutil.copy( "../topol.top","topol.top")
symlink( "../0_Setup/2_Min_steep/index.ndx","index.ndx")
symlink( "../0_Setup/6_Production/ener.edr","ener.edr")
system( "ln -s ~/fep_toolkit/TO_MAKE_THIS_FOLDER ")
system( "cp ~/fep_toolkit/job_sumit.job job_sumit.job")
for i in open('../%s'%args.itp,'r').readlines():
    shutil.copy("../0_Setup/2_Min_steep/%s"%i.split('\n')[0],"%s"%i.split('\n')[0])
system("ln -s ~/fep_toolkit/md_fep_all.mdp .")

chdir('..')
chdir("1_Charge")
system("echo $PWD")
system("ln -s ~/fep_toolkit/fep_gen_lambdasP.py .")
system("ln -s ~/fep_toolkit/fep_run_gromppP.py .")
system("ln -s ~/fep_toolkit/LAMBDAS .")
system("ln -s ../%s ."%args.itp)
system("cp ~/fep_toolkit/md_fep_lamdba.mpd .")
chdir('..')
chdir("2_Atom")
system("ln -s ~/fep_toolkit/fep_gen_lambdasP.py .")
system("ln -s ~/fep_toolkit/fep_run_gromppP.py .")
system("ln -s ~/fep_toolkit/LAMBDAS .")
system("ln -s ../%s ."%args.itp)
system("cp ~/fep_toolkit/md_fep_lamdba.mpd .")
