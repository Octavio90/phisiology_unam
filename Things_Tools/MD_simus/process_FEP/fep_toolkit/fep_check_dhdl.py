#!/usr/bin/env python2.7




from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists
from os import chdir
from os import mkdir
from os import symlink
from os import system
from fep_lib import *
from random import choice

parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="check the status of lambda-step simulation",
                                 add_help=True)

parser.add_argument("-lambda_file", help="store rmsd improvement due to ring-currents", default="LAMBDAS" );
#parser.add_argument("-overwrite", help="overwrite existing lambda directories", default=False, action="store_true")

args = parser.parse_args()

lambda_prefix="lambda"
lambdas=read_lambda_vector( args.lambda_file )

for ld in lambdas:
    dir="%s_%s"%(lambda_prefix, ld)
    chdir( dir )
    if exists ("dhdl.xvg"):
            print "run for lambda %s is in the step..."%ld
            system("tail -1 dhdl.xvg")
    else:
            print "FOR LAMDBA %s theres no dhdl.xvg"%ld

    chdir("..")

