#!/usr/bin/python

from glob import glob
import argparse
from os import system
from fep_lib import *


parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="Generates correct bonding sequence acording to the atom name of the topolgy file.",
                                 add_help=True)

parser.add_argument("-itp", help="the itp or top file of the ligand", required=True )

args = parser.parse_args()

input_file = args.itp

# Taking atoms names from the original file
atom_list = []
infile = open(input_file)
for name in infile.readlines():
    if 'opls_' in name: 
       a = name.split()
       atom_list.append(a[4])
infile.close()

# Replacing the atom name for its position in the list
count = 1 # Starting position of the atom name list
for atom_name in atom_list:
    change = open(input_file,'r').read().replace("%s"%atom_name , "%s"%count) 
    fd=open("correct.top",'w')
    fd.write(change)
    fd.close()
    input_file = "correct.top"
    count = int(count) + 1 
    
# Removing "-" for easy copy and paste on original input file
inf = open("correct.top", 'r+').read().replace("-"," ")
info = open("correct.top", "w")
info.write(inf) 
info.close()    
