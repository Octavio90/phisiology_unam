#!/usr/bin/python



from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists
from os import chdir
from os import mkdir
from os import symlink
from fep_lib import *

parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="setup bunch of lambda-step directories",
                                 add_help=True)


parser.add_argument("-lambda_file", help="store rmsd improvement due to ring-currents", default="LAMBDAS" );
parser.add_argument("-lambdas", nargs="*" );
parser.add_argument("-overwrite", help="overwrite existing lambda directories", default=False, action="store_true")
parser.add_argument("-itp", help="the itp file with the ligand- A and B topologies", required=True )
parser.add_argument("-mdp", help="the template mdp-file with FEP_XXX tags for replacement", required=True )

args = parser.parse_args()


lambda_prefix="lambda"
backup_prefix="zzz_backup_"
input_files=['topol.top','index.ndx','conf.gro','ener.edr','start_frames.trr','job_sumit.job','posreslig.itp']
share_input_files='share_input_files';


def link_required_input_files( files, template_dir, dir ):
        for file in files:
                symlink( "../"+template_dir+"/"+file, dir+"/"+file)

if args.lambdas:
        lambdas=args.lambdas
else:
        lambdas=read_lambda_vector( args.lambda_file )

for ld in lambdas:
        dir="%s_%s"%(lambda_prefix, ld)
        if args.overwrite and exists(dir):
                backup( backup_prefix, dir )
        if not exists( dir ):
                print "generate lambda-step %s..."%ld
                mkdir( dir )
                link_required_input_files( input_files, share_input_files , dir )
                symlink( "../"+args.itp, dir+"/"+args.itp )

                #fix the mdp-file for the specific lambda
                md_all=open("share_input_files/md.mdp",'r').read()
                md_fep=open(args.mdp,'r').read().replace("FEP_LAMBDA",ld)
                fd=open(dir+"/md.mdp",'w')
                fd.write(md_all)
                fd.write(md_fep)
        else:
                print "lambda-step %s already exists... do nothing"%ld

