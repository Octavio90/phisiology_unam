#!/usr/bin/python



from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists
from os import chdir
from os import mkdir
from os import symlink
from fep_lib import *

parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="setup bunch of lambda-step directories",
                                 add_help=True)

parser.add_argument("-mdp", help="the folders name with the mdp-files with GROUP_XXX tags for replacement", required=True )
parser.add_argument("-top", help="the topology file for the system", required=True )
parser.add_argument("-gro", help="the gro file with the system coordinates ligand- A and B topologies", required=True )
parser.add_argument("-groups", help="File with the names of the groups to monitor", default=False)
parser.add_argument("-itp", help="topology file of the ligand", default=False)
args = parser.parse_args()

folder_name=['2_Min_steep','3_Min_cg','4_NTV','5_NTP','6_Production']
		
for process in folder_name:
	dir="%s"%(process)
        print "generate folder for step %s..."%process
	mkdir( dir )
	if  process == "2_Min_steep":
	     shutil.copy( args.gro, dir )
	     shutil.copy( args.top, dir )
	     shutil.copy( args.mdp+"/"+dir+".mdp", dir)
	     if args.itp:
                shutil.copy( args.itp, dir )
        else:
             symlink( "~/fep_toolkit/job_sumit.job", dir+"/job_sumit.job" )
	     symlink( "../2_Min_steep/index.ndx", dir+"/index.ndx" )
             symlink( "../2_Min_steep/"+args.top, dir+"/"+args.top )
             if args.itp:
                symlink( "../2_Min_steep/"+args.itp, dir+"/"+args.itp )
            #fix the mdp-file for the specific lambda
             if args.groups: 
                gro=open( args.groups, "r" ).readlines()
		groups="\n".join(gro)
	        in_file=open(args.mdp+"/"+dir+".mdp",'r').read().replace("GROUPS_TO_MONITOR",groups)
	        fd=open(dir+"/"+dir+".mdp",'w')
		fd.write(in_file)
	     if not args.groups:
	        in_file=open(args.mdp+"/"+dir+".mdp",'r').read().replace("GROUPS_TO_MONITOR"," ")
	        fd=open(dir+"/"+dir+".mdp",'w')
		fd.write(in_file)

symlink( "../2_Min_steep/confout.gro", "3_Min_cg/conf.gro" )
symlink( "../3_Min_cg/confout.gro", "4_NTV/conf.gro" )
symlink( "../4_NTV/confout.gro", "5_NTP/conf.gro" )
symlink( "../4_NTV/state.cpt", "5_NTP/state.cpt" )
symlink( "../5_NTP/confout.gro", "6_Production/conf.gro" )
symlink( "../5_NTP/state.cpt", "6_Production/state.cpt" )
