


from glob import glob
import shutil
from os.path import basename
from os.path import exists
from os import chdir
from os import mkdir
from os import symlink


def backup( backup_prefix, dir ):
	if exists( dir ):
		id=0
		new_dir="%s_%05d_%s"%(backup_prefix,id,dir)
		while exists( new_dir ):
			id = id + 1
			new_dir="%s_%05d_%s"%(backup_prefix,id,dir)
		shutil.move( dir, new_dir)

def read_lambda_vector( file ):
	lines = open( file, "r" ).readlines()
	lambdas=[]
	for line in lines:
		#add line without \n
		tags=line.split()
		if len(tags):
			lambdas.append(tags[0])
	return lambdas
