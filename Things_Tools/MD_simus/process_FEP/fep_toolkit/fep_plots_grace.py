#!/usr/bin/env python2.7



from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists
from os import chdir
from os import mkdir
from os import symlink
from fep_lib import *
from math import sqrt

parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="setup bunch of lambda-step directories",
                                 add_help=True)


parser.add_argument("-lambda_file", help="store rmsd improvement due to ring-currents", default="LAMBDAS" );
parser.add_argument("-lambdas", nargs="*" );
parser.add_argument("-overwrite", help="overwrite existing lambda directories", default=False, action="store_true")
#parser.add_argument("-itp", help="the itp file with the ligand- A and B topologies", required=True )
#parser.add_argument("-mdp", help="the template mdp-file with FEP_XXX tags for replacement", required=True )

args = parser.parse_args()

plots_dir="plots"
lambda_prefix="lambda"
backup_prefix="zzz_backup_"
input_files="dhdl.xvg"
shared_input_files='shared_files';
# For trapezodial integration
dhdl = [] # Direct to trapezoidal
std_dhdl = [] # Direct to trapezoidal
all_dhdl = [] # Direct to the file
###	
# Function for Dot producto of two vectors same length
def dot_prod(a,b):
    store = []
    for i in range(len(a)):
        store.append(b[int(i)]*a[int(i)])
    return store

# Function that gets the difference between consecutive vectors elements
def delta_vec(a):
    store = []
    for i in range(len(a)-1):
        j = i+1
        store.append((a[int(j)]-a[int(i)]))
    return store
## End of Definition section


# Reading the lambdas input file
if args.lambdas:
	lambdas=args.lambdas
else:
	lambdas=read_lambda_vector( args.lambda_file )
# Checking if the plot directory exists already and make backup
if exists(plots_dir):
   backup( backup_prefix, plots_dir )
# Creating the plot directory in case it is not there   
if not exists( plots_dir ):     
   mkdir(plots_dir)
   for ld in lambdas:
       spliter = [] # This just store the values in an array
       dir="%s_%s"%(lambda_prefix, ld)
       infile_name="%s/%s" % (dir,input_files) # The dhdl.xvg file 
       if not exists( infile_name ): #Just in case it doesnt exists
	  continue	
       infile = open(infile_name)
       for line in infile.readlines():
          if '@' in '%s' % (line) or '#' in '%s' % (line): # Get rid of the comments
              pass #Do noting
          else:
              for i in line.split(' '): # Take each column and put it as float
                  spliter.append(float(i)) 
# Taking just the dhdl values and getting the average and std from them.  
#       dhdl_average = numpy.average(spliter[1::2])
#       dhdl_std = numpy.std(spliter[1::2])
       dhdl_average = (sum(spliter[1::2]))/len(spliter[1::2])
       difference = []
       for x in spliter[1::2]:
           y= x-dhdl_average
           difference.append(y*y)
       sum_x2 = (sum(difference)/(len(spliter[1::2])))
       dhdl_std = sqrt(sum_x2/len(spliter[1::2] ))
# Storing the result in the file for plotting and integrating.
       all_dhdl.append(( float(ld), dhdl_average, dhdl_std) )
       dhdl.append(dhdl_average)
       std_dhdl.append(dhdl_std)
# Checking quality of dhdl.xvg file
       delta_time = delta_vec(spliter[0::2])
       count = 1
       for time in delta_time:
           if time <= 0.0:
              print "This file is corrupted lambda_%s at time %s" % (ld,count)        
           count = count +1
# Calculating the intergral value
# Adding consecutive function values
   lambdas_num=[]
   for x in lambdas:
       lambdas_num.append(float(x))

# difference of lambda values (see "delta_vec" definition) 
   delta = delta_vec(lambdas_num)      

#   print delta  ASK OLIVER ABOUT THE VALUES
   pair_to_add = zip(dhdl,dhdl[1::1])
   trapez = []
   for pair in pair_to_add:
       trapez.append(sum(pair))
# Adding pair of std error for trapezoidal   
   pair_std_to_add = zip(std_dhdl,std_dhdl[1::1])
   trapez_std = []
   for pair in pair_std_to_add:
       trapez_std.append(sum(pair))

# Each element of trepezoidal formula. (see "dot_prod" definition)
   steps_of_trapez=[]
   for x in dot_prod(trapez,delta):
       steps_of_trapez.append(0.5*float(x)) 
   area = sum(steps_of_trapez) # Adding all the elements

# Each element of trepezoidal formula for Std error. (see "dot_prod" definition)
   steps_of_trapez_std=[]
   for x in dot_prod(trapez_std,delta):
       steps_of_trapez_std.append(0.5*float(x))
   area_std = sum(steps_of_trapez_std) # Adding all the elements
   
   print ("%s" " +/- "  "%s")%(area, area_std)

# Calulating the partial sums of area
   partial=[]
   partial.append(0.0)
   par=0.0
   for x in steps_of_trapez:
       par = par+x
       partial.append(par)
# Calculating the partial sums of area_std   
   partial_std=[]
   partial_std.append(0.0)
   par_std=0.0
   for x in steps_of_trapez_std:
       par_std = par_std+x
       partial_std.append(par_std)

# Create the file for ploting
   final_file = open("%s/to_plot_energy.xvg"%plots_dir, 'w')
   final_file.writelines("@  title \"The integral is %s +/- %s\"\n" % (area,area_std))
   final_file.writelines("@    xaxis  label \"Lambda\"\n")
   final_file.writelines("@    yaxis  label \"(kJ/mol)\"\n")
   final_file.writelines("@Lambda average std partial_sum\n")
   for i in range(0,len(all_dhdl)):
       array_line=list(all_dhdl[int(i)])
       array_line.append(partial[int(i)])
       array_line.append(partial_std[int(i)])
       line=" ".join(map(str, array_line))
       final_file.writelines("%s\n" % line)
