#!/usr/bin/env python2.7



from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists
from os import chdir
from os import mkdir
from os import symlink
from os import system
from fep_lib import *
from random import choice

parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="setup bunch of lambda-step directories",
                                 add_help=True)


parser.add_argument("-lambda_file", help="store rmsd improvement due to ring-currents", default="LAMBDAS" );
parser.add_argument("-lambdas", nargs="*" );
#parser.add_argument("-overwrite", help="overwrite existing lambda directories", default=False, action="store_true")

args = parser.parse_args()


lambda_prefix="lambda"
backup_prefix="zzz_backup_"
input_files=['topol.top','index.ndx','conf.gro','ener.edr','start_frames.trr']
shared_input_files='shared_input_files';
production_frames=open("../shared_files/START_FRAMES",'r').readlines()

if args.lambdas:
    lambdas=args.lambdas
else:
    lambdas=read_lambda_vector( args.lambda_file )

control = open('lambda.frame','w')
for ld in lambdas:
    dir="%s_%s"%(lambda_prefix, ld)
    chdir( dir )
    if not exists ("topol.tpr"):
            start_frame=choice(production_frames)
            print "run grompp on lambda-step %s ..."%ld
            system("grompp -f md.mdp -p topol.top -e ener.edr -maxwarn 3 -t start_frames.trr -n index.ndx -time %s "%start_frame)
            control.write('%s    %s'%(ld,start_frame))
    else:
            print "topol.tpr exists for lambda-step %s already... did not run grompp"%ld

    chdir("..")
