#!/usr/bin/env python

import os 
## The input file should consist in two columms. Columm 1 the name of the topology file to modify and the second the line number where 
#  the first atom of the last gly starts.
# To generate the input file run:
#  for i in `cat list.topol `; do echo $i ;awk '{if ($2=="bonds") print NR-9}' $i; done 
# To modify the posre files do these two commands:
#  for i in `cat list.topol `; do echo $i; echo "[ position_restraints ]" > $i.posre ;awk '{if ($5 == "CA") print $1"   1  1000  1000  1000"  }' $i >> $i.posre; done
#  for i in `cat list.topol | sed 's/topol_Protein_chain_//g' | sed 's/.itp//g' `; do echo $i; mv topol_Protein_chain_$i.itp.posre posre_Protein_chain_$i.itp ; done

files = open('inputforfix.python','r').readlines()
dic = {}
for i in files:
    dic['%s'%i.split()[0]] = int(i.split()[1]) 

for n in dic.keys():
  os.system("awk '{if (NR < 25 || NR > 33)a[NR]=$0}($7=\"0.0\"){if (NR > 24 && NR < 34) a[NR]=\"    \"$0 }{b=NR} END {for (i = 1; i <= b; i++) print a[i]} ' %s > yaa "%n) 
  os.system("awk '{ini=%d}{if (NR < ini || NR > ini+7)a[NR]=$0}($7=\"0.0\"){if (NR > ini-1 && NR < ini+8) a[NR]=\"    \"$0 }{b=NR} END {for (i = 1; i <= b; i++) print a[i]} ' yaa > %s" %(dic[n],n))
  os.system("rm yaa")
