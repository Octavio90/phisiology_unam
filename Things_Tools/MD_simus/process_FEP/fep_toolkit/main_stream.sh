#!/bin/bash
#For an specific lambda value. All the folders have to created prior launch this main stream. 
#Each Folder must contain the suitable jobsumit.job file. Special attention schould be paid on the 
#inputs and outputs. 
lamb=0.05 #This is the unique input need (assuming that the folder exist and have the right files). 
process=( "2_Min_steep" "3_Min_cg" "4_NTV" "5_NTP" "6_Production" )
NO_OF_JOBS=$[${#process[@]}-1]
#define jobname
NAME=FEP_$lamb
i=0
# make a distinct jobname
J_NAME=$NAME"_"${process[$i]}
#submit the start job
echo "msub -N $J_NAME job_sumit.job"
msub -N $J_NAME job_sumit.job
while [ $i -lt $NO_OF_JOBS ]; do
   J_PREV=$J_NAME                                                                                                                        
   let i=i+1                                                                                                                                                     
   J_NAME=$NAME"_"${process[$i]}                                                                                                                               
   cd ../${process[$i]}                                                                                                                                     
# submit the next jobs with dependency defined                                                                                                                  
   echo "msub -N $J_NAME -W x=depend:afterok:$J_PREV job_sumit.job"                                                                                             
   msub -N $J_NAME -W x=depend:afterok:$J_PREV job_sumit.job 
done
