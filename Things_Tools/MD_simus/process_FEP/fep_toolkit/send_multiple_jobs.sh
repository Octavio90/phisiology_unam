#msub -l nodes=30:ppn=16,walltime=24:00:00 send_multiple_jobs.sh
#
#launch.sh:
module load parastation
module load gromacs/4.5.3

mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.0/topol.tpr -rcon 0.9
sleep 10s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.01/topol.tpr -rcon 0.9
sleep 10s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.05/topol.tpr -rcon 0.9
sleep 10s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.1/topol.tpr -rcon 0.9
sleep 10s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.2/topol.tpr -rcon 0.9
sleep 10s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.3/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.4/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.5/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.6/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.7/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.8/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.9/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.95/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_0.99/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/benetr_to_ben/1_change_charge/lambda_1.0/topol.tpr -rcon 0.9
sleep 1s

mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.0/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.01/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.05/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.1/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.2/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.3/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.4/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.5/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.6/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.7/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.8/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.9/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.95/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_0.99/topol.tpr -rcon 0.9
sleep 1s
mpiexec -np 16 --exports GMXLIB $GROMACS_ROOT/bin/mdrun -s ~/fep_runs/trypsin/1_in_solvent/beneth_to_ben/1_change_charge/lambda_1.0/topol.tpr -rcon 0.9
sleep 1s
