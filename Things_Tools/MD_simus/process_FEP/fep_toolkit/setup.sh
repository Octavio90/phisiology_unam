#!/bin/bash 
# This file must be chance to python
# Take the PDB and generate the .gro and .top files.
# Add the ligand topology and coordinates. 
# Setup the systen ready for mdrun command.
proceso="/lustre/jhome12/hmu18/hmu181/MD_gromacs/lisozyme/lisozyme_FEP/phenol_to_phenyl/process_MDP/"
pdbes="/lustre/jhome12/hmu18/hmu181/MD_gromacs/lisozyme/lisozyme_FEP/phenol_to_phenyl/pdbs_base"
enzyme="$pdbes/no_lig_1LI2"
lig_dir="/lustre/jhome12/hmu18/hmu181/MD_gromacs/lisozyme/lisozyme_FEP/phenol_to_phenyl/Normal_MD"
ligand="IPH"
#genration of the coordinates and topology for the enzyme
pdb2gmx -f $enzyme.pdb -ff oplsaa -water tip3p -o gmx_enzyme.gro -p topol_enzyme -i respos_enzyme
# adding the ligand topology and coordinates 
# the idea is to cut the pure coordinates from ligand.gro and paste it at the end of the gmx_enzyme.gro but before the vectors
#
#First the coordinates
grep $ligand $lig_dir/$ligand.gro > templig.txt
sed '$!d' gmx_enzyme.grp >> templig.txt
sed '$d' gmx_enzyme.gro > temppro.txt
cat templig.txt >> temppro.txt
awk '{if ($1==2601) sub(/2601/,"2614");print}' temppro.txt > gmx_enzyme.gro
rm temppro.txt templig.txt
#something is needed to change the number of ATOMS!!!!
#now the topology
awk 'NR==24387,NR==24406' topol_enzyme.top > tempol.pro
sed -e :a -e '$d;N;2,20ba' -e 'P;D' topol_enzyme.top > temppro.sec
echo "; Include ligand topology" >> temppro.sec
echo "#include \"$ligand.itp\"" >> temppro.sec
echo ";" >> tempro.sec
cat tempol.pro >> temppro.sec
mv temppro.sec topol_enzyme.top
rm tempol.pro
echo "$ligand            1" >> topol_enzyme.top
#Continuation with norma procedure
editconf -f .gro -o box_conf.gro -d 2 -bt cubic
genbox -cp box_conf.gro -p topol.top -o sol_conf.gro -cs spc216.gro
grompp -f 2_Min_steep.mdp -c sol_conf.gro -p topol.top
genion -s topol.tpr -p topol.top -g genion.log -o conf.gro -conc 0.15 -pname 'NA' -nname 'CL' -neutral
grompp -f 2_Min_steep.mdp -c conf.gro -p topol.top

