#!/bin/bash
g_energy -f ../2_Min_steep/ener.edr -s ../2_Min_steep/topol.tpr -o potential2.xvg  <<EOF
Potential
g_energy -f ../4_NTV/ener.edr -s ../4_NTV/topol.tpr -o temperature4.xvg  <<EOF
Temperature
EOF
g_energy -f ../5_NTP/ener.edr -s ../5_NTP/topol.tpr -o pressure5.xvg  <<EOF 
Pressure
EOF
g_energy -f ../5_NTP/ener.edr -s ../5_NTP/topol.tpr -o density5.xvg   <<EOF
Density 
EOF
g_dist -f ../6_Production/traj.xtc -s ../6_Production/topol.tpr -n ../6_Production/index.ndx -o dist_amber99.xvg  <<EOF
21
22
EOF
trjconv -f ../6_Production/traj.xtc -s ../6_Production/topol.tpr -n ../6_Production/index.ndx -skip 10 -fit rot+trans -o trajectory_amber99.pdb  <<EOF
1
1
EOF
trjconv -f ../5_NTP/traj.trr -s ../5_NTP/topol.tpr -o system_trajectory.pdb <<EOF
1
EOF
./plot_with_average.py potential2.xvg potential2.png 2>&1 | tail -15 >> my_ploting.log 
./plot_with_average.py temperature4.xvg temperature4.png 2>&1 | tail -15 >> my_ploting.log 
./plot_with_average.py pressure5.xvg pressure5.png 2>&1 | tail -15 >> my_ploting.log 
./plot_with_average.py density5.xvg density5.png 2>&1 | tail -15 >> my_ploting.log 
./gromacs_plot.py dist_amber99.xvg simu_amber99.png 2>&1 | tail -15 >> my_ploting.log 
#echo 1 1 | trjconv -f ../6_Production/traj.xtc -s ../6_Production/traj.xtc -n index.ndx -skip 10 -fit rot+trans -b XX -e YY -o cluster1_amber99.pdb 2>&1 | tail -15 >> my_ploting.log 
#echo 1 1 | trjconv -f ../6_Production/traj.xtc -s ../6_Production/traj.xtc -n index.ndx -skip 10 -fit rot+trans -b XX -e YY -o cluster2_amber99.pdb 2>&1 | tail -15 >> my_ploting.log 
