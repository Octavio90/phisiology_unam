#!/bin/bash

liggro="$1"
ffield="$2"
if [ -f "$liggro".gro ] && [ -f "$liggro".itp ]
  then 
        tail -1 gro.gro > axis
        sed '$d' gro.gro > temp # removes the last line
        mv temp gro.gro
        lignumberatoms=`awk '{if (NR==2) print $1}' "$liggro".gro`
        awk '{if (NR<=2) print $0}' gro.gro > headofgro
        awk '{if (NR==2) $1=$1+'$lignumberatoms'; print $0}' headofgro > newheadofgro
        awk '{if (NR>2) print $0}' gro.gro >> newheadofgro
        mv newheadofgro gro.gro
        awk '{if (NR>2) print $0}' "$liggro".gro >> gro.gro
        cat axis >> gro.gro
        placeline=`awk '/tip3p.itp/{print NR-2}' topol.top`
        awk '{if (NR=='$placeline') $0="#include \"'$liggro'.itp\"" ; print $0}' topol.top > totemp.top
        echo "$liggro           1" >> totemp.top
        mv totemp.top topol.top
        for i in `ls topol_*.itp`; do echo "$i" | sed 's/.itp//g' | awk -F"_" '{ if (NF>1) print substr($NF,1,1)}' ; done > itp.list
        ####
        ## This change the value of the fourth field in a file between lines 25 and 33
        for i in `cat gly_begin.list `;
            do
              echo $i 
              initial=`awk '{if ($2=="nr") print NR+2}' $i`
              awk '{ini='$initial'}{if (NR < ini || NR > ini+8)a[NR]=$0}($7="0.0"){if (NR >= ini && NR <= ini+8) a[NR]="    "$0 }{b=NR} END {for (i = 1; i <= b; i++) print a[i]}' $i > begin"$i"
              mv begin"$i" "$i"  
            done
        # This gets the line of the first atom of the last gly for all the files. This value is input for the next command
        for i in `cat gly_end.list `
            do
              echo $i 
              initial=`awk '{if ($2=="bonds") print NR-9}' $i`
              awk '{ini='$initial'}{if (NR < ini || NR > ini+8)a[NR]=$0}($7="0.0"){if (NR >= ini && NR <= ini+7) a[NR]="    "$0 }{b=NR} END {for (i = 1; i <= b; i++) print a[i]}' $i > end"$i"
              mv end"$i" "$i" 
            done
        ##### This generate a posre file with the selected atoms of the specified chains
        for i in `cat all_yes_posre.list`;
            do
               echo $i
               echo "[ position_restraints ]" > posre_Protein_chain_"$i".itp.posre 
               awk '{if ($5 == "CA") print $1"   1  1000  1000  1000"  }' topol_Protein_chain_"$i".itp >> posre_Protein_chain_"$i".itp.posre 
                mv posre_Protein_chain_"$i".itp.posre posre_Protein_chain_"$i".itp
            done

        for i in `cat all_no_posre.list`; 
            do 
               echo $i 
               if [ "$ffild" != "oplsaa" ] && [  "$i" != "A" ] # ingnore this commans if oplsaa and chain A 
                 then
                   delete=`awk '{if ($2=="POSRES") print NR}' topol_Protein_chain_$i.itp` 
                   awk '{del='$delete'}{if (NR<del) print $0}'  topol_Protein_chain_$i.itp > delete_const_"$i" 
                   mv delete_const_"$i" topol_Protein_chain_$i.itp 
               fi
            done

        rm axis headofgro
  else
        echo "One of this files were not found: "
        echo "    "
        echo " $liggro.gro $liggro.itp   "
        echo "    "
        echo "This script makes corrections in the topolgies "
        echo "to include the ligand and the restrains on proteasome chains fragments."
        echo "To run it needs the following files:"
        echo " gly_end.list, gly_begin.list, all_yes_posre.list, all_no_posre.list"
        echo "The gly_*.list are the proteasome chains that contains a fake glycine at the extremes"
        echo "Chains I and G have begin with Gly. Chain L finish with Asp. This chains do not need modification."
        echo "They should not be found in some of the gly_*.list's"
        echo "The all_*_posre.list are the proteasome chains that need (or not) to be restrained"
        echo "The scripts takes one input; the ligand name, example:"
        echo "./correct_topology.sh LIG_NAME"
fi      
