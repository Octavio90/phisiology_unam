#!/bin/bash
if [ -f $1 ]
 then
   pro="$1"
   lig="$2"
   forcefild="FORCEFIELD"
   water="tip3p"
   gr_index="SOL"  # adjust depending on position of the SOL group
   gmx pdb2gmx -f "$pro" -ff "$forcefild" -water "$water" -o gro.gro 
   if [ -f gro.gro ]
     then
        gmx editconf -f gro.gro -o box.gro -d 1.2 -bt triclinic
        old=`awk 'END{print NR}' topol.top`
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "This command did not work"
        echo "gmx pdb2gmx -f $pro -ff $forcefild -water $water -o gro.gro"
        exit
     fi
   if [ -f box.gro ]
     then
       #genbox -cp box.gro -o sol.gro -p topol.top -cs spc216.gro  
       gmx solvate -cp gro.gro -cs spc216.gro -o sol.gro -p topol.top
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "This command did not work"
        echo "gmx editconf -f protein.gro -o box -bt cubic -box 4.0"
        exit
     fi
   #if [ -f sol.gro ]
   #  then
   #    ./1_Setup_nucleo.sh -t topol.top -l "$lig" -o temp.top
   #    mv temp.top topol.top
   #fi
   if [ -f sol.gro ]
     then
        gmx grompp -f 2_Min_steep.mdp -c sol.gro -p topol.top 
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "This command did not work"
        echo "gmx genbox -cp box.gro -o sol.gro -p topol.top -cs spc216.gro"
        exit
     fi
   if [ -f topol.tpr ]
    then
     echo "$gr_index" | gmx genion -s topol.tpr -o conf.gro -p topol.top -pname NA -nname CL -conc 0.15 -neutral
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "This command did not work"
        echo "gmx grompp -f 2_Min_steep.mdp -c sol.gro -p topol.top"
        exit
     fi
   check=`awk 'END{print NR}' topol.top `
   (( expected=$check-$old ))
   if [ 3 == $expected ]
     then
        echo " Everything seems fine"
        echo " In resume"
        ./make_setup_resume.sh
        cat setup.resume
        echo " "
        echo " "
        echo " Ready for Minimization"
        echo "gmx  grompp -f 2_Min_steep.mdp -c conf.gro -p topol.top "
        echo "  "
        echo "  "
     else
        echo "  "
        echo "  "
        echo "HEEEEEEEERRRRRRRRRRRRRRRRRREEEEEEEEEEEEEE"
        echo "gmx genion -s topol.tpr -o conf.gro -p topol.top -pname K -nname CL -conc 0.15 -neutral"
        echo "type \"15\" when ask "
        echo "Also check topol.top file"
     fi
 else
   echo "Protein file no found $1 "
   echo "This scprits setup the system to perform a simulation in gromacs." 
   echo "This scprits runs internaly the correct_topologies.sh, a script " 
   echo "designed to make the requiered changes in the topologies to deal with " 
   echo "the ligand and proteasome chains fragments restrictions. "  
   echo "It also runs a checking script: ./make_setup_resume.sh"
   echo "It takes two arguments the protein.pdb and the ligand name."  
   echo "Example:"
   echo "./1_Setup.sh PROTEIN.pdb LIG_NAME"
  exit
fi
