#!/bin/bash
set -e
usage()
{
cat << EOF
usage: $0 options

This script is specially designed for the Actin project. It adds atp or adp topological data.

OPTIONS:
   -h      Show this message.
   -l      Give "ATP" or "ADP" accordangly.
   -t      Topology file where the data is going to be added.
   -o      Topology file: output.
EOF
}

check_if_size()
{
  file_1=`awk 'END{print NR}' $1`
  file_2=`awk 'END{print NR}' $2`
  if [ "$file_1" -ne "$file_2" ]; then
     echo " the file $1 and the $2 do not have the same number of lines "
     echo " Thats wierd. Check it"
     exit 1
  fi
}



lig=""
topfile=""
topfileout=""

while getopts “hl:t:o:” OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         l)
             lig=$OPTARG
             ;;
         t)
             topfile=$OPTARG
             ;;
         o)
             topfileout=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [ -z "$lig" ] || [ -z "$topfile" ] || [ -z "$topfileout" ]
then
     usage
     exit 1
fi

if [ ! "$lig"  == "ADP" ] ;  then
     if [ ! "$lig"  == "ATP" ] ; then
         echo -e "The ligand option should be ATP or ADP \n and not $lig "
      exit 1
     fi
fi

if [ ! -f "$topfile" ] ; then
      echo "There is no $topfile file."
      exit 1
fi

if [ ! -f adp_atp.topologies ] ; then
      echo "There is no file adp_atp.topologies."
      echo "$lig" "$topfile" "$topfileout"
      exit 1
fi
# Storing the inital and final atoms numbers from ADP or ADT molecules in the topology file
inat=`awk -v lit="$lig" '($4==lit && ($1!=";")){print $1}' "$topfile" | head -1`
enat=`awk -v lit="$lig" '($4==lit && ($1!=";")){print $1}' "$topfile" | tail -1`

# Storing the number of the line above position restraints
line_posre=`awk '/; Include Position restraint file/{print NR-1}' "$topfile"`

# Storing the line number where the proper dihedral are defined for the ADP or ATP
line_beg=`awk '($2=="dihedrals"){flag+=1;next}(flag==1){print NR" "$0}' "$topfile" | awk -v inj="$inat" '{if (($2!=";") && ($2>=inj && $3>=inj && $4>=inj && $5>=inj)) print $1 }' | head -1`
line_end=`awk '($2=="dihedrals"){flag+=1;next}(flag==1){print NR" "$0}' "$topfile" | awk -v inj="$inat" '{if (($2!=";") && ($2<=inj && $3<=inj && $4<=inj && $5<=inj)) print $1 }' | tail -1` # the minus one is to avoid the blank line

# Check which lines
#echo $line_posre $line_beg $line_end

# Split the topology file into two and anexing the improper terms
awk -v lip="$line_posre" '(NR<lip){print $0}' "$topfile" > first.half
awk -v lit="$lig" '($3==lit) {for(i=1; i<=5; i++) {getline; print}}' adp_atp.topologies | awk -v at="$inat" '{$1+=at-1;$2+=at-1;$3+=at-1;$4+=at-1; print $0 }' >> first.half
awk -v lip="$line_posre" '(NR>=lip){print $0}' "$topfile" >> first.half

# Split the topology into two and change the functional form of the proper dihedrals
# this step should be performed after the anexing the improper terms (line numbering!!!!)
awk -v lip="$line_beg" '(NR<lip){print $0}' first.half > "$topfileout"
awk -v lip="$line_beg" -v lie="$line_end" '(NR>=lip && NR<=lie){print $0}' first.half | sed 's/ 9 / 3 /g' >> "$topfileout"
awk -v lie="$line_end" '(NR>lie){print $0}' first.half >> "$topfileout"

check_if_size first.half "$topfileout"
rm first.half
