#!/bin/bash
cd 2_Min_steep 
if [ ! -f topol.top ] || [ ! -f conf.gro ]
   then
     cp ../1_Setup/topol.top .
     cp ../1_Setup/conf.gro .
     cp ../1_Setup/posre.itp .
fi
make_ndx -f conf.gro<<EOF
del 21 
del 21
del 21
del 21
name 13 LIG
"Protein" | "LIG" 
name 22 Protein_lig
"Protein" | "LIG" | "MG"
name 23 Protein_lig_mg
ri 3-35 & 5 | ri 50-367 & 5
name 24 noDBloop
q
EOF
awk '($1>=5789)' posre.itp > 2min_posre.itp 
#echo '[ position_restraints ]' > 2min_posre.itp
awk '($5=="CA"){print "   "$1"     1  1000  1000  1000"}' topol.top >> 2min_posre.itp
cp 2min_posre.itp posre.itp
ln -s ../../amber99sb-ildn.ff

if [ ! -f confout.gro ] 
  then
     cp conf.gro confout.gro
fi 
for i in `seq 5`
    do
      if [ -f md.log ]
         then 
            check_steep=`grep 'Steepest Descents converged' md.log`
            if [ ! -z "$check_steep" ]
               then
                  break
            fi
         else
            check_steep='no'
      fi
      if [ -z "$check_steep" ] || [ "$check_steep" == "no" ]
         then
            grompp -f 2_Min_steep.mdp -c confout.gro -p topol.top -n index.ndx
            mdrun
            checkp=`grep 'Steepest Descents converged' md.log`
      fi
      if [ "$i" -eq 5 ] && [ ! -z "$checkp" ]
         then
            exit
      fi
   done

cd ../3_Min_cg
ln -s ../../amber99sb-ildn.ff
ln -s ../2_Min_steep/posre.itp
if [ ! -f confout.gro ] 
  then
     cp conf.gro confout.gro
fi 

for i in `seq 3`
   do
      if [ -f md.log ]
         then 
            check_steep=`grep 'Polak-Ribiere Conjugate Gradients converged' md.log`
            if [ ! -z "$check_steep" ]
               then
                  echo "Alles Klar"
                  break
            fi
         else
            check_steep='no'
            echo "$i"
      fi
      if [ -z "$check_steep" ] || [ "$check_steep" == "no" ]
         then  
            checkp=''  
            grompp -f 3_Min_cg.mdp -c confout.gro -p topol.top -n index.ndx
            mdrun 
            checkp=`grep 'Polak-Ribiere Conjugate Gradients converged' md.log`
      fi
      if [ "$i" -eq 3 ] && [ ! -z "$checkp" ]
         then
            exit
      fi
   done
