#!/bin/bash

cd 4_NTV
echo "qsub job_sumit_MD_4_NTV.gromacs"
JOBID4=`qsub job_sumit_MD_4_NTV.gromacs 2>&1 | awk '{print $3}'`
#JOBID4=`sbatch job_sumit_MD_4_NTV.gromacs 2>&1 | awk '{print $NF}'`

cd ../5_NTP
echo "sbatch job_sumit_MD_5_NTP.gromacs"
JOBID5=`qsub -hold_jid "$JOBID4" job_sumit_MD_5_NTP.gromacs 2>&1 | awk '{print $3}'`
#JOBID5=`sbatch --dependency=afterok:"$JOBID4" job_sumit_MD_5_NTP.gromacs 2>&1 | awk '{print $NF}'`

cd ../6_Production
echo "sbatch job_sumit_MD_6_Production.gromacs"
JOBID6=`qsub -hold_jid "$JOBID5" job_sumit_MD_6_Production.gromacs 2>&1 | awk '{print $3}'`
#JOBID6=`sbatch --dependency=afterok:"$JOBID5" job_sumit_MD_6_Pulling.gromacs 2>&1 | awk '{print $NF}'`
cd ../
