#!/bin/bash

function clustname()
{ 
 clnum=`hostname | awk -F'hpcl' '{print $2}'`; 
 if [ "$clnum" -gt 2005 ]  
    then 
      echo "NewCluster" 
    else
       echo "OldCluster" 
 fi
}

function submit_params() 
{ # In which cluster is working.
  if [ "$( clustname )" = "NewCluster" ];
     then
        sed 's/MEMG/192G/g'  ../../process_MD/slurm_files/$1 > temp
        sed 's/PROS/16/g'    temp > ../process/$1
     else
        sed 's/MEMG/45G/g'  ../../process_MD/slurm_files/$1 > temp
        sed 's/PROS/8/g'    temp > ../process/$1
  fi
}

function check_links()
{ if [ ! -L amber99sb-ildn.ff ] || [ ! -L itps ];
     then
       ln -s ../../amber99sb-ildn.ff/
       ln -s ../2_Min_steep/itps
  fi
}

cd 4_NTV
check_links
submit_params job_sumit_MD_4_NTV.gromacs
echo "qsub job_sumit_MD_4_NTV.gromacs"
JOBID4=`qsub job_sumit_MD_4_NTV.gromacs 2>&1 | awk '{print $3}'`

cd ../5_NTP_res
check_links
submit_params job_sumit_MD_5_NTP_res.gromacs
echo "qsub job_sumit_MD_5_NTP_res.gromacs"
JOBID5r=`qsub -hold_jid "$JOBID4" job_sumit_MD_5_NTP_res.gromacs 2>&1 | awk '{print $3}'`

cd ../5_NTP
check_links
submit_params job_sumit_MD_5_NTP.gromacs
echo "qsub job_sumit_MD_5_NTP.gromacs"
JOBID5=`qsub -hold_jid "$JOBID5r" job_sumit_MD_5_NTP.gromacs 2>&1 | awk '{print $3}'`

cd ../6_Production
check_links
submit_params job_sumit_MD_6_Production.gromacs
echo "qsub job_sumit_MD_6_Production.gromacs"
JOBID6=`qsub -hold_jid "$JOBID5" job_sumit_MD_6_Production.gromacs 2>&1 | awk '{print $3}'`
cd ../
