#!/bin/bash

cd 4_NTV_SWARM
ln -s ../../amber99sb.ff
ln -s ../2_Min_steep/posre.itp
echo "qsub job_sumit_MD_4_NTV_SWARM.gromacs"
JOBID4=`qsub job_sumit_MD_4_NTV_SWARM.gromacs 2>&1 | awk '{print $3}'`

cd ../5_NTV_SWARM
ln -s ../../amber99sb.ff
ln -s ../2_Min_steep/posre.itp
echo "sbatch job_sumit_MD_5_NTV_SWARM.gromacs"
JOBID5=`qsub -hold_jid "$JOBID4" job_sumit_MD_5_NTV_SWARM.gromacs 2>&1 | awk '{print $3}'`

cd ../6_Production_SWARM
ln -s ../../amber99sb.ff
ln -s ../2_Min_steep/posre.itp
echo "sbatch job_sumit_MD_6_Production_SWARM.gromacs"
JOBID6=`qsub -hold_jid "$JOBID5" job_sumit_MD_6_Production_SWARM.gromacs 2>&1 | awk '{print $3}'`
cd ../

echo "$JOBID6" 
