#!/bin/bash

cd conf_pulling_files 
echo 0 | trjconv -s topol.tpr -f traj.xtc -o conf.gro -sep
echo 1 13 | g_dist -f traj.xtc -s topol.tpr -n index.ndx -xvg no
cd ..

cd 7_Equilibration
./pull_gen_distances_spec.py dist.xvg listof.search > listof.searchCONF

awk '!($1 in a){a[$1] ; print $0 }' listof.searchCONF > temp
mv temp listof.searchCONF
for i in `awk '{print $1}' listof.searchCONF`
      do
        mkdir conf$i 
        b=`awk -v a="$i" '{if($1==a) print $4}' listof.searchCONF`
        cp 7_Equilibration.mdp conf"$i"/7_Equilibration.mdp 
        cd conf$i 
        sed "s/NUMBER/$i/g" ../job_sumit_MD_7_Equilibration.gromacs > job_sumit_MD_7_Equilibration.gromacs
        ln -s ../../conf_pulling_files/conf$i.gro 
        ln -s ../../6_Pulling/itps
        ln -s ../../6_Pulling/index.ndx 
        ln -s ../../6_Pulling/topol.top 
        ln -s ../../6_Pulling/ligand.itp
        sbatch job_sumit_MD_7_Equilibration.gromacs
        cd .. 
      done
cd ..
