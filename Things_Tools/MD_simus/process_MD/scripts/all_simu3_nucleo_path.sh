#!/bin/bash
cd 6_Production_SWARM
echo 3 24 | trjconv -f conformations.xtc -n index -o conf_ca.xtc -fit rot+trans 
echo 3 3  | g_anaeig -s ref.pdb -f conf_ca.xtc -first 1 -last 5 -v eigenvec_actinsactallnoout.trr -proj pca_conf_ca.xvg
cd ../
