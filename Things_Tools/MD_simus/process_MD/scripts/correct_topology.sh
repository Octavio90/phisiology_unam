#!/bin/bash
liggro="$1"
if [ -f "$liggro".gro ] && [ -f "$liggro".itp ]
  then 
        # Fixing the .gro file 
        axis=`tail -1 gro.gro`
        sed '$d' gro.gro > temp ; mv temp gro.gro    # this removes the last line
        lignumberatoms=`awk 'END{print NR}' "$liggro".gro`
        awk '{if (NR<=2) print $0}' gro.gro > headofgro
        awk '{if (NR==2) $1=$1+'$lignumberatoms'; print $0}' headofgro > newheadofgro
        awk '{if (NR>2) print $0}' gro.gro >> newheadofgro
        mv newheadofgro gro.gro
        strnum=`awk 'END {print substr($0,16,5)}' gro.gro`
        awk -v str="$strnum" '{str=str+1}{printf "%s%5s%s\n",substr($0,0,15),str,substr($0,21)}' "$liggro".gro >> gro.gro 
        echo "$axis" >> gro.gro
        rm headofgro

        # Fixing the .top file
        placeline=`awk '/tip3p.itp/{print NR-2}' topol.top`
        awk '{if (NR=='$placeline') $0="#include \"'$liggro'.itp\"" ; print $0}' topol.top > totemp.top
        echo "$liggro           1" >> totemp.top
        mv totemp.top topol.top
        #### 
        if [[ ! -z topol_*.itp ]] 
           then
               chain=`awk '/Compound        #mols/{getline;print $1}' topol.top | awk -F"_" '{print $NF}'`
               name_top=topol_Protein_chain_"$chain".itp
               awk '/\[ moleculetype \]/{exit}1' topol.top > temp_topol.top
               cat ../../pdbs/lig/nucleo_atom_types.itp >> temp_topol.top ; echo " " >> temp_topol.top # adp or atp atom types
               echo "[ moleculetype ]" >> $name_top
               awk '/\[ moleculetype \]/ {flag=1;next} /\#include \"ligand.itp\"/{flag=0} flag {print}' topol.top >> $name_top
               sed "s/posre.itp/posre_Protein_chain_$chain.itp/g" $name_top > temp_pos ; mv temp_pos $name_top 
               awk '/\#include \"ligand.itp\"/ {flag=1} flag {print}' topol.top >> temp_topol.top
               awk -v ch=$name_top '/\#include \"ligand.itp\"/ {print "#include \""ch"\""}1' temp_topol.top > topol.top
               #awk '/forcefield.itp/{print;print "#include \"topol_chain_A.itp\"";next}1' temp_topol.top > topol.top
               mv posre.itp "posre_Protein_chain_$chain.itp"
        fi
        for i in `ls topol_*.itp`; do echo "$i" | sed 's/.itp//g' | awk -F"_" '{ if (NF>1) print substr($NF,1,1)}' ; done > itp.list
        ####
  else
        echo "One of this files were not found: "
        echo "    "
        echo " $liggro.gro $liggro.itp   "
        echo "    "
        echo "This script makes corrections in the topolgies "
        echo "to include the ligand."
        echo "The scripts takes one input; the ligand name, example:"
        echo "./correct_topology.sh LIG_NAME"
fi      
