#!/bin/bash
cd ../2_Min_steep/
if [ ! -f topol.top ] || [ ! -f conf.gro ] || [ ! -d itps ] || [ ! -f ligand.itp ] 
   then
     cp ../1_Setup/topol.top .
     cp ../1_Setup/conf.gro .
     cp -r ../1_Setup/itps .
     cp ../1_Setup/ligand.itp .
   else
     echo "The needed files are already here"
     echo "Any file was copied from 1_Setup"
     exit 
fi
cp conf.gro confout.gro
for i in `seq 5`
   do
      if [ ! -f md.log ]
         then 
            check_steep=''
      fi
      if [ ! -z "$check_steep" ]
         then  
            grompp -f 2_Min_steep.mdp -c confout.gro -p topol.top -n index.ndx
            salloc -n 16 mpirun mdrun
            check_steep=`grep congereafd md.log`
         else
            break
      fi
      if [ "$i" -eq 5 ] && [ ! -z "$check_steep" ]
         then
            exit
      fi
   done

cd ../3_Min_cg
cp conf.gro confout.gro
for i in `seq 3`
   do
      if [ ! -f md.log ]
         then  
            check_steep=''
      fi
      if [ ! -z "$check_steep" ]
         then  
            grompp -f 3_Min_cg.mdp -c confout.gro -p topol.top -n index.ndx
            salloc -n 16 mpirun mdrun
            check_steep=`grep congereafd md.log`
         else
            break
      fi
      if [ "$i" -eq 3 ] && [ ! -z "$check_steep" ]
         then
            exit
      fi
   done

cd ../4_NTV
echo "sbatch job_sumit_MD_4_NTV.gromacs"
JOBID4=`sbatch job_sumit_MD_4_NTV.gromacs 2>&1 | awk '{print $NF}'`

cd ../5_NTP
echo "sbatch job_sumit_MD_5_NTP.gromacs"
JOBID5=`sbatch --dependency=afterok:"$JOBID4" job_sumit_MD_5_NTP.gromacs 2>&1 | awk '{print $NF}'`

cd ../6_Pulling
echo "sbatch job_sumit_MD_6_Pulling.gromacs"
JOBID6=`sbatch --dependency=afterok:"$JOBID5" job_sumit_MD_6_Pulling.gromacs 2>&1 | awk '{print $NF}'`
cd ../
