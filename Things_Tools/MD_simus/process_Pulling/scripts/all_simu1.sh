#!/bin/bash
cd 2_Min_steep 
if [ ! -f topol.top ] || [ ! -f conf.gro ] || [ ! -d itps ] || [ ! -f ligand.itp ] 
   then
     cp ../1_Setup/topol.top .
     cp ../1_Setup/conf.gro .
     cp -r ../1_Setup/itps .
     cp ../1_Setup/ligand.itp .
   else
     echo "The needed files are already here"
     echo "Any file was copied from 1_Setup"
fi
make_ndx -f conf.gro<<EOF
del 20 
del 20
del 20
"Protein" | "LIG"
name 21 Protein_lig
ri 363
name 22 SpeWat
"SOL" & ! "SpeWat"
name 23 SOL_m1
"Protein_lig" | "SpeWat"
name 24 Protein_lig_spewat
q
EOF

if [ ! -f confout.gro ] 
  then
     cp conf.gro confout.gro
fi 
for i in `seq 5`
    do
      if [ -f md.log ]
         then 
            check_steep=`grep 'Steepest Descents converged' md.log`
            if [ ! -z "$check_steep" ]
               then
                  break
            fi
         else
            check_steep='no'
      fi
      if [ "$check_steep"=='no' ] || [ -z "$check_steep" ]
         then
            grompp -f 2_Min_steep.mdp -c confout.gro -p topol.top -n index.ndx
#            salloc -n 16 mpirun mdrun
            mdrun -s topol.tpr
            checkp=`grep 'Steepest Descents converged' md.log`
      fi
      if [ "$i" -eq 5 ] && [ ! -z "$checkp" ]
         then
            exit
      fi
   done

cd ../3_Min_cg

if [ ! -f confout.gro ] 
  then
     cp conf.gro confout.gro
fi 

for i in `seq 3`
   do
      if [ -f md.log ]
         then 
            check_steep=`grep 'Polak-Ribiere Conjugate Gradients converged' md.log`
            if [ ! -z "$check_steep" ]
               then
                  echo "Alles Klar"
                  break
            fi
         else
            check_steep='no'
            echo "$i"
      fi
      if [ "$check_steep"=='no' ] || [ -z "$check_steep" ]
         then  
            checkp=''  
            grompp -f 3_Min_cg.mdp -c confout.gro -p topol.top -n index.ndx
#            salloc -n 16 mpirun mdrun
            mdrun -s topol.tpr
            checkp=`grep 'Polak-Ribiere Conjugate Gradients converged' md.log`
      fi
      if [ "$i" -eq 3 ] && [ ! -z "$checkp" ]
         then
            exit
      fi
   done
