#!/bin/bash

#cd 4_NTV
#echo "sbatch job_sumit_MD_4_NTV.gromacs"
#JOBID4=`sbatch job_sumit_MD_4_NTV.gromacs 2>&1 | awk '{print $NF}'`
#
#cd ../5_NTP
#echo "sbatch job_sumit_MD_5_NTP.gromacs"
#JOBID5=`sbatch --dependency=afterok:"$JOBID4" job_sumit_MD_5_NTP.gromacs 2>&1 | awk '{print $NF}'`
#
#cd ../6_Pulling
#echo "sbatch job_sumit_MD_6_Pulling.gromacs"
#JOBID6=`sbatch --dependency=afterok:"$JOBID5" job_sumit_MD_6_Pulling.gromacs 2>&1 | awk '{print $NF}'`
#cd ../


cd 4_NTV
echo "msub job_sumit_MD_4_NTV.gromacs"
JOBID4=`msub job_sumit_MD_4_NTV.gromacs 2>&1 | grep -v -e '^$' | sed -e 's/\s*//'`

cd ../5_NTP
echo "msub job_sumit_MD_5_NTP.gromacs"
JOBID5=`msub -W depend=afterok:"$JOBID4" job_sumit_MD_5_NTP.gromacs 2>&1 | grep -v -e '^$' | sed -e 's/\s*//'`

cd ../6_Pulling
echo "msub job_sumit_MD_6_Pulling.gromacs"
JOBID6=`msub -W depend=afterok:"$JOBID5" job_sumit_MD_6_Pulling.gromacs 2>&1 | grep -v -e '^$' | sed -e 's/\s*//'`
cd ../
