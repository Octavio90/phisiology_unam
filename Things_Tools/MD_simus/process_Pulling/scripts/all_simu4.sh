#!/bin/bash
if [ ! -d 8_Analysis ]
  then
     mkdir 8_Analysis
     cd 8_Analysis
     ln -s ~/scripts_python/plot_wham.py .
     cp ../7_Equilibration/listof.searchCONF .
  else
     echo "There was already a folder for analysis"
     cd 8_Analysis
fi
j=0; 
for i in `awk '{print $1}' listof.searchCONF `
    do
        ln -s ../7_Equilibration/conf"$i"/topol.tpr topol"$j".tpr ; echo topol"$j".tpr >> tpr.dat 
        ln -s ../7_Equilibration/conf"$i"/pullx.xvg pullx"$j".xvg ; echo pullx"$j".xvg >> pullx.dat 
        j=$(( j + 1 )) 
    done
awk '!($0 in a){a[$0]; print }' tpr.dat > tmp ; mv tmp tpr.dat
awk '!($0 in a){a[$0]; print }' pullx.dat > tmp ; mv tmp pullx.dat
g_wham -it tpr.dat -ix pullx.dat -o -hist -unit kCal
cd ..

#./plot_wham.py profile.xvg histo.xvg default
#j=0; for i in `awk '{print $1}' listof.searchCONF `; do unlink pullx"$j".xvg ; j=$(( j + 1 )) ; done
#j=0; for i in `awk '{print $1}' listof.searchCONF `; do unlink topol"$j".tpr ; j=$(( j + 1 )) ; done
