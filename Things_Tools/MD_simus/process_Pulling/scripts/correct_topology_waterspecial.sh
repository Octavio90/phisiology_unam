#!/bin/bash

# Making correction of SOL to WAT. (Just the first molecule)
awk 'BEGIN {matches=0}; matches < 3 && /SOL/ { sub(/SOL/,"WAT"); matches++ };  { print $0 }' conf.gro > tmp.gro
mv tmp.gro conf.gro

cp single_water.itp itps/
# Adding the special water topology
placeline=`awk '/tip3p.itp/{print NR-2}' topol.top`
awk '{if (NR=='$placeline') print "#include \"itps/single_water.itp\"" ; print $0}' topol.top > totemp.top
mv totemp.top topol.top

# Adding the special water restrain
placeline=`awk '/tip3p.itp/{print NR-2}' topol.top`
awk '{if (NR=='$placeline') print "[ position_restraints ]" ; print $0}' topol.top > totemp.top
mv totemp.top topol.top

# Adding the special water restrain
placeline=`awk '/tip3p.itp/{print NR-2}' topol.top`
awk '{if (NR=='$placeline') print "  1    1       500       500       500" ; print $0}' topol.top > totemp.top

tail -4 totemp.top | head -1  > sol_number
tail -3 totemp.top > last_3
# Reducing by one the Solvent
awk '($2=$2-1){print $1"     "$2 }' sol_number > final_num
size=`awk 'END{print NR}' totemp.top`
awk -v a=$size '(NR<a-3){print $0}' totemp.top > store 
mv store topol.top
echo "WAT               1" >> topol.top
cat final_num >> topol.top
cat last_3 >> topol.top
rm last_3 sol_number totemp.top final_num
