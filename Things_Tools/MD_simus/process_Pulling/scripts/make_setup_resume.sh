#!/bin/bash
date > setup.resume 
head -1 conf.gro >> setup.resume 
echo "Atoms in sytem: " >> setup.resume
awk '{if (NR==2) print $0 }' conf.gro >> setup.resume 
echo "Molecules in sytem: " >> setup.resume 
awk '/\#mols/,0' topol.top >> setup.resume
echo -n "Box vectors " >> setup.resume 
tail -1 conf.gro >> setup.resume
echo Forcefield: >> setup.resume
grep forcefield.itp topol.top >> setup.resume
echo Water Model: >> setup.resume
awk '/water/{getline;print}' topol.top | grep "#" >> setup.resume
last2=''
last2=`grep oplsaa topol.top`
# Cheking the right constainst setup
for i in `cat all_no_posre.list`; 
    do 
      last='' 
      last=`grep "#ifdef POSRES"  itps/topol_Protein_chain_"$i".itp`
      if [ ! -z "$last" ]
         then
           if [ ! -z "$last2" ] && [ "$i" != "A" ]
             then
                echo "ERROR in deleting constrains flags for "$i" ERROR"
                echo " $last" '' 
           fi
      fi  
    done >> setup.resume
# Checking the rigth charge nullification in gly_end.list and gly_begin.list
for i in `cat gly_end.list`
   do
     row1=`awk '/residue/{x=NR}END{print x}' itps/"$i"`
     row2=`awk '/\[ bonds \]/{x=NR}END{print x}' itps/"$i"`
     file=`echo $i | sed 's/.itp//g'`
     file="$file"_end_gly
     awk '{a='$row1'}{b='$row2'}{if (NR<b && NR>a && NF > 2) sto[i++]=$7}END{ for ( k=1 ; k <= length(sto) ; k++ ) if (k in sto) print sto[k]}' itps/"$i" | awk -v lin="$file" '{if ($1!=0.0) print "ERROR  " lin}'  >> setup.resume
  done
for i in `cat gly_begin.list`
   do
     row1=`awk '{if ($1==1 && $4=="GLY") print NR}' itps/"$i"`
     row2=`awk '{if ($1==9 && $4=="GLY") print NR}' itps/"$i"`
     file=`echo $i | sed 's/.itp//g'`
     file="$file"_begin_gly
     awk '{a='$row1'}{b='$row2'}{if (NR<=b && NR>=a && NF > 2) sto[i++]=$7}END{ for ( k=1 ; k <= length(sto) ; k++ ) if (k in sto) print sto[k]}' itps/"$i" | awk -v lin="$file" '{if ($1!=0.0) print "ERROR  " lin}'  >> setup.resume
   done
    
cat setup.resume 
