#!/bin/bash
# set_up_MD.sh pdbs/pdbfile forcefield temperature simulation-time integrador
pdbfile=$1 
ff=$2
t=$3
simutime=$4
integrador=$5
ligand="$6"

if [ "$pdbfile" == 'h' ] || [ "$pdbfile" == "-h" ]
   then 
     echo "This scripts setup a normal MD simulation "
     echo "It takes 6 arguments"
     echo " 1.- receptor (This file should be properly aligned and stored in pdbs/rec/)"
     echo " 3.- forcefield to use (check correpondece with the files in the topologies folder )"
     echo " 4.- temperature 300 or 310 are the usual choice "
     echo " 5.- time in femtoseconds:  Use 500000 (1 ns) "
     echo " 6.- integrador: sd or md"
     echo " 2.- ligand (This file should be properly aligned and stored in pdbs/lig/ and also the topology pdbs/lig/topology)"
     echo " Example: "
     echo " ./set_up_MD.sh RECEPTOR.pdb FORCEFILD TEMPERATURE SIMULTIME INTEGRADOR LIGAND"
     exit
fi

variable="$pdbfile"_"$ff"_"$t"_"$integrador"
echo "$variable"
if [ ! -d "$variable" ]
   then 
       mkdir "$variable"
   else
     echo "There is already a folder with this simulation parameters:"
     echo "receptor        : $pdbfile"
     echo "ligand          : $ligand"
     echo "forcefild       : $ff"
     echo "temperature     : $t"
     echo "simulation time : $simutime"
     echo "integrador      : $integrador"
     exit
fi
cd "$variable"

if [ -d ../process_MD ]
  then
    mkdir process
    sed "s/FORCEFIELD/$ff/g" ../process_MD/scripts/1_Setup.sh > process/1_Setup.sh ; chmod +x process/1_Setup.sh ; 
    cp ../process_MD/scripts/make_setup_resume.sh process/
    cp ../process_MD/scripts/1_Setup_nucleo.sh process/
    cp ../process_MD/list_of_files/adp_atp.topologies process/
    for i in 4_NTV 5_NTP_res 5_NTP 6_Production ;
       do
         sed "s/TEMPERATURE/$t/g" ../process_MD/mdps/"$i".mdp > process/"$i".mdp
         sed "s/INTEGRADOR/$integrador/g" process/"$i".mdp > process/temp ; mv process/temp process/"$i".mdp
         if  [ "$integrador" == "sd" ]
            then
               sed "s/tcoupl/;tcoupl/g" process/"$i".mdp > process/temp ; mv process/temp process/"$i".mdp
         fi
       done
    sed "s/TIME/$simutime/g" process/6_Production.mdp > process/temp ; mv process/temp process/6_Production.mdp  
    cp ../process_MD/mdps/2_Min_steep.mdp process/
    cp ../process_MD/mdps/3_Min_cg.mdp process/
    for i in `cat ../process_MD/list_of_files/common_files.list`
       do
         cp ../process_MD/"$i" process/
       done 
  else
    echo "Theres no folder \"process\"    Be careful " >> my.log
    cd ..
    exit
fi
echo "$pdbfile" 
if [ -f ../pdbs/rec/"$pdbfile" ]
  then
    echo -n "DATE   " >> my.log
    date >> my.log
    old=`awk 'END{print NR}' my.log`
    if [ ! -d 1_Setup ]
       then
          mkdir 1_Setup
          cd 1_Setup
          ln -s ../../pdbs/rec/$pdbfile protein.pdb 
          ln -s ../process/1_Setup.sh .
          ln -s ../process/2_Min_steep.mdp .
          ln -s ../process/make_setup_resume.sh .
          ln -s ../../amber99sb-ildn.ff .
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ..
       else 
           if [ -f 1_Stepup/conf.gro ]
              then
                  echo "There was already a folder \"1_Setup\" containg a topol.tpr file      Be careful " >> my.log
           fi       
    fi
    if [ ! -d 2_Min_steep ] ;  
       then 
          mkdir 2_Min_steep ;
          cd 2_Min_steep
          ln -s ../process/2_Min_steep.mdp
          mkdir itps
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ..
       else 
           if [ -f 2_Min_steep/topol.tpr ]
              then
                  echo "There was already a folder \"2_Min_steep\" containg a topol.tpr file      Be careful " >> my.log
           fi       
    fi
    if [ ! -d 3_Min_cg ] ;
       then
          mkdir 3_Min_cg ; 
          cd 3_Min_cg
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../2_Min_steep/confout.gro conf.gro 
          ln -s ../2_Min_steep/itps
          ln -s ../process/3_Min_cg.mdp
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 3_Min_cg/topol.tpr ]
              then
                  echo "There was already a folder \"3_Min_cg\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    if [ ! -d 4_NTV ] ;
       then
          mkdir 4_NTV ; 
          cd 4_NTV
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/4_NTV.mdp
          ln -s ../3_Min_cg/confout.gro conf.gro 
          ln -s ../process/job_sumit_MD_4_NTV.gromacs
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 4_NTV/topol.tpr ]
              then
                  echo "There was already a folder \"4_NTV\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    if [ ! -d 5_NTP_res ] ;
       then
          mkdir 5_NTP_res ; 
          cd 5_NTP_res
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/5_NTP_res.mdp
          ln -s ../4_NTV/confout.gro conf.gro 
          ln -s ../4_NTV/state.cpt state_4.cpt
          ln -s ../process/job_sumit_MD_5_NTP_res.gromacs
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 5_NTP_res/topol.tpr ]
              then
                  echo "There was already a folder \"5_NTP\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi

    if [ ! -d 5_NTP ] ;
       then
          mkdir 5_NTP ; 
          cd 5_NTP
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/5_NTP.mdp
          ln -s ../5_NTP_res/confout.gro conf.gro 
          ln -s ../5_NTP_res/state.cpt state_5_res.cpt
          ln -s ../process/job_sumit_MD_5_NTP.gromacs
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 5_NTP/topol.tpr ]
              then
                  echo "There was already a folder \"5_NTP\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi

    if [ ! -d 6_Production ] ;
       then
          mkdir 6_Production ; 
          cd 6_Production
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/6_Production.mdp
          ln -s ../5_NTP/confout.gro conf.gro 
          ln -s ../5_NTP/state.cpt state_5.cpt
          ln -s ../process/job_sumit_MD_6_Production.gromacs
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 6_Production/topol.tpr ]
              then
                  echo "There was already a folder \"6_Production\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    ln -s ../process_MD/scripts/all_simu1_nucleo.sh .
    ln -s ../process_MD/scripts/all_simu2_nucleo.sh .
    check=`awk 'END{print NR}' my.log `
    (( expected=$check-$old ))
    if [[ $expected -ne 34 ]]
      then
        echo " Something seems strange. Check my.log file to find it out"
        echo " Something seems strange. Check my.log file to find it out" >> my.log
      else
        echo "Everything seems fine here"
        echo "Everything seems fine here" >> my.log
    fi
  else
     echo "No file $pdbfile found"
     echo "Give a PDB file as argument"
     echo "Example: "
     echo "./set_up_MD.sh myprotein.pdb"
     exit
fi

