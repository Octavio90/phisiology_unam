#!/bin/bash
# set_up_MD.sh pdb/pdbfile forcefield temperature simulation-time integrador
pdbfile=$1 
ligand="$2"
ff=$3
t=$4
simutime=$5
integrador=$6
if [ "$pdbfile" == 'h' ]
   then 
     echo "This scripts setup a pulling simulation "
     echo "It takes 6 arguments"
     echo " 1.- receptor (This file should be properly aligned and stored in pdb/rec/)"
     echo " 2.- ligand (This file should be properly aligned and stored in pdb/lig/ and also the topology in topologies/lig)"
     echo " 3.- forcefield to use (check correpondece with the files in the topologies folder )"
     echo " 4.- temperature 300 or 310 are the usual choice "
     echo " 5.- time in femtoseconds:  Use 500000 (1 ns) "
     echo " 6.- integrador: sd or v-rescale "
     echo " Example: "
     echo " ./set_up_Pulling_v2.sh RECEPTOR.pdb LIGAND FORCEFILD TEMPERATURE SIMULTIME INTEGRADOR "
     exit
fi


variable="$ligand"_"$ff"_"$t"_"$integrador"
echo "$variable"
if [ ! -d "$variable" ]
   then 
       mkdir "$variable"
   else
     echo "There is already a folder with this simulation parameters:"
     echo "receptor        : $pdbfile"
     echo "ligand          : $ligand"
     echo "forcefild       : $ff"
     echo "temperature     : $t"
     echo "simulation time : $simutime"
     echo "integrador      : $integrador"
     exit
fi
cd "$variable"

if [ -d ../process_Pulling ]
  then
    mkdir process
    sed "s/FORCEFIELD/$ff/g" ../process_Pulling/scripts/1_Setup.sh > process/1_Setup.sh ; chmod +x process/1_Setup.sh ; 
    sed "s/FORCEFIELD/$ff/g" ../process_Pulling/folder_commands/commands.1_Setup > process/commands.1_Setup ; 
    cp ../process_Pulling/scripts/make_setup_resume.sh process/
    for i in 4_NTV 5_NTP 6_Pulling 7_Equilibration 8_Production
       do
         sed "s/TEMPERATURE/$t/g" ../process_Pulling/mdps/"$i".mdp > process/"$i".mdp
         sed "s/INTEGRADOR/$integrador/g" process/"$i".mdp > process/temp ; mv process/temp process/"$i".mdp
         if  [ "$integrador" == "sd" ]
            then
               sed "s/tcoupl/;tcoupl/g" process/"$i".mdp > process/temp ; mv process/temp process/"$i".mdp
         fi
       done
    sed "s/TIME/$simutime/g" process/7_Equilibration.mdp > process/temp ; mv process/temp process/"$i".mdp  
    cp ../process_Pulling/mdps/2_Min_steep.mdp process/
    cp ../process_Pulling/mdps/3_Min_cg.mdp process/
    cp ../process_Pulling/scripts/correct_topology_waterspecial.sh process/
    cp ../process_Pulling/list_of_files/single_water.itp process/
    for i in `cat ../process_Pulling/list_of_files/common_files.list`
       do
         cp ../process_Pulling/"$i" process/
       done 
  else
    echo "Theres no folder \"process\"    Be careful " >> my.log
    cd ..
    exit
fi
pwd
echo "$pdbfile" 
if [ -f ../pdb/rec/"$pdbfile" ] && [ -f ../pdb/lig/$ff/gro/$ligand.gro ]
  then
    echo -n "DATE   " >> my.log
    date >> my.log
    old=`awk 'END{print NR}' my.log`
    if [ ! -d 1_Setup ]
       then
          mkdir 1_Setup
          cd 1_Setup
          ln -s ../../pdb/rec/$pdbfile protein.pdb 
          ln -s ../../pdb/lig/$ff/gro/$ligand.gro  ligand.gro
          ln -s ../../pdb/lig/$ff/itp/$ligand.itp  ligand.itp
          ln -s ../process/correct_topology.sh .
          ln -s ../process/1_Setup.sh .
          ln -s ../process/commands.1_Setup .
          ln -s ../process/2_Min_steep.mdp .
          ln -s ../process/make_setup_resume.sh .
#          ln -s ../process/gly_begin.list
#          ln -s ../process/gly_end.list
          ln -s ../process/all_no_posre.list
          ln -s ../process/all_yes_posre.list
          ln -s ../process/list_of_files/single_water.itp
          ln -s ../process/scripts/correct_topology_waterspecial.sh
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ..
       else 
           if [ -f 1_Stepup/conf.gro ]
              then
                  echo "There was already a folder \"1_Setup\" containg a topol.tpr file      Be careful " >> my.log
           fi       
    fi
    if [ ! -d 2_Min_steep ] ;  
       then 
          mkdir 2_Min_steep ;
          cd 2_Min_steep
          ln -s ../process/2_Min_steep.mdp
          ln -s ../process/commands.2_Min_steep
          ln -s ../1_Setup/itp.list
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ..
       else 
           if [ -f 2_Min_steep/topol.tpr ]
              then
                  echo "There was already a folder \"2_Min_steep\" containg a topol.tpr file      Be careful " >> my.log
           fi       
    fi
    if [ ! -d 3_Min_cg ] ;
       then
          mkdir 3_Min_cg ; 
          cd 3_Min_cg
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/ligand.itp 
          ln -s ../2_Min_steep/itps 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../2_Min_steep/confout.gro conf.gro 
          ln -s ../process/3_Min_cg.mdp
          ln -s ../process/commands.3_Min_cg
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 3_Min_cg/topol.tpr ]
              then
                  echo "There was already a folder \"3_Min_cg\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    if [ ! -d 4_NTV ] ;
       then
          mkdir 4_NTV ; 
          cd 4_NTV
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/ligand.itp 
          ln -s ../2_Min_steep/itps 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/4_NTV.mdp
          ln -s ../3_Min_cg/confout.gro conf.gro 
          ln -s ../process/commands.4_NTV
          ln -s ../process/job_sumit_MD_4_NTV.gromacs
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 4_NTV/topol.tpr ]
              then
                  echo "There was already a folder \"4_NTV\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    if [ ! -d 5_NTP ] ;
       then
          mkdir 5_NTP ; 
          cd 5_NTP
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/ligand.itp 
          ln -s ../2_Min_steep/itps 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/5_NTP.mdp
          ln -s ../4_NTV/confout.gro conf.gro 
          ln -s ../4_NTV/state.cpt state_4.cpt
          ln -s ../process/commands.5_NTP
          ln -s ../process/job_sumit_MD_5_NTP.gromacs
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 5_NTP/topol.tpr ]
              then
                  echo "There was already a folder \"5_NTP\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    if [ ! -d 6_Pulling ] ;
       then
          mkdir 6_Pulling ; 
          cd 6_Pulling
          ln -s ../2_Min_steep/topol.top 
          ln -s ../2_Min_steep/ligand.itp 
          ln -s ../2_Min_steep/itps 
          ln -s ../2_Min_steep/index.ndx 
          ln -s ../process/6_Pulling.mdp
          ln -s ../5_NTP/confout.gro conf.gro 
          ln -s ../5_NTP/state.cpt state_5.cpt
          ln -s ../process/commands.6_Pulling
          ln -s ../process/job_sumit_MD_6_Pulling.gromacs
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else 
           if [ -f 6_Pulling/topol.tpr ]
              then
                  echo "There was already a folder \"6_Pulling\" containg a topol.tpr file       Be careful " >> my.log
           fi       
    fi
    if [ ! -d conf_pulling_files ] ;
       then
          mkdir conf_pulling_files ;
          cd conf_pulling_files
          ln -s ../6_Pulling/traj.xtc
          ln -s ../6_Pulling/topol.tpr
          ln -s ../6_Pulling/index.ndx
          ln -s ~/scripts_python/plot.py .
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else
           if [ -f conf_pulling_files/topol.tpr ]
              then
                  echo "There was already a folder \"conf_pulling_files\" containg a topol.tpr file       Be careful " >> my.log
           fi
    fi
    if [ ! -d 7_Equilibration ] ;
       then
          mkdir 7_Equilibration ;
          cd 7_Equilibration
          ln -s ../process/commands.7_Equilibration
          ln -s ../process/job_sumit_MD_7_Equilibration.gromacs
          ln -s ../process/7_Equilibration.mdp .
          ln -s ~/scripts_python/pull_gen_distances_spec.py .
          ln -s ../conf_pulling_files/dist.xvg .
          cp ../process/listof.search .
          echo $PWD >> ../my.log
          ls >> ../my.log
          cd ../
       else
           if [ -f 7_Equlibration/listof.frames ]
              then
                  echo "There was already a folder \"7_Equilibration\" containg a listof.frames file       Be careful " >> my.log
           fi
    fi
    ln -s ../process_Pulling/scripts/all_simu1.sh .
    ln -s ../process_Pulling/scripts/all_simu2.sh .
    ln -s ../process_Pulling/scripts/all_simu3.sh .
    ln -s ../process_Pulling/scripts/all_simu4.sh .
    check=`awk 'END{print NR}' my.log `
    (( expected=$check-$old ))
    if [ 66 != $expected ]
      then
        echo " Something seems strange. Check my.log file to find it out"
        echo " Something seems strange. Check my.log file to find it out" >> my.log
      else
        echo "Everything seems fine here"
        echo "Everything seems fine here" >> my.log
    fi
  else
     echo "No file $pdbfile found"
     echo " ../pdb/rec/"$pdbfile""
     echo "Give a PDB file as argument"
     echo "Example: "
     echo "./set_up_MD.sh myprotein.pdb"
     exit
fi
