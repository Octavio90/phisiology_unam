#!/bin/bash

file_to_treat=$1
cp "$file_to_treat" temp_"$file_to_treat"

for frag in TYR GLN GLU PRO ASP ASN LYS PHE TRP LEU SER ARG HIS MET CYS;
    do
      sed "s/HB2 $frag/HB1 $frag/g" temp_"$file_to_treat" |\
      sed "s/HB3 $frag/HB2 $frag/g" > temp
      mv temp temp_"$file_to_treat"
    done

for frag in GLN GLU PRO LYS ARG MET;
    do
      sed "s/HG2 $frag/HG1 $frag/g" temp_"$file_to_treat" |\
      sed "s/HG3 $frag/HG2 $frag/g" > temp
      mv temp temp_"$file_to_treat"
    done

for frag in PRO LYS ARG;
    do
      sed "s/HD2 $frag/HD1 $frag/g" temp_"$file_to_treat" |\
      sed "s/HD3 $frag/HD2 $frag/g" > temp
      mv temp temp_"$file_to_treat"
    done

for frag in LYS;
    do
      sed "s/HE2 $frag/HE1 $frag/g" temp_"$file_to_treat" |\
      sed "s/HE3 $frag/HE2 $frag/g" > temp
      mv temp temp_"$file_to_treat"
    done

for frag in GLY ;
    do
      sed "s/HA2 $frag/HA1 $frag/g" temp_"$file_to_treat" |\
      sed "s/HA3 $frag/HA2 $frag/g" > temp
      mv temp temp_"$file_to_treat"
    done

for frag in ILE;
    do
      sed "s/HG12 $frag/HG11 $frag/g" temp_"$file_to_treat" |\
      sed "s/HG13 $frag/HG12 $frag/g" > temp
      mv temp temp_"$file_to_treat"
    done

gawk-3.1.8 'BEGIN{OFS="_";}/HD1 HIS/{print substr($0,18,3),substr($0,22,1),strtonum(substr($0,23,3))}'\
     temp_"$file_to_treat" > his_to_changed_in_"$file_to_treat".log

nhis=`awk 'END{print NR}' his_to_changed_in_"$file_to_treat".log`

if [ ! "$nhis" -eq 0 ];                                               #
   then                                                               #
      echo "\nProtonation state have been changed on some HIS"        #
      echo "check the file his_to_changed_in_"$file_to_treat".log\n"  #
      for i in `cat his_to_changed_in_"$file_to_treat".log`;          #
          do                                                          #
             oo=`echo "$i" | sed 's/\_/ /g'`                          #
             nn=`echo "$i" | sed 's/\_/ /g' | sed 's/HIS/HID/'`       #
             sed "s/$oo/$nn/g" temp_"$file_to_treat" > temp           #
             mv temp temp_"$file_to_treat"                            #
          done                                                        #
   else                                                               #
      rm his_to_changed_in_"$file_to_treat".log                       #
fi                                                                    #
