#!/bin/bash

# This script converts a frame from full atom to cg.
# The cg consists on N,CA,C,O,CB and OG. Where OG is the averega of all atoms above CB.
# Accordantly, the output is conformed by GLY, ALA and SER.
# It works for frames without hydrogens and heteroatoms. 
# Also the script recognizes N as the begining of the Residue and O as the end. 
# This is the key feature that it uses to make the selective average. 
 
ref=$1

gawk-3.1.8 '!($1=="ATOM"){print $0;next}($1=="ATOM"&&$3=="N"){f=1;r=$4;i=$5}($1=="ATOM"&&$3=="C"){if (k>0) printf "ATOM    881  OG  %3s   %3s    %8.3f%8.3f%8.3f  1.00  0.00\n",r,i,a/k,b/k,c/k;f=0;k=0;a=0;b=0;c=0}(f&&!($3=="N"||$3=="CA"||$3=="CB"||$3=="C"||$3=="O")){a+=strtonum(substr($0,31,8));b+=strtonum(substr($0,39,8));c+=strtonum(substr($0,47,8));k++}($1=="ATOM"&&($3=="N"||$3=="CA"||$3=="CB"||$3=="C"||$3=="O")){print $0}' $ref > temp_1
awk '!($1=="ATOM"){print $0;next}(/ATOM/&&/CA  PRO/){printf "%s\n%s\n",$0,x}!(/CA  PRO/||/CB  PRO/){print}{x=$0}' temp_1 > temp_2
awk '/ENDMDL/{c=0}!($1=="ATOM"){print $0;next}/ATOM/{c++}!(/ALA/||/GLY/){printf "%s%7s%s%s%s\n",substr($0,1,4),c,substr($0,12,6),"SER",substr($0,21);next}(/ALA/||/GLY/){printf "%s%7s%s\n",substr($0,1,4),c,substr($0,12)}' temp_2 > cg_$ref
rm temp_1 temp_2

echo "Please read the nodes inside the script to ensure that input file has the right format"
