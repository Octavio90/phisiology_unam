#!/usr/bin/python

import string
from sys import argv,stderr,stdout
from os import popen,system
from os.path import exists


## example of input : pdb-file number-atom-start number-residue-start chain

assert( len(argv)>1)
pdbname = argv[1]

if len(argv) == 3:
   countatom = int(argv[2])
if len(argv) < 3:
   countatom = 1

if len(argv) == 4:
   countatom = int(argv[2])
   res_start = int(argv[3])
if len(argv) < 4:
   res_start = 0

if len(argv) == 5:
   countatom = int(argv[2])
   res_start = int(argv[3])
   chain = argv[4]
if len(argv) < 5:
   chain = 'A'

lines = open(pdbname,'r').readlines()


oldresnum = '   '
count = res_start
atco = 0
outid = stdout

for line in lines:
        line_edit = line
        if line[0:3] == 'TER':
            continue

        if line_edit[0:4] == 'ATOM' or line_edit[0:6] == 'HETATM':
            
            if not (line[16]==' ' or line[16]=='A'): continue

            resnum = line_edit[23:26]
            if not resnum == oldresnum:
                count = count + 1
            oldresnum = resnum

            newnum = '%3d' % count
            newatnum = '%s'% str(countatom+atco)
            if len(newatnum) == 5:
                pass
            if len(newatnum) == 4:
                newatnum = ' '+newatnum
            if len(newatnum) == 3:
                newatnum = '  '+newatnum
            if len(newatnum) == 2:
                newatnum = '   '+newatnum
            if len(newatnum) == 1:
                newatnum = '    '+newatnum
            atco += 1
            line_edit = line_edit[0:6]+newatnum+line_edit[11:22] +' '+ newnum + line_edit[26:]

            outid.write(line_edit)

outid.close()
