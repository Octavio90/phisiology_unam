#!/usr/bin/env python

from sys import argv
assert( len(argv)>1)

# This script eliminates the letters in the sequence
# This may only apply to the groll's proteasome structures 

inf1=open('%s'%argv[1],'r')
flag = 0
for i in inf1.readlines():
    if "END" in i or "TER" in i:
        print i.split('\n')[0]
    else: 
        sop = i.split()
        if flag < 1 and sop[2]=="N":
            num=sop[5]
            flag = 1
            j = i
        elif flag == 1:
            if sop[2]=="N":
               num = int(num)+1
            if len(str(num)) < len(sop[5]):
               j = i.replace("%s"%sop[5], "%s "%num) 
            if len(str(num)) > len(sop[5]):
               j = i.replace(" %s"%sop[5], "%s"%num)
            if len(str(num)) == len(sop[5]):
               j = i.replace("%s"%sop[5], "%s"%num)
        print j.split('\n')[0]
