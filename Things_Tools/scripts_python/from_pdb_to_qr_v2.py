#!/usr/bin/env python
import sys
import numpy as np

def fix_atom(at_name):
    at_name = at_name.strip(' ')
    if len(at_name) == 4 :
       new_name = at_name[1:]+at_name[0]
       at_name = new_name
    return at_name

def fix_resi(rs_name):
    return rs_name.strip(' ')

def sigma_to_radii(sigma):
    """Sigma value obtained from amber99sb as in gromacs"""
    return '%.4f'%(sigma*np.power(2,1./6)*5.)

def retain_atoms(pdbfile):
    dat = []
    for line in pdbfile:
        line = line.split('\n')[0]
        if line.split()[0] == "ATOM" or line.split()[0] == "HETATM":
           dat.append(line)
    return dat

def get_coordinates(in_line):
    return [float(in_line[30:38]),\
            float(in_line[38:46]),\
            float(in_line[46:54])]

def read_nonbonded():
    dat = {}
    for line in open(path_to_ff+'/'+'ffnonbonded.itp','r')\
        .readlines():
        line = line.split('\n')[0].split()
        if len(line) == 7 and line[4] == 'A' :
           dat[line[0]] = line[5]+'_'+line[6]
    return dat

def read_restype():
    dat = {}
    for line in open(path_to_ff+'/'+'residuetypes_for_qr.dat','r')\
        .readlines():
        line = line.split('\n')[0].split()
        dat[line[0]] = line[1]
    return dat

def get_resi_info(resfile,res_am,ato_am):
    resi_data = open(resfile,'r').readlines()
    flag = False
    for line in resi_data:
        line = line.split('\n')[0]
        if len(line) > 1 and line[0] == '[' and line.split()[1] == res_am:
           flag = True
           continue
        if flag and len(line.split()) > 1 and line.split()[0] == ato_am:
           amber_type = line.split()[1]
           amber_char = '%.4f'%float(line.split()[2])
           break
    return amber_type,amber_char

def get_atomtype(pdb_atom,pdb_res):
    resi_type = resitype_dic[pdb_res]
    if pdb_res == 'HIS':
       pdb_res = 'HIE'
    return get_resi_info(path_to_ff+'/'+resi_type,pdb_res,pdb_atom)

def get_atomrad(amber_atom_sig):
    return sigma_to_radii(amber_atom_sig)

def get_atomsig(amber_atom_type):
    return float(atomtype_dic[amber_atom_type].split('_')[0])

def get_atomeps(amber_atom_type):
    return float(atomtype_dic[amber_atom_type].split('_')[1])

def update_extrm(value,curr_0,curr_1):
    if value < curr_0:
       curr_0 = value
    if value > curr_1:
       curr_1 = value
    return [curr_0,curr_1]

def store_extrem():
    data = []
    if count > 0:
       temp = update_extrm(coords[0],curr[0],curr[1])
       data += temp
       temp = update_extrm(coords[1],curr[2],curr[3])
       data += temp
       temp = update_extrm(coords[2],curr[4],curr[5])
       data += temp
    else:
       data = [coords[0],coords[0],\
               coords[1],coords[1],\
               coords[2],coords[2]]
    return data

def base_length(val_min,val_max):
    return val_max-val_min

def grid_length():
    dim = []
    dim.append(base_length(curr[0],curr[1]))
    dim.append(base_length(curr[2],curr[3]))
    dim.append(base_length(curr[4],curr[5]))
    dim = np.array(dim)
    return dim+20,(dim+20)*1.333

def check_dime(cutoff=0.5):
    #c = 4 # for 4 processors
    c = 5 # for 5 processors
    grid_dimentions = []
    for val in fine_grid:
        grd_step = val
        cnt = 1
        while grd_step > cutoff:
              num = val
              dem = float(cnt*np.power(2,c+1)) + 1.0
              grd_step = num/dem
              cnt +=1
        grid_dimentions.append(int(dem))
    return grid_dimentions

infile = sys.argv[1]
resolution = float(sys.argv[2])
path_to_ff = 'amber99sb-ildn.ff'
atomtype_dic = read_nonbonded()
resitype_dic = read_restype()

pdb = open(infile,'r').readlines()
base_name = infile.split('.')[0]
pdbqr = open('%s.pqr'%base_name,'w')
pdbap = open('%s.prm'%base_name,'w')
pdbap.write('WAT OW  0.0000 1.7683 0.6364\n') # taken form apbs/tools/conversion/param/vparam/vparam-amber-parm94.dat
dones = []
pdb = retain_atoms(pdb)
count = 0
while count < len(pdb):
    line = pdb[count][:54]
    coords = get_coordinates(line)
    curr = store_extrem()
    atom = fix_atom(line[12:16])
    resi = fix_resi(line[17:20])
    atom_type,atom_chg = get_atomtype(atom,resi)
    atom_sig = get_atomsig(atom_type)
    atom_eps = get_atomeps(atom_type)
    atom_rad = get_atomrad(atom_sig)
    #print '%s%8s%7s'%(line,atom_chg,atom_rad)
    out_line = '%s%8s%7s'%(line,atom_chg,atom_rad)
    pdbqr.write('%s\n'%out_line)
    if not resi+"_"+atom in dones:
       dones.append(resi+"_"+atom)
       out_line_apo = '%s %s %s %s %s'%(line[17:20].strip(' '),\
                                        line[12:16].strip(' '),\
                                        atom_chg,\
                                        atom_rad,\
                                        atom_eps)
       pdbap.write('%s\n'%out_line_apo)
    count += 1
pdbqr.close()
pdbap.close()

#print "X min and max: %8.3f %8.3f"%(curr[0],curr[1])
#print "Y min and max: %8.3f %8.3f"%(curr[2],curr[3])
#print "Z min and max: %8.3f %8.3f"%(curr[4],curr[5])
fine_grid,coarse_grid = grid_length()
grid_dime = check_dime(resolution)

apbs_input = open('%s.in'%base_name,'w')
apbs_input.write('read\n')
apbs_input.write('       mol  pdb   %s.pdb\n'%base_name)
apbs_input.write('       parm flat  %s.prm\n'%base_name)
apbs_input.write('end\n')
apbs_input.write('ELEC   name           solv-elec\n')
apbs_input.write('       mg-auto\n')
#apbs_input.write('     dime     161 161 193\n')
#apbs_input.write('     cglen    107.4995 101.2506 119.4165\n')
#apbs_input.write('     fglen     83.2350  79.5592  90.2450\n')
apbs_input.write('       dime           %-4s %-4s %-4s\n'%(grid_dime[0],\
                                                           grid_dime[1],\
                                                           grid_dime[2]))
apbs_input.write('       cglen          %-9.4f %-9.4f %-9.4f\n'%(coarse_grid[0],\
                                                                 coarse_grid[1],\
                                                                 coarse_grid[2]))
apbs_input.write('       fglen          %-9.4f %-9.4f %-9.4f\n'%(fine_grid[0],\
                                                                 fine_grid[1],\
                                                                 fine_grid[2]))
apbs_input.write('       cgcent         mol 1\n')
apbs_input.write('       fgcent         mol 1\n')
apbs_input.write('       mol            1\n')
apbs_input.write('       lpbe            \n')
apbs_input.write('       bcfl           sdh\n')
apbs_input.write('       pdie           2.0000\n')
apbs_input.write('       sdie           78.5400\n')
apbs_input.write('       ion            -1.0  0.15  2.4700\n')
apbs_input.write('       ion            1.0  0.15  1.8680\n')
apbs_input.write('       srfm           smol\n')
apbs_input.write('       chgm           spl2\n')
apbs_input.write('       sdens          100.00\n')
apbs_input.write('       srad           1.40\n')
apbs_input.write('       swin           0.30\n')
apbs_input.write('       temp           298.15\n')
apbs_input.write('       calcenergy     total\n')
apbs_input.write('END\n')
apbs_input.write('\n')
apbs_input.write('ELEC   name           unso-elec\n')
apbs_input.write('       mg-auto\n')
apbs_input.write('       dime           %-4s %-4s %-4s\n'%(grid_dime[0],\
                                                           grid_dime[1],\
                                                           grid_dime[2]))
apbs_input.write('       cglen          %-9.4f %-9.4f %-9.4f\n'%(coarse_grid[0],\
                                                                 coarse_grid[1],\
                                                                 coarse_grid[2]))
apbs_input.write('       fglen          %-9.4f %-9.4f %-9.4f\n'%(fine_grid[0],\
                                                                 fine_grid[1],\
                                                                 fine_grid[2]))
apbs_input.write('       cgcent         mol 1\n')
apbs_input.write('       fgcent         mol 1\n')
apbs_input.write('       mol            1\n')
apbs_input.write('       lpbe            \n')
apbs_input.write('       bcfl           sdh\n')
apbs_input.write('       pdie           2.0000\n')
apbs_input.write('       sdie           1.0000\n')
apbs_input.write('       ion            -1.0  0.15  2.4700\n')
apbs_input.write('       ion            1.0  0.15  1.8680\n')
apbs_input.write('       srfm           smol\n')
apbs_input.write('       chgm           spl2\n')
apbs_input.write('       sdens          100.00\n')
apbs_input.write('       srad           1.40\n')
apbs_input.write('       swin           0.30\n')
apbs_input.write('       temp           298.15\n')
apbs_input.write('       calcenergy     total\n')
apbs_input.write('END\n')
apbs_input.write('\n')
apbs_input.write('APOLAR name           solv-apol\n')
apbs_input.write('       grid           0.3 0.3 0.3\n')
apbs_input.write('       mol            1\n')
apbs_input.write('       srfm           sacc\n')
apbs_input.write('       swin           0.3\n')
apbs_input.write('       press          0.2394    # convertion form paper value would be 0.23012\n')
apbs_input.write('       gamma          0.0085    # paper said near to zero.\n')
apbs_input.write('       srad           1.4       # 0.65 in alkane example. Paper said a bit smaller than 1.4\n')
apbs_input.write('       dpos           0.2       # 0.05 in paper. This probably needs increase "sdens" value\n')
apbs_input.write('       bconc          0.033428  # same as in paper\n')
apbs_input.write('       sdens          100.0     # important to control accuracy\n')
apbs_input.write('       temp           298.15\n')
apbs_input.write('       calcenergy     total\n')
apbs_input.write('END\n')
apbs_input.write('print elecEnergy solv-elec end\n')
apbs_input.write('print elecEnergy unso-elec end\n')
apbs_input.write('print apolEnergy solv-apol end\n')
apbs_input.write('quit\n')
apbs_input.write('\n')
apbs_input.close()
