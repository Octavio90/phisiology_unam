#!/usr/bin/env python

from openbabel import *
from pybel import *
import numpy as np
from sys import argv

assert( len(argv)>1)
realname = sys.argv[1]
molecule_name = realname.split('.')[0]   
molecule_format = realname.split('.')[1]   

for mymol in readfile("%s"%molecule_format,"%s.%s"%(molecule_name,molecule_format)):
     ax = []                                # Store coordinates 
     mx = []                                # Store atomicmass
     for atom in mymol:
        if not atom.atomicmass == 1.00794:  # Avoiding the Hydrogens
           ax.append(atom.coords)           
           mx.append(atom.atomicmass)
     mx = np.array(mx)
     ax = np.array(ax)
     mw = np.sum(mx)                        # Total mass
     b = []                                 # Store Partial masses
     for i in range(len(mx)):
         b.append(ax[i]*mx[i])
     b=np.array(b)
     be_mean = b.sum(axis=0)/mw             # Computing COM
     print "%.3f %.3f %.3f"%(be_mean[0],be_mean[1],be_mean[2])
