#!/usr/bin/env python
import numpy as np
atom_name = []
mean_values = []
tolerance_values = []
std_values = []
number_of_info = []
for file in open('list.txt','r').readlines(): # Read the file with the names of the files where the CA are stored
         # Get the values from each file.
         values = [ (float(line.split()[6]),float(line.split()[7]),float(line.split()[8])) for line in open('%s'%file.split('\n')[0],'r').readlines() if 'HETATM' in line ] 
         values = np.array(values)
         # Compute the average position and its std 
         val_mean = np.mean(values,axis=0) 
         val_sdt  = np.std(values,axis=0)
         # Compute the distance of each CA to the average position
         val_dis = [ val_mean-i for i in values]
         val_dis = np.array(val_dis)
         vl = np.sum(val_dis*val_dis,axis=1)
         dist_to_average= np.sqrt(vl)
         # Set the toleracne value to the maximun distance and stored all data
         tolerance_values.append(np.max(dist_to_average))
         mean_values.append((val_mean[0],val_mean[1],val_mean[2]))
         atom_name.append(file.split('.')[0])
         std_values.append(np.sqrt(np.sum(val_sdt*val_sdt)))
         number_of_info.append(len(values))

tolerance_values = np.array(tolerance_values)
number_of_info = np.array(number_of_info)
max_info_value = np.max(number_of_info)
for i in range(len(atom_name)):
    constrain = tolerance_values[i] + (tolerance_values[i]*(1-np.power((number_of_info[i]/max_info_value),0.333333)))
    print "%s\t%s\t%s\t%s\t0.0\t%s\t%s"%(atom_name[i], mean_values[i][0], mean_values[i][1], mean_values[i][2],std_values[i],constrain )
