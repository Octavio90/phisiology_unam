#!/usr/bin/env python
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


colors = ['b','g','r','y','k','c','m']

def smoothListGaussian(list,degree=5):
     window=degree*2-1
     weight=np.array([1.0]*window)
     weightGauss=[]
     for i in range(window):
         i=i-degree+1
         frac=i/float(window)
         gauss=1/(np.exp((4*(frac))**2))
         weightGauss.append(gauss)
     weight=np.array(weightGauss)*weight
     smoothed=[0.0]*(len(list)-window)
     for i in range(len(smoothed)):
         smoothed[i]=sum(np.array(list[i:i+window])*weight)/sum(weight)
     return smoothed

from sys import argv
assert( len(argv)>1)
in2 = argv[1]
columns = argv[2]
type_plot = argv[3]
if type_plot == 'smooth':
   try:
      save_info = argv[5]
      degree_smooth = int(argv[4])
   except:
      save_info = argv[4]
else:
      save_info = argv[4]

infil2=open('%s'%in2,'r')
for line in infil2.readlines():
     if not '@' in line and not '#' in line :
         line=line.split()
         for i in range(len(line)):
             exec "group_%s= []" % i
infil2.close()
infil2=open('%s'%in2,'r')
for line in infil2.readlines():
     if not '@' in line and not '#' in line :
         line=line.split()
         for i in range(len(line)):
             globals()["group_%s"%i].append(float(line[i]))

t=group_0
#t = range(len(group_0))
if i == 0:
   t = range(len(group_0))

if not columns == 'all':
   col_plot = [int(i) for i in columns.split(',') ]
else:
   col_plot = range(1,len(line))

fig = plt.figure()
ax = fig.add_subplot(111)
count_c = 0
for i in col_plot:
   if type_plot == 'hist':
      n, bins, patches = ax.hist( globals()["group_%s"%i] , 100, normed=1, facecolor='green', alpha=0.75)
      bincenters = 0.5*(bins[1:]+bins[:-1])
      # add a 'best fit' line for the normal PDF
      mu =  np.average(np.array(globals()["group_%s"%i]))
      sigma =  np.std(np.array(globals()["group_%s"%i]))
      y = mlab.normpdf( bincenters, mu, sigma)
      l = ax.plot(bincenters, y, 'r--', linewidth=1)
   if type_plot == 'plot' or type_plot == 'smooth':
      if type_plot == 'smooth':
         smoothed = smoothListGaussian(globals()["group_%s"%i],degree_smooth)
         #ax.plot(group_0, group_1[:5] +smoothed+group_1[-5:-1], color='%s'%colors[count_c], lw=3)
         ax.plot(t, globals()["group_%s"%i], '-', alpha=0.3, color='%s'%colors[count_c] )
         ax.plot(t[degree_smooth:(-1)*(degree_smooth-1)], smoothed, lw=3, color='%s'%colors[count_c])
         count_c += 1
      else:
         #ax.plot(range(len(globals()["group_%s"%i])), globals()["group_%s"%i], '-' )
         ax.plot(t, globals()["group_%s"%i], '-',lw=3 )

### HELP Fast and dirty ###
#ax.set_ylim(0.0,300)
###

if type_plot == 'scatter':
      ax.scatter( globals()["group_%s"%col_plot[0]], globals()["group_%s"%col_plot[1]])
if type_plot == 'error':
      degree_smooth=100
      smoothed = smoothListGaussian(globals()["group_%s"%i],degree_smooth)
      er_smoot = smoothListGaussian(globals()["group_%s"%(int(i)+1)],degree_smooth)
      t_to_plot = t[degree_smooth:(-1)*(degree_smooth-1)]
      lag = int(len(t_to_plot)/10)
      ax.errorbar( t_to_plot[::lag] , smoothed[::lag], yerr=er_smoot[::lag],lw=0.5 )
      ax.plot(t_to_plot, smoothed, lw=3)

if type_plot == 'hist2d':
   z,x,y = np.histogram2d(globals()["group_%s"%col_plot[0]],globals()["group_%s"%col_plot[1]],bins=50)
   # compute free energies
   F = -np.log(z)
   # contour plot
   extent = [x[0], x[-1], y[0], y[-1]]
   ax.contourf(F.T, 50, cmap=plt.cm.hot, extent=extent)
   #axcb = plt.colorbar(orientation="horizontal", cax = inset_axes(ax,\
   #                                              width="50%",\
   #                                              height="5%",\
   #                                              loc=1),\
   #                                              ticks=[-0.003, 0 , 0.003])
   #axcb.set_label('Correlation',size='11')
   #axcb.ax.set_xticklabels(['','',''],size='9')




if not save_info=='no':
   plt.savefig('plot_%s'%save_info,dpi=300,bbox_inches='tight')
   plt.grid()
   #plt.show()
else:
   plt.grid()
   plt.show()
