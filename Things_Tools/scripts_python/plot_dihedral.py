#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from sys import argv

assert( len(argv)>1)
pdbname = argv[1]
inf1=open('%s'%pdbname,'r')
a= argv[2]
b= argv[3]
c= argv[4]
d= argv[5]

def readcoor(row):
    x = [ row[i] for i in range(31,38)]
    for i in range(len(x)):
       if i == 0:
         xc = x[i]
       else:
         xc += x[i]
    y = [ row[i] for i in range(39,46)]
    for i in range(len(y)):
       if i == 0:
         yc = y[i]
       else:
         yc += y[i]
    z = [ row[i] for i in range(47,54)]
    for i in range(len(z)):
       if i == 0:
         zc = z[i]
       else:
         zc += z[i]
    return (float(xc),float(yc),float(zc))




atom = ('%s'%a,'%s'%b,'%s'%c,'%s'%d) # give the names in order of appereace
cor1=[]
cor2=[]
cor3=[]
cor4=[]
for line in inf1.readlines():
  i=line.split()
  if i[0]=='ATOM':
     if i[2] == atom[0]:
        cor1.append(readcoor(line))
     if i[2] == atom[1]:
        cor2.append(readcoor(line))
     if i[2] == atom[2]:
        cor3.append(readcoor(line))
     if i[2] == atom[3]:
        cor4.append(readcoor(line))

v1=(np.array(cor2)-np.array(cor1))
v2=(np.array(cor3)-np.array(cor2))
v3=(np.array(cor4)-np.array(cor3))

per1= [ np.cross(v2[i],v1[i]) for i in range(len(v2))]
per1=np.array(per1)
pper1= [ i*(1/np.sqrt(np.sum(i*i))) for i in per1 ]
per1=np.array(pper1)

per2= [ np.cross(v3[i],v2[i]) for i in range(len(v2))]
per2=np.array(per2)
pper2= [ i*(1/np.sqrt(np.sum(i*i))) for i in per2 ]
per2=np.array(pper2)

dot = np.array(per1)*np.array(per2)
dotf = [np.sum(i) for i in dot]
lena = range(len(dotf))
ave = np.arccos(np.array(dotf))*57.2957795
print '%.3f'%np.mean(ave),"+-",'%.3f'%np.std(ave)
for i in ave:
   print '%.3f'%i

plt.plot(lena, ave )
plt.axis('tight')
plt.show()
