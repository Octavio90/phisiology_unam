#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from sys import argv
assert( len(argv)>1)
in1=argv[1]
in2=argv[2]


inf1 = open('%s'%in1,'r').readlines()
amino_names = [ i.split()[0] for i in inf1 if len(i.split()) > 4 ]
p4 = [ float(i.split()[1]) for i in inf1 if len(i.split()) > 4 ]        
p3 = [ float(i.split()[2]) for i in inf1 if len(i.split()) > 4 ]        
p2 = [ float(i.split()[3]) for i in inf1 if len(i.split()) > 4 ]        
p1 = [ float(i.split()[4]) for i in inf1 if len(i.split()) > 4]        


ind = np.arange(len(amino_names))  # the x locations for the groups
width = 0.2       # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

rects4=ax.bar(ind, p4, width, color='g' )
rects3=ax.bar(ind+width, p3, width, color='r')
rects2=ax.bar(ind+(width*2), p2, width, color='y')
rects1=ax.bar(ind+(width*3), p1, width, color='b')

# add some
ax.set_ylabel('Scores')
ax.set_title('Overall residudes acceptance for beta 5 ROSETTA %s'%in2)
ax.set_xticks(ind+0.5)
v= [0, len(amino_names), 0, 1 ]
plt.axis(v)
ax.set_xticklabels( amino_names )
ax.legend( (rects4[0], rects3[0], rects2[0], rects1[0]), ( 'P4', 'P3', 'P2', 'P1') )
plt.savefig('OverallHisB5ROSETA_%s'%in2,dpi=300,bbox_inches='tight')

plt.show()
