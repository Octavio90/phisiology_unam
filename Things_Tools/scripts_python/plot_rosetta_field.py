#!/usr/bin/env python
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sys import argv
assert( len(argv)>1)
in1=argv[1]
in2=argv[2]
in3=argv[3]

infile=open('%s'%in1,'r')
data1 = [ float(i.split()[int(in2)]) for i in infile.readlines() if not '@' in i and not '#' in i ]
infile.close()
infile=open('%s'%in1,'r')
seq1 = [ i.split()[0] for i in infile.readlines() if not '@' in i and not '#' in i ]
infile.close()
data1 = np.array(data1)
print "  average     std" 
print np.average(data1),np.std(data1)

infile=open('%s'%in3,'r')
data2 = [ float(i.split()[int(in2)]) for i in infile.readlines() if not '@' in i and not '#' in i ]
infile.close()
infile=open('%s'%in3,'r')
seq2 = [ i.split()[int(in2)-1] for i in infile.readlines() if not '@' in i and not '#' in i ]
infile.close()
data2 = np.array(data2)
print "  average     std" 
print np.average(data2),np.std(data2)

##### Entropy 
entr1 = {}
for i in seq1:
  if not i in entr1.keys():
     entr1['%s'%i] = 1
  else:
     entr1[i] = entr1[i] + 1
for i in entr1.keys():
    entr1['%s'%i] = float(entr1['%s'%i]*(1.0/len(seq1)))
entr2 = {}
for i in seq2:
  if not i in entr2.keys():
     entr2[i] = 1
  else:
     entr2[i] = entr2[i] + 1
for i in entr2.keys():
    entr2['%s'%i] = float(entr2['%s'%i]*(1.0/len(seq2)))
entro1 = np.array(np.array(entr1.values()))*np.log2(np.array(entr1.values()))
entro2 = np.array(np.array(entr2.values()))*np.log2(np.array(entr2.values()))
entropy1 = np.sum(entro1)*(-1)
entropy2 = np.sum(entro2)*(-1)
print entropy1 , np.max(entr1.values())*len(seq1)
print entropy2 , np.max(entr2.values())*len(seq1)
tresh = np.max(entr2.values())
tresh2 = np.max(entr1.values())*0.5

######## Entropy seq
long = len(seq1[0])
for i in range(long):
    exec "pos_%s = {}" % i
    exec "entro_ami_%s = {}" % i
    alias = globals()["pos_%s"%i]
    alias_2 = globals()["entro_ami_%s"%i]
    for j in seq1:
       if not j[i] in alias.keys():
           alias[j[i]] = 1
           alias_2[j[i]] = 1
       else:
           alias[j[i]] = alias[j[i]] + 1
    suma = np.sum(np.array(alias.values()))
    for k in alias.keys():
       alias[k] = float(alias[k]*(1.0/suma))
       alias_2[k] = (-1.0)*alias[k]*np.log2(alias[k])

for i in range(long):
    exec "posH_%s = {}" % i
    exec "entroH_ami_%s = {}" % i
    alias = globals()["posH_%s"%i]
    alias_2 = globals()["entroH_ami_%s"%i]
    for j in seq2:
       if not j[i] in alias.keys():
           alias[j[i]] = 1
           alias_2[j[i]] = 1
       else:
           alias[j[i]] = alias[j[i]] + 1
    suma = np.sum(np.array(alias.values()))
    for k in alias.keys():
       alias[k] = float(alias[k]*(1.0/suma))
       alias_2[k] = (-1.0)*alias[k]*np.log2(alias[k])


##################
for i in entr1.keys():
   if entr1[i] >= tresh:
      entro_seq = []
      for j in range(long):
         alias_2 = globals()["entro_ami_%s"%j]
         entro_seq.append(alias_2[i[j]])
      print i , np.sum(np.array(entro_seq))
print "#######"
for i in entr1.keys():
   if entr1[i] >= tresh2:
      entro_seq = []
      for j in range(long):
         alias_2 = globals()["entro_ami_%s"%j]
         entro_seq.append(alias_2[i[j]])
      print i , np.sum(np.array(entro_seq))
print "#######"
for i in entr2.keys():
   if entr2[i] >= tresh*0.5:
      entro_seq = []
      for j in range(long):
         alias_2 = globals()["entroH_ami_%s"%j]
         entro_seq.append(alias_2[i[j]])
      print i , np.sum(np.array(entro_seq))
print "#######"




fig = plt.figure()
ax1=fig.add_subplot(211)
ax1.plot(range(len(data1)), data1)
ax1.set_title('avg. %s, std %s, S %s, MaxFrq %s, TotSeq %s '%( np.average(data1),np.std(data1), entropy1 , np.max(entr1.values())*len(seq1), len(entr1.keys()) ) )
ax2=fig.add_subplot(212)
ax2.plot(range(len(data1)), data2)
ax2.set_title('avg. %s, std %s, S %s, MaxFrq %s, TotSeq %s'%( np.average(data2),np.std(data2), entropy2 , np.max(entr2.values())*len(seq2), len(entr2.keys()) ) )

#plt.savefig('value%s_from_%s'%(in1.split('.')[0],str(in2)),dpi=300,bbox_inches='tight')
#plt.show()
