#!/usr/bin/env python
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from pylab import *
import matplotlib.pyplot as plt
from sys import argv
import matplotlib.mlab as mlab

def smoothListGaussian(list,degree=5):
     window=degree*2-1
     weight=np.array([1.0]*window)
     weightGauss=[]
     for i in range(window):
         i=i-degree+1
         frac=i/float(window)
         gauss=1/(np.exp((4*(frac))**2))
         weightGauss.append(gauss)
     weight=np.array(weightGauss)*weight
     smoothed=[0.0]*(len(list)-window)
     for i in range(len(smoothed)):
         smoothed[i]=sum(np.array(list[i:i+window])*weight)/sum(weight)
     return smoothed

def plot_labels():
    xl = raw_input("Please enter x_limits: ")
    yl = raw_input("Please enter y_limits: ")
    xlb = raw_input("Please enter x label: ")
    ylb = raw_input("Please enter y label: ")
    tlt = raw_input("Please enter title: ")
    return float(xl.split(',')[0]),float(xl.split(',')[1]),float(yl.split(',')[0]),float(yl.split(',')[1]),xlb,ylb,tlt

assert( len(argv)>1)
in1 = argv[1]
columns = argv[2]
type_plot=argv[3]
flag = False
if type_plot == 'smooth':
   try:
      degree_smooth = int(argv[4])
      save_info = argv[5]
      if argv[6] and argv[6] == 'edit':
         flag = True
   except:
      save_info = argv[4]
else:
      save_info = argv[4]
      if argv[5] and argv[5] == 'edit':
         flag = True

infile=open('%s'%in1,'r').readlines()


rcParams['legend.loc'] = 'best'
colors = ['b','r','g','y','k','c','m']
#colors = ['b','r','g','y','k']
#colors = ['b','r','g','k']
#colors = ['r','y','g','y','k','c','m']
markers = ['o','v','^','<','>','s']
lin_markers = ['-','--','-.']

fig = plt.figure()
ax = fig.add_subplot(111)
count_c = 0
count_m = 0
if len(columns.split(',')) == 1:
   col_0 = 0
   col_1 = int(columns)
else:
   col_0 = int(columns.split(',')[0])
   col_1 = int(columns.split(',')[1])

datanames=[]
for data_file in infile:
    print data_file,'color=' ,colors[count_c]
    data_file = data_file.split('\n')[0]
    group_0= []
    group_1= []
    group_2= []
    datanames.append(data_file)
    for line in open(data_file,'r').readlines():
        if not '#' in line:
           if not '@' in line:
              group_0.append(float(line.split('\n')[0].split()[col_0]))
              group_1.append(float(line.split('\n')[0].split()[col_1]))
              if type_plot == 'error':
                 group_2.append(float(line.split('\n')[0].split()[col_1+1]))
    if type_plot == 'scatter':
       ax.scatter(group_0 , group_1, color='%s'%colors[count_c] , marker='%s'%markers[count_m],s=80,label="%s"%data_file)
    if type_plot == 'smooth':
       smoothed = smoothListGaussian(group_1,degree_smooth)
       #ax.plot(group_0, group_1[:5] + smoothed+group_1[-5:-1], color='%s'%colors[count_c], lw=3)
       ax.plot(group_0, group_1,lw=2 ,color='%s'%colors[count_c],alpha=0.3)
       ax.plot(group_0[degree_smooth:(-1)*(degree_smooth-1)],smoothed, color='%s'%colors[count_c], lw=2)
       #ax.plot(range(len(group_0[degree_smooth:(-1)*(degree_smooth-1)])),smoothed, color='%s'%colors[count_c], lw=2)
    if type_plot == 'plot':
       ax.plot(group_0, group_1,lw=2 ,color='%s'%colors[count_c])
    if type_plot == 'hist':
       n, bins = np.histogram(group_1 , 100 , normed=True)
       #n, bins = np.histogram(group_1 , 100 , normed=True, range=(-1.3,0.9))
       #n, bins = np.histogram(group_1 , 100 , normed=True, range=(0.25,1.65))
       #n, bins = np.histogram(group_1 , 180 , normed=True, range=(-189,185))
       bins = np.array(bins)
       mptn = (bins[1]-bins[0])*0.5
       bins = bins[:-1]+mptn
       simb = '-'
       degree_smooth=5
       smoothed = smoothListGaussian(n/np.sum(n),degree_smooth)
       ax.plot(bins[degree_smooth:(-1)*(degree_smooth-1)],smoothed,ls=lin_markers[count_m],lw=3,color='%s'%colors[count_c])
       #ax.plot( bins, n/np.sum(n), simb , lw=1.5, color='%s'%colors[count_c])
    if type_plot == 'error':
       #ax.errorbar( group_0, group_1, yerr=group_2,lw=2, color='%s'%colors[count_c])
       degree_smooth=50
       error_bar_samp=int(len(group_0)*.01)
       smoothed = smoothListGaussian(group_1,degree_smooth)
       ax.errorbar( group_0[::error_bar_samp] , group_1[::error_bar_samp], yerr=group_2[::error_bar_samp],lw=2, elinewidth=1, color='%s'%colors[count_c] )
       #ax.plot(group_0[degree_smooth:(-1)*(degree_smooth-1)], smoothed, lw=2, color='%s'%colors[count_c])

    count_c += 1
    if count_c == len(colors):
       count_c = 0
       count_m += 1

#ax.set_ylim(-0.5,0.5)
#ax.set_ylim(0.4,2)
#ax.set_ylim(0.4,1.6)
#ax.set_xticklabels( [ int(i) for i in ax.get_xticks() ] , size='large')
#ax.set_yticklabels( [ '%.1f'%i for i in ax.get_yticks() ] , size='large')

if flag:
   xln,xlx,yln,ylx,xlb,ylb,tlt = plot_labels()
   ax.set_xlim(xln,xlx)
   ax.set_ylim(yln,ylx)
   ax.set_xticklabels( [ int(i) for i in ax.get_xticks() ] , size='large')
   ax.set_yticklabels( [ '%.2f'%i for i in ax.get_yticks() ] , size='large')
   title("%s"%tlt,  size='large')
   xlabel("%s"%xlb, size='large')
   ylabel("%s"%ylb, size='large')
#plt.grid()

#plt.legend(loc=2)
if not save_info == 'no':
   plt.savefig('%s'%save_info,dpi=300,bbox_inches='tight')
plt.show()
