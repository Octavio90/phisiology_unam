#!/usr/bin/env python2.6
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sys import argv
assert( len(argv)>1)
in1=argv[1]
in2=argv[2]

infil1=open('%s'%in1,'r').readlines()
infil2=open('%s'%in2,'r').readlines()
count = 0
for line in infil1:
     if not '@' in line and not '#' in line :
         if '&' in line:
             exec "group1_%s= []" % count
             exec "group2_%s= []" % count
             count += 1
count = 0
for line in infil1:
     if not '@' in line and not '#' in line :
         if '&' in line:
             count += 1
             continue
         line=line.split('\n')[0]
         line=line.split()
         globals()["group1_%s"%count].append(line[0])
         globals()["group2_%s"%count].append(line[1])
position = []
Energy = []
DEnergy = []
for line in infil2:
     if not '@' in line and not '#' in line :
         line=line.split('\n')[0]
         line=line.split()
         position.append(float(line[0]))       
         Energy.append(float(line[1]))       
         DEnergy.append(float(line[2]))       

fig = plt.figure()
ax = fig.add_subplot(111)
#plt.savefig('profile_1ns',dpi=300,bbox_inches='tight')
#fig1 = plt.figure()
#ax = fig.add_subplot(111)
for i in range(count):
   ax.plot( globals()["group1_%s"%i], globals()["group2_%s"%i] )
ax.errorbar(position, Energy, DEnergy,linewidth=5,color='k' , elinewidth=5,ecolor='k')
ax.grid()
if len(argv) == 4:
   plt.savefig('histopull_%sns'%argv[3],dpi=300,bbox_inches='tight')
   print " PLOT SAVED AS histopull_%sns"%argv[3]
   print " Rename if wanted"
   print "mv histopull_%s.png"%argv[3] 
else:
   plt.show()
