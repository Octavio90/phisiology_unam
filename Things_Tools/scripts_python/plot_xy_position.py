#!/usr/bin/env python2.6
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sys import argv
assert( len(argv)>1)


in2=argv[1]

infil2=open('%s'%in2,'r').readlines()
for line in infil2:
     if not '@' in line and not '#' in line :
         line=line.split()
         for i in range(len(line)):
             exec "group_%s= []" % i
for line in infil2:
     if not '@' in line and not '#' in line :
         line=line.split()
         
         for i in range(len(line)):
             globals()["group_%s"%i].append(float(line[i]))

fig = plt.figure()
ax = fig.add_subplot(211)
t=group_0
group_2 = np.array(group_2)
group_3 = np.array(group_3)
colors = group_0
plt.scatter( group_2, group_3,c=colors )
ax = fig.add_subplot(212)
group_4 = np.array(group_4)
group_1 = np.array(group_1)
plt.plot(group_0,group_1)
plt.plot(group_0,group_4)

if len(argv)==3:
   plt.savefig('plot_position',dpi=300,bbox_inches='tight')
   plt.show()
else:
   plt.show()
