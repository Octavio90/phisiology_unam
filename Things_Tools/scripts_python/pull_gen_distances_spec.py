#!/usr/bin/env python2.6
import numpy as np
import sys
import operator

class Get_frames():
    def __init__(self,time,target_dist,picked_dist=''):
       self.time = time
       self.target_dist = target_dist
       self.picked_dist = picked_dist

# This script returns a list containing 
# The number of the conf.gro file to use
# the distances between the lig and protein
# and the distance value that the user asked for.
# It takes 2 argument:
# 1 is the dist.xvg
# 2 is an list used in the first round (file genreated by gen_pull_distances.py).
# The script search for interpolation distances amomong the already existing ones
# EXEMPLE : gen_pull_distances_spe.py listof.frames listof.search

inf1 = sys.argv[1]
inf2 = sys.argv[2]
file1 = open('%s'%inf1,'r').readlines()
file2 = open('%s'%inf2,'r').readlines()
frames = [] # To store the frames from dist.xvg
target = [] # To store the distances that we what to look for 

for line in file1:
    line = line.split('\n')[0]
    line = line.split() 
    dist = float(line[4])
    print dist
    time = line[0].split('.')[0]
    option = Get_frames(time,dist)
    frames.append(option)

for i in range(len(file2)):
   dis1 = float(file2[i].split('\n')[0].split()[-1])
   dist = dis1
   target.append(dist)
   
for val in target:
    for fra in frames:
        fra.picked_dist = np.abs(fra.target_dist-val)
    sorted_x = sorted(frames, key=operator.attrgetter('picked_dist'))
#    frames = sorted_x
#   frame   deviation_from_target  distance_to_be_use target_distance      
    print '%-4s'%sorted_x[0].time, '%-3.3f'%sorted_x[0].picked_dist, '%-3.3f'%sorted_x[0].target_dist , '%-3.3f'%val
