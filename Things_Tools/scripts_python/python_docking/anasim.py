#!/usr/bin/env python

from openbabel import *
from pybel import *
import numpy as np
import argparse
from os.path import basename
from os.path import exists


##### Parser
parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="Read molecules in a the global MASTER KEY file, get its similar molecules and get the poses from the simulation runs ",
                                 add_help=True)


parser.add_argument("-sdf_files", help="sdf from docking results" );
parser.add_argument("-candidates", help="read the master_global output file" );
parser.add_argument("-lib", help="Which docking input librery file to analyze", default='hdu_03' )
parser.add_argument("-thres", help="Use the cluster definition generated with a tanimoto threshold", default=85 )
parser.add_argument("-overwrite", help="overwrite existing plot directories", default=False, action="store_true")

args = parser.parse_args()


infi2 = open('output_global_master/%s'%args.candidates,'r')
infi1 = open('input_for_master_key/%s'%args.sdf_files,'r')
sim_thres = args.thres


trace = []
for line in infi2.readlines():
   trace.append(line.split(',')[0])

list_of_runs = []
for line in infi1.readlines():
    run = line.split('.')[0]
    list_of_runs.append(run)

print trace
print list_of_runs
input_file ="../ligands_files/%s.sdf"%args.lib

count = 0
mol_name = []
mol_fp = []
for molecule in readfile("sdf", input_file):# THE INPUT FILE SHOULD BE IN PARSE
     count = count+1
     if not molecule.title in mol_name: # THIS SHOULD BE IN PARSE
        mol_name.append(molecule.title)  # Storing the fragment's name
        mol_fp.append(molecule.calcfp()) # Stoing the Fingerprint
print len(mol_name), len(mol_fp)
# Generation of the dictionary name and fingerprint
FFingerprint = {}
k = 0
for name in mol_name:
    FFingerprint[name] = mol_fp[k]
    k += 1

### Generating the sdf
code_pose = open('code_pose_%s_%s_.txt'%(args.lib,args.thres),'w')
for each in trace:
  if args.overwrite and exists( "poses_%s_similar_%s.sdf"%(each,args.lib) ):
          id=0
          new_pic="%s_%05d_%s"%(backup_prefix,id, "poses_%s_similar_%s.sdf"%(each,args.lib))
          while exists( new_pic ):
                  id = id + 1
                  new_pic="%s_%05d_%s"%(backup_prefix,id, "poses_%s_similar_%s.sdf"%(each,args.lib))
          shutil.move(  "poses_%s_similar_%s.sdf"%(each,args.lib), new_pic)
  if not exists( "poses_%s_similar_%s.sdf"%(each,args.lib) ):
    pybemol = Outputfile("sdf", "poses_%s_similar_%s.sdf"%(each,args.lib))
    for name_run in list_of_runs:
       count = 0
       for mol_x in readfile("sdf", "sdf_files_results/%s.sdf"%name_run):
           if count > 50:
              break
           elif mol_x.title == each:
                pybemol.write(Molecule(mol_x))
                code_pose.write('%s\t%s\t%s\t%s\n'%(each,mol_x.title,name_run,count+1))
           elif not mol_x.title == each:
                tanimoto = FFingerprint['%s'%each] | FFingerprint['%s'%mol_x.title]
                if tanimoto >= sim_thres: #similarity threshold
                       pybemol.write(Molecule(mol_x))
                       code_pose.write('%s\t%s\t%s\t%s\n'%(each,mol_x.title,name_run,count+1))
           count += 1
    pybemol.close()
  else:
     print "Nothing was done with %s"%input_file
code_pose.close()
