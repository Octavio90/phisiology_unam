# !/usr/bin/env python

from itertools import groupby
from openbabel import *
from pybel import *
from pyx import *
import numpy as np
import matplotlib.pyplot as plt

# Probably read it from a file would be better!!!!
thres = 40
inputfile= 'data_his_40c.txt'
input_groupnames= 'singletones_%s.txt'%str(thres)
get_data_from=open('%s'%input_groupnames,'r')
groups = []
groupss = []
for line in get_data_from.readlines():
   if '#' in line:
     continue
   else:
      temp = line.split()
      if '_' in temp[2]:
          rename = temp[2].split('_')
          groupss.append("%s%s%s"%(str(rename[0]),str(rename[1]),str(rename[2])))
          groups.append(temp[2])
      else:
          groups.append(temp[2])


# Generating the variables for storing each group
hisfile = open('%s'%inputfile,'r')
his_data = []

for j in hisfile.readlines():
   if 'MD' in j: 
      his_name= j.split()
   else:
      line= j.split()
      new_line = []
      for k in line:
          k = float(k)
          new_line.append(k)
      his_data.append(new_line)


his_data = np.array(his_data)

#for i in range(7):
#  print np.average(his_data[i,:]), np.std(his_data[i,:])

# Plot the histogram
fig = plt.figure()
width = 0.1 # width of the bar
ind = np.arange(len(groups)) # position in the x axis for the groups

ax = fig.add_subplot(111)
rec1 = ax.bar( ind, his_data[:,0], width, color='r')
rec2 = ax.bar( ind+width, his_data[:,1], width, color='r')
rec3 = ax.bar( ind+(width*2), his_data[:,2], width, color='r')
rec4 = ax.bar( ind+(width*3), his_data[:,3], width, color='b')
rec5 = ax.bar( ind+(width*4), his_data[:,4], width, color='b' )
rec6 = ax.bar( ind+(width*5), his_data[:,5], width, color='b')
#rec7 = ax.bar( ind+(width*6), his_data[:,6], width, color='r')
#rec8 = ax.bar( ind+(width*7), his_data[:,7], width, color='r')

plt.title('Histogram after docking.Thres %s. Input %s'%(thres,inputfile))
plt.xticks(ind+width, groups , rotation=45 )
#plt.axis([0, len(groups), 0, (max(his_data)+(max(his_data)/4.))])
plt.axis([0, len(groups), 0, 18])
#plt.text(2, 6, r'an equation: $E=mc^2$', fontsize=15, rotation=45)
plt.show()
#plt.savefig('histogram_docking_struc_%s'%thres,dpi=300,bbox_inches='tight')
