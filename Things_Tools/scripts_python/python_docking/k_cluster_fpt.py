#!/usr/bin/env python


from openbabel import *
from pybel import *
import numpy as np
from random import shuffle
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

# From an initial sdf file get the fragments and generate the fingerprint
# for each fragment. With an average distance divide in different groups.

#######################
###### Defenitions ####
#######################
def singletone_indentification(flags_list,k,FFingerprint): # flags vector, number of groups, fingerprint dictionary
 singletone_list = []
 for gr in range(k):
    grouplist = []
    values_ave = []
    promedio = [] # Promedio means average in spanish
    exec "group_%s= []" % gr # generating the group list
    for oth in flags_list:
        if oth[1] == gr: # condition for checking the group
           locals()["group_%s"%gr].append(oth[0])
    for each in locals()["group_%s"%gr]:      # Loops for getting all the tanimoto
        for other in locals()["group_%s"%gr]: # values of molecule with all the other
            if other == each:                      # molecules in the group. The average of
               continue                            # average of this values is stored for each
            else:                                  # molecule. The molecule with the highest average value
              tanimoto = FFingerprint['%s'%each] | FFingerprint['%s'%other] # is the choosen as the singletone
              promedio.append(tanimoto)                                     # of the group.
        grouplist.append([each, gr, np.average(promedio)])
    for val in grouplist:
        values_ave.append(val[2])
    position = values_ave.index(max(values_ave))
    singletone_list.append(grouplist[position])
 return singletone_list

def update(mol_name,centroid_list,k,FFingerprint): # all_name, centroids, number of groups, fingerprint dictionary
 group_flag_updated=[]                             # This loops just read a list of molecular names
 for gr in range(k):                      # and asign each molecule in this list to the gruoup
    exec "NEWgroup_%s= []" % ( gr )  # to which has the highest tanimoto coefficient.
 for each in mol_name:
    tanimoto_vec = []
    for centroid in centroid_list:
        tanimoto_vec.append(FFingerprint['%s'%each] | FFingerprint['%s'%centroid[0]])
    group = tanimoto_vec.index(max(tanimoto_vec))
    locals()["NEWgroup_%s"%int(group)].append([each, group, max(tanimoto_vec)])
    group_flag_updated.append([each, group, max(tanimoto_vec)])
 return group_flag_updated

###################################################
############### Main programm section #############
###################################################
mol_fp = []  # For storing the molecule fingerprint
mol_name = [] # For storing the molecule name
sim_thres = 0.90 # The similarity threshold. THIS SHOULD BE IN PARSE
thres = '90' # For an easy output naming.
input_file ="../ligands_files/HDU_03.sdf" #"../../general_libraries/fragments.sdf"
count = 0
for molecule in readfile("sdf", input_file):# THE INPUT FILE SHOULD BE IN PARSE
     count = count+1    
#   if count < 501:
     print count 
     if not molecule.title in mol_name: # THIS SHOULD BE IN PARSE
        mol_name.append(molecule.title)  # Storing the fragment's name
        mol_fp.append(molecule.calcfp()) # Stoing the Fingerprint
print len(mol_name), len(mol_fp)
# Generation of the dictionary name and fingerprint
FFingerprint = {}
k = 0
for name in mol_name:
    FFingerprint[name] = mol_fp[k]
    k += 1
# The shuffle will not be necessary in real cases.
shuffle(mol_name)
# FLAGS VECTOR GENERATION.
flags_list=[]
# POS 0 = FRAGMENT'S NAME,
# POS 1 = GROUP AT WHICH THE FRAGMENT IS ASSIGNED.
# POS 2 = FLAG THAT INDICATES THAT THE FRAGMENT WAS ALREADY COMPARED.
for name in mol_name:
  flags_list.append([name, -1, 0])
np.array(flags_list) # just for easy handling.
gru = 0 # for indicate the group
pos = 0 # cycle counter
pos_old = 0 # cycle counter for storing
for each in mol_name:
    for other in flags_list:
     if other[1] > -1 or other[0] == each or other[2] > 0:
        other[2] = 2
        continue
     tanimoto = FFingerprint['%s'%each] | FFingerprint['%s'%other[0]]
     if tanimoto >= sim_thres: #similarity threshold
        if pos-pos_old >= 1:   # if the cycle of the first "for" has changed then
            gru=gru+1          # increase the group assignment.
        if pos-pos_old == 0:
			pass
        other[1] = gru
#        print "%s\t%s\t%s\t%s "%(gru, pos, each, other[0])
        pos_old = pos
    pos = pos + 1
##### Renaming the "unknowns" group and getting the total number of groups
vector = []
thereiszero = 1
for x in flags_list: # Get the biggest group number and check if there is zero group
  vector.append(x[1])
  if x[1] == 0: thereiszero = 0
max_group_value = sorted(set(vector), reverse= True)
if thereiszero == 0:
   for x in flags_list:
       if x[1] == -1: x[1] = max_group_value[0]+1
   nun_of_gr = max_group_value[0]+1
else:
   for x in flags_list:
       if x[1] == -1: x[1] = 0
   nun_of_gr = max_group_value[0]
k = nun_of_gr+1 # Number of Clusters
#### Identification of singletones
centroid_list = singletone_indentification(flags_list,k,FFingerprint)
#for x in centroid_list:
#    print x[0], x[2]
### Using the singletones from the last step, reasign the original list
new_distribution = update(mol_name,centroid_list,k,FFingerprint)
# Loop to search for a high-distance clustering
# the "cluster distance" is intented to measure the similarity within a given cluster
# by adding the averaged tanimoto values of all the elements in the cluster. Each molecule has
# has an averaged tanimoto value, this value is obtained by averaging all the tanimoto coefficients
# of a given molecule with all the molecules in the cluster. So the higher the better similarity.
cluster_distance = 0.0
new_cluster_distance = 0.0
times = 0
while True:
  for y in new_distribution:
      new_cluster_distance = new_cluster_distance+y[2]
  if  new_cluster_distance > cluster_distance and times < 10:
      cluster_distance = new_cluster_distance
      distribution = new_distribution
      new_centro = singletone_indentification(new_distribution,k,FFingerprint)
      new_distribution = update(mol_name,new_centro,k,FFingerprint)
#      print times, cluster_distance
      new_cluster_distance = 0.0
      print times
      times = times + 1
  else:
	  break
print 'Finally'

####################################################
############## Final output file and plots #########
####################################################
# Generating the histogram
fd=open("singletones_%s.txt"%thres,'w')
fd.write("##### This singletones comes from file in : %s\n"%input_file)
group_counting = []
for y in distribution:
	group_counting.append(y[1])
data = {}
for i in set(group_counting):
    data[i] = group_counting.count(i)
#print data
his_data = []
for v in range(len(data)):
   his_data.append(data[v])
label_for_plot=[]
c = 0
fd.write("Group \t Name \t tanimoto group  #elements\n")
group_name_num = {}
for x in new_centro:
          fd.write('%s  :   %s\t %.3f\t %s\n'%(c,x[0],x[2],his_data[c]))
          label_for_plot.append(x[0])
          group_name_num['%s'%x[0]] = c
          c = c+1
fd.close()
# Plot the histogram
width = 0.7 # width of the bar
ind = np.arange(len(data)) # position in the x axis for the groups
#pp = PdfPages('k_cluster_groups.pdf')
plt.bar( ind, his_data, width, color='r')
plt.title('Histogram k cluster groups. Threshold %s'%sim_thres)
plt.xticks(ind, label_for_plot , rotation=45 )
plt.axis([0, len(data), 0, (max(his_data)+(max(his_data)/4.))])
#plt.text(2, 6, r'an equation: $E=mc^2$', fontsize=15, rotation=45)
plt.savefig('histogram_k_means_%s'%thres,dpi=300,bbox_inches='tight')
#plt.show()
#parser = argparse.ArgumentParser(prog=basename(__file__),
#                                 fromfile_prefix_chars='@',
#                                 description="setup bunch of lambda-step directories",
#                                 add_help=True)
#
#
#parser.add_argument("-lambda_file", help="store rmsd improvement due to ring-currents", default="LAMBDAS" );
#parser.add_argument("-lambdas", nargs="*" );
#parser.add_argument("-overwrite", help="overwrite existing lambda directories", default=False, action="store_true")
#parser.add_argument("-itp", help="the itp file with the ligand- A and B topologies", required=True )
#parser.add_argument("-mdp", help="the template mdp-file with FEP_XXX tags for replacement", required=True )
#
#args = parser.parse_args()
get_data_from=open("singletones_%s.txt"%thres,'r')
group_name = []
for line in get_data_from.readlines():
   if '#' in line:
     continue
   else:
      temp = line.split()
      group_name.append(temp[2])

for gname in group_name:
   list_class = open('group_%s'%gname,'w')
   for a in distribution:
      if a[1] == group_name_num['%s'%gname]:
         list_class.write("%s \n"%a[0])
   list_class.close()
