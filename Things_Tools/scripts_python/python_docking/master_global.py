#!/usr/bin/env python
from pylab import *

from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists

from openbabel import *
from pybel import *
import argparse
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import math
from matplotlib.ticker import NullFormatter

##### Parser
parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="Read MASTER KEY and plot the rmsd vs docking, with freq and lambda-k as colors ",
                                 add_help=True)


parser.add_argument("-master_files", help="Master key files from the master_key_output folder", default="all_masterkeys_b5.txt" );
parser.add_argument("-single_master", nargs="*");
parser.add_argument("-freq", help="Master key files from the master_key_output folder", default=6 );
parser.add_argument("-rmsd", help="Master key files from the master_key_output folder", default=3.5 );
parser.add_argument("-dock", help="Master key files from the master_key_output folder", default=-7.0 );
parser.add_argument("-overwrite", help="overwrite existing plot directories", default=False, action="store_true")

args = parser.parse_args()


rerank_prefix="reranking_"
backup_prefix="zzz_backup_"

if args.single_master:
        single_master=args.single_master
else:
       fsdf = open('%s'%args.master_files,'r')
       single_master= [line for line in fsdf.readlines()]
       fsdf.close()

global_candidates = []
for infile in single_master:
        input_file = infile.split('.')[0]
        data_master = open('output_master_key/%s'%infile.split()[0],'r')
        data = [ i for i in data_master.readlines() if not '#' in i ]
        candid = [ i for i in data if float(i.split()[1]) >= args.freq and float(i.split()[2]) <= args.rmsd and float(i.split()[3]) <= args.dock ]
        if len(candid) >= 1:
           for i in range(len(candid)):
              candidates = candid[i].split()
              candidates.append('%s'%input_file)
              global_candidates.append(candidates)
#              global_candidates.append(['%s'%input_file,'%s'%candid[i].split()[0]])
mol_run=[]
for i in global_candidates:
   mol_run_code = []
   mol_run_code.append(i[0])
   for infile in single_master:
       input_file = infile.split('.')[0]
       if i[6] == input_file:
         mol_run_code.append(1)
       else:
         mol_run_code.append(0)
   mol_run.append(mol_run_code)
please= []
names_mol = []
names_run = [ a.split('.')[0] for a in single_master]
for i in range(len(mol_run)):
  if not mol_run[i][0] in names_mol:
    final = []
    final_entry = mol_run[i]
    fin = [ final_entry[x] for x in range(1,len(final_entry))]
    final.append(fin)
    done = 'no'
    for j in range(1,len(mol_run)):
       if mol_run[i][0] == mol_run[j][0] and j > i:
          done='yes'
          names_mol.append(mol_run[j][0])
          tempj = mol_run[j]
          finj = [ tempj[x] for x in range(1,len(tempj))]
          final.append(finj)
    final=np.array(final)
    if done == 'yes':
       final=np.sum(final,axis=0)
#### to final output
       promi=np.sum(final)
       for x in range(len(global_candidates)):
           if mol_run[i][0] == global_candidates[x][0]:
              global_candidates[x].append(promi)
#############
    elif done == 'no':
       names_mol.append(mol_run[i][0])
       final=final[0]
#### to final output
       for x in range(len(global_candidates)):
           if mol_run[i][0] == global_candidates[x][0]:
              global_candidates[x].append(promi)
############
    please.append(final)
#######################
###### write output#### 
fo=open('global_master_%s'%args.master_files,'w')
for x in range(len(global_candidates)):
   a = [ str(i) for i in (global_candidates[x])]
   fo.write('%s\n'%(',').join(a))
fo.close()
###############################
####################### Gen the docking vectors
dock = np.zeros([np.shape(please)[0],np.shape(please)[1]])
names = []
count=0
for n in names_mol:
    if not n in names:
       names.append(n)
       count += 1
for x in range(len(names)):
  for y in range(len(global_candidates)):
    for z in range(len(single_master)):
        input_file = (single_master[z]).split('.')[0]
        if names[x] == global_candidates[y][0] and input_file == global_candidates[y][6]:
           dock[x][z]= global_candidates[y][3]
###############################################
######## To Draw the molecules ################
pybemol = Outputfile("sdf", "global_candidates_%s.sdf"%(args.freq))
nombres=[]
for element in names:
    for mymol in readfile("sdf", "../ligands_files/HDU_00.sdf"):
            if mymol.title == element and not mymol.title in nombres:
                pybemol.write(Molecule(mymol))
                nombres.append(mymol.title)
                continue
pybemol.close()


##############################
######## Plot ###############
please = np.array(please)
to_plot= np.shape(please)
dock=dock*-1.0
dock=dock/np.max(dock)
subplot(1,1,1)
c = pcolor(dock)
#c = pcolor(please, edgecolors='k', linewidths=3)
ind = np.arange(to_plot[1] )
moles = np.arange(to_plot[0])
title('Docking bitmap')
xticks(ind+0.5 , names_run , rotation=90 )
xlabel('Docking runs')
axis([0, to_plot[1], 0, to_plot[0]])
#yticks(moles, names_mol , rotation=45 )
ylabel('molecule candidates')
savefig('Docking_MAP_%s'%args.freq,dpi=300,bbox_inches='tight')
close()

#show()
