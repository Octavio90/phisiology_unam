#!/usr/bin/env python
from pylab import *
from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists

from openbabel import *
from pybel import *
import argparse
import matplotlib.pyplot as plt
import numpy as np
import math

##### Parser
parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="Read MASTER KEY and plot the rmsd vs docking, with freq and lambda-k as colors ",
                                 add_help=True)

parser.add_argument("-master_files", help="Master key files from the master_key_output folder", default="masterkeys_hdu02.txt" );
parser.add_argument("-single_master", nargs="*");
parser.add_argument("-freq", help="Master key files from the master_key_output folder",type=float ,default=4 );
parser.add_argument("-rmsd", help="Master key files from the master_key_output folder",type=float ,default=2.0 );
parser.add_argument("-dock", help="Master key files from the master_key_output folder",type=float , default=-8.0 );
parser.add_argument("-scale", help="Master key files from the master_key_output folder", default='color' );
parser.add_argument("-overwrite", help="overwrite existing plot directories", default=False, action="store_true")
parser.add_argument("-writesdf", help="write sdf of the global candidates", default=False, action="store_true")
parser.add_argument("-save", help="save the image and the output file ", default=False, action="store_true")

args = parser.parse_args()

#############################################
############ Definition #####################
#############################################
## give the index of the property to plot from the global_candidates list

def get_matrix(property_key):
    property_name = np.zeros([len(names),len(single_master)])
    property_name = property_name+1
    for x in range(len(names)):
        for y in range(len(global_candidates)):
            for z in range(len(single_master)):
                input_file = (single_master[z]).split('.')[0]
                if names[x] == global_candidates[y][0] and input_file == global_candidates[y][6]:
                   property_name[x][z]= global_candidates[y][property_key]
    return property_name

#def get_rescore(name):
#    for pose in sdfile:
#        mean_val = []
#        sdt_val = []
#        if name == pose.title:
           

def get_complete_chart(property_key):
    property_name = np.zeros([len(names),len(single_master)])
    property_name = property_name+1
    for z in range(len(single_master)):
        input_file = (single_master[z]).split('.')[0]
        data_master = open('output_master_key/%s.txt'%input_file,'r')
        data = [ i for i in data_master.readlines() if not '#' in i ]
        for x in range(len(names)):
            for y in range(len(data)):
                if names[x] == data[y].split()[0]:
                   property_name[x][z] = data[y].split()[property_key]
    return property_name

##############################################
##############################################
##############################################

rerank_prefix="reranking_"
backup_prefix="zzz_backup_"

if args.single_master:
        single_master=input_for_master_global/args.single_master
else:
       fsdf = open('input_for_master_global/%s'%args.master_files,'r')
       single_master= [line for line in fsdf.readlines()]
       fsdf.close()
output_code = 'frd%s%s%s'%(str(args.freq).split('.')[0],str(args.rmsd).split('.')[0],str(args.dock).split('.')[0])
global_candidates = []

if args.overwrite and exists('output_global_master/global_master_%s_%s'%(output_code,args.master_files) ):
        id=0
        new_pic="%s_%05d_%s"%(backup_prefix,id,'output_global_master/global_master_%s_%s'%(output_code,args.master_files))
        while exists( new_pic ):
                id = id + 1
                new_pic="%s_%05d_%s"%(backup_prefix,id,'output_global_master/global_master_%s_%s'%(output_code,args.master_files))
        shutil.move( 'output_global_master/global_master_%s_%s'%(output_code,args.master_files), new_pic)
if not exists( 'output_global_master/fakeglobal_master_%s_%s'%(output_code,args.master_files) ):
     for infile in single_master:                                                                                                                       
             input_file = infile.split('.')[0]
             data_master = open('output_master_key/%s'%infile.split()[0],'r')
             data = [ i for i in data_master.readlines() if not '#' in i ]
             candid = [ i for i in data if float(i.split()[1]) >= args.freq and float(i.split()[2]) <= args.rmsd and float(i.split()[3]) <= args.dock ]
             if len(candid) >= 1:
                for i in range(len(candid)):
                   candidates = candid[i].split()
                   candidates.append('%s'%input_file)
                   global_candidates.append(candidates)
     data_master.close()
     print global_candidates[0]

     ###################################################
     ######## IMportant to get_matrix DEFINITION########

     names = []
     count=0
     molmes = [global_candidates[i][0] for i in range(len(global_candidates)) ]
     for n in molmes:
       if not n in names:
           names.append(n)
           count += 1
     names_run = [ (single_master[z]).split('.')[0] for z in range(len(single_master)) ]
     
     ##################################
     ##### GEN the docking vectors#####
     
     dock = get_matrix(3)
     all_dock = get_complete_chart(3)
     dock = np.abs(dock)
     all_dock = np.abs(all_dock)
     freq = get_matrix(1)
     all_freq = get_complete_chart(1)
     rmsd = get_matrix(2)
     all_rmsd = get_complete_chart(2)
     lamb = get_matrix(4)
     all_lamb = get_complete_chart(4)
     
     ########### Get COLOR ############
     
     all_color = (all_dock*all_lamb/5)-(2*all_rmsd/all_freq)
     dic = {'dock':all_dock,'freq':all_freq,'rmsd':all_rmsd,'lamb': all_lamb,'color':all_color}

     ###############################################
     ######## To Draw the molecules ################

     if args.writesdf:
          pybemol = Outputfile("sdf", "global_candidates_%s.sdf"%(output_code))
          nombres=[]
          for element in names:
              for mymol in readfile("sdf", "../ligands_files/HDU_03.sdf"):
                      if mymol.title == element and not mymol.title in nombres:
                          pybemol.write(Molecule(mymol))
                          nombres.append(mymol.title)
                          continue
          pybemol.close()

     ##########################
     ###### write output ######

     if args.save:
          fo=open('global_master_%s_%s'%(output_code,args.master_files),'w')
          for x in range(len(global_candidates)):
             a = [ str(i) for i in (global_candidates[x])]
             fo.write('%s\n'%(',').join(a))
          fo.close()
     else:
          for x in range(len(global_candidates)):
             a = [ str(i) for i in (global_candidates[x])]
             print a

     ##############################
     ######## plot ###############

     subplot(1,1,1)
     c = pcolor(dic['%s'%args.scale])
     ind = np.arange(np.shape(dock)[1] )
     moles = np.arange(np.shape(dock)[0])
     title('docking bitmap %s'%args.scale)
     xticks(ind+0.5 , names_run , rotation=90 )
     xlabel('docking runs')
     axis([0, np.shape(dock)[1], 0, np.shape(dock)[0]])
     ylabel('molecule candidates')
     colorbar()
     if args.save:
       savefig('docking_map_%s'%output_code,dpi=300,bbox_inches='tight')
       close()
     else:
       show()                                                        
else:
    print "Nothing was done with %s"%(args.master_files)
