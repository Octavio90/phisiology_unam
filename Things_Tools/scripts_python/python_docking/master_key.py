#!/usr/bin/env python

from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists

from openbabel import *
from pybel import *
import argparse
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import math
from matplotlib.ticker import NullFormatter

##### Parser
parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="Read the top 500 poses from the original Docking output-file and writea MASTER KEY file. The KEY is a vector containing (name,freq,rmsd,docking_score,lambda_k)",
                                 add_help=True)


parser.add_argument("-sdf_files", help="sdf from docking results", default="SDFs" );
parser.add_argument("-single_sdf", nargs="*" );
parser.add_argument("-lib", help="Which docking input librery file to analyze", default='hdu_03' )
parser.add_argument("-thres", help="Use the cluster definition generated with a tanimoto threshold", default=60 )
parser.add_argument("-overwrite", help="overwrite existing plot directories", default=False, action="store_true")

args = parser.parse_args()


rerank_prefix="reranking_"
backup_prefix="zzz_backup_"

# The idea of this program is to read the top 500
# ligands from the original Docking output file and
# and write a MASTER KEY file. The KEY is a vector
# containing : (name,freq,rmsd,docking_score)
#################################################
######### Section for define cluster groups ######
#################################################
input_groupnames= 'output_k_cluster/output_%s/singletones_%s.txt'%(args.lib,str(args.thres)) # input file generated the k_clust_ftp.py
get_data_from=open('%s'%input_groupnames,'r')
groups = []                    # This stores the names of the groups and the lamdba value
#groupss = []                   # This if trick to store problematic names
group_wg= []                   # Stores lambda-cluster value
for line in get_data_from.readlines():
   if '#' in line:
     continue
   else:
      temp = line.split()
      if '_' in temp[2]:
          rename = temp[2].split('_')
#          groupss.append("%s%s%s"%(str(rename[0]),str(rename[1]),str(rename[2])))
          groups.append(temp[2])
      else:
          groups.append(temp[2])
      group_wg.append(temp[4])

total = 0
for value in group_wg:
    total += float(value)

for i in range(len(group_wg)):
    group_wg[i] = float(group_wg[i])/total
for name in groups:
        exec "group_%s= []" % ( name )
##############################################
############ Begin fomal script ##############
#############################################
if args.single_sdf:
        single_sdf=input_for_master_key/args.single_sdf
else:
       fsdf = open('input_for_master_key/%s'%args.sdf_files,'r') 
       single_sdf= [line for line in fsdf.readlines()]
       fsdf.close()

for infile in single_sdf:
        mol_freq = {}
        his_data = []
        master_key = [] 
        input_file = infile.split('.')[0]
        if args.overwrite and exists( 'output_master_key/master_key_%s.txt'%infile.split('.')[0] ):
                id=0
                new_pic="output_master_key/%s_%05d_%s"%(backup_prefix,id,'master_key_%s.txt'%infile.split('.')[0])
                while exists( new_pic ):
                        id = id + 1
                        new_pic="output_master_key/%s_%05d_%s"%(backup_prefix,id,'master_key_%s.txt'%infile.split('.')[0])
                shutil.move( 'output_master_key/master_key_%s.txt'%infile.split('.')[0], new_pic)
        if not exists( 'output_master_key/master_key_%s.txt'%infile.split('.')[0] ):
                print "%s"%input_file                  # print name of the input file
                count = 0                               
                for mymol in readfile("sdf", "sdf_files_results/%s.sdf"%input_file):# take jus the first 500 poses
                    if count < 500:  
                            cluster_assig = '0'                          # for cluster assigent control                                                 
                            if not mymol.title in mol_freq.keys():       # if the molecules is new
                                mol_freq['%s'%mymol.title]= 1            # Start the frequency count
                                exec "coord_%s= []" % ( mymol.title )
                                for atom in mymol:
                                    if not atom.atomicmass == 1.00794:                              # avoid the count of Hydrogens
                                      globals()["coord_"+"%s"%mymol.title].append(atom.coords)      # Store the coodinates as
                                globals()["coord_"+"%s"%mymol.title] = np.array(globals()["coord_"+"%s"%mymol.title]) # numpy array
###########################################
########### section for cluster ###########
###########################################
                                for name_grp in groups:
                                   if cluster_assig == '0':
                                       group_file = open('output_k_cluster/output_%s/groups_%s/group_%s'%(args.lib,args.thres,name_grp),'r')
                                       for element in group_file.readlines():
                                           if mymol.title == element.split()[0]:
                                               cluster_assig = name_grp
                                               globals()["group_"+"%s"%name_grp].append(mymol.title)
                                               break
                                   else:
                                       break
###########################################
############# Continuation  ###############
###########################################
                                master_entry = [mymol.title,1,1,1,1,cluster_assig]   # setting the master record (name,freq,rmsd,docking_score)
                                master_key.append(master_entry)
                            else:                                   # if the molecule exit already in the record
                                mol_freq['%s'%mymol.title] += 1     # add one othe frequency 
                                coord_temp = []             
                                for atom in mymol:
                                    if not atom.atomicmass == 1.00794:
                                      coord_temp.append(atom.coords)              # Get the coordinates of molecule that is reappering
                                coordina = globals()["coord_"+"%s"%mymol.title]   # call the coordinates of the first pose
                                rmsd_temp = (coord_temp-coordina)*(coord_temp-coordina) # compute the rmsd 
                                rmsd_temp = rmsd_temp.sum(axis=0)                       # between the the two poses
                                rmsd_temp = rmsd_temp.sum(axis=0)/len(coord_temp)       
                                rmsd_temp = np.sqrt(rmsd_temp)
                                for i in range(len(master_key)):
                                  if master_key[i][0]== mymol.title and master_key[i][1] == 1: # if it is the second pose found 
                                       master_key[i][2]= rmsd_temp                             # for this ligand then store directly the rmsd
                                       master_key[i][1]= mol_freq['%s'%mymol.title] 
                                  elif master_key[i][0]== mymol.title and master_key[i][1] > 1:  # if it is the third or higher
                                       master_key[i][2]= rmsd_temp + master_key[i][2]      # store the average
                                       master_key[i][1]= mol_freq['%s'%mymol.title] 
                            count += 1
                    else:
                        break   
                print len(master_key)
#####################################################
################## Setting master_key[x][2]##########
                for entry in range(len(master_key)):
                    master_key[entry][2] = master_key[entry][2]/master_key[entry][1]

######################################################
################## Settting master_key[x][3]##########
                false = 1               # Flag to indicate that the file begin
                count = 0
                store = []
                infil = open("sdf_files_results/%s.sdf"%input_file,'r')  # Open the results and take the best docking score to complete 
                for line in iter(infil):                                 # the master record 
                    if false == 1:                                       
#                       print line
                       a = line.split('\n')[0]
                       store.append(a)                             # Store the name of the first pose
                       false += 1
                    elif count < 500 and "glide_gscore" in line: 
                            count += 1
#                           print infil.next()
                            a = infil.next().split('\n')[0]        
                            store.append(a)                        # Store the Docking score
                    elif count < 500 and "$$$" in line: 
#                            print infil.next()
                            a = infil.next().split('\n')[0]        # Store the name of the pose
                            store.append(a)  
                infil.close()
                pos = 0      
                for i in store[::2]: 
                  for j in range(len(master_key)):
                    if i == master_key[j][0] and master_key[j][3]== 1:
                        master_key[j][3] = store[1::2][pos]             # Store in the master key just the best score
                  pos += 1 
####################################################################     
########################### Setting master_key[x][4] ###############
                i = 0
                for v in groups:
                   number_in_group = [ float(master_key[a][1]) for a in range(len(master_key)) if master_key[a][5] == v ]
                   for x in range(len(master_key)):
                       if master_key[x][5] == v:
                          master_key[x][4] = (np.sum(number_in_group)*1.0/500)/(group_wg[i])
                   i += 1
#####################################################################
############# Writing output file ##################################                
                out_files = open('master_key_%s.txt'%infile.split('.')[0],'w')  # Creates a master key file
                out_files.write('# Mol name   Frequency   RMSD   BestScore   lambdaKluster   Kluster\n')
                for x in range(len(master_key)):
                   out_files.write(('%s  %s  %s  %s  %s  %s\n')%(master_key[x][0],master_key[x][1],master_key[x][2],master_key[x][3],master_key[x][4],master_key[x][5]) )
                out_files.close()
 
##                foa v in mol_freq.keys():
#                   his_data.append(mol_freq['%s'%v])
#                   if mol_freq['%s'%v] > int(args.freq):
#                     print v, mol_freq['%s'%v], input_file
                
                # Plot the histogram
#                width = 0.7 # width of the bar
#                ind = np.arange(len(mol_freq.keys())) # position in the x axis foa the group
#                plt.bar( ind, his_data, width, color='r')
#                plt.title('Histogram after docking. Input %s'%(input_file))
#                plt.xticks(ind, mol_freq.keys() , rotation=45 )
#                plt.axis([0, len(mol_freq.keys()), 0, (max(his_data)+(max(his_data)*(0.25)))])
                #plt.axis([0, len(names), 0, 7])
                #plt.text(2, 6, r'an equation: $E=mc^2$', fontsize=15, rotation=45)
#                plt.savefig('reranking_%s'%(input_file),dpi=300,bbox_inches='tight')
        else:
           print "Nothing was done with %s"%input_file
