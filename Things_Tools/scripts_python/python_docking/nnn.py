# !/usr/bin/python

from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists

from openbabel import *
from pybel import *
import argparse
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import math

####### Description #######
# This script reads the master_key files and add the number of structures (docking frames)
# in which the fragment appears. This number should be negative for inmuno or positve for
# constitutive. 
##########################


##### Parser
parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="Read the file where the more frequent fragments are listed",
                                 add_help=True)

parser.add_argument("-frameslist", help="file with the list of frequent fragments for each docking run" );
parser.add_argument("-overwrite", help="overwrite existing plot directories", default=False, action="store_true")
parser.add_argument("-freq", help="Set the name for the plot", required=True )
parser.add_argument("-folder", help="Set the name for the plot", required=True )

args = parser.parse_args()


rerank_prefix="reranking_"
backup_prefix="zzz_backup_"
inputfile = args.frameslist
#plot_name = args.oname
# Generating the variables for storing each group
#if args.overwrite and exists('histogram_among_frames_%s.png'%(plot_name) ):
#        id=0
#        new_pic="%s_%05d_%s"%(backup_prefix,id,'histogram_among_frames_%s.png'%(plot_name))
#        while exists( new_pic ):
#                id = id + 1
#                new_pic="%s_%05d_%s"%(backup_prefix,id,'histogram_among_frames_%s.png'%(plot_name))
#        shutil.move( 'histogram_among_frames_%s.png'%(plot_name), new_pic)
#if not exists( 'histogram_among_frames_%s.png'%(plot_name)):
hisfile = open('%s'%inputfile,'r') 
names=[]
for line in iter(hisfile):
    if 'MD' in line:    
       names.append(line.split()[0])
hisfile.close()

hisfile = open('%s'%inputfile,'r')
for name in names:
        exec "group_%s= []" % name 

i=0
for line in iter(hisfile):
    if 'MD' in line and line.split()[0]== names[i]:
#        print '1',line.split()[0]
        globals()["group_"+"%s"%names[i]].append(hisfile.next().split()[0])
#        print hisfile.next().split()[0]
#        print line.split()[0]
        i += 1
        continue
    if not 'MD' in line:
         globals()["group_"+"%s"%names[i-1]].append(line.split()[0])
         continue
#print globals()["group_"+"%s"%names[0]], names[0]
hisfile.close() 

for k in names:
   for m in globals()["group_"+"%s"%k]:
         count_no_rep = []
         for mymol in readfile("sdf", "mol_freq_amog_%s/mol_freq_%s.sdf"%(args.folder,args.freq)):
             if mymol.title == m  and not m in count_no_rep:
                count_no_rep.append(mymol.title)
                print k, m 
 
#else:
#   print "Nothing was done with %s"%inputfile
