#!/usr/bin/env python

from glob import glob
import shutil
import argparse
from os.path import basename
from os.path import exists

from openbabel import *
from pybel import *
import argparse
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import math
from matplotlib.ticker import NullFormatter

##### Parser
parser = argparse.ArgumentParser(prog=basename(__file__),
                                 fromfile_prefix_chars='@',
                                 description="Read MASTER KEY and plot the rmsd vs docking, with freq and lambda-k as colors ",
                                 add_help=True)


parser.add_argument("-master_files", help="Master key files from the master_key_output folder", default="SDFs" );
parser.add_argument("-single_master", nargs="*" );
parser.add_argument("-overwrite", help="overwrite existing plot directories", default=False, action="store_true")

args = parser.parse_args()


rerank_prefix="reranking_"
backup_prefix="zzz_backup_"

if args.single_master:
        single_master=args.single_master
else:
       fsdf = open('%s'%args.master_files,'r')
       single_master= [line for line in fsdf.readlines()]
       fsdf.close()

for infile in single_master:
        mol_freq = {}
        his_data = []
        master_key = []
        input_file = infile.split('.')[0]
        if args.overwrite and exists( 'RFS_%s.png'%input_file ):
                id=0
                new_pic="%s_%05d_%s"%(backup_prefix,id,'RFS_%s.png'%input_file)
                while exists( new_pic ):
                        id = id + 1
                        new_pic="%s_%05d_%s"%(backup_prefix,id,'RFS_%s.png'%input_file)
                shutil.move( 'RFS_%s.png'%input_file, new_pic)
        if not exists( 'RFS_%s.png'%infile.split('.')[0] ):
                data_master = open('master_key_output/%s'%infile.split()[0],'r')
                data = [ i for i in data_master.readlines() if not '#' in i ]
                data_freq = [ float(i.split()[1]) for i in data ]
                data_rmsd = [ float(i.split()[2]) for i in data ]
                data_dock = [ float(i.split()[3]) for i in data ]
                data_klam = [ float(i.split()[4]) for i in data ]
##################################################################
##################### Ploting secction ###########################
#                v= [0, 10, -10.0, -4.5 ]    #Set the axis for plot [xmin, xmax, ymin, ymax]
                v= [0, 8, 0, 18 ]    #Set the axis for plot [xmin, xmax, ymin, ymax]
                plt.subplot(121)
#                plt.scatter(data_rmsd, data_dock, c=data_freq,s=35)
                plt.scatter(data_rmsd, data_freq, c=data_dock,s=35)
		plt.axis(v)
                plt.xlabel('RMSD')
                plt.ylabel('Frequency')
                plt.colorbar()
                plt.title('Colored by DockingScore')
                plt.subplot(122)
#                plt.scatter(data_rmsd, data_dock, c=data_klam,s=35 )
                plt.scatter(data_freq, data_rmsd, c=data_dock,s=35)
                plt.axis(v)
                plt.xlabel('RMSD')
#                plt.ylabel('DockingScore')
                plt.colorbar()
                plt.title('Colored by Cluster lambda')
#                plt.show()
                plt.savefig('RFS_%s'%input_file,dpi=300,bbox_inches='tight')
                plt.close()
        else:
           print "Nothing was done with %s"%input_file
