#!/usr/bin/env python
import string
from sys import argv,stderr,stdout
from os import popen,system
from os.path import exists

assert( len(argv)>1)
pdbname = argv[1]

lines = open(pdbname,'r').readlines()

count = int(argv[2]);

outid  = stdout

for line in lines:
        line_edit = line
        if line[0:3] == 'TER':
           continue
        if line_edit[0:4] == 'ATOM':

           if line_edit[13:17].strip()=="N":
              count = count + 1
           newnum = '%3d' % count
           line_edit = line_edit[0:23] + newnum + line_edit[26:]
           outid.write(line_edit)
outid.close()
