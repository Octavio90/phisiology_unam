#! /usr/bin/env python

import math,sys
from pymol import cmd

def distx2(x1,x2):
  """
  Calculate the square of the distance between two coordinates.
  Returns a float
  """
  distx2 = (x1[0]-x2[0])**2 + (x1[1]-x2[1])**2 + (x1[2]-x2[2])**2
  return distx2


def rmsd_matrix(selection):

# initialize the arrays used
  models = []
  coord_array = []

  num_states = cmd.count_states(selection)

# get models into models array
  for i in range(num_states):
    models.append(cmd.get_model(selection,state=i+1))

# extract coordinates and atom identification information out of the models
# loop over the states
  for i in range(num_states):
    coord_array.append([])
# loop over the atoms in each state
    for j in range(len(models[0].atom)):
        atom = models[i].atom[j]
        coord_array[i].append(atom.coord)


  final_matrix = []
  for st in range(num_states):
# calculate the rmsd
    diff2 = []
    for i in range(num_states):
      diff2.append(0.0)
      if st <= i:
        for j in range(len(coord_array[0])):
# reference_state to get the correct reference_state
          diff2[i] += distx2(coord_array[i][j],coord_array[st][j]) # running over each atom, then over each model
        diff2[i] = math.sqrt(diff2[i]/len(coord_array[0]))
    final_matrix.append(diff2)
  text_file = open("matrix_for_%s.txt"%selection,"w")
  for h in range(num_states):
    text_file.write("%s\n"%final_matrix[h])
  text_file.close()
cmd.extend("rmsd_matrix",rmsd_matrix)
