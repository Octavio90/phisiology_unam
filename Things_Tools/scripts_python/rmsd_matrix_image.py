#! /usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

# TODO: make it parse, get 4 clusters, get plot for a single frame
 
file_1 = open('matrix_for_backbone.txt','r')
value = 4

matrix = []
for j in file_1.readlines():
  a = j.split("[")
  b = a[1].split("]")
  c = b[0].split(",")
  d = [float(i) for i in c]
  matrix.append(d)
image = np.array(matrix)


print image.max()
plt.imshow(image)
plt.savefig('rmsd_matix1.png')

cluster_gap = image.max()/value
cluster_bord = []
for i in range(value-1):
   cluster_bord.append(0.0)
   cluster_bord[i] += cluster_gap*(i+1)
print cluster_bord
for i in range(len(image[:,0])):
   for j in range(len(image[:,0])):
       if i < j: 
          for k in range(len(cluster_bord)-1):
            if image[i,j] >= cluster_bord[len(cluster_bord)-1]:
               image[i,j] = cluster_bord[len(cluster_bord)-1]+(cluster_gap/2)
               continue
            elif image[i,j] <= cluster_bord[0]:
               image[i,j] = cluster_bord[0]-(cluster_gap/2)
               continue
            elif image[i,j] > cluster_bord[k] and image[i,j] < cluster_bord[k+1]:
               image[i,j] = cluster_bord[k]+(cluster_gap/2)
plt.imshow(image)
plt.savefig('rmsd_matix2.png')

hist = {}
for i in range(len(image[:,0])):
   for j in range(len(image[:,0])):
       if i < j:
          if image[i,j] in hist:
             hist[image[i,j]] += 1
          else:
             hist[image[i,j]] = 1
       
print hist
