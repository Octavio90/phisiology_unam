#! /usr/bin/python
# Copyright (c) 2009 Robert L. Campbell (rlc1@queensu.ca)
import math,sys
from pymol import cmd
from sets import Set

def distx2(x1,x2):
  """
  Calculate the square of the distance between two coordinates.
  Returns a float
  """
  distx2 = (x1[0]-x2[0])**2 + (x1[1]-x2[1])**2 + (x1[2]-x2[2])**2
  return distx2

# READ THIS: nameof is name of ouputfile.
# In general, the script calculates the average rmsf for aminoacid and
# writes a file for each chain.  
def rmsf_by_res(select,nameof,reference_state=1):

  reference_state = int(reference_state)
  num_states = cmd.count_states(select)

  print len( Set( [(i.chain,i.resi,i.resn) for i in
  cmd.get_model(select).atom] ) ) 
  for ch in cmd.get_chains(select):
    nres = len( Set( [(i.chain,i.resi,i.resn) for i in
    cmd.get_model(select).atom if i.chain == ch ] ) ) 
    of = open('chain_%s_500%s.txt'%(ch,nameof),'w')

    for h in range(1,nres+1):
        cmd.select('selection',"%s & i. %s & c. %s"%(select,h,ch) )
        models = []
        coord_array = []
        chain=[]
        resi=[]
        resn=[]
        name=[]
        
         
        for i in range(num_states):
          models.append(cmd.get_model('selection',state=i+1))
          
        for i in range(num_states):
          coord_array.append([])
          chain.append([])
          resi.append([])
          resn.append([])
          name.append([])
          
          for j in range(len(models[0].atom)):
              atom = models[i].atom[j]
              coord_array[i].append(atom.coord)
              chain[i].append(atom.chain)
              resi[i].append(atom.resi)
              resn[i].append(atom.resn)
              name[i].append(atom.name)
        diff2 = []
        for j in range(len(coord_array[0])):
          diff2.append(0.)
          for i in range(num_states):
        
            diff2[j] += distx2(coord_array[i][j],coord_array[reference_state - 1][j])
        
          diff2[j] = math.sqrt(diff2[j]/num_states)
        
        b_dict = {}
        
        sum_rmsf = 0
        
        for i in range(len(diff2)):
            sum_rmsf += diff2[i]
        mean_rmsf = sum_rmsf/len(diff2)
        
        of.write('%g\n'%mean_rmsf)
#        print "%g" %  mean_rmsf
    of.close()
cmd.extend("rmsf_by_res",rmsf_by_res)
