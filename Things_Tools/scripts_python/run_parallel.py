#!/usr/bin/env python
##-*- mode:python;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t;python-indent:2 -*-'

# :noTabs=true:
# (c) Copyright Rosetta Commons Member Institutions.
# (c) This file is part of the Rosetta software suite and is made available under license.
# (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
# (c) For more information, see http://www.rosettacommons.org. Questions about this can be
# (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

# For help, run with -h.  Requires Python 2.4+, like the unit tests.

import sys
if not hasattr(sys, "version_info") or sys.version_info < (2,4):
    raise ValueError("Script requires Python 2.4 or higher!")

import os, shutil, threading, subprocess, signal, time, re, random, datetime
from os import path
from optparse import OptionParser, IndentedHelpFormatter


def main(argv):
	'''
A simple system for running regression tests on Mini.

Each test has its own subdirectory in tests/, which serves as its name.
Each test has a file named "command", which should have the command to be executed.
Variable substitution is possible using Python printf format; see examples.
To create a new test, just make a new directory with a "command" file and other needed files.
See tests/HOW_TO_MAKE_TESTS for more information.

When the tests are run, one of two things happens.
(1) If the "ref" directory does not exist, it is created.
Each subdirectory of tests/ is copied to ref/, and the test is executed there.
(2) Otherwise, the "new" directory is wiped out and re-created, and tests are run in new/ instead.
Afterwards, "diff" is used to compare each subdirectory of ref/ with the corresponding subdirectory in new/.
A test is considered passed if there are no differences, and failed otherwise.

Intended usage is to run the tests once with a clean checkout, and again after changes.
The exact results of many runs are hardware-dependent, so "expected" results cannot be kept in SVN.
If tests fail in expected ways, the appropriate subdirectories can be copied from new/ to ref/ to update the expected results.

EXAMPLES:

rm -r ref/; ./integration.py    # create reference results using only default settings

./integration.py    # test reference results against new results

./integration.py -d ~/minidb -j2    # again, but using 2 processors and custom database location

./integration.py ligand_dock_7cpa    # only run the "ligand_dock_7cpa" test
'''
	parser = OptionParser(usage="usage: %prog [OPTIONS] [TESTS]", formatter=ParagraphHelpFormatter())
	parser.set_description(main.__doc__)
    # parser.add_option("-short", ["--long"],
    #   action="store|store_true|store_false|append",
    #   default=True|False|...
    #   type="string|int|float",
    #   dest="opt_name",
    #   help="store value in PLACE",
    #   metavar="PLACE",
    # )
	parser.add_option("-j", "--num_procs",
      default=1,
      type="int",
      help="number of processors to use on local machine (default: 1)")
	parser.add_option("--host",
										default=[],
										action="append",
										help="ssh to HOST to run some tests. Assumes shared filesystem. May specify multiple times. May specify HOST or USER@HOST. For 5 jobs per host, HOST/5",
										)
	parser.add_option("--digs",
										default=0,
										type="int",
										help="Baker lab shortcut: use NUM processors from the digs, selected at random",
										metavar="NUM",
										)
	parser.add_option("-t", "--timeout",
										default=0,
										type="int",
										help="Maximum runtime for each test, in minutes (default: no limit)",
										metavar="MINUTES",
										)

	(options, args) = parser.parse_args(args=argv)
	if options.digs > 0:
		options.num_procs = 0 # don't use local processors too, b/c local *is* a dig
        # From the "freewhip" script on the whips themselves:
        # digs = ["dig01","dig02","dig03","dig04","dig05","dig06","dig07","dig08", "dig09","dig11","dig12","dig13","dig14","dig15","dig16", "dig17","dig19","dig20","dig21","dig22","dig23", "dig25","dig26"] * 2
		digs = [ "dig"+str(x) for x in range(1,33) ] * 4 # we use up to 4 processors per box
		assert options.digs <= len(digs)
		random.shuffle(digs)
		options.host.extend( digs[:options.digs] )

    # Each test consists of a directory with a "command" file in it.
	cmds =[]
	for l in sys.stdin:
		cmds.append(l[:-1])

  print cmds

	queue = Queue()
	queue.TotalNumberOfTasks = len(cmds)
	for job in cmds:
		queue.put(job)
         # Start worker thread(s)
	for i in range(options.num_procs):
		worker = Worker(queue, options, timeout_minutes=options.timeout)
		thread = threading.Thread(target=worker.work)
            #thread.setDaemon(True) # shouldn't be necessary here
		thread.start()

	for host in options.host:
		if host.count('/') > 1:
			sys.exit("only one forward slash per host specification")
		parts = host.split('/')
		nodes=None
		if len(parts) == 1:
			nodes=1
		if len(parts) == 2:
			host= parts[0]
			nodes= int(parts[1])
		for node in range(nodes):
			worker = Worker(queue, options, host=host, timeout_minutes=options.timeout)
			thread = threading.Thread(target=worker.work)
              #thread.setDaemon(True) # shouldn't be necessary here
			thread.start()

        # Wait for them to finish
	queue.join()


class Worker:
	def __init__(self, queue, opts, host=None, timeout_minutes=0):
		self.queue = queue
		self.opts = opts
		self.host = host
		self.timeout = timeout_minutes * 60

	def work(self):
		running=0
		try:
			while True:
				job = self.queue.get_nowait()
				try: # Actually catch exception and ignore it.  Python 2.4 can't use "except" and "finally" together.
					start = time.time() # initial guess at start time, in case of exception
					try: # Make sure job is marked done even if we throw an exception
                        # Variables that may be referrenced in the cmd string:
						python = sys.executable
										# Read the command from the file "command"
						wd=os.getcwd()
						cmd=''
						if self.host is not None:
							cmd = 'PATH="%s"\n%s\n' % (os.environ["PATH"], cmd)
						cmd += job
						if self.host is None:
							print "Running  %-40s on localhost ..." % cmd
							proc = subprocess.Popen(cmd, shell=True)

						# Can't use cwd=workdir b/c it modifies *local* dir, not remote dir.
						else:
							print "Running  %-40s on %20s ..." % (job, self.host)
							bash_cmd='cd %s;'%wd+cmd
							proc = subprocess.Popen(["ssh", self.host, bash_cmd], preexec_fn=os.setpgrp)#, cwd=workdir)
							#os._exit(os.EX_IOERR)
						start = time.time() # refined start time
						if self.timeout == 0:
							retcode = proc.wait() # does this block all threads?
						else:
							while time.time() - start <= self.timeout:
								retcode = proc.poll()
								if retcode is not None: break
								time.sleep(1)
							if retcode is None:
								print "*** Job %s exceeded the timeout and will be killed! [%s]\n" % (job, datetime.datetime.now())
                                #os.kill(proc.pid, signal.SIGTERM)
								os.killpg(os.getpgid(proc.pid), signal.SIGKILL)
						if retcode != 0 and retcode is not None:
							if self.host is None:
								error_string = "***  %s did not run on host %s!  Check your --mode flag and paths. [%s]\n" % (job.split()[0], 'local_host', datetime.datetime.now())
							else:
								error_string = "***  %s did not run on host %s!  Check your --mode flag and paths. [%s]\n" % (job.split()[0], self.host, datetime.datetime.now())
							print 'ERROR: ',error_string,



					finally: # inner try
						percent = (100* (self.queue.TotalNumberOfTasks-self.queue.qsize())) / self.queue.TotalNumberOfTasks
						print "Finished %-40s in %3i seconds\t [~%4s job (%s%%) started, %4s in queue, %4d running]" % (job.split()[0], time.time() - start, self.queue.TotalNumberOfTasks-self.queue.qsize(), percent, self.queue.qsize(), self.queue.unfinished_tasks-self.queue.qsize() )
						self.queue.task_done()

				except Exception, e: # middle try
					print 'ERR :', e
		except Empty: # outer try
			pass # we're done, just return


class ParagraphHelpFormatter(IndentedHelpFormatter):
    '''
    A help formatter that respects paragraph breaks (blank lines) in usage strings.
    '''
    def _format_text(self, text):
        paragraphs = re.split('\n([ \t]*\n)+', text)
        paragraphs = [ IndentedHelpFormatter._format_text(self, p.strip()) for p in paragraphs ]
        return '\n'.join(paragraphs) # each already ends in a newline

def copytree(src, dst, symlinks=False, accept=lambda srcname, dstname: True):
    """Recursively copy a directory tree using copy2(), with filtering.
    Copied from shutil so I could filter out .svn entries.
    """
    names = os.listdir(src)
    os.makedirs(dst)
    errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        if not accept(srcname, dstname): continue
        try:
            if symlinks and os.path.islink(srcname):
                linkto = os.readlink(srcname)
                os.symlink(linkto, dstname)
            elif os.path.isdir(srcname):
                copytree(srcname, dstname, symlinks, accept)
            else:
                shutil.copy2(srcname, dstname)
            # XXX What about devices, sockets etc.?
        except (IOError, os.error), why:
            errors.append((srcname, dstname, str(why)))
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except shutil.Error, err:
            errors.extend(err.args[0])
    try:
        shutil.copystat(src, dst)
    #except WindowsError:
        # can't copy file access times on Windows
        pass
    except OSError, why:
        errors.extend((src, dst, str(why)))
    if errors:
        raise shutil.Error, errors

################################################################################
# Python 2.4 lacks support for join() / task_done() in the Queue class,
# so I pasted the 2.5 implementation here.
# With 2.5+, you can just do "from Queue import *" instead.

from time import time as _time
from collections import deque

class Empty(Exception):
    "Exception raised by Queue.get(block=0)/get_nowait()."
    pass

class Full(Exception):
    "Exception raised by Queue.put(block=0)/put_nowait()."
    pass

class Queue:
    """Create a queue object with a given maximum size.

    If maxsize is <= 0, the queue size is infinite.
    """
    def __init__(self, maxsize=0):
        try:
            import threading
        except ImportError:
            import dummy_threading as threading
        self._init(maxsize)
        # mutex must be held whenever the queue is mutating.  All methods
        # that acquire mutex must release it before returning.  mutex
        # is shared between the three conditions, so acquiring and
        # releasing the conditions also acquires and releases mutex.
        self.mutex = threading.Lock()
        # Notify not_empty whenever an item is added to the queue; a
        # thread waiting to get is notified then.
        self.not_empty = threading.Condition(self.mutex)
        # Notify not_full whenever an item is removed from the queue;
        # a thread waiting to put is notified then.
        self.not_full = threading.Condition(self.mutex)
        # Notify all_tasks_done whenever the number of unfinished tasks
        # drops to zero; thread waiting to join() is notified to resume
        self.all_tasks_done = threading.Condition(self.mutex)
        self.unfinished_tasks = 0

    def task_done(self):
        """Indicate that a formerly enqueued task is complete.

        Used by Queue consumer threads.  For each get() used to fetch a task,
        a subsequent call to task_done() tells the queue that the processing
        on the task is complete.

        If a join() is currently blocking, it will resume when all items
        have been processed (meaning that a task_done() call was received
        for every item that had been put() into the queue).

        Raises a ValueError if called more times than there were items
        placed in the queue.
        """
        self.all_tasks_done.acquire()
        try:
            unfinished = self.unfinished_tasks - 1
            if unfinished <= 0:
                if unfinished < 0:
                    raise ValueError('task_done() called too many times')
                self.all_tasks_done.notifyAll()
            self.unfinished_tasks = unfinished
        finally:
            self.all_tasks_done.release()

    def join(self):
        """Blocks until all items in the Queue have been gotten and processed.

        The count of unfinished tasks goes up whenever an item is added to the
        queue. The count goes down whenever a consumer thread calls task_done()
        to indicate the item was retrieved and all work on it is complete.

        When the count of unfinished tasks drops to zero, join() unblocks.
        """
        self.all_tasks_done.acquire()
        try:
            while self.unfinished_tasks:
                self.all_tasks_done.wait()
        finally:
            self.all_tasks_done.release()

    def qsize(self):
        """Return the approximate size of the queue (not reliable!)."""
        self.mutex.acquire()
        n = self._qsize()
        self.mutex.release()
        return n

    def empty(self):
        """Return True if the queue is empty, False otherwise (not reliable!)."""
        self.mutex.acquire()
        n = self._empty()
        self.mutex.release()
        return n

    def full(self):
        """Return True if the queue is full, False otherwise (not reliable!)."""
        self.mutex.acquire()
        n = self._full()
        self.mutex.release()
        return n

    def put(self, item, block=True, timeout=None):
        """Put an item into the queue.

        If optional args 'block' is true and 'timeout' is None (the default),
        block if necessary until a free slot is available. If 'timeout' is
        a positive number, it blocks at most 'timeout' seconds and raises
        the Full exception if no free slot was available within that time.
        Otherwise ('block' is false), put an item on the queue if a free slot
        is immediately available, else raise the Full exception ('timeout'
        is ignored in that case).
        """
        self.not_full.acquire()
        try:
            if not block:
                if self._full():
                    raise Full
            elif timeout is None:
                while self._full():
                    self.not_full.wait()
            else:
                if timeout < 0:
                    raise ValueError("'timeout' must be a positive number")
                endtime = _time() + timeout
                while self._full():
                    remaining = endtime - _time()
                    if remaining <= 0.0:
                        raise Full
                    self.not_full.wait(remaining)
            self._put(item)
            self.unfinished_tasks += 1
            self.not_empty.notify()
        finally:
            self.not_full.release()

    def put_nowait(self, item):
        """Put an item into the queue without blocking.

        Only enqueue the item if a free slot is immediately available.
        Otherwise raise the Full exception.
        """
        return self.put(item, False)

    def get(self, block=True, timeout=None):
        """Remove and return an item from the queue.

        If optional args 'block' is true and 'timeout' is None (the default),
        block if necessary until an item is available. If 'timeout' is
        a positive number, it blocks at most 'timeout' seconds and raises
        the Empty exception if no item was available within that time.
        Otherwise ('block' is false), return an item if one is immediately
        available, else raise the Empty exception ('timeout' is ignored
        in that case).
        """
        self.not_empty.acquire()
        try:
            if not block:
                if self._empty():
                    raise Empty
            elif timeout is None:
                while self._empty():
                    self.not_empty.wait()
            else:
                if timeout < 0:
                    raise ValueError("'timeout' must be a positive number")
                endtime = _time() + timeout
                while self._empty():
                    remaining = endtime - _time()
                    if remaining <= 0.0:
                        raise Empty
                    self.not_empty.wait(remaining)
            item = self._get()
            self.not_full.notify()
            return item
        finally:
            self.not_empty.release()

    def get_nowait(self):
        """Remove and return an item from the queue without blocking.

        Only get an item if one is immediately available. Otherwise
        raise the Empty exception.
        """
        return self.get(False)

    # Override these methods to implement other queue organizations
    # (e.g. stack or priority queue).
    # These will only be called with appropriate locks held

    # Initialize the queue representation
    def _init(self, maxsize):
        self.maxsize = maxsize
        self.queue = deque()

    def _qsize(self):
        return len(self.queue)

    # Check whether the queue is empty
    def _empty(self):
        return not self.queue

    # Check whether the queue is full
    def _full(self):
        return self.maxsize > 0 and len(self.queue) == self.maxsize

    # Put a new item in the queue
    def _put(self, item):
        self.queue.append(item)

    # Get an item from the queue
    def _get(self):
        return self.queue.popleft()
################################################################################


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
