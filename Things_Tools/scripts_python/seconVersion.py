#! /usr/bin/env python

import math,sys
from pymol import cmd
import pylab as pyl

def distx2(x1,x2):
  """
  Calculate the square of the distance between two coordinates.
  Returns a float
  """
  distx2 = (x1[0]-x2[0])**2 + (x1[1]-x2[1])**2 + (x1[2]-x2[2])**2
  return distx2


def rmsd_select(selection, selection2):

# initialize the arrays used
  models = []
  coord_array = []
  coord_ref = []
  num_states = cmd.count_states(selection)

# get models into models array
  for i in range(num_states):
    models.append(cmd.get_model(selection,state=i+1))
  refmod=[]
  refmod.append(cmd.get_model(selection2,state=1))
# extract coordinates and atom identification information out of the models
# loop over the states
  for i in range(num_states):
    coord_array.append([])
# loop over the atoms in each state
    for j in range(len(models[0].atom)):
        atom = models[i].atom[j]
        coord_array[i].append(atom.coord)
# the Reference structure
  for j in range(len(refmod[0].atom)):
      atom2 = refmod[0].atom[j]
      coord_ref.append(atom2.coord)

  print coord_ref[0], coord_array[0][1]
# calculate the rmsd
  diff2 = []
  for i in range(num_states):
      diff2.append(0.0)
      for j in range(len(coord_array[0])):
# reference_state to get the correct reference_state
        diff2[i] += distx2(coord_array[i][j], coord_ref[j]) # running over each atom, then over each model
      diff2[i] = math.sqrt(diff2[i]/len(coord_ref))
  print diff2
# Printing and ploting
  text_file = open("rmsd_selection_%s.txt"%selection2,"w")
  for h in range(num_states):
    text_file.write("%s\n"%diff2[h])
  text_file.close()
  
#  pyl.axes([0.1, 0.15, 0.8, 0.75])
#  pyl.plot(diff2)
#  pyl.title('rmsd of %s vs %s'%(selection2,selection))
#  pyl.xlabel('Model or Frame of the MD')
#  pyl.ylabel('rmsd (A)')
#  pyl.savefig('aaaa.png')
cmd.extend("rmsd_select",rmsd_select)
