#! /usr/bin/env python
 
from threading import Event, Thread
import os 

class RepeatTimer(Thread):
    def __init__(self, interval, function, iterations=0, args=[], kwargs={}):
        Thread.__init__(self)
        self.interval = interval
        self.function = function
        self.iterations = iterations
        self.args = args
        self.kwargs = kwargs
        self.finished = Event()
 
    def run(self):
        count = 0
        while not self.finished.is_set() and (self.iterations <= 0 or count < self.iterations):
            self.finished.wait(self.interval)
            if not self.finished.is_set():
                self.function(*self.args, **self.kwargs)
                count += 1
 
    def cancel(self):
        self.finished.set()

def check_rate():
	os.system("cat results.xml | compute_rate_constant >> perro.txt")
 
r = RepeatTimer(5.0, check_rate, 10)
r.start()
print "it is running"
