#!/usr/bin/env python
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
import sys

def fill_up_coor(coord):
    beg = coord*atoms
    end = beg + atoms
    row = np.zeros(atoms*3)
    cnt = 0
    for line in infile[beg:end]:
        line = line.split('\n')[0].split()
        for j in range(3):
            row[cnt+j] = float(line[j])
        cnt += 3
    return row


infile = open(sys.argv[1],'r').readlines()
try:
   numbering_from = int(sys.argv[2])
except:
   numbering_from = 0

try:
  method_num = int(sys.argv[3])
except:
  method_num = 1


rw = len(infile)
cl = len(infile[0].split())
atoms = int(np.sqrt(rw/cl))

methods = [None, 'none', 'nearest', 'bilinear', 'bicubic', 'spline16',
           'spline36', 'hanning', 'hamming', 'hermite', 'kaiser', 'quadric',
           'catrom', 'gaussian', 'bessel', 'mitchell', 'sinc', 'lanczos']
grid = []
for i in range(atoms*3):
   grid.append(fill_up_coor(i))
grid = np.array(grid)

print grid.min(),grid.max()

fig = plt.figure(1, figsize=(6, 6))
ax = fig.add_subplot('111')
plt.imshow(grid, interpolation=methods[method_num],cmap='Spectral')
plt.colorbar()
new_x_axis = [ int(i/3) + numbering_from for i in  ax.get_xticks()]
#ax.set_xticklabels( new_x_axis  , size='large')
new_y_axis = [ int(i/3) + numbering_from for i in  ax.get_yticks()]
#ax.set_yticklabels( new_y_axis  , size='large')
plt.show()
