#!/bin/bash

script_dir="/home/scidb/Documents/Fisiologia/scripts"

vis=$1
num_file=0
while read file; do 
	name=$(echo $file | cut -d'.' -f1)
	((num_file++))
	sdiff <(sort "../../results/$file" | cut -d'|' -f1) <(sort "../results_2/$file") | \
	grep ">" | awk -F'>' '{print $2}' | cut -d$'\t' -f2 > "grep_$name.tmp"

	while read row; do
		res=$(echo $row | cut -d',' -f1)
		code=$(echo $row | cut -d',' -f2)
		num=$(echo $row | cut -d',' -f3)
		
		awk -v r="$res" -v c="$code" -v n="$num" -F' ' \
			'{if(NF==13){if($4==r && $5==c && $6==n){print $0}}else{if($4==r && $5==c""n){print $0}}  }' \
			"../../pdb/$name.pdbqt" >> "$name.tmp"

	done < "grep_$name.tmp"

	if [[ vis -eq "1" ]]; then
		pymol "$name.tmp" "../../box/$name.box" > /dev/null 
		rm "$name.tmp" "grep_$name.tmp"
	else
		rm "$name.tmp"
		#echo "$file"
		new_row=""
		if [[ -f "$name.tmp" ]]; then
			rm "$name.tmp"
		fi
		while read row; do
			#key data
			res=$(echo $row | cut -d',' -f1)
			code=$(echo $row | cut -d',' -f2)
			num=$(echo $row | cut -d',' -f3)
			
			#rsa|non-polar|polar (rel|abs|abs)
			rsa_data=$(grep RES "../../rsa/$name.rsa" | grep -v REM | \
				awk -v r="$res" -v c="$code" -v n="$num" -F' ' \
				'{if(NF==14){ if($2=r && $3==c && $4==n){print $6"|"$11"|"$13}}else{ if($2==r && $3==c""n){print $5"|"$10"|"$12}}}')

			#rsa
			rsa=$(echo $rsa_data | cut -d'|' -f1)

			#letter
			letter=$(grep -w "$res" "$script_dir/amino_acid.class" | cut -d'|' -f1 | \
				awk -v rsa="$rsa" '{if(rsa<=13.1){print tolower($0)}else{print $0} }')

			polar_data=$(echo $rsa_data | cut -d'|' -f2-)
			num_branch=$(grep -w "$res" "$script_dir/branches_catalog.txt" | cut -d"|" -f2)

			log_num=$(grep "residue:" "../../disicl/DISICL_$name.log" | \
				awk -F' ' -v n="$num" -v rn="$res" -v rc="$code" \
				'$2==rn && $3==n && $4==rc {print $8}')

			if [[ ! -z "$letter" ]]; then
				
				if [[ ! -z "$log_num" ]]; then
					#print res"|"region"|"region_num
					stat_row=$(grep -v "#" "../../disicl/DISICL_pdet_$name.stat" | \
						grep -w "$log_num" | awk -f "$script_dir/filter_regions.awk" | cut -d'|' -f2-)

					if [[ ! -z "$stat_row" ]]; then
						disicl_class=$(echo $stat_row | cut -d'|' -f2 )
						mfra=$(grep -w "$letter" "$script_dir/mf_ra.csv" | \
							awk -F',' -v num="$disicl_class" '{print $(num+1)}')
						
						echo "$row|$rsa|$letter|$stat_row|$mfra|$polar_data|$num_branch" >> "$name.tmp"
					else
						mfra=$(grep -w "$letter" "$script_dir/mf_ra.csv" | \
							awk -F',' '{print $21}')	
						echo "$row|$rsa|$letter|Other|20|$mfra|$polar_data|$num_branch" >> "$name.tmp"
					fi
				else
					mfra=$(grep -w "$letter" "$script_dir/mf_ra.csv" | \
							awk -F',' '{print $21}')
					echo "$row|$rsa|$letter|Other|20|$mfra|$polar_data|$num_branch" >> "$name.tmp"
				fi
			fi
		done < "grep_$name.tmp"	
		rm 	"grep_$name.tmp"
		cat "../../results_polar/$file" "$name.tmp" > "../results_3/$file"
		rm "$name.tmp"
		echo -e "$num_file\t$name"
 	fi
done
