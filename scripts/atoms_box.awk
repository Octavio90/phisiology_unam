BEGIN{
	FS=" ";
	split(coords,coord,",");
	c_total=0;
	c_partial=0;
}
{
	if(length($5) > 1){
		sub_name=rcode rnum
		if($4==rname && $5==sub_name){

			if(split($6,a,".") == 3){
				c1=a[1]"."substr(a[2],1,3)
				c2=substr(a[2],4)"."a[3]
				c3=$7
			} 
			else if(split($6,a,".") == 4){
				c1=a[1]"."substr(a[2],1,3)
				c2=substr(a[2],4)"."substr(a[3],1,3)
				c3=substr(a[3],4)"."a[4]
			}
			else if(split($7,a,".") == 3){
				c1=$6
				c2=a[1]"."substr(a[2],1,3)
				c3=substr(a[2],4)"."a[3]
			} 
			else{
				c1=$6
				c2=$7
				c3=$8
			}

			if(c1>=coord[1] && c1<=coord[2]){
				if(c2>=coord[3] && c2<=coord[4]){
					if(c3>=coord[5] && c3<=coord[6]){
						c_partial++;
						c_total++;
					}else{c_total++}
				}else{c_total++}
			}else{c_total++}
		}
	}
	else{
		if($4==rname && $5==rcode && $6==rnum){
			
			if(split($7,a,".") == 3){
				c1=a[1]"."substr(a[2],1,3)
				c2=substr(a[2],4)"."a[3]
				c3=$8
			} 
			else if(split($7,a,".") == 4){
				c1=a[1]"."substr(a[2],1,3)
				c2=substr(a[2],4)"."substr(a[3],1,3)
				c3=substr(a[3],4)"."a[4]
			}
			else if(split($8,a,".") == 3){
				c1=$7
				c2=a[1]"."substr(a[2],1,3)
				c3=substr(a[2],4)"."a[3]
			} 
			else{
				c1=$7
				c2=$8
				c3=$9
			}

			if(c1>=coord[1] && c1<=coord[2]){
				if(c2>=coord[3] && c2<=coord[4]){
					if(c3>=coord[5] && c3<=coord[6]){
						c_partial++;
						c_total++;
					}else{c_total++}
				}else{c_total++}
			}else{c_total++}
		}	
	}
}
END{
	if(c_total != 0){
		if( ((c_partial*100)/c_total) > 50 ){
			print "1,"c_total","c_partial
		}else{
			print "0,"c_total","c_partial
		}
	}
}