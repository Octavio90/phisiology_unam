#!/bin/bash

result_dir="/home/scidb/Documents/Fisiologia/pdbs/results_polar"
#result_dir="/media/sysadmin/Data/Other/results"
num=0
while read file; do
	((num++))
	name=$(echo $file | cut -d'.' -f1)
	duplicated_res=$(sort "$result_dir/$file" | \
		uniq -c | sort -rn | awk '$1>1{print $0}' | wc -l)
	empty_fields=$(awk -F'|' 'BEGIN{cont=0}{for(i=1;i<=NF;i++) \
		{if($i==""){print $1; cont++; break }} }END{print cont}' \
		"$result_dir/$file")
	total=$(echo $empty_fields | awk '{print $NF}')
	less_fields=$(awk -F'|' 'BEGIN{e=0}{if(NF!=9){e=1; exit 1}}END{print e}' "$result_dir/$file")

	if (( total != 0 )) || (( $duplicated_res != 0 )) || (( less_fields != 0 )); then
		echo -e "$num  $name\tCampos_Vacios:$total\tCampos_Duplicados:$duplicated_res\tLess_fields:$less_fields"
	else
		echo -e "$num  $name\tGood!"
	fi

done