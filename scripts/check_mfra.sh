

script_dir="/home/scidb/Documents/Fisiologia/scripts"

#pdbqt name sin extencion
while read file_name; do

	if (( $1 == 0 )); then
	
		if [[ -f "../results_3/$file_name.var" ]]; then
			rm "../results_3/$file_name.var"
		fi
		while read row; do
			num_class=$(echo $row | cut -d'|' -f5)
			mfra=$(echo $row | cut -d'|' -f6)
			if [[ "$num_class" == "$mfra" ]]; then
				res=$(echo $row | cut -d'|' -f1)
				rsa=$(echo $row | cut -d'|' -f2)
				letter=$(echo $row | cut -d'|' -f3)
				class=$(echo $row | cut -d'|' -f4)
				mfra=$(grep -w "$letter" "$script_dir/mf_ra.csv" | awk -F',' -v num="$num_class" '{print $(num+1)}')
				echo "$res|$rsa|$letter|$class|$num_class|$mfra" >> "../results_3/$file_name.var"
			else
				echo $row >> "../results_3/$file_name.var"
			fi
		done < "../results_2/$file_name.var"

	else
		if [[ -f "../results_2/$file_name.var" ]]; then
			rm "../results_2/$file_name.var"
		fi

		while read row; do
			flag=$(echo $row | awk -F'|' 'BEGIN{f=0}{for(i=1;i<=NF;i++){if($i==""){f=1;exit 1}}}END{ print f}')
			if (( flag == 1 )); then
				res=$(echo $row | cut -d'|' -f1)
				rsa=$(echo $row | cut -d'|' -f2)
				letter=$(echo $row | cut -d'|' -f3)

				res_name=$(echo $res | cut -d',' -f1)
				code=$(echo $res | cut -d',' -f2)
				res_num=$(echo $res | cut -d',' -f3)

				log_num=$(grep "residue:" "../../disicl/DISICL_$file_name.log" | \
				awk -F' ' -v n="$res_num" -v rn="$res_name" -v rc="$code" \
				'$2==rn && $3==n && $4==rc {print $8}')

				clog_num=$(echo $log_num | wc -l)
				if (( clog_num > 1)); then
					echo "$res|$rsa|$letter|Log +1 rec" >> "../results_2/$file_name.var"
				fi

				if [[ ! -z "$log_num" ]]; then
					stat_row=$(grep -v "#" "../../disicl/DISICL_pdet_$file_name.stat" | \
					grep -w "$log_num" | awk -f "$script_dir/filter_regions.awk" | cut -d'|' -f2-)
					if [[ ! -z "$stat_row" ]]; then
						row="$res|$rsa|$letter|$stat_num|"
						num_class=$(echo $stat_row | cut -d'|' -f2 )
						mfra=$(grep -w "$letter" "$script_dir/mf_ra.csv" | \
						awk -F',' -v num="$num_class" '{print $(num+1)}')
						echo "$row|$mfra" >> "../results_2/$file_name.var"
					else
						mfra=$(grep -w "$letter" "$script_dir/mf_ra.csv" | \
						awk -F',' '{print $21}')
						echo "$res|$rsa|$letter|Other|20|$mfra" >> "../results_2/$file_name.var"
					fi
				else
					echo "$res|$rsa|$letter|No encontrado" >> "../results_2/$file_name.var"
				fi
			
			else
				echo "$row" >> "../results_2/$file_name.var"
			fi

		done < "../results_3/$file_name.var"

	fi
done