#!/bin/bash

dir="/media/sysadmin/Data/Results_Fisiologia/results"

error=$1
num=0

while read -r file; do	
	while read -r line; do
		fields=$(echo $line | awk -F'|' '{print NF}')
		if [[ $fields -ne 6 ]]; then
			key=$(echo $line | cut -d'|' -f1)
			rsa=$(echo $line | cut -d'|' -f2)
			name=$(echo $key | cut -d',' -f1)
		fi
	done < "$dir/$file.var"
done