#!/bin/bash

############################################################################################
## Variables ##
###############

op="both"
r_pdbqt=""
l_pdbqt=""
mgltools_dir="/home/scidb/MGLTools-1.5.6/MGLToolsPckgs/AutoDockTools/Utilities24"
vina_dir="/home/scidb/Documents/Fisiologia/pdbs/pdb2/software/autodock_vina_1_1_2_linux_x86/bin"
############################################################################################
## Functions ##
###############

function help(){
	echo "Usage:"
	echo -e "\t-h --help       : help"
	echo -e "\t-f --pdb        : PDB file          "
	echo -e "\t-r --residue    : Residue name      "
	echo -e "\t-c --chain      : Chain identifier  "
	echo -e "\t-s --size_box   : String of coordenates separated by ',' (default: \"15,15,15\")"
	echo -e "\t-d --delete     : Delete only pdb files (keep pdbqt files)"
	echo -e "\t-o --score_only : Perform docking with score_only parameter"
	echo -e "\t   --r_pdbqt    : Receptor in pdbqt format (to avoid pre-computing)"
	echo -e "\t   --l_pdbqt    : Ligand in pdbqt format (to avoid pre-computing)"
}

function filter(){
	local rec_name=$(echo "$1" | cut -d'/' -f-1 | cut -d'.' -f1 | awk '{print toupper($0)}')
	local receptor="R_$rec_name.pdb"
	local ligand="L_$rec_name""_$2""_$3.pdb"
	
	if [[ -f $receptor ]]; then
		rm $receptor
	fi

	if [[ -f $ligand ]]; then
		rm $ligand
	fi

	if [[ "$4" == "ligand" ]]; then
		./filter_pdb.py -f $1 -r $2 -c $3
	elif [[ "$4" == "receptor" ]]; then
		./filter_pdb.py -f $1
	else
		./filter_pdb.py -f $1
		./filter_pdb.py -f $1 -r $2 -c $3
	fi

	echo "$receptor,$ligand"
}

function prepare_pdbqt(){

	if [[ "$3" == "ligand" ]]; then
		pythonsh "$mgltools_dir/prepare_ligand4.py"   -l "$2"  -A "hydrogens" >> /dev/null
	elif [[ "$3" == "receptor" ]]; then
		pythonsh "$mgltools_dir/prepare_receptor4.py" -r "$1"  -A "hydrogens" >> /dev/null
	else
		pythonsh "$mgltools_dir/prepare_receptor4.py" -r "$1"  -A "hydrogens" >> /dev/null
		pythonsh "$mgltools_dir/prepare_ligand4.py"   -l "$2"  -A "hydrogens" >> /dev/null
	fi
}

function make_config(){
	if [[ ! -f $1 ]]; then
		echo "Missing file for config file"
		exit 1
	fi
	local conf_file=$(echo "$1" | cut -d'.' -f1 | awk '{print $0"_config.txt"}') 
	if [[ -f "$conf_file" ]]; then
		rm $conf_file
	fi

	./make_config.sh -f "$1" -s "$2" 

	echo "$conf_file"
}

function delete(){
	if [[ -f $1 ]]; then
		rm $1
	fi
	if [[ -f $2 ]]; then
		rm $2
	fi
}

function docking(){
	local name=$(echo "$2" | cut -d'.' -f1 | awk -F'_' '{print $2"_"$3"_"$4}')
	
	if [[ "$4" == "true" ]]; then
		"$vina_dir/vina" --receptor "$1" --ligand "$2" --score_only > output
		affinity=$(grep "Affinity" output | cut -d' ' -f2)
		echo "$name $affinity"
		if [[ -f "output" ]]; then
			rm "output"
		fi
	fi
}

############################################################################################
## Parameters ##
################


options=$(getopt -o :p:r:c:s:hd --long pdb:,residue:,chain:,size_box:,help,delete,score_only,r_pdbqt:,l_pdbqt: -- "$@")
set -- $options
while [[ $# -gt 0 ]]; do
	case $1 in
		-p|--pdb)
			pdb=$(echo $2 | tr -d \') 
			shift
			;;
		-r|--residue)
			residue=$(echo $2 | tr -d \') 
			shift
			;;
		-c|--chain)
			chain=$(echo $2 | tr -d \') 
			shift
			;;
		-s|--size_box)
			size_box=$(echo $2 | tr -d \') 
			shift
			;;
		-o|--score_only)
			score_only="true"
			;;
		--r_pdbqt)
			r_pdbqt=$(echo $2 | tr -d \')
			shift
			;;
		--l_pdbqt)
			l_pdbqt=$(echo $2 | tr -d \')
			shift
			;;
		-d|--delete)
			delete="true"
			;;
		-h|--help)
			help 
			exit 0
			;;
		--) shift; break;;
		* ) echo "Error! "; exit 1 ;;
	esac
	shift
done

############################################################################################
## Main Program ##
##################

if [[ "$r_pdbqt" != "" ]] && [[ "$l_pdbqt" != "" ]]; then
	op="none"
elif [[ "$l_pdbqt" != "" ]]; then
	op="receptor"
elif [[ "$r_pdbqt" != "" ]]; then
	op="ligand"
else
	op="both"
fi

#echo "PDB    :   $pdb"
#echo "Residue:   $residue"
#echo "Chain  :   $chain"
#echo "Score  :   $score_only"
#echo "Delete :   $delete"
#echo "Op     :   $op"
#echo "R_pdbqt:   $r_pdbqt"
#echo "L_pdbqt:   $l_pdbqt"
#echo ""
#exit 0



if [[ -f "output" ]]; then
	rm "output"
fi

if [[ "$r_pdbqt" == "" ]] || [[ "$r_pdbqt" == "" ]]; then
	if [[ -z $pdb ]] || [[ -z $residue ]] || [[ -z $chain ]]; then
		help
		exit 0
	fi
fi

if [[ -z $size_box ]]; then
	size_box="15,15,15"
fi

# Filter de PDB into ligando and receptor files

if [[ "$op" != "none" ]]; then
	output=$(filter $pdb $residue $chain $op)
	if [[ "$op" == "receptor" ]]; then
		receptor=$(echo "$output" | cut -d',' -f1)
		ligand="none"
	elif [[ "$op" == "ligand" ]]; then
		ligand=$(echo "$output" | cut -d',' -f2)
		receptor="none"
	else
		receptor=$(echo "$output" | cut -d',' -f1)
		ligand=$(echo "$output" | cut -d',' -f2)
	fi
fi


# Using MGTools for preparing pdbqt files


if [[ "$op" != "none" ]]; then
	prepare_pdbqt $receptor $ligand $op

	if [[ "$delete" == "true" ]]; then
		delete $receptor $ligand
	fi


	if [[ "$op" == "receptor" ]]; then
		receptor=$(echo "$receptor""qt")
		ligand="$l_pdbqt"	
	elif [[ "$op" == "ligand" ]]; then
		receptor="$r_pdbqt"
		ligand=$(echo "$ligand""qt")
	else
		receptor=$(echo "$receptor""qt")
		ligand=$(echo "$ligand""qt")
	fi
else
	receptor="$r_pdbqt"
	ligand="$l_pdbqt"	
fi

# Make Config File for docking
conf_file="config.txt"
if [[ "$score_only" != "true" ]]; then
	conf_file=$(make_config $receptor $size_box)
fi

# Perform Docking
docking $receptor $ligand $conf_file $score_only
