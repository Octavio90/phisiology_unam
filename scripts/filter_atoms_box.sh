#!/bin/bash

ligand_dir=$1*
size_box=$2
echo "working on "$ligand_dir
rm ~/Documents/Fisiologia/scripts/box.pdb

for receptor in $ligand_dir;
do
	receptor_name=$(echo $receptor | awk -F'/' '{print $NF}')
	python ../scripts_python/get_visual_box.py $receptor $size_box
	edges=`grep HETATM box.pdb | awk -f get_edges.awk`
	python ../scripts_python/read_pdb.py $receptor $edges >> ../pdbs/pdbs_atoms_box/$receptor_name 
	echo $receptor_name" - "$size_box" - "$edges
done
