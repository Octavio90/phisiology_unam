#!/bin/bash

# Programa que toda cada pdbqt y genera los archivos
# que contiene informacion de los atomos dentro de 
# la caja 
#
# Archivos o Programas asociados: 
#		get_visual_box2.py	(caja en formato pdb)
# 		get_edges.awk		(obtiene coordenadas de la caja)
#		box.pdb 			(caja de cada pdbqt)
#		read_pdb.py			(programa que filtra atomos dentro de la caja)
#		naccess				(programa que obtine el valor asa)		
# Salida
#		log_execution 		(resumen de la ejecucion)
#
# Parametros
#		$1	Directorio donde se encuntran los archivos pdbqt's (local ./)
#		$2  Dimensiones de la caja separada por comas (dx,dy,dz)

dir=$1
receptors="$dir*.pdbqt"
size_box=$2

# variables con los directorios
output_dir="/home/scidb/Documents/Fisiologia/pdbs/results"
scripts_dir="/home/scidb/Documents/Fisiologia/scripts"
scripts_python_dir="/home/scidb/Documents/Fisiologia/scripts_python"
naccess_dir="/home/scidb/Documents/Fisiologia/Things_Tools/naccess2.1.1"

log_file="log"


# REvision del archiv log
if [ -f "$output_dir/$log_file" ] ; then 
	rm "$output_dir/$log_file"
fi

# Creacion del log
touch "$output_dir/$log_file"
fecha=$(date)
echo "Working on     : $dir 					">> "$output_dir/$log_file"
echo "Size Box       : $size_box				">> "$output_dir/$log_file"
echo "Output dir     : $output_dir 				">> "$output_dir/$log_file"
echo "Output file    : $log_file 				">> "$output_dir/$log_file"
echo "Start Execution: $fecha 					">> "$output_dir/$log_file"
echo -e "pbdqt_name\tcoords\tcount_atom_in_box  ">> "$output_dir/$log_file"
number_of_files=0
count_atoms=0


for receptor in $receptors;
do
	receptor_name=$(echo $receptor | awk -F'/' '{print $NF}')
	name=$(echo $receptor_name | cut -f1 -d'.')
	
	# Calculo de la caja para el pdbqt actual y coordenadas de la cajha
	python "$scripts_python_dir/get_visual_box2.py" "$dir$receptor_name" $size_box
	edges=`grep HETATM box.pdb | awk -f "$scripts_dir/get_edges.awk"`
	mv box.pdb "$output_dir/boxes/box_$name"
	
	# Filtrado de atomos pertenecientes a la caja
	python "$scripts_python_dir/read_pdb.py" "$dir$receptor_name" $edges > "$output_dir/pdbs_atoms_box/atoms_in_box_$name"
	count_atoms=$(wc -l "$output_dir/pdbs_atoms_box/atoms_in_box_$name" | cut -d' ' -f1) 
	
	# Creacion de los archivos .asa con la herramienta naccess
	csh "$naccess_dir/naccess" -a "$dir$receptor_name"
	
	# Filtrando atomos que se encuentra en la caja
	grep -f "$output_dir/pdbs_atoms_box/atoms_in_box_$name" "$name.asa" > "$output_dir/pdbs_atoms_box_asa/box_asa_$name"
	
	# Moviendo archivo .asa a un directorio historico
	mv "$name.asa" "$output_dir/pdbs_asa/"
	rm "$name.log"
	
	number_of_files=$((number_of_files+1))	
	echo -e "$name\t$edges\t$count_atoms" >> "$output_dir/$log_file" 
done
fecha2=$(date)
echo "End Execution: $fecha2 						" >> "$output_dir/$log_file"
echo "Number of files proccessed: $number_of_files	" >> "$output_dir/$log_file"
