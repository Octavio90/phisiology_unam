BEGIN{
	FS=" ";
}
{
	res=$1
	if($0 != ""){
		if($2 == 1){
			region="3H"
			region_num=1}
		else if($4 == 1){
			region="ALH"
			region_num=2}
		else if($6 == 1){
			region="PIH"
			region_num=3}
		else if($8 == 1){
			region="EBS"
			region_num=4}
		else if($10 == 1){
			region="NBS"
			region_num=5}
		else if($12 == 1){
			region="PP"
			region_num=6}
		else if($14 == 1){
			region="TI"
			region_num=7}
		else if($16 == 1){
			region="TII"
			region_num=8}
		else if($18 == 1){
			region="TVIII"
			region_num=9}
		else if($20 == 1){
			region="GXT"
			region_num=10}
		else if($22 == 1){
			region="SCH"
			region_num=11}
		else if($24 == 1){
			region="HP"
			region_num=12}
		else if($26 == 1){
			region="TC"
		region_num=13}
		else if($28 == 1){
			region="HC"
			region_num=14}
		else if($30 == 1){
			region="BC"
			region_num=15}
		else if($32 == 1){
			region="BU"
			region_num=16}
		else if($34 == 1){
			region="LTII"
			region_num=17}
		else if($36 == 1){
			region="LHH"
		region_num=18}
		else if($38 == 1){
			region="UC"
			region_num=19}
		else{
			region="Other"
			region_num=20}
		print res"|"region"|"region_num
	}
}
END{}
