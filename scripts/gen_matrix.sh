#!/bin/bash

###############################################################################################
## Programa que procesa los archivos X.res generados por residue_filter.sh y obtiene
## una matriz de residuos con sus respectivos RSA y MF-RA (Multi Factor Preference for
## Amino Acid)
###############################################################################################

###############################################################################################
## INPUT:
## 	- X.res (Residuos completos dentro de la caja)
## OUTPUT:
##	- Archivo con el nombre del pdbqt que contenga:
##		* Informacion del Residuo:
##			- Nombre del residuo en codigo de tres letras
##			- Identificador de una letra
##			- Identificador numerico del residuo
##		* Relative Acceccible Area Surface (%)
##		* Codido de una letra: 
##			- si su rsa > 13.1 (Capital letter) 
##			- si su rsa <= 13.1 (Lower case)			
##		* Region en el Ramachandran Plot segun DISICl
##		* Numero de la Region Ramachandran Plot segun DISICl
##		* Valor MF-RA correspondiente
## Programas o Archivos Relacionados:
##	- residue_filter.sh
##	- X.res (matriz con los residuos completos dentro de la caja)
## Ultima Actualizacion: Dom 16 Jul, 2017
###############################################################################################

user="scidb"
#user="sysadmin"
#Fisiologia="FisiologiaGit"
Fisiologia="Fisiologia"

# Asignando direcciones de carpetas y nombres de archivos
root="/home/$user/Documents/$Fisiologia"
naccess_dir="$root/Things_Tools/naccess2.1.1"
scripts_python_dir="$root/scripts_python"
disicl_dir="$root/Things_Tools/DISICL"
scripts_dir="$root/scripts"
output_dir="$root/pdbs"
var_dir="$output_dir/results"
stat_dir="$output_dir/disicl"
log_file="gen_matrix.log"
rsa_dir="$output_dir/rsa"
res_dir="$output_dir/res"
log_name="gen_matrix.log"
number_of_files=0

###############################################################################################
## Program ##
#############



pdbqt=$1
name=$(echo $pdbqt | awk -F'/' '{print $NF}' | cut -d'.' -f1)

# Uso de naccess para la obtencion del Relative Surface Area (%)
csh "$naccess_dir/naccess" $pdbqt > /dev/null

rm  "$name.asa" "$name.log"
if [ ! -f "$name.rsa" ]; then
	echo -e "#Error1: $name.pdbqt\tNo pudo crearse archivos naccess">> "$output_dir/$log_file"
	exit 1
fi


# Creacion de un archivo temporal que contenga el residuo y su RSA
grep RES "$name.rsa" | grep -v REM | awk -F' ' \
	'{print $2","$3","$4"|"$6}' | grep -w \
	-f "$res_dir/$name.res" > "naccess_$name.txt"
mv "$name.rsa" "$rsa_dir/"


# Agregando columna con el codigo del residuo dependiendo de su RSA
bash "$scripts_dir/get_amino_code.sh"  "naccess_$name.txt" $scripts_dir > "subMatrix_$name.txt"


# Uso de DISICL para la obtencion de las regiones
cd "$output_dir/pdb"
python "$disicl_dir/DISICL_main.py" "$name.pdbqt" > /dev/null

if [ ! -f "DISICL_pdet_$name.stat" ] || [ ! -f "DISICL_$name.log" ]; then
	echo -e "#Error2: $name.pdbqt\tNo pudo crearse archivos disicl" >> "$output_dir/$log_file"
	if [ -f "$output_dir/naccess_$name.txt" ]; then
		rm "$output_dir/naccess_$name.txt" 
	fi
	if [ -f "$output_dir/subMatrix_$name.txt" ]; then
		rm "$output_dir/subMatrix_$name.txt"  
	fi
	exit 1
fi

rm "$name""_prot.dih"
rm "$name""_DISICL.pdbqt"
rm "DISICL_pdet_$name.out" 

# Creacion de archivos temporales que contenga los resultados de
# 	la matriz ubicada en .stat y .log
grep -v "#" "DISICL_pdet_$name.stat" | awk -f \
	"$scripts_dir/filter_regions.awk" > "disicl_stat_$name.txt"

grep "renamed to" "DISICL_$name.log" | awk -F' ' \
	'{print $2","$4","$3"|"$8}' | grep -w \
	-f "$res_dir/$name.res" > "disicl_log_$name.txt"

mv "DISICL_pdet_$name.stat" "$stat_dir/"
mv "DISICL_$name.log" "$stat_dir/"

# Elementos en los archivos temporales

count_sub=$(wc -l "$output_dir/subMatrix_$name.txt" | cut -d' ' -f1)
count_log=$(wc -l "disicl_log_$name.txt" | cut -d' ' -f1)
count_stat=$(wc -l "disicl_stat_$name.txt" | cut -d' ' -f1)

if [ $count_stat -ge $count_log ]
then
	join -a1 -1 2 -2 1 -o 1.1,2.2,2.3 -t'|' <(sort -t'|' -k2,2 disicl_log_$name.txt) \
 	<(sort -t'|' -k1,1 disicl_stat_$name.txt) | awk -F'|' \
 	'{if($2 == ""){print $1"|Other|20"}else{print $0}}' > "log_stat_$name.txt"
else
	echo -e "#Error3: $name\tMas residuos en log que en stat" >> "$output_dir/$log_file"
	if [ -f "$output_dir/subMatrix_$name.txt" ]; then
		rm "$output_dir/subMatrix_$name.txt"
	fi
	exit 1
fi


if [ $count_sub -ge $count_log ]
then
	join -a1 -1 1 -2 1 -t'|' -o 1.1,1.2,1.3,2.2,2.3 <(sort -k1,1 -t'|' -d "$output_dir/subMatrix_$name.txt") \
	<(sort -k1,1 -t'|' -d log_stat_$name.txt) | awk -F'|' \
	'{if($3 == ""){print $1"|"$2"|Other|20" }else{print $0}}' > "$name.tmp"
else
	echo -e "#Error4: $name\tMas residuos en log que en la sub matriz" >> "$output_dir/$log_file"
	if [ -f "$output_dir/subMatrix_$name.txt" ]; then
		rm "$output_dir/subMatrix_$name.txt"
	fi
	exit 1
fi


if [ -f "$name.var" ]; then
	rm "$name.var"
fi

# Agregando mf_ra column
while read line; do
	code=$(echo $line | cut -d'|' -f3)
	region=$(echo $line | cut -d'|' -f5)
	mf_ra=$(grep -w $code "$scripts_dir/mf_ra.csv" | awk -F',' -v r="$region" '{print $(r+1)}')
	echo "$line|$mf_ra" >> "$name.var" 
done < "$name.tmp"

rm "$name.tmp"
rm "disicl_stat_$name.txt" 
rm "disicl_log_$name.txt" 
rm "log_stat_$name.txt" 
mv "$name.var" "$var_dir/"
rm "$output_dir/naccess_$name.txt"
rm "$output_dir/subMatrix_$name.txt" 
cd - > /dev/null

###############################################################################################
