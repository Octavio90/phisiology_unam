#!/bin/bash

result_dir="/home/scidb/Documents/Fisiologia/pdbs/results_polar"
desc_dir="/home/scidb/Documents/Fisiologia/descriptors"
output_dir="/home/scidb/Documents/Fisiologia/pdbs/results"
output_file="protein_matrix.var"
log_file="protein_matrix.log"

if [[ -f "$output_file" ]]; then
	rm "$output_file"
fi

# PDB's con extension
echo "name mfplb n_polar polar #branch #atoms #bonds #Abonds #Sbonds #dbonds #Tbonds logP TPSA HBD HBA1 HBA2 MR MP nF MW" >> "$output_dir/$output_file"

num=0
while read file; do
	((num ++))
	name=$(echo $file | cut -d'.' -f1 | cut -d'_' -f2-)

	summary=$(awk -F'|' \
		'BEGIN{mfplb=0;npolar=0;polar=0;branch=0;OFS=" "} \
		{mfplb+=$6;npolar+=$7;polar+=$8;branch+=$9}\
		END{print mfplb,npolar,polar,branch}' "$result_dir/$file")

	row=$(join -t' ' -j 1 <(echo "$name $summary") \
					<(sort "$desc_dir/ligands_descriptors.txt" ) )
	num_fields=$(echo $row | awk '{print NF}')

	if (( $num_fields == 20 )); then
		echo "$row" >> "$output_dir/$output_file"
		echo -e "$num\t$name\tGood!"
	else
		echo "$row" >> "$output_dir/$log_file"
		echo -e "$num\t$name\tWrong!"
	fi
done
