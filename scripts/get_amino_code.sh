#!/bin/bash

###############################################################################################
## Programa que agrega la columna del codigo del aminoacido dependiendo si su rsa es
## menor o igual que 13.1 (lower case) o su rsa es mayor a 13.1 (upper case)
###############################################################################################

###############################################################################################
## INPUT:
## 	- naccess_X.txt
## OUTPUT:
##	- Archivo con el nombre X.code que contenga:
##		* Informacion del Residuo:
##			- Nombre del residuo en codigo de tres letras
##			- Identificador de una letra
##			- Identificador numerico del residuo
##		* Relative Acceccible Area Surface (%)
##		* Codido de una letra: 
##			- si su rsa > 13.1 (Capital letter) 
##			- si su rsa <= 13.1 (Lower case)			
## Programas o Archivos Relacionados:
##	- residue_filter.sh
##	- naccess_X.txt
##	- gen_matrix.sh
## Ultima Actualizacion: Dom 16 Jul, 2017
###############################################################################################


naccess_file=$1
script_dir=$2
name=$(echo $naccess_file | cut -d'.' -f1 | cut -d'_' -f2-)

if [ -f "subMatrix_$name.txt" ] ; then 
	rm "subMatrix_$name.txt"
fi

while read line; do
	res=$(echo $line | cut -d',' -f1)
	rsa=$(echo $line | cut -d'|' -f2)
	code=$(grep $res "$script_dir/amino_acid.class" | cut -d'|' -f1)
	if (( $(echo $rsa 13.1 | awk '{print ($1 <= $2)}') )); then
		code=$(echo $code | tr '[:upper:]' '[:lower:]')
	fi
	echo $line"|"$code >> "subMatrix_$name.txt"
done < $naccess_file