BEGIN{
	FS=" "
	min_X = 10000; 
	max_X =-10000;
	min_Y = 10000; 
	max_Y =-10000;
	min_Z = 10000; 
	max_Z =-10000;
}
{
	if($7<min_X){min_X=$7}
	if($7>max_X){max_X=$7}
	if($8<min_Y){min_Y=$8}
	if($8>max_Y){max_Y=$8}
	if($9<min_Z){min_Z=$9}
	if($9>max_Z){max_Z=$9}
}
END{
	print min_X","max_X","min_Y","max_Y","min_Z","max_Z
}


