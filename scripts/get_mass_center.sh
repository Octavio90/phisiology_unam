#!/bin/bash


ligand_dir=$1*
echo "working on "$ligand_dir
rm ~/me/ligand_mass_center.txt

for ligand in $ligand_dir;
do
	lig_name=$(echo $ligand | awk -F'/' '{print $NF}')
	mass_center=$(cat $ligand)
	echo $lig_name $mass_center >> ~/me/ligand_mass_center.txt
done

if [ $2 -eq 1 ];then
	more ~/me/ligand_mass_center.txt
fi