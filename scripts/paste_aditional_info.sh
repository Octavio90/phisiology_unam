#!/bin/bash

main_dir="/home/scidb/Documents/Fisiologia"
script_dir="$main_dir/scripts"
result_dir="$main_dir/pdbs/results"
rsa_dir="$main_dir/pdbs/rsa"
output_dir="$main_dir/pdbs/analyze/results_2"


num_file=0
# PDB's con extension
while read file; do
	name=$(echo $file | cut -d'.' -f1)
	((num_file++))
	if [[ -f "$output_dir/$file" ]]; then
		rm "$output_dir/$file"
	fi
	while read row; do
		res=$(echo $row | cut -d',' -f1)
		code=$(echo $row | cut -d',' -f2)
		num=$(echo $row | cut -d',' -f3 | cut -d'|' -f1)

		polar_row=$(grep -w RES "$rsa_dir/$name.rsa" | grep -v REM | \
		awk -F' ' -v res="$res" -v num="$num" -v code="$code" '$2==res && $3==code && $4==num {print $11"|"$13; exit 1}')

		branch_num=$(grep "$res" "$script_dir/branches_catalog.txt" | cut -d'|' -f2)

		echo "$row|$polar_row|$branch_num" >> "$output_dir/$file" 

	done < "$result_dir/$file"
	echo -e "$num_file\t$name"
done