#!/bin/bash

# Programa que evalua la obtencion de coordenadas 
# negativas del archivo box.pdb que se encuantran 
# juntas como una sola cadena 

# Archivo asociado: 
# 		get_edges.awk
#		box.pdb (para cada pdbqt en pdbs_error/)
# Salida
#		log_boxes (lista de archivos y las coordenadas obtenidas)


dir="$1*.pdbqt"
size_box=$2
python_dir="/home/scidb/Documents/Fisiologia/scripts_python"
awk_dir="/home/scidb/Documents/Fisiologia/scripts"
log_dir="/home/scidb/Documents/Fisiologia/pdbs/results/pdbs_error"
log_file="log_boxes"

if [ -f "$log_dir/$log_file" ]; then
	rm "$log_dir/$log_file" 
fi

touch "$log_dir/$log_file"
fecha=$(date)
echo -e "Working on: $dir\texecution_date: $fecha" >> "$log_dir/$log_file"
number_of_files=0

for file in $dir; do
	receptor=$(echo $file | awk -F'/' '{print $NF}')
	name=$(echo $receptor | cut -d'.' -f1)
	python "$python_dir/get_visual_box2.py" $receptor $size_box
	
	edges=`grep HETATM box.pdb | awk -f "$awk_dir/get_edges.awk"`
	count=$(echo $edges | awk -F',' '{print NF}')
	rm box.pdb
	
	number_of_files=$((number_of_files+1))
	echo -e "$name\t$edges\t$count" >> $log_file
done

echo "Number of processed files: $number_of_files" >> $log_file

