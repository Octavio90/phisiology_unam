#!/bin/bash

###############################################################################################
##	Programa principal que obtiene las caracteristicas necesarias paara el 
##	entrenamiento de los datos
###############################################################################################

###############################################################################################
## INPUT:
## 	- Archivo que contiene la lista de pdbs a procesar  
##	- Direccion donde se encuentran almacenados los pdbs de la lista (con una diagonal final)
## 	- Tamaño de un lado de la caja (se asumen cajas cuadradas) 
## OUTPUT:
## 	- Archivos con las caracteristicas de cada pdb en la carpeta var/ 
## Programas Relacionados:
##	- residue_filter.sh
##	- gen_matrix.sh
##	- get_visual_box.py
##	- get_edges.awk
## Ultima Actualizacion: Dom 16 Jul, 2017
###############################################################################################


pdb_list=$1
pdb_dir=$2
size_box=$3

if [ -z ${pdb_list} ] || [ -z ${pdb_dir} ] || [ -z ${size_box} ]; then
	echo -e "\nAlgun parametro fue omitido"
	echo -e "proccess_pdb.sh\t[pdb list file]\t[pdb dir]\n[box size]"
	exit 1
fi

# Variables del sistema
working_dir="Fisiologia"
user="scidb"

# Variables del sistema de archivos
scripts_python="/home/$user/Documents/$working_dir/scripts_python"
scripts_dir="/home/$user/Documents/$working_dir/scripts"

# Directorio raiz donde se encuentran todas las carpetas concernientes a los resultados
output_dir="/home/$user/Documents/$working_dir/pdbs"
log_file="proccess_pdb.log"
box_dir="$output_dir/box"

# Revision del archivo log
if [ -f "$output_dir/$log_file" ] ; then 
	rm "$output_dir/$log_file"
fi

if [ -f "$output_dir/gen_matrix.log" ] ; then 
	rm "$output_dir/gen_matrix.log"
fi

if [ -f "$output_dir/residue_filter.log" ] ; then 
	rm "$output_dir/residue_filter.log"
fi

###############################################################################################
## Log ##
#########
touch "$output_dir/$log_file"
fecha=$(date)
number_of_files=0
echo "##############################################################">> "$output_dir/$log_file"
echo "# Working on     : $output_dir			                    ">> "$output_dir/$log_file"
echo "# Size Box       : $size_box				                    ">> "$output_dir/$log_file"
echo "# Output dir     : $output_dir/results	                    ">> "$output_dir/$log_file"
echo "# Output file    : $log_file 				                    ">> "$output_dir/$log_file"
echo "# Start Execution: $fecha 				                    ">> "$output_dir/$log_file"
echo "##############################################################">> "$output_dir/$log_file"
echo -e "#pdb_name              #rsa    #res    #var\tcoord         ">> "$output_dir/$log_file"
###############################################################################################
## Program ##
#############

while read pdb; do

	number_of_files=$((number_of_files+1))
	name=$(echo $pdb | cut -d'.' -f1 )
	python "$scripts_python/get_visual_box.py" "$pdb_dir$pdb" "$size_box,$size_box,$size_box"
	mv "box.pdb" "$box_dir/$name.box"
	edges=$(grep HETATM "$box_dir/$name.box" | awk -f "$scripts_dir/get_edges.awk") 

	# Verificando si se pudo obtener exitosamente las coordenadas de la caja 
	if (( $(echo "$edges" | awk -F',' '{for(i=1;i<=NF;i++){if($i==""){print 1; exit 1}} print 0}') )); then
		echo "$pdb Error: No pudo obtenerse las coordenadas correctamente  #file: $number_of_files"
		echo -e "#Error1: $pdb\t$edges" >> "$output_dir/$log_file"
		continue
	fi

	# Filtrado de residuos completos y verificacion de la existencia de res/X.res
	bash "$scripts_dir/residue_filter.sh" "$pdb_dir$pdb" "$edges"
	if [ ! -f "$output_dir/res/$name.res" ] ; then 
		echo "$pdb Error: No existe el archivo ./res/$name.res  #file: $number_of_files"
		echo -e "#Error2: $pdb\t$edges" >> "$output_dir/$log_file"
		continue
	fi
	count_res=$(wc -l "$output_dir/res/$name.res" | cut -d' ' -f1)

	# Generacion de la matriz de caracteristicas final
	bash "$scripts_dir/gen_matrix.sh" "$pdb_dir$pdb"
	if [ ! -f "$output_dir/results/$name.var" ] ; then 
		echo "$pdb Error: No existe el archivo ./results/$name.var  #file: $number_of_files"
		echo -e "#Error3: $pdb\t$edges" >> "$output_dir/$log_file"
		continue
	fi
	count_var=$(wc -l "$output_dir/results/$name.var" | cut -d' ' -f1)
	count_rsa=$(grep RES "$output_dir/rsa/$name.rsa" | grep -v REM | wc -l)

	echo  "$pdb processed... #file: $number_of_files"
	echo -e "$pdb\t$count_rsa\t$count_res\t$count_var\t$edges" >> "$output_dir/$log_file"
done < $pdb_list

###############################################################################################
## Log ##
#########
fecha=$(date)
echo "##############################################################">> "$output_dir/$log_file"
echo "# End Execution  : $fecha 						            ">> "$output_dir/$log_file"
echo "# Number of files proccessed: $number_of_files	            ">> "$output_dir/$log_file"
echo "##############################################################">> "$output_dir/$log_file"
###############################################################################################