
# Ejemplo para la lectura de coordenadas unidas de un pdb 

BEGIN{
	FS=" "
	OFS="\t"
}
{
	if(split($7,a,".") == 3){
		c1=a[1]"."substr(a[2],1,3)
		c2=substr(a[2],4)"."a[3]
		c3=$8
	} 
	else if(split($7,a,".") == 4){
		c1=a[1]"."substr(a[2],1,3)
		c2=substr(a[2],4)"."substr(a[3],1,3)
		c3=substr(a[3],4)"."a[4]
	}
	else if(split($8,a,".") == 3){
		c1=$7
		c2=a[1]"."substr(a[2],1,3)
		c3=substr(a[2],4)"."a[3]
	} 
	else{
		c1=$7
		c2=$8
		c3=$9
	}

	print $4,$5,$6,c1,c2,c3
}

#-2.757-30.301