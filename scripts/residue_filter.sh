#!/bin/bash

###############################################################################################
##	Programa que filtra residuos completos
##  del pdb mediante las coordenadas de la caja
###############################################################################################

###############################################################################################
## INPUT:
## 	- Archivo PDB  
## 	- coordenadas de la caja (separado por comas) 
## OUTPUT:
## 	- Archivo X.res que contenga los residuos cuyos atomos en su 
##	  mayoria se encuentren dentro de la caja en el formato:
##		* Nombre del residuo en codigo de tres letras
##		* Identificador de una letra
##		* Identificador numerico del residuo 
## Programas Relacionados:
##	- get_edges.awk
## Ultima Actualizacion: Dom 16 Jul, 2017
###############################################################################################

#user="sysadmin"
#sandbox_dir="sand_box"
#Fisiologia="FisiologiaGit"
Fisiologia="Fisiologia"
sandbox_dir="sandbox"
user="scidb"


scripts_python_dir="/home/$user/Documents/$Fisiologia/scripts_python"
output_dir="/home/$user/Documents/$Fisiologia/pdbs"
scripts_dir="/home/$user/Documents/$Fisiologia/scripts"
dic_dir="$output_dir/dic"
res_dir="$output_dir/res"
log_file="residue_filter.log"

number_of_files=0
pdbqt=$1
edges=$2

###############################################################################################
## Program ##
#############

name=$(echo $pdbqt | awk -F'/' '{print $NF}' | cut -d'.' -f1)

# Filtrado de atomos que se encuentran dentro de la caja sin contar Hidorgenos
python "$scripts_python_dir/read_pdb.py" $pdbqt $edges | sort > "$output_dir/atom_in_box_$name.txt"
cat "$output_dir/atom_in_box_$name.txt" | uniq > "$output_dir/$name.dic"

if [ -f "$output_dir/$name.res" ] ; then 
	rm "$output_dir/$name.res"
fi
if [ -f "$res_dir/$name.res" ] ; then 
	rm "$res_dir/$name.res"
fi

while read residue; do
	res3=$(echo $residue | cut -d' ' -f1 )
	res1=$(echo $residue | cut -d' ' -f2 )
	res_num=$(echo $residue | cut -d' ' -f3 )

	# Numero de atomos del residuo analizado en el pdb sin contar hidrogenos
	count_total=$(grep "$residue" $pdbqt | egrep -v "TER|HD"  | wc -l)
	if [ "$count_total" -eq "0" ]; then
		echo -e "pdb: $name\t$residue\tNo se encuentra el residuo en el pdb" >> "$output_dir/residue_filter.log"
		continue
	fi
	# Numero de atomos del residuo analizado en la caja sin contar hidrogenos
	count_inBox=$(grep "$residue" "$output_dir/atom_in_box_$name.txt" | wc -l)
	div=$(($count_inBox*100/$count_total))
	
	if (( $div > 50 )); then
		echo "$res3,$res1,$res_num" >> "$output_dir/$name.res"  
	fi

done < "$output_dir/$name.dic"

if [ -f "$output_dir/$name.res" ]; then
	mv "$output_dir/$name.res" "$res_dir/"
fi

if [ -f "$output_dir/atom_in_box_$name.txt" ]; then
	rm "$output_dir/atom_in_box_$name.txt"
fi

if [ -f "$output_dir/$name.dic" ]; then
	mv "$output_dir/$name.dic" "$dic_dir/"
fi

###############################################################################################
