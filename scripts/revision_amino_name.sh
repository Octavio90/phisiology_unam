#!/bin/bash

dir="/home/scidb/Documents/Fisiologia/pdbs/results"
script="/home/scidb/Documents/Fisiologia/scripts"
o_dir="results_2"
accion=$1

if [[ $accion -eq 0 ]]; then
	echo "Analizando ..."
	file_num=0
	while read -r file; do
		row_num=0
		file_num=$((file_num+1))
		while read -r line; do
			row_num=$((row_num+1))
			name=$(echo $line | cut -d',' -f1)
			exist=$(grep $name "$script/amino_acid.class" | wc -l)
			if [[ $exist -ne 1 ]]; then
				echo "$file_num|$file|$name|$row_num"
			fi
		done < "$dir/$file"
	done
elif [[ $accion -eq 1 ]]; then
	echo "Repararando ..."
	while read file; do
		num_1=$(wc -l "$dir/$file" | cut -d' ' -f1)
		while read line; do
			name=$(echo $line | cut -d',' -f1)
			exist=$(grep $name "$script/amino_acid.class" | wc -l)
			if [[ $exist -eq 1 ]]; then
				echo $line >> "$o_dir/$file" 
			fi
		done < "$dir/$file"
		num_2=$(wc -l "$o_dir/$file" | cut -d' ' -f1)
		echo -e "## $file\t$num_1\t$num_2"
	done
fi
