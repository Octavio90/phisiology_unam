#!/bin/bash

dir="/home/scidb/Documents/Fisiologia/pdbs/results"
script="/home/scidb/Documents/Fisiologia/scripts"
o_dir="results_2"
disicl_dir="/home/scidb/Documents/Fisiologia/pdbs/disicl"

num_files=0

#Validar usando:
#R_1AWT_MSE_A.var
#R_1C0I_FAD_A.var
#R_1C0K_FAD_A.var
#R_1C12_TRZ_A.var
#R_1CE8_IMP_A.var


function find_in_log(){
	local res_num_log=$(echo $1 | cut -d',' -f1)
	local row=$(grep -v "#" "$disicl_dir/DISICL_pdet_$2.stat" | \
				grep -w "$res_num_log")

	local region=$(echo $row | awk -f "$script/filter_regions.awk")
	echo "$(echo $region | cut -d'|' -f2-)"	
}




while read file; do
	ext=$(echo $file | awk -F'.' '{print NF}')
	if [[ ext -eq 1 ]]; then
		name=$(echo "$file")
		file=$(echo "$file.var")
	else
		name=$(echo "$file" | awk -F'.' '{ print $(NF-1) }')
	fi
	
	while read line; do
		#echo "Analizando linea $num_line"
		flag=$(echo $line | awk -F'|' \
		'BEGIN{flag=0}{ \
			for(i=1;i<=NF;i++){ if($i==""){flag=1;exit 1;} } \
		}END{ print flag}')
		if [[ $flag -eq 1 ]]; then
			## Var for key
			res_name=$(echo $line | cut -d'|' -f1 | cut -d',' -f1)
			res_code=$(echo $line | cut -d'|' -f1 | cut -d',' -f2)
			res_num=$(echo $line  | cut -d'|' -f1 | cut -d',' -f3)
			## Var for the rest
			rsa=$(echo $line | cut -d'|' -f2)
			amino_code=$(echo $line | cut -d'|' -f3)
			
			## Revision en archivos DISICL
			if [[ -f "$disicl_dir/DISICL_$name.log" ]]; then
				num_log=$(grep -w "residue:" "$disicl_dir/DISICL_$name.log" | \
				awk -F' ' -v res_name="$res_name" \
				-v res_num="$res_num" -v res_code="$res_code" \
				'{if($2==res_name && $3==res_num && $4==res_code){print $8}}')
				
				if [[ ! -z $num_log ]]; then
					region=$(find_in_log $num_log $name)
					region_code=$(echo $region | cut -d'|' -f1)
					region_num=$(echo $region | cut -d'|' -f2)
					mf_ra=$(grep "$amino_code" "$script/mf_ra.csv" | awk -F',' -v r_num="$region_num" '{print $(r_num+1)}')
					echo "$res_name,$res_code,$res_num|$rsa|$amino_code|$region_code|$region_num|$mf_ra" >> "$o_dir/$name.var"
				else
					mf_ra=$(grep "$amino_code" "$script/mf_ra.csv" | cut -d',' -f21)
					echo "$res_name,$res_code,$res_num|$rsa|$amino_code|Other|20|$mf_ra" >> "$o_dir/$name.var"
					echo "$name  $line" >> "revision_faltantes.log"
				fi
			else
				echo "No se encontro archivo disicl: $name  $line" >> "revision_faltantes.log" 
			fi
		else
			echo "$line" >> "$o_dir/$name.var"
		fi
	done < "$dir/$file"
	echo "$((++num_files)): $file"
done