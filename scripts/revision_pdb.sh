#!/bin/bash

base_dir="/home/scidb/Documents/Fisiologia"
pdb_dir="$base_dir/pdbs/pdb"
box_dir="$base_dir/pdbs/box"
script_dir="$base_dir/scripts"
p_script_dir="$base_dir/scripts_python"
results_dir="$base_dir/pdbs/results"
log="revision_pdb.log"

function pdb_box_check(){
	local res_name=$(echo $1 | cut -d',' -f1)
	local res_letter=$(echo $1 | cut -d',' -f2)
	local res_number=$(echo $1 | cut -d',' -f3)

	local flag=$(egrep -v "TER|HD" "$pdb_dir/$2.pdbqt" | awk -f "$script_dir/atoms_box.awk" \
	-v rname="$res_name" -v rcode="$res_letter" -v rnum="$res_number" -v coords="$3") 
	echo $flag
}


number=0

e_time=$(date)
echo "$e_time" > $log
while read file; do
	((++number))
	name=$(echo $file | cut -d'.' -f1)
	egrep -v "TER" "$pdb_dir/$name.pdbqt" | awk -F' ' '{if(length($5)==1){print $4","$5","$6}else{print $4","substr($5,1,1)","substr($5,2)}}' | uniq > "$name.tmp" 
	
	coords=$(grep HETATM "$box_dir/$name.box" | awk -f "$script_dir/get_edges.awk")
	while read res; do
		string=$(pdb_box_check $res $name $coords)
		flag=$(echo $string | cut -d',' -f1)
		total=$(echo $string | cut -d',' -f2)
		partial=$(echo $string | cut -d',' -f3)
		if (( flag == 1 )); then
			echo "$res,$total,$partial" >> "$name.tmp2"
		fi
	done < "$name.tmp"
	c_dif=$(diff <(cut -d',' -f1 $name.tmp2 | sort ) <(cut -d'|' -f1 "$results_dir/$name.var" | sort) | wc -l)
	if (( c_dif > 0 )); then
		echo -e "$name\t$c_dif" >> $log 
		echo -e "$number\t$name\tWrong!" 
		cp "$name.tmp2" "../results_3/$name.var"
	else
		echo -e "$number\t$name\tGood!" 
	fi
	rm "$name.tmp" "$name.tmp2"
done

e_time=$(date)
echo "----------------------------------" >> $log
echo "Files Processed: $number" >> $log
echo "$e_time" >> $log
