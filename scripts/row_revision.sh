#!/bin/bash

dir="/media/sysadmin/Data/Results_Fisiologia/results"



num=0
while read -r file; do
	#file=$(echo "$file.var")
	name=$(echo $file | cut -d'.' -f1)
	count=$(wc -l "$dir/$file" | cut -d' ' -f1)
	row=$(awk -F'|' \
	'BEGIN{flag=0} \
		{ if(NF != 6){flag=2; exit 1;} \
		  for(i=1;i<=NF;i++){ \
		  	if($i == ""){flag=1; field=i; exit 1;} \
		  	}\
		} \
	 END{print flag","field}' "$dir/$file")
	flag=$(echo $row | cut -d',' -f1)
	field=$(echo $row | cut -d',' -f2)
	num=$(( num+1 ))
	echo -e "$num-$name $flag field: $field" 
done