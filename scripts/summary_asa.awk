BEGIN{
	amino   ="" 
	nucleic =""
	number  =""
	summary =0
	idx     =0
	result[idx] = ""  
}
{
	if(amino ==$4 && nucleic ==$5 && number==$6 ){
		summary += $10
	}
	else{
		if(amino !="" && nucleic !="" && number!=""){
			result[idx] = amino+" - "+nucleic+" - "+number+" - "+summary
		}
		amino   =$4 
		nucleic =$5
		number  =$6
		summary =$10
		idx ++
	}
}
END{
	for (i = 1; i <= idx; i++)
		print result[i]
}
