#!/bin/bash

## Scritp que realiza la visualizacion de cada uno de 
## los residuos junto con su caja

pdb_dir="/home/scidb/Documents/Fisiologia/pdbs/pdb"
box_dir="/home/scidb/Documents/Fisiologia/pdbs/box"
input_dir="/home/scidb/Documents/Fisiologia/pdbs/analyze/results_3"

while read file; do
	name=$(echo $file | cut -d'.' -f1)
	while read row; do
		#key data
		res=$(echo $row | cut -d',' -f1)
		code=$(echo $row | cut -d',' -f2)
		num=$(echo $row | cut -d',' -f3 | cut -d'|' -f1)
		grep -v "TER" "$pdb_dir/$name.pdbqt" | grep "$res $code$num" > "grep.tmp"
		count=$(wc -l "grep.tmp" | cut -d' ' -f1)

		if (( count == 0 )); then
			grep -v "TER" "$pdb_dir/$name.pdbqt" | grep "$res $code $num" > "grep.tmp"
			count=$(wc -l "grep.tmp" | cut -d' ' -f1)
			if (( count == 0 )); then
				grep -v "TER" "$pdb_dir/$name.pdbqt" | grep "$res $code  $num" > "grep.tmp"
				count=$(wc -l "grep.tmp" | cut -d' ' -f1)
				if (( count == 0 )); then
					grep -v "TER" "$pdb_dir/$name.pdbqt" | grep "$res $code   $num" > "grep.tmp"	
				fi	
			fi
		fi
		
		echo "$res $code $num $count"
		pymol "grep.tmp" "$box_dir/$name.box" > /dev/null
		rm "grep.tmp"
	done < "$input_dir/$name.var"
done